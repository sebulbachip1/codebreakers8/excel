VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AlphaList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       AlphaList (base for a number of alphabetic functions)
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime"
'
'USAGE:
'       Translates a string or a number into an Alphabet.  Provides routines for
'       searching the Alphabet for a given character, returning the index of a character
'       (default function) and filtering a string to only include characters in the list.
'
'       The definition of an "alphabet" can include numbers or special characters.
'
'       If passing in a number less than 10, it is assumed to be one of the textType
'       constants, and all valid values for that type (numbers, punctuation, letters) will
'       be added to the list.
'
'       If the passed in value is a number greater than 10, then it is assumed to be a
'       string (a list of digits to include).
'
'       The passed in string does not need to contain a complete alphabet.  It does not
'       need to be in order.  It cannot contain any duplicate values.  It is NOT case
'       sensitive by default.  Call Sensitive *before* Init to enable a case sensitive
'       Alphabet.
'
'       Characters in the list are numbered starting at zero - can adjust with optional
'       offset on Init.  Init Filter, [repl], Asc(<first character>) to build an ASCII
'       based list.  Init Filter, [repl], 1 to build a 1 based list.
'
'       Call Init a second time to add a second range of characters with an overlapping
'       set of indices: Init "ABC", Init "012", , 0.  Cannot call with overlapping
'       character ranges - strings must be different.
'
'       Call AltIndex to build a reverse lookup (by index) if needed - usually only for
'       non-zero based indexes (e.g. ASCII, 1 based, etc.).  Cannot use with overlapping
'       indices (all index entries must be unique).  Char2(idx) and Idx2(Ch) are used
'       with the Alternate Index.  Idx2 is only added for completeness, as Idx will
'       return the same value.
'
'       Caller must trap errors.
'
'       801, "AlphaList:Idx", "Character " & Ch & " not found in Alphabet"
'
'       802, "AlphaList:Char", "Index " & I & " is not in range 0-" & aList.Count - 1
'
'       803, "AlphaList:Init*", "Replacement character previously added: " & Repl
'
'       811, "AlphaList:Init_", "Unknown text filter type, expecting String or Number; Vartype = " & VarType(Filt)
'       812, "AlphaList:Init_", "Unknown filter value [" & tType & "], expecting -7, -3 to 3 or 5"
'       804, "AlphaList:Init_", "Unknown parm type for range (expecting string/range): " & TypeName(Repl)
'
'       813, "AlphaList:Init", "Duplicate character at position " & I & " in filter [" & Ch & "]"
'       815, "AlphaList:Init", "Replacement key " & Ch & " found in filter" & strAlpha
'       814, "AlphaList:Init", "Replacement value " & Ch & " not found in filter" & strAlpha
'
'       822, "Alphalist:Undup", "List not initialized, call Dedup before Undup"
'       823, "Alphalist:Recompact", "List not initialized, call Compact before Recompact"
'
'       806, "AlphaList:AltIndex", "Duplicate index at position " & I & " in list [" & V & "]"
'
'       801, "AlphaList:Char2", "Index " & V & " not found in list"
'       807, "AlphaList:Char2", "Alternate index not initialized"
'
'GROUPS:
'       Alphalist
'            Char, Idx, Count, Exists, Init, Replacement, ReplacementExists, Filter
'            strAlpha, AltIndex, Char2, Idx2, Items, Init2, Dedup, Compact, Undup,
'            Recompact, toOffset, toText
'
'       * Idx is the default function
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 03/12/2021 Chip Bloch    -Original Version
' 03/21/2021 Chip Bloch    -Added oZero offset, Items
' 03/22/2021 Chip Bloch    -Added ChkRlist_ (Prep for dedup)
' 03/29/2021 Chip Bloch    -Migrated Dedup, Compact from General
' 04/03/2021 Chip Bloch    -Added Recompact
' 04/10/2021 Chip Bloch    -Added "&" default text (reverse)
'                          -Added numFilter, textFilter
' 04/11/2021 Chip Bloch    -Eliminated possibility of double pad for final letter
'                          -Added default textType for empty (ttAlphaUpper)
' 04/17/2021 Chip Bloch    -Bug: Collapse on Init_ has no override
' 04/21/2021 Chip Bloch    -Renamed Items as Item, NumFilter as toOffset, textFilter as toChars
' 04/22/2021 Chip Bloch    -Added oASCII for toOffset, toChars
'                          -Fixed bug with ttASCII not allowing lowercase
' 04/27/2021 Chip Bloch    -Added ttAlphaPlus (50 letter alphabet, 2x25)
' 04/28/2021 Chip Bloch    -Added SwpCh
'-------------------------------------------------------------------------------------------

Private aList As New Scripting.Dictionary ' Alphabet list
Private rList As New Scripting.Dictionary ' Replacements list
Private iList As New Scripting.Dictionary ' Index2 list
Private Inserts As New Scripting.Dictionary ' Characters to be inserted after other characters

Private rpch() As String  ' Default Replacements
Private tType As textType ' default filters - Upper Alpha, Alpha+number, Alpha 25, Number only

Private MaxOfs As Integer

Private isCollapsed As Boolean
Private MustReplace As Boolean

' Vars for Dedup
Private Undupable As Boolean

' Vars for Compact
Private Compactable As Boolean
Private Even As Boolean
Private PadCh As String
Private fCh As String
Private Swap As Boolean
Private Punct As Boolean
Private Final As Boolean

Private Const aText As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
Private Const eText As String = "ETAOINSRHLDCUMFGPWYBVKJXZQ"
Private Const pText As String = "1234567890,;-:"".?' !/()*"
Private Const nText As String = "0123456789"

Private bSensitive As Boolean

Public Enum oType
    oZero = 1024
    oASCII = 2048
End Enum

'Private bValid As Boolean

' Set case insensitive on the dictionaries (and create empty)
Private Sub Class_Initialize()
'bValid = False
' ignore case
aList.CompareMode = TextCompare
rList.CompareMode = TextCompare
iList.CompareMode = BinaryCompare
ReDim rpch(1)
rpch(0) = "JI"
rpch(1) = "ZY"
tType = ttNone ' Not Initialized
isCollapsed = True
End Sub

' change to not ignore case (can only be set before calling Init)
Public Sub Sensitive()
aList.CompareMode = BinaryCompare
rList.CompareMode = BinaryCompare
bSensitive = True
End Sub

' Set default replacement (must be called BEFORE init)
Public Property Let Rep(Ch As String)
rpch(0) = Ch
End Property

' Set second default replacement (must be called BEFORE init)
Public Property Let Rep2(Ch As String)
rpch(1) = Ch
End Property

Public Property Let defType(Flt As textType)
tType = Flt
End Property

Public Property Let Collapse(b As Boolean)
isCollapsed = b
End Property

' Values in filter
Public Property Get strAlpha() As String
strAlpha = Join(aList.Keys, "")
End Property

' Count of letters in the alphabet
Public Property Get Count() As Integer
Count = aList.Count
End Property

' Returns true if the character exists in the alphabet.  Automatically translates any replacement characters
Public Property Get Exists(Ch As String) As Boolean
If rList.Exists(Ch) Then Ch = rList(Ch)
Exists = aList.Exists(Ch)
End Property

' Returns the index of the letter in the alphabet (zero-based).

' Note: This is the default method for this class.  Added the following line in the exported class function and re-imported it
' Attribute Idx.VB_UserMemId = 0
' See http://www.cpearson.com/excel/DefaultMember.aspx

Public Property Get Idx(Ch As String) As Integer
Attribute Idx.VB_UserMemId = 0
If Not aList.Exists(Ch) Then
    Err.Raise 801, "AlphaList:Idx", "Character " & Ch & " not found in Alphabet"
End If
Idx = aList.Item(Ch)
End Property

Public Property Let Idx(Ch As String, V As Integer)
If Not aList.Exists(Ch) Then
    Err.Raise 801, "AlphaList:Idx", "Character " & Ch & " not found in Alphabet"
End If
aList.Item(Ch) = V
End Property

' Returns the character associated with the passed in index

Public Property Get Char(I As Integer) As String
If I < 0 Or I >= aList.Count Then
    Err.Raise 802, "AlphaList:Char", "Index " & I & " is not in range 0-" & aList.Count - 1
End If
Char = aList.Keys(I)
End Property

' Returns the item by number

Public Property Get Item(I As Integer) As Integer
Item = aList.Items(I)
End Property

' Properties for replacement list

Public Property Get ReplExists(Ch As String) As Boolean
ReplExists = rList.Exists(Ch)
End Property

Public Property Get Replacement(Ch As String) As String
If rList.Exists(Ch) Then
    Replacement = rList.Item(Ch)
End If
End Property

' generate alternate index

Public Sub AltIndex()
Dim I As Integer
Dim V As Integer
For I = 0 To aList.Count - 1
    V = aList.Items(I)
    If iList.Exists(V) Then
        Err.Raise 806, "AlphaList:AltIndex", "Duplicate index at position " & I & " in list [" & V & "]"
    End If
    iList.Add V, aList.Keys(I)
Next I
End Sub

' This is a lookup for the integer in the alternate index instead of using the integer AS an index
Public Property Get Char2(V As Integer) As String
If iList.Count = 0 Then
    Err.Raise 807, "AlphaList:Char2", "Alternate index not initialized"
End If
If Not iList.Exists(V) Then
    Err.Raise 801, "AlphaList:Char2", "Index " & V & " not found in list"
End If
Char2 = iList.Item(V)
End Property

' Alias for the regular index

Public Property Get Idx2(Ch As String) As String
Idx2 = Me(Ch)
End Property

' Handle replacements for 24/25 letter alphabets (source text/keys)
' Defaults to J to I for the first replacement.
' Replacement can be a range or an array, so there can be mroe than one
' Defaults to Z to Y for the second replacement.
' There are no defaults for the third or later replacements.
' Returns true if a replacement was added to rList.

Private Function Replaced_(ByVal Repl As String) As Boolean
Dim RCh As String

Replaced_ = False

If InStr(Repl, "-") > 0 Then ' ignore when there is a hyphen
    Exit Function
End If

Repl = ToAlpha(Repl) ' Only Uppercase letters

If Repl = "" Then ' Assign default, if available
    If rList.Count = 0 Then
        Repl = rpch(0)
    ElseIf rList.Count = 1 Then
        Repl = rpch(1)
    Else
        ' empty and no default
        Exit Function
    End If
    If InStr(Repl, "-") > 0 Then ' ignore when default is a hyphen
        Exit Function
    End If
    Repl = ToAlpha(Repl) ' Only Uppercase letters
End If

RCh = Mid(Repl, 2, 1)
Repl = Left(Repl, 1)
' replacement character is optionally the 2nd character of Repl, e.g. "CK"
If RCh = "" Then ' No 2nd character.
    RCh = Chr(65 + (Asc(Repl) - 66) Mod 26) ' replacement char defaults to previous letter
End If
' Can't add two of the same character
If rList.Exists(Repl) Then
    Err.Raise 803, "AlphaList:AddReplacement_", "Replacement character previously added: " & Repl
End If
' The replacement character is also replaced...
If rList.Exists(RCh) Then
    RCh = rList(RCh)
End If
If Repl <> RCh Then ' don't replace with self
    rList.Add Repl, RCh
    Replaced_ = True
End If
End Function

' Return complete alphabet for one of the textType constants (no replacements)
' Alphalist must have been initialized

Private Function Expand_() As String
Dim P As Integer
Dim Template As String
Dim Wrk As String

If tType = ttNone Then
    Err.Raise 816, "AlphaList:Expand_", "AlphaList not initialized, call Init_ before expanding text"
End If

If bSensitive Then ' Upper/Lower case
    Template = LCase(aText) ' Lowercase Alpha
Else
    Template = aText ' Uppercase Alpha
End If

' Build list of filters
Select Case tType
    Case textType.ttAlphaDigit ' Uppercase, insert numbers
        For P = 1 To Len(Template)
            Wrk = Wrk & Mid(Template, P, 1)
            If P <= 10 Then Wrk = Wrk & Mid(pText, P, 1)
        Next P
        ' Inserts = left(pText,10)
    Case textType.ttASCIIPunct ' Uppercase, insert punctuation
        For P = 1 To Len(Template)
            Wrk = Wrk & Mid(Template, P, 1)
            If P >= 11 And P <= 20 Then Wrk = Wrk & Mid(pText, P, 1)
        Next P
        ' Inserts = Mid(pText,11,10)
    Case textType.ttASCIIAll ' Uppercase, insert numbers and punctuation
        For P = 1 To Len(Template)
            Wrk = Wrk & Mid(Template, P, 1)
            If P <= 23 Then Wrk = Wrk & Mid(pText, P, 1)
        Next P
        ' Inserts = pText
    Case textType.ttAlphaPlus
        For P = 1 To Len(Template)
            Wrk = Wrk & Mid(Template, P, 1)
            If P <= 24 Then Wrk = Wrk & Mid(pText, P, 1)
        Next P
    Case textType.ttAlphaUpper, textType.ttAlpha25Upper, textType.ttAlpha, textType.ttAlphaKeep, textType.ttAlpha25 ' Upper/Lowercase letters
        Wrk = Template
    Case textType.ttNumber ' Numbers
        Wrk = nText
    Case textType.ttAnum, textType.ttAnumUpper ' Numbers plus Upper/Lowercase Letters
        'Wrk = Template
        Wrk = nText & Template
    Case Else ' 7-bit ASCII (no DEL)
        For P = 32 To 126
            Wrk = Wrk & Chr(P)
            If P = 64 Then
                ' default alphabet
                Wrk = Wrk & Template
                P = P + 26
            End If
            If P = 96 Then
                ' it's uppercase or lowercase and not case sensitive
                If bSensitive Then Wrk = Wrk & aText
                P = P + 26
            End If
        Next P
End Select
Expand_ = Wrk
End Function

' Initialize the alphabet.  Filter is a variant containing a string or a textType constant
' Repl is a string, an array of strings or a range containing a replacement character
' Repl is only used with 24/25 letter alphabets.

' Returns a string of all possible characters (including unreplaced characters), i.e. 26 letter alphabet...
' Also initializes replacement list

' Collapses alphabets by default, that is ANum becomes AlphaDigit.

' If UnCollapsed is true, will set case sensitive for textType > 0

' TODO TODO TODO TODO TODO ******************************
' This is a private function, only called from this module.  But it always collapses.  Not what was intended.

Private Function Init_(Filt As Variant, Repl As Variant) As String
Dim I As Integer
Dim Cel As Range
Dim Ch As String
Dim L As Long
Dim Tmp As String
Dim Skipped As Boolean
Dim sAlpha As String

' text filter is either a string OR a text type
If IsMissing(Filt) Or IsEmpty(Filt) Then ' just default to straight alphabet
    If tType = ttNone Then ' we didn't set a default
        sAlpha = cs_Alphabet ' default A-Z
    End If
ElseIf VBA.IsNumeric(Filt) Then
    Tmp = Filt
    If Len(Tmp) > 2 Then
        L = 100
    Else
        L = Tmp ' pass number through (textType)
    End If
    If L < 10 Then ' Single digit or less
        tType = L
    Else ' if it is > 9, assume we meant to filter on digits instead
        sAlpha = Filt ' Pass as string (preserve leading zero)
    End If
ElseIf VarType(Filt) = vbString Then
    sAlpha = Filt ' Any string becomes the Alphabet
Else
    Err.Raise 811, "AlphaList:Init_", "Unknown text filter type, expecting String or Number; Vartype = " & VarType(Filt)
End If

If sAlpha <> "" Then
    tType = ttString
ElseIf tType = ttNone Then ' default to cs_Alphabet
    tType = ttAlphaUpper
Else ' must have passed text type instead of a string, build appropriate filter
    If isCollapsed Then ' This is the default
        Select Case tType
            ' Translate tType into an uppercase, expanded alphabet
            Case textType.ttAnum, textType.ttAnumUpper
                tType = textType.ttAlphaDigit  ' "A1B2C3D4..."
            Case textType.ttAlpha, textType.ttAlphaKeep
                tType = textType.ttAlphaUpper  ' A-Z
            Case textType.ttAlpha25
                tType = textType.ttAlpha25Upper ' A-Z with replacements
'            Case textType.ttASCII
'                tType = textType.ttASCIIUpper ' A-Z with replacements
        End Select
    End If
    Select Case tType
        Case ttAlphaPlus To ttASCIIAny
        Case Else ' wasn't a text type we're handling
            Err.Raise 812, "AlphaList:Init_", "Unknown filter value [" & tType & "], expecting -8 to 6"
    End Select
End If

Select Case tType
    Case ttASCII
        ' Only ttASCII is Sensitive by default. Also, don't replace
        Me.Sensitive
        MustReplace = False
    Case ttAnum, ttAlphaKeep, ttAlpha, ttAnumUpper, ttAlphaUpper
        ' use default replacement
    Case ttAlpha25, ttAlpha25Upper, ttAlphaPlus
        MustReplace = True
    Case Else
        MustReplace = False
End Select

' Handle replacements for 24/25 letter alphabets
If MustReplace Then
    ' Process SR Parameter - must be a String or a Range of characters
    If IsMissing(Repl) Or IsEmpty(Repl) Then
        If Not Replaced_("") Then Skipped = True  ' use default
    ElseIf VarType(Repl) = vbString Then
        ' It's a String
        Ch = Repl
        If Not Replaced_(Ch) Then Skipped = True  ' use passed in string
    ElseIf TypeName(Repl) = "Range" Then ' It's a range of cells from a spreadsheet
        For Each Cel In Repl
            Ch = Cel.Value2
            If Not Replaced_(Ch) Then Skipped = True  ' use individual cell values
        Next Cel
    ElseIf TypeName(Repl) = "String()" Then ' It's an array from another function (encMondi)
        For I = LBound(Repl) To UBound(Repl)
            Ch = Repl(I)
            If Not Replaced_(Ch) Then Skipped = True  ' use individual array values
        Next I
    Else
        Err.Raise 804, "AlphaList:Init_", "Unknown parm type for range (expecting string/range): " & TypeName(Repl)
    End If
    
    If tType = ttAlphaPlus Then
        If rList.Count = 1 Then ' must have two for this Alphabet
            If Not Replaced_("") Then Skipped = True
        End If
        If rList.Count <> 2 Then
            Err.Raise 805, "AlphaList:Init_", "Expecting two replacements for this alphabet, found " & rList.Count
        End If
    End If
    
    ' Check if multiple replacements and items added out of order
    For I = 0 To rList.Count - 1
        Ch = rList.Items(I)
        If rList.Exists(Ch) Then
            'Example IJ, JK will become IK, JK
            rList(rList.Keys(I)) = rList(Ch)
            ' not checking for circular replacements
            '    Err.Raise 805, "AlphaList:SetReplacement_", "Circular Replacement: " & rList.Keys(I) & "=>" & Ch & " and " & Ch & "=>" & rList(Ch)
        End If
    Next I
End If

If sAlpha = "" Then ' must have been one of the textType constants
    sAlpha = Expand_
End If

Init_ = sAlpha
End Function

' Guarantee all things are copacetic when replacements are present (probably overkill)
' Don't think this can happen with the replacement array we built for dedup
' But, can't hurt to check

' Must be done after aList is loaded (Init or Init2)

Private Sub ChkRlist_()
Dim I As Integer
Dim Ch As String
If tType <> ttAlphaPlus Then
    If rList.Count Then
        For I = 0 To rList.Count - 1
            Ch = rList.Keys(I)
            If aList.Exists(Ch) Then ' replacing this letter, but it's still in the list...
                Err.Raise 815, "AlphaList:Init", "Replacement key " & Ch & " found in filter" & strAlpha
            End If
            Ch = rList.Items(I)
            If Not aList.Exists(Ch) Then ' value we are changing to is not in the list
                Err.Raise 814, "AlphaList:Init", "Replacement value " & Ch & " not found in filter" & strAlpha
            End If
        Next I
    End If
End If
End Sub

' Initialize the alphabet.  Filter is a variant containing a string or a textType constant
' Repl is a string, an array of strings or a range containing a replacement character
' Repl is only used with 24/25 letter alphabets.

Public Sub Init(Optional Filt As Variant, Optional Repl As Variant, Optional Offset As Integer)
Dim I As Integer
Dim V As Integer
Dim Ch As String
Dim sAlpha As String
Dim Replaced As New Scripting.Dictionary
Replaced.CompareMode = TextCompare

sAlpha = Init_(Filt, Repl)

' Load text filter into dictionary
For I = 1 To Len(sAlpha) ' put the string into the text filter list
    Ch = Mid(sAlpha, I, 1)
    If Ch >= " " Then ' ignore control characters
        ' replacement
        If tType <> ttAlphaPlus Then
            If rList.Exists(Ch) Then
                If (tType = 0) And (Ch <> UCase(Ch)) Or (tType > 0) Then
                    Ch = LCase(rList(Ch))
                Else
                    Ch = rList(Ch)
                End If
                If Not Replaced.Exists(Ch) Then
                    Replaced.Add Ch, Replaced.Count
                End If
            End If
        End If
        If aList.Exists(Ch) Then
            If Not Replaced.Exists(Ch) Then ' no dups allowed
                Err.Raise 813, "AlphaList:Init", "Duplicate character at position " & I & " in filter [" & Ch & "]"
            End If
        Else
            Select Case Offset
                Case oType.oZero
                    aList.Add Ch, 0
                Case oType.oASCII
                    V = Asc(Ch)
                    If V > MaxOfs Then MaxOfs = V
                    aList.Add Ch, V
                Case Else
                    MaxOfs = aList.Count + Offset
                    aList.Add Ch, MaxOfs ' zero based index without offset
            End Select
        End If
    End If
Next I

ChkRlist_

'bValid = True
End Sub

' This is the core undup routine

Private Function Undup_(Key As String, text As String, Bonus As String) As String
Dim I As Integer
Dim Ln As Integer
Dim Ch As String
Dim Cv As String
Dim RCh As String
Dim Work As String
Dim Result As String
Dim List As New Scripting.Dictionary

If tType = ttASCII Then
    List.CompareMode = BinaryCompare ' Case sensitive
Else
    List.CompareMode = TextCompare ' Case insensitive
End If

' Default expansion text on wildcard
If text = "*" Or text = "&" Or text = "@" Then
    Work = text ' save value
    If tType = ttNumber Then
        text = nText
    Else
        If Work = "@" Then
            text = LCase(eText)
        Else
            text = LCase(aText)
        End If
    End If
    If Work = "&" Then
        text = Reverse(text)
    End If
End If

' Dedup text

Work = Key & text & Bonus

Ln = Len(Work)
For I = 1 To Ln
    Ch = Mid(Work, I, 1)
    If List.CompareMode = BinaryCompare Then
        Cv = Ch
    Else
        Cv = UCase(Ch) ' Ch = original case, Cv = Uppercase
    End If
    If tType <> ttAlphaPlus Then ' don't replace here, only on compact
        If rList.Exists(Ch) Then
            RCh = rList(Ch)
            If Ch = UCase(Cv) Then ' replaced character is uppercase
                Ch = RCh
            Else
                Ch = LCase(RCh) ' preserve case of replaced character
            End If
            Cv = RCh
        End If
    End If
    If Not List.Exists(Cv) Then ' This character is not already in the list
        If aList.Exists(Cv) Then ' We want it
            If Inserts.Exists(Cv) Then ' There could be a follow-up
                If Not List.Exists(Inserts(Cv)) Then ' follow-up not in list, add this character & switch
                    Result = Result & Cv ' All inserts are currently case insensitive (uppercase)
                    List.Add Cv, List.Count
                    Ch = Inserts(Cv) ' All follow-ups are currently not case sensitive, but...
                    Cv = UCase(Ch)
                End If
            End If
            If tType < textType.ttAlphaKeep Then ' Negative, fold to uppercase
                Ch = Cv
            End If
            Result = Result & Ch
            List.Add Ch, List.Count
        End If
    End If
Next I

If tType = ttAlphaPlus Then ' add SwpCh
    Work = Result
    RCh = rList.Keys(0)
    Cv = rList.Keys(1)
    Result = ""
    For I = 1 To Len(Work)
        Ch = Mid(Work, I, 1)
        If Ch = RCh Or Ch = Cv Then Result = Result & SwpCh
        Result = Result & Ch
    Next I
    Result = Paginate(Result, -1)
End If

Undup_ = Result

End Function

' This function will remove duplicate characters from two strings based on an alphabet.  That is all characters
' not in the alphabet are removed, and any remaining suplicate characters are also removed.

' By default, folds all characters to uppercase.  Only ttASCII is case sensitive and allows both upper and lower case.

' The last three parameters make up the alphabet.  textType determines the base alphabet - the default is simply an
' uppercase alphabet.  Repl is a string or range containing a list of replacement pairs for a 25 letter alphabet - the
' default for a 25 letter alphabet is to replace "J" with "I".  Finally, any bonus characters to the alphabet as well.

' The first two parameters (key+text) are the characters that are passed through the filter and deduped.  Often the
' first parameter is a key, and the second is an alphabet... but not always.  The text parameter may also be an "*",
' which will default to a complete alphabet (or a string of digits is textType is ttNumber).

' if the textType is ttAlphaDigit, ttASCIIPunct or ttASCIIAll, then either digits or punctuation (or both) will be
' inserted following specific characters.  For example, a 2 will follow a B and a "." will follow a P, even if those
' were not present in either the key or the text (unless they were already inserted).

Public Function Dedup(Key As String, Optional text As String, _
    Optional Flt As Variant, Optional Repl As Variant, Optional Bonus As String = "") As String
Dim I As Integer
Dim A As Integer
Dim b As Integer
Dim Ch As String

If tType = ttNone Then tType = ttAlphaUpper

isCollapsed = False

Inserts.CompareMode = TextCompare

Me.Init Flt, Repl

' Add bonus to Alphabet

For I = 1 To Len(Bonus)
    Ch = Mid(Bonus, I, 1)
    If Not aList.Exists(Ch) Then
        aList.Add Ch, aList.Count
    End If
Next I

' Build list of inserts
Select Case tType
    Case ttAlphaDigit ' A-J
        A = 1
        b = 10
    Case ttASCIIPunct ' K-T
        A = 11
        b = 20
    Case ttASCIIAll   ' A-W
        A = 1
        b = Len(pText) - 1
    Case ttAlphaPlus  ' A-X
        A = 1
        b = Len(pText)
    Case Else
        b = -1
End Select
For I = A To b
    Ch = Mid(aText, I, 1)
    Inserts.Add Ch, Mid(pText, I, 1)
Next I

Undupable = True

Dedup = Undup_(Key, text, Bonus)

End Function

' Repeats a dedup operation using the already initialized lists

Public Function Undup(Key As String, Optional text As String, Optional Bonus As String) As String

If Not Undupable Then
    Err.Raise 822, "AlphaList:Undup", "List not initialized, call Dedup before Undup"
End If

Undup = Undup_(Key, text, Bonus)

End Function

' This is the core Compact routine

Private Function Compact_(text As String) As String
Dim I As Integer
Dim Ch As String
Dim Prev As String
Dim Work As String
Dim Result As String

' Fold case
If tType < 0 Then
    Work = UCase(text)
Else
    Work = text
End If

' Compact
For I = 1 To Len(Work)
    Ch = Mid(Work, I, 1)
    If rList.Exists(Ch) Then ' perform replacement first
        Ch = rList(Ch)
    End If
    If aList.Exists(Ch) Then ' Only process letters
        ' ch2 is previous character
        If Prev = Ch And Even And PadCh <> "" Then ' Do we need to add padding?
            If Swap Then
                Ch = PadCh
            Else
                Result = Result & PadCh
                Even = False
            End If
        End If
        Result = Result & Ch
        Even = Not Even
        Prev = Ch
    ElseIf Punct Then
        If Ch >= " " Then Result = Result & Ch
    End If
Next I

' Final character
If Even Then
    If Final Then ' Handle even boundary pad
        PadCh = fCh
    End If
    If Prev = PadCh Then ' we don't want a double letter.  Skip to the next letter
        Work = strAlpha ' get alphabet into Work
        Do
            I = InStr(1, Work, PadCh, aList.CompareMode) ' index of pad character in alphabet (1 based)
            PadCh = Mid(Work, (I Mod aList.Count) + 1, 1) ' mod returns zero based (wrapped), add 1 to get new string index
        Loop Until Not rList.Exists(PadCh) ' in case next character was a replacement - not sure this can happen, but...
    End If
    Result = Result & PadCh
End If

Compact_ = Result
End Function

' This function strips punctuation and spaces from a string
' it also optionally inserts a padding character between doubled letters (default behavior).
' If pad = "+", then do not insert pad between doubled letters, but still pad final "X" if needed
' If pad = "", then do not insert pad at all
' if pad = "-X" then replace rather than insert between double letters
' Finally, it replaces a specific character with its predecessor, e.g. J becomes I (always does this)
' Compact ("Oh, Hello World", "X") = "OHHELXLOWORLDX"

Public Function Compact(text As String, Optional Pad As String = "X", Optional Repl As Variant, _
    Optional Filt As Variant, Optional Bonus As String = "") As String
Dim I As Integer
Dim Ch As String

If tType = ttNone Then tType = textType.ttAlpha25Upper
isCollapsed = False
MustReplace = True

Me.Init Filt, Repl

' Add bonus to Alphabet

For I = 1 To Len(Bonus)
    Ch = Mid(Bonus, I, 1)
    If Not aList.Exists(Ch) Then
        aList.Add Ch, aList.Count
    End If
Next I

' ---< Flags

' Flag for doubled letters (even boundry)
Even = False
' Flag for final pad character
Final = False

Punct = InStr(Pad, ".") > 0 ' Don't remove punctuation
Final = InStr(Pad, "+") > 0 ' Only final "X" if odd length
If Not Final Then Swap = InStr(Pad, "-") > 0 ' Swap instead of insert

' ---< Pad character

PadCh = Left(ToAlpha(Pad), 1)
If Swap Or Final Then
    If PadCh = "" Then PadCh = "X"
End If
If PadCh <> "" Then
    If rList.Exists(PadCh) Then ' Can't pad with replacement character
        PadCh = rList(PadCh)
    End If
End If
If Final Then
    fCh = PadCh
    PadCh = ""
End If

Compactable = True

Compact = Compact_(text)

End Function

' Repeats a compact operation using the already initialized lists.

Public Function Recompact(text As String) As String
If Not Compactable Then
    Err.Raise 823, "Alphalist:Recompact", "List not initialized, call Compact before Recompact"
End If
Recompact = Compact_(text)
End Function


' Initialize the digraphic alphabet.  Filter is a variant containing a string or a textType constant
' Repl is a string, an array of strings or a range containing a replacement character
' Repl is only used with 24/25 letter alphabets.

' This is a Digraphic alphabet made up of all the combinations of characters in the base alphabet

Public Sub Init2(Optional Filt As Variant, Optional Repl As Variant, Optional Offset As Integer)
Dim I As Integer, J As Integer
Dim Ch As String, Ch2 As String
Dim C1 As String, C2 As String
Dim DI As String, DR As String
Dim sAlpha As String
Dim Replaced As New Scripting.Dictionary
Replaced.CompareMode = TextCompare

sAlpha = Init_(Filt, Repl)

' Load text filter into dictionary
For I = 1 To Len(sAlpha) ' put the string into the text filter list
    ' replacement
    Ch = Mid(sAlpha, I, 1)
    C1 = Ch
    If rList.Exists(Ch) Then
        If (tType = 0) And (Ch <> UCase(Ch)) Or (tType > 0) Then
            Ch = LCase(rList(Ch))
        Else
            Ch = rList(Ch)
        End If
    End If
    If Ch >= " " Then ' ignore control characters
        For J = 1 To Len(sAlpha)
            Ch2 = Mid(sAlpha, J, 1)
            C2 = Ch2
            If rList.Exists(Ch2) Then
                If (tType = 0) And (Ch2 <> UCase(Ch2)) Or (tType > 0) Then
                    Ch2 = LCase(rList(Ch2))
                Else
                    Ch2 = rList(Ch2)
                End If
            End If
            If Ch2 >= " " Then
                DI = Ch & Ch2
                DR = C1 & C2
                If DR <> DI Then
                    If Not Replaced.Exists(DI) Then Replaced.Add DI, 0
                End If
                If aList.Exists(DI) Then
                    If Not Replaced.Exists(DI) Then ' no dups allowed
                        Err.Raise 813, "AlphaList:Init", "Duplicate character at position " & I & "/" & J & " in filter [" & DI & "]"
                    End If
                Else
                    If Offset = oType.oZero Then
                        aList.Add DI, 0
                    Else
                        aList.Add DI, aList.Count + Offset ' zero based index without offset
                    End If
                End If
            End If
        Next J
    End If
Next I

ChkRlist_

'bValid = True
End Sub

' Filter a string to contain only characters in the alphabet
Public Function Filter(text As String) As String
Dim I As Integer
Dim Ch As String
Dim Wrk As String

For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If rList.Exists(Ch) Then Ch = rList(Ch) ' Handle replacement, if any
    If aList.Exists(Ch) Then
        Wrk = Wrk & Ch
    End If
Next I

Filter = Wrk
End Function

' translate a string of characters from the alphabet into numbers (alphabet offset)
' need format string (for hex)
Public Function toOffset(text As String, Optional AsHex As Boolean) As String
Dim I As Integer
Dim Width As Integer
Dim Ch As String
Dim Wrk As String
Dim Pad As String
Dim Fmt As String

If AsHex Then
    I = MaxOfs
    Do
        Width = Width + 1
        I = I \ 16
    Loop Until I = 0
Else
    Width = Len("" & MaxOfs)
    Fmt = String$(Width, "0")
End If

For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If rList.Exists(Ch) Then Ch = rList(Ch) ' Handle replacement, if any
    If aList.Exists(Ch) Then
        If AsHex Then
            Wrk = Wrk & Application.WorksheetFunction.Dec2Hex(aList(Ch), Width)
        Else
            Wrk = Wrk & Application.WorksheetFunction.text(aList(Ch), Fmt)
        End If
    End If
Next I

toOffset = Wrk
End Function

' translate a string of numbers (offsets) into characters from the alphabet
' need format string (for hex)
Public Function toChars(text As String, Optional AsHex As Boolean) As String
Dim I As Integer
Dim V As Integer
Dim Ln As Integer
Dim Width As Integer
Dim Ch As String
Dim Cd As String
Dim Wrk As String
Dim Digits As New Scripting.Dictionary

Ln = Len(text)

If Ln = 0 Then
    Exit Function
End If

For I = 0 To 9
    Ch = I
    Digits.Add Ch, Digits.Count
Next I

If AsHex Then
    For I = 1 To 6
        Ch = Mid("ABCDEF", I, 1)
        Digits.Add Ch, Digits.Count
    Next I
    I = MaxOfs
    Do
        Width = Width + 1
        I = I \ 16
    Loop Until I = 0
Else
    Width = Len("" & MaxOfs)
End If

If iList.Count = 0 Then
    Me.AltIndex
End If

For I = 1 To Ln
    Ch = Mid(text, I, 1)
    If Digits.Exists(Ch) Then
        Cd = Cd & Ch
        If Len(Cd) = Width Then
            If AsHex Then
                V = Application.WorksheetFunction.Hex2Dec(Cd)
            Else
                V = Cd
            End If
            Cd = ""
            If Not iList.Exists(V) Then
                Err.Raise 825, "AlphaList:textFilter", "Error: Numeric code (" & V & ") is not in the alphabet"
            End If
            Wrk = Wrk & iList(V)
        End If
    End If
Next I

If Cd <> "" Then
    Err.Raise 824, "AlphaList:textFilter", "Error: Filtered text length (" & Len(Wrk & Cd) & ") is not a multiple of code width (" & Width & ")"
End If

toChars = Wrk
End Function

' Filter a string to contain only digraphs in the alphabet

' needs work; haven't written this one yet

'Public Function Filter2(text As String) As String
'Dim I As Integer
'Dim Ch As String
'Dim Ch2 As String
'Dim Wrk As String

'If rList.Count Then ' check rlist first
'    For I = 1 To Len(text)
'        Ch = Mid(text, I, 1)
'        If rList.Exists(Ch) Then Ch = rList(Ch) ' Handle replacement, if any
'        If aList.Exists(Ch) Then
'            Wrk = Wrk & Ch
'        End If
'    Next I
'Else
'    For I = 1 To Len(text)
'        Ch = Mid(text, I, 1)
'        If aList.Exists(Ch) Then
'            Ch2 = Mid(text, I + 1, 1)
'
'            Wrk = Wrk & Ch
'        End If
'    Next I
'End If

'Filter2 = Wrk
'End Function

