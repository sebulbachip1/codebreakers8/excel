Attribute VB_Name = "Digraphic"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Digraphic
'
' USAGE:
'       Foursquare/Twosquare/Playfair codes (digraphic, Trigraphic)
'
' GROUPS:
'       Digraphic Substitution
'            encDiSub, decDiSub
'
'       Foursquare
'            EncFoursquare, DecFoursquare
'
'       Twosquare
'            EncTwosquare, DecTwosquare
'
'       Playfair
'            Encode, Decode (single digraph)
'            EncPlayfair, DecPlayfair
'
'       Seriated Playfair
'            EncSeriated, DecSeriated
'
'       DIGRAPHID
'            encDIGRAPHID, decDIGRAPHID
'
'       Trisquare
'            encTrisquare, decTrisquare
'
'       Dynamic (undocumented)
'            EncodeSquare, DecodeSquare
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 01/28/2019 Chip Bloch    -Added Twosquare, Foursquare routines
' 06/03/2019 Chip Bloch    -Added pass-through for punctuation on playfair
' 06/13/2019 Chip Bloch    -Made pad character on Playfair optional
' 06/25/2020 Chip Bloch    -Enc/DecPlayFair using Polybius
'                          -Removed MCase in Enc/DecPlayfair
' 08/08/2020 Chip Bloch    -Reordered parms for Polybius
' 08/08/2020 Chip Bloch    -Calling Filter in Polybius for decChecker functions
' 08/21/2020 Chip Bloch    -Updated Encode, Decode, EncTwoSquare, DecTwoSquare to use Polybius
' 08/23/2020 Chip Bloch    -Updated EncodeFour, EncFoursquare, DecodeFour, DecFoursquare to us Polybius
' 09/17/2020 Chip Bloch    -Migrated from Playfair, removed deprecated enc/decodetwo/four routines
' 09/18/2020 Chip Bloch    -Added DIGRAPHID
' 09/29/2020 Chip Bloch    -Added error check for odd length playfair
' 11/25/2020 Chip Bloch    -Added EncSeriated, DecSeriated
' 12/05/2020 Chip Bloch    -Dynamic Playfair squares (5x5, 6x6, 7x7)
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType (pSquare.Make)
' 12/17/2020 Chip Bloch    -Made Tri-Square, Seriated Playfair, Two-Square, Four-Square Dynamic
' 12/31/2020 Chip Bloch    -Made Digraphid ignore separator if used in key
' 02/09/2021 Chip Bloch    -Added reference to AddSep
' 02/21/2021 Chip Bloch    -Added Digraphic Substitution (Digraphic "Tabula Recta")
'                          -Added Vertical option for TwoSquare
' 04/03/2021 Chip Bloch    -Trapped filtered values missing from keys in *DiSub
'                          -Using Undup in *DiSub
' 04/11/2021 Chip Bloch    -Added period -1 in DIGRAPHID
' 04/22/2021 Chip Bloch    -Calling Seriate_ in DIGRAPHID
'-------------------------------------------------------------------------------------------

' Dynamic Decryption

Public Function DecodeSquare(Digraph As String, R1 As Range, Optional R2 As Range = Nothing, Optional R3 As Range = Nothing, Optional R4 As Range = Nothing) As String
If R2 Is Nothing Then
    DecodeSquare = DecPlayfair(Digraph, R1)
ElseIf R3 Is Nothing Then
    DecodeSquare = DecTwosquare(Digraph, R1, R2)
ElseIf R4 Is Nothing Then
    DecodeSquare = DecFoursquare(Digraph, R1, R2, R3, R1)
Else
    DecodeSquare = DecFoursquare(Digraph, R1, R2, R3, R4)
End If
End Function

' Dynamic Encryption

Public Function EncodeSquare(Digraph As String, R1 As Range, Optional R2 As Range = Nothing, Optional R3 As Range = Nothing, Optional R4 As Range = Nothing) As String
If R2 Is Nothing Then
    EncodeSquare = EncPlayfair(Digraph, R1)
ElseIf R3 Is Nothing Then
    EncodeSquare = EncTwosquare(Digraph, R1, R2)
ElseIf R4 Is Nothing Then
    EncodeSquare = EncFoursquare(Digraph, R1, R2, R3, R1)
Else
    EncodeSquare = EncFoursquare(Digraph, R1, R2, R3, R4)
End If
End Function

' Playfair encryption/decription methods

' This function decodes a digraph using a reference to a 5x5 grid
' This is the new Playfair decryption method using Polybius squares

Private Function pDecode(Digraph As String, Poly As Polybius) As String
Dim X As Integer, Y As Integer
Dim R As Integer, C As Integer
Dim Char1 As String
Dim Char2 As String
Dim Result As String
Char1 = Left(Digraph, 1)
Char2 = Mid(Digraph, 2, 1)
Poly.RC Char1, R, C
Poly.RC Char2, Y, X
If (R = Y) Then ' 1 line
    If (X = C) Then ' 1 cell (double letter) - Not technically allowed
        Result = Poly.chrVal(Poly.RDown(R), Poly.CDown(C)) & Poly.chrVal(Poly.RDown(Y), Poly.CDown(X))
    Else
        Result = Poly.chrVal(R, Poly.CDown(C)) & Poly.chrVal(R, Poly.CDown(X))
    End If
Else
    If (X = C) Then ' 1 col
        Result = Poly.chrVal(Poly.RDown(R), C) & Poly.chrVal(Poly.RDown(Y), C)
    Else ' rectangle
        Result = Poly.chrVal(R, X) & Poly.chrVal(Y, C)
    End If
End If

pDecode = Result
End Function

' This function encodes a digraph using a reference to a 5x5 grid
' This is the new Playfair encryption method using Polybius squares

Private Function pEncode(Digraph As String, Poly As Polybius) As String
Dim X As Integer, Y As Integer
Dim R As Integer, C As Integer
Dim Char1 As String
Dim Char2 As String
Dim Result As String
Char1 = UCase(Left(Digraph, 1))
If Len(Digraph) > 1 Then
    Char2 = Mid(Digraph, 2, 1)
Else
    Char2 = "X"
End If
Poly.RC Char1, R, C
Poly.RC Char2, Y, X
If (R = Y) Then ' 1 line
    If (X = C) Then ' 1 cell - NOT technically allowed
        Result = Poly.chrVal(Poly.RUp(R), Poly.CUp(C)) & Poly.chrVal(Poly.RUp(Y), Poly.CUp(X))
    Else
        Result = Poly.chrVal(R, Poly.CUp(C)) & Poly.chrVal(R, Poly.CUp(X))
    End If
Else
    If (X = C) Then ' 1 col
        Result = Poly.chrVal(Poly.RUp(R), C) & Poly.chrVal(Poly.RUp(Y), C)
    Else ' rectangle
        Result = Poly.chrVal(R, X) & Poly.chrVal(Y, C)
    End If
End If

pEncode = Result
End Function

' This function encodes a digraph using a reference to a 5x5 grid
' This is the old Playfair encryption method
' the result of ENCODE(A1, B1:F5) depends on the values in A1 and B1:F5...
' Deprecated; retained for compatibility

Public Function encode(Digraph As String, Rng As Variant) As String
Dim pSquare As New Polybius
On Error GoTo Catch
Call pSquare.Make(Rng, 5, textType.ttAlphaUpper)
encode = pEncode(Digraph, pSquare)
Exit Function
Catch:
encode = Err.Description
End Function

' This function decodes a digraph using a reference to a 5x5 grid
' This is the Playfair decryption method
' the result of DECODE(A1, B1:F5) depends on the values in A1 and B1:F5...
' Deprecated; retained for compatibility
Public Function decode(Digraph As String, Rng As Variant) As String
Dim pSquare As New Polybius
On Error GoTo Catch
Call pSquare.Make(Rng, 5, textType.ttAlphaUpper)
decode = pDecode(Digraph, pSquare)
Exit Function
Catch:
decode = Err.Description
End Function

' This function is the general Playfair encryption method (Private function because of seriated playfair)
Private Function EncPlayfair_(text As String, pSquare As Polybius, Optional Pad As String = "X") As String
Dim I As Integer
Dim Ch As String
Dim Digraph As String
Dim LeftCh As String
Dim Punctuation As String
Dim Work As String
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Not pSquare.Exists(Ch, ftSquare) Then
        If LeftCh = "" Then
            Work = Work & Ch
        Else
            Punctuation = Punctuation & Ch
        End If
    Else
        If LeftCh = "" Then
            LeftCh = Ch
        Else
            If UCase(LeftCh) = UCase(Ch) And Pad <> "" Then
                Digraph = LeftCh & Pad
                LeftCh = Ch
            Else
                Digraph = LeftCh & Ch
                LeftCh = ""
            End If
            Digraph = pEncode(Digraph, pSquare)
            Work = Work & Left(Digraph, 1) & Punctuation & Right(Digraph, 1)
            Punctuation = ""
        End If
    End If
Next I
If LeftCh <> "" Then
    Digraph = LeftCh & Pad
    Digraph = pEncode(Digraph, pSquare)
    Work = Work & Left(Digraph, 1) & Punctuation & Right(Digraph, 1)
End If
'Work = MCase(Work, text)
EncPlayfair_ = Work
End Function

' This function is the general Playfair encryption method
Public Function EncPlayfair(text As String, Rng As Variant, Optional Pad As String = "X") As String
Dim pSquare As New Polybius
On Error GoTo Catch
Call pSquare.Make(Rng, -1) ' Dynamic square
EncPlayfair = EncPlayfair_(text, pSquare, Pad)
Exit Function
Catch:
EncPlayfair = Err.Description
End Function

' This function is the general Playfair decryption method (Private function because of seriated playfair)
Private Function DecPlayfair_(text As String, pSquare As Polybius) As String
Dim I As Integer
Dim Ch As String
Dim Digraph As String
Dim LeftCh As String
Dim Punctuation As String
Dim Work As String
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Not pSquare.Exists(Ch, ftSquare) Then
        If LeftCh = "" Then
            Work = Work & Ch
        Else
            Punctuation = Punctuation & Ch
        End If
    Else
        If LeftCh = "" Then
            LeftCh = Ch
        Else
            Digraph = LeftCh & Ch
            Digraph = pDecode(Digraph, pSquare)
            Work = Work & Left(Digraph, 1) & Punctuation & Right(Digraph, 1)
            LeftCh = ""
            Punctuation = ""
        End If
    End If
Next I
If LeftCh <> "" Then ' this shouldn't happen if the encryption worked?
    Err.Raise 866, "DecPlayfair_", "Cipher text is odd length"
End If
'Work = MCase(Work, text)
DecPlayfair_ = Work
End Function

' This function is the general Playfair decryption method
Public Function DecPlayfair(text As String, Rng As Variant) As String
Dim pSquare As New Polybius
On Error GoTo Catch
Call pSquare.Make(Rng, -1) ' Dynamic square
DecPlayfair = DecPlayfair_(text, pSquare)
Exit Function
Catch:
DecPlayfair = Err.Description
End Function

' This function is the general twosquare encryption method

Public Function EncTwosquare(text As String, LR As Variant, RR As Variant, Optional HorV As String) As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim I As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pLR As New Polybius
Dim pRR As New Polybius

On Error GoTo Catch

Call pLR.Make(LR, -1)
Call pRR.Make(RR, -1)

HorV = Left(ToAlpha(HorV), 1)
If HorV <> "V" Then HorV = "H"

For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    pLR.RC Char1, R, C
    
    Char2 = Mid(text, I + 1, 1)
    If Char2 = "" Then Char2 = "X"
    pRR.RC Char2, Y, X
    
    If HorV = "H" Then
        Work = Work & pRR.chrVal(R, X) & pLR.chrVal(Y, C)
    Else
        Work = Work & pRR.chrVal(Y, C) & pLR.chrVal(R, X)
    End If
Next I

EncTwosquare = Work
Exit Function
Catch:
EncTwosquare = Err.Description
End Function

' This function is the general twosquare encryption method

Public Function DecTwosquare(text As String, LR As Variant, RR As Variant, Optional HorV As String) As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim I As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pLR As New Polybius
Dim pRR As New Polybius

On Error GoTo Catch

Call pLR.Make(LR, -1)
Call pRR.Make(RR, -1)

HorV = Left(ToAlpha(HorV), 1)
If HorV <> "V" Then HorV = "H"

For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    pRR.RC Char1, R, C
    
    Char2 = Mid(text, I + 1, 1)
    pLR.RC Char2, Y, X
    
    If HorV = "H" Then
        Work = Work & pLR.chrVal(R, X) & pRR.chrVal(Y, C)
    Else
        Work = Work & pLR.chrVal(Y, C) & pRR.chrVal(R, X)
    End If
Next I

DecTwosquare = Work
Exit Function
Catch:
DecTwosquare = Err.Description
End Function

' This function is the general foursquare encryption method

Public Function EncFoursquare(text As String, TL As Variant, TR As Variant, BL As Variant, Optional BR As Variant) As String
Dim I As Integer
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pTL As New Polybius
Dim pTR As New Polybius
Dim pBL As New Polybius
Dim pBR As New Polybius
On Error GoTo Catch
Call pTL.Make(TL, -1)
Call pTR.Make(TR, -1)
Call pBL.Make(BL, -1)
If IsMissing(BR) Or IsEmpty(BR) Then ' Use Top Left
    Call pBR.Make(TL, -1)
Else
    Call pBR.Make(BR, -1)
End If
For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    Char2 = Mid(text, I + 1, 1)
    If Char2 = "" Then Char2 = "X"
    pTL.RC Char1, R, C
    pBR.RC Char2, Y, X
    Work = Work & pTR.chrVal(R, X) & pBL.chrVal(Y, C)
Next I
EncFoursquare = Work
Exit Function
Catch:
EncFoursquare = Err.Description
End Function

' This function is the general Foursquare decryption method

Public Function DecFoursquare(text As String, TL As Variant, TR As Variant, BL As Variant, Optional BR As Variant) As String
Dim I As Integer
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pTL As New Polybius
Dim pTR As New Polybius
Dim pBL As New Polybius
Dim pBR As New Polybius
On Error GoTo Catch
Call pTL.Make(TL, -1)
Call pTR.Make(TR, -1)
Call pBL.Make(BL, -1)
If IsMissing(BR) Or IsEmpty(BR) Then
    Call pBR.Make(TL, -1)
Else
    Call pBR.Make(BR, -1)
End If
For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    Char2 = Mid(text, I + 1, 1)
    If Char2 = "" Then Char2 = "X"
    pTR.RC Char1, R, C
    pBL.RC Char2, Y, X
    Work = Work & pTL.chrVal(R, X) & pBR.chrVal(Y, C)
Next I
DecFoursquare = Work
Exit Function
Catch:
DecFoursquare = Err.Description
End Function

' This function is the Trisquare encryption method
Public Function encTrisquare(text As String, S1 As Variant, S2 As Variant, S3 As Variant, Optional Repl As String = "J") As String
Dim I As Integer
Dim R1 As Integer, C1 As Integer
Dim R2 As Integer, C2 As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim pSquare3 As New Polybius
Dim Work As String
On Error GoTo Catch

InitRand text ' Repeatable random

' Initialize squares
Call pSquare1.Make(S1, -1)
Call pSquare2.Make(S2, -1)
Call pSquare3.Make(S3, -1)

' Remove non-alphabetic characters and replace substitution charater, pad with "X" if needed
text = Compact(text, "+", Repl, pSquare1.PolyVals)

' Encode digraphs
For I = 1 To Len(text) Step 2
    pSquare1.RC Mid(text, I, 1), R1, C1 ' Row1 Col1 from square 1 for char 1
    pSquare2.RC Mid(text, I + 1, 1), R2, C2 ' Row2 Col2 from square 2 for char 2
    ' letter from random row/col1 from square 1, row1/col2 from square 3, row2/random col from square 2...
    Work = Work & pSquare1.chrVal(Int(Rnd() * pSquare1.Cols) + 1, C1) & pSquare3.chrVal(R1, C2) & pSquare2.chrVal(R2, Int(Rnd() * pSquare2.Cols) + 1)
Next I

encTrisquare = Work
Exit Function
Catch:
encTrisquare = Err.Description
End Function

' This function is the Trisquare decryption method
Public Function decTrisquare(text As String, S1 As Variant, S2 As Variant, S3 As Variant) As String
Dim I As Integer, lt As Integer
Dim R1 As Integer, C1 As Integer
Dim R2 As Integer, C2 As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim pSquare3 As New Polybius
Dim Work As String
On Error GoTo Catch

' Initialize squares
Call pSquare1.Make(S1, -1)
Call pSquare2.Make(S2, -1)
Call pSquare3.Make(S3, -1)

' Limit to only alphabetic (remove padding)
text = pSquare1.Filter(text, ftSquare)
lt = Len(text)

If lt Mod 3 <> 0 Then
    decTrisquare = "ERROR: Text is not a multiple of length 3- [" & text & "]"
    Exit Function
End If

' Now decode with Polybius Squares
For I = 1 To lt Step 3
    C1 = pSquare1.Col(Mid(text, I, 1)) ' get col1 from square 1
    pSquare3.RC Mid(text, I + 1, 1), R1, C2 ' get row1/col2 from square 3
    R2 = pSquare2.Row(Mid(text, I + 2, 1)) ' get row2 from square 2
    Work = Work & pSquare1.chrVal(R1, C1) & pSquare2.chrVal(R2, C2)
Next I

decTrisquare = Work
Exit Function

Catch:
decTrisquare = Err.Description
End Function

' This function is the Trisquare encryption method
Public Function encDIGRAFID(text As String, HS As Variant, VS As Variant, Period As Integer, Optional Sep As String = "") As String
Dim I As Integer
Dim R1 As Integer, C1 As Integer
Dim R2 As Integer, C2 As Integer
Dim RC As Integer
Dim Vert As New Polybius
Dim Hor As New Polybius
Dim Work As String
Dim Ch As String
Dim Block As String
On Error GoTo Catch

' Initialize squares
Call Hor.Make(HS, 9, textType.ttASCIIUpper, 3)
Work = range2str(VS)
' second square must contain same characters
Call Vert.Make(Hor.Filter(Work, ftSquare), 3, textType.ttASCIIUpper, 9)

' remove punctuation
text = Hor.Filter(text, ftSquare)

If Len(text) Mod 2 <> 0 Then
    text = text & "X"
End If

If Period = -1 Then Period = Len(text) \ 2
If Period < 1 Then ' period should be >= 2
    encDIGRAFID = "Error: Period must be > 0 [" & Period & "]"
    Exit Function
End If

' Convert text to characters (digits)
For I = 1 To Len(text) Step 2
    Ch = Mid(text, I, 1)
    Call Hor.RC(Ch, R1, C1)
    Ch = Mid(text, I + 1, 1)
    Call Vert.RC(Ch, R2, C2)
    RC = (R1 - 1) * 3 + C2 ' Calculate middle digit (1-9)
    Block = Block & C1 & RC & R2
Next I

' Deseriate by period over block height (3)
Block = Seriate_(Block, Period, st_Deseriate, ttNumber, "-", 3)

Work = ""
' Now convert 3 digit values back to text
For I = 1 To Len(Block) Step 3
    C1 = Mid(Block, I, 1)
    R1 = Mid(Block, I + 1, 1)
    
    C2 = (R1 - 1) Mod 3 + 1 ' Calculate r/c from middle digit
    R1 = (R1 - 1) \ 3 + 1
    
    R2 = Mid(Block, I + 2, 1)
    Work = Work & Hor.chrVal(R1, C1) & Vert.chrVal(R2, C2)
Next I

Work = AddSep(Work, Sep, Period * 2)

encDIGRAFID = Work
Exit Function

Catch:
encDIGRAFID = Err.Description
End Function

' This function is the Trisquare encryption method
Public Function decDIGRAFID(text As String, HS As Variant, VS As Variant, Period As Integer) As String
Dim I As Integer
Dim R1 As Integer, C1 As Integer
Dim R2 As Integer, C2 As Integer
Dim RC As Integer
Dim Ln As Integer
Dim Vert As New Polybius
Dim Hor As New Polybius
Dim Work As String
Dim Ch As String
Dim Block As String
On Error GoTo Catch

' Initialize squares
Call Hor.Make(HS, 9, textType.ttASCIIUpper, 3)
Work = range2str(VS)
' second square must contain same characters
Call Vert.Make(Hor.Filter(Work, ftSquare), 3, textType.ttASCIIUpper, 9)

' remove punctuation
text = Hor.Filter(text, ftSquare)
Ln = Len(text)

If Ln Mod 2 <> 0 Then
    decDIGRAFID = "Error: Input text is not length multiple of 2: " & Len(text)
    Exit Function
    'text = text & "X"
End If

If Period = -1 Then Period = Ln \ 2
If Period < 1 Then ' period should be >= 2
    decDIGRAFID = "Error: Period must be > 0 [" & Period & "]"
    Exit Function
End If

' Convert text to characters (digits)
For I = 1 To Ln Step 2
    Ch = Mid(text, I, 1)
    Call Hor.RC(Ch, R1, C1)
    Ch = Mid(text, I + 1, 1)
    Call Vert.RC(Ch, R2, C2)
    RC = (R1 - 1) * 3 + C2 ' Calculate middle digit (1-9)
    Block = Block & C1 & RC & R2
Next I

' Seriate by period over block height (3)
Block = Seriate_(Block, Period, st_Seriate, ttNumber, "-", 3)

Work = ""
' Now convert 3 digit values back to text
For I = 1 To Len(Block) Step 3
    C1 = Mid(Block, I, 1)
    R1 = Mid(Block, I + 1, 1)
    
    C2 = (R1 - 1) Mod 3 + 1 ' Calculate r/c from middle digit
    R1 = (R1 - 1) \ 3 + 1
    
    R2 = Mid(Block, I + 2, 1)
    Work = Work & Hor.chrVal(R1, C1) & Vert.chrVal(R2, C2)
Next I

decDIGRAFID = Work
Exit Function

Catch:
decDIGRAFID = Err.Description
End Function

' This function is the Seriated Playfair encryption method
Public Function EncSeriated(text As String, Rng As Variant, Period As Integer, _
    Optional Pad As String = "X", Optional Repl As String = "", Optional Sep As String = "") As String
Dim I As Integer
Dim J As Integer
Dim K As Integer
Dim P As Integer
Dim Ln As Integer
Dim BlkLen As Integer
Dim Fin As Integer
Dim Ch As String
Dim Work As String
Dim Tail As String
Dim Out As String
Dim AltPad As String
Dim pSquare As New Polybius
On Error GoTo Catch

If Period < 1 Then ' Can't deal
    EncSeriated = "Invalid period: " & Period & ", must be > 0"
    Exit Function
End If

Call pSquare.Make(Rng, -1)

Pad = Left(ToAlpha(Pad), 1) ' Only pad, never replace (-X, ".", "+", etc.)
If Pad = "" Then Pad = "X"
If Pad = "X" Then
    AltPad = "Z"
Else
    AltPad = "X"
End If

text = Compact(text, "", Repl, pSquare.PolyVals) ' Uppercase, Handle j->i, etc.

BlkLen = Period * 2

Ln = Len(text)

P = 1
Do While P <= Ln
    Fin = P ' Base
    I = Application.Min(Ln - P + 1, BlkLen) ' # Characters
    K = (I + 1) \ 2 ' # characters / block (partial)
    Do
        Tail = ""
        P = Fin + K ' Tail
        For J = Fin To Fin + K - 1 ' Loop over K characters
            Tail = Tail & Mid(text, J, 1)
            Ch = Mid(text, P, 1)
            If Ch = "" Then Ch = Pad
            If Mid(text, J, 1) <> Ch Then ' Use both
                Tail = Tail & Ch
                P = P + 1
            Else
                If Ch = Pad Then
                    Tail = Tail & AltPad
                Else
                    Tail = Tail & Pad
                End If
            End If
        Next J
        If K = Period Then ' Complete block
            Exit Do
        End If
        If P <= Ln Then ' Didn't get to end of text, but not complete block
            K = K + 1 ' Make block wider and try again
        End If
    Loop Until P > Ln
    Work = Work & Tail
Loop

Work = EncPlayfair_(Work, pSquare, Pad)
Ln = Len(Work)

For I = 1 To Ln Step BlkLen
    Tail = ""
    For J = I To Application.Min(I + BlkLen - 1, Ln) Step 2
        Out = Out & Mid(Work, J, 1)
        Tail = Tail & Mid(Work, J + 1, 1)
    Next J
    Out = Out & Tail
Next I

Out = AddSep(Out, Sep)

EncSeriated = Out
Exit Function
Catch:
EncSeriated = Err.Description
End Function

' This function is the Seriated Playfair encryption method
Public Function DecSeriated(text As String, Rng As Variant, Period As Integer, _
    Optional Sep As String = "") As String
Dim I As Integer
Dim J As Integer
Dim K As Integer
Dim P As Integer
Dim Ln As Integer
Dim BlkLen As Integer
Dim Work As String
Dim Tail As String
Dim Out As String
Dim pSquare As New Polybius
On Error GoTo Catch

If Period < 1 Then ' Can't deal
    DecSeriated = "Invalid period: " & Period & ", must be > 0"
    Exit Function
End If

Call pSquare.Make(Rng, -1) ' Dynamic square

text = pSquare.Filter(text, ftSquare)

BlkLen = Period * 2

Ln = Len(text)

If Ln Mod 2 = 1 Then
    DecSeriated = "Error: Cipher text is odd length (" & Ln & ")"
    Exit Function
End If

For I = 1 To Ln Step BlkLen
    K = Application.Min(I + BlkLen - 1, Ln)
    P = (K - I + 1) \ 2
    'Tail = ""
    For J = I To I + P - 1
        Work = Work & Mid(text, J, 1) & Mid(text, J + P, 1)
    Next J
Next I

Work = DecPlayfair_(Work, pSquare)

Ln = Len(Work)

For I = 1 To Ln Step BlkLen
    Tail = ""
    For J = I To Application.Min(I + BlkLen - 1, Ln) Step 2
        Out = Out & Mid(Work, J, 1)
        Tail = Tail & Mid(Work, J + 1, 1)
    Next J
    Out = Out & Tail
Next I

DecSeriated = Out
Exit Function
Catch:
DecSeriated = Err.Description
End Function

Public Function encDiSub(text As String, rowKey As String, colKey As String, Optional tType As textType = textType.ttAlphaUpper, _
    Optional Sep As String = "", Optional Pad As String = "X", Optional RCO As String) As String
Dim I As Integer
Dim R As Integer, C As Integer
Dim ColFirst As Integer
Dim SwapRC As Integer
Dim Alpha As String
Dim Wrk As String
Dim Dup As New AlphaList
On Error GoTo Catch

If Pad = "" Then Pad = "+"
' filter, pad to even boundary
text = toText(text, tType, , Pad)

If text = "" Then
    encDiSub = ""
    Exit Function
End If

' Default alphabet
Alpha = Dup.Dedup("", "*", tType)

' Key alphabets - probably not necessary to fill out as these are likely already done
rowKey = Dup.Undup(rowKey, "*")
colKey = Dup.Undup(colKey, "*")

' rowcol
Select Case UCase(RCO)
    Case "", "RC"
        ColFirst = 0
        SwapRC = 0
    Case "RCCR"
        ColFirst = 0
        SwapRC = 1
    Case "CRRC"
        ColFirst = 1
        SwapRC = 1
    Case "CR"
        ColFirst = 1
        SwapRC = 0
    Case Else
        Err.Raise 302, "encDiSub", "Error: Unknown RowCol Order: " & RCO & ", expecting RC, RCCR, CRRC or CR"
End Select

For I = 1 To Len(text) Step 2
    If ColFirst = 1 Then ' Read across Column, then down row
        C = InStr(1, Alpha, Mid(text, I, 1), vbTextCompare)
        R = InStr(1, Alpha, Mid(text, I + 1, 1), vbTextCompare)
    Else ' read down row, then across column
        R = InStr(1, Alpha, Mid(text, I, 1), vbTextCompare)
        C = InStr(1, Alpha, Mid(text, I + 1, 1), vbTextCompare)
    End If
    If R = 0 Or C = 0 Then
        Err.Raise 1101, "Digraphic:encDiSub", "Keys do not contain all characters allowed by filter"
    End If
    Wrk = Wrk & Mid(rowKey, R, 1)
    Wrk = Wrk & Mid(colKey, C, 1)
    If SwapRC = 1 Then
        ColFirst = 1 - ColFirst
    End If
Next I

' Optional group by
Wrk = AddSep(Wrk, Sep)

encDiSub = Wrk

Exit Function
Catch:
encDiSub = Err.Description
End Function

Public Function decDiSub(text As String, rowKey As String, colKey As String, Optional tType As textType = textType.ttAlphaUpper, _
    Optional Sep As String = "", Optional RCO As String) As String
Dim I As Integer
Dim R As Integer, C As Integer
Dim ColFirst As Integer
Dim SwapRC As Integer
Dim Alpha As String
Dim Wrk As String
Dim Dup As New AlphaList
On Error GoTo Catch

' Remove separator, if present
text = UngroupBy(text, Sep)

If text = "" Then
    decDiSub = ""
    Exit Function
End If

' Default alphabet
Alpha = Dup.Dedup("", "*", tType)

' Key alphabets - probably not necessary to fill out as these are likely already done
rowKey = Dup.Undup(rowKey, "*")
colKey = Dup.Undup(colKey, "*")

' rowcol
Select Case UCase(RCO)
    Case "", "RC"
        ColFirst = 0
        SwapRC = 0
    Case "RCCR"
        ColFirst = 0
        SwapRC = 1
    Case "CRRC"
        ColFirst = 1
        SwapRC = 1
    Case "CR"
        ColFirst = 1
        SwapRC = 0
    Case Else
        Err.Raise 302, "decDiSub", "Error: Unknown RowCol Order: " & RCO & ", expecting RC, RCCR, CRRC or CR"
End Select

For I = 1 To Len(text) Step 2
    R = InStr(1, rowKey, Mid(text, I, 1), vbTextCompare)
    C = InStr(1, colKey, Mid(text, I + 1, 1), vbTextCompare)

    If R = 0 Or C = 0 Then
        Err.Raise 1101, "Digraphic:decDiSub", "Keys do not contain all characters allowed by filter"
    End If
    
    If ColFirst = 1 Then ' Read across Column, then down row
        Wrk = Wrk & Mid(Alpha, C, 1)
        Wrk = Wrk & Mid(Alpha, R, 1)
    Else ' read down row, then across column
        Wrk = Wrk & Mid(Alpha, R, 1)
        Wrk = Wrk & Mid(Alpha, C, 1)
    End If
    If SwapRC = 1 Then
        ColFirst = 1 - ColFirst
    End If
Next I

decDiSub = Wrk

Exit Function
Catch:
decDiSub = Err.Description
End Function
