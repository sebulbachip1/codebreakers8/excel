Attribute VB_Name = "Trans"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Trans
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime"
'
' USAGE:
'       Transposition cipher routines
'
' GROUPS:
'       Block/route Transposition
'            Transmute
'       Cadenas
'            encCadenas, decCadenas
'       Columnar Transposition (Complete or Incomplete)
'       (AMSCO)
'       (Myszkowski)
'            encCCol, decCCol
'       Grille (rotating)
'            encGrille,decGrille,genGrilleKey / grilleQuarter
'       RailFence (& redfence)
'            encRailFence, decRailFence
'       Prime
'            encPrimeStr, decPrimeStr
'            encRepPrimeStr, decRepPrimeStr (repeating)
'            getPrime
'       Scytale
'            encScytale, decScytale
'       Seriation
'            Seriate, DeSeriate
'       Shuffle
'            encShuffle, decShuffle
'       Swagman
'            encSwagman, decSwagman, genSwagmanKey
'       Tree
'            OrderedTraversal
'            InorderTraversal (deprecated)
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 06/04/2019 Chip Bloch    -Rail Fence
' 06/09/2019 Chip Bloch    -Migrated encCCol/decCCol from Cipher
' 06/11/2019 Chip Bloch    -Added Prime routines
' 06/13/2019 Chip Bloch    -Added Repeating Prime routines, red fence
' 10/06/2019 Chip Bloch    -Added InorderTraversal
' 06/28/2020 Chip Bloch    -Moved pseudo-object key routines to OrderedKey
' 07/06/2020 Chip Bloch    -Inverted key order for encCCol/decCCol
' 07/13/2020 Chip Bloch    -Added encScytale, decScytale
' 07/14/2020 Chip Bloch    -Added enc5col, encNicodemus
' 07/15/2020 Chip Bloch    -Added dec5col, decNicodemus
'                          -improved decCCol (speed, simplicity)
' 07/16/2020 Chip Bloch    -Updated parm for Vigenaire function
' 07/26/2020 Chip Bloch    -Using Application.Max, Application.Min
'                          -Added Transmute
'                          -Migrated Nicodemus to Combo
'                          -Added encSTrans, decSTrans
' 07/28/2020 Chip Bloch    -Change Pad to Sep on Enc5Col
' 07/31/2020 Chip Bloch    -Convert Prime Types to Enum
'                          -Added Sep to encCCol
'                          -Not folding to uppercase in encCCol, decCCol, enc5Col, dec5Col
' 08/12/2020 Chip Bloch    -Renamed Transmute to Transposed, Added new Transmute, fixed bug in decSTrans
' 09/09/2020 Chip Bloch    -Added Diagonal, AltDiag to Transmute()
' 09/10/2020 Chip Bloch    -Testing alternate code for Spiral in Transmute()
' 09/11/2020 Chip Bloch    -encCCol now performs encSTrans, enc5Col
' 09/12/2020 Chip Bloch    -enc/decScytale now calling Transmute()
' 09/13/2020 Chip Bloch    -AMSCO almost finished (encCCol,decCCol)
' 09/18/2020 Chip Bloch    -Corrected bug in AltCol/AltRow
' 10/04/2020 Chip Bloch    -Added Public to encRepPrimeStr, decRepPrimeStr
' 10/13/2020 Chip Bloch    -Added Offset to encRailFence, decRailFence
' 11/21/2020 Chip Bloch    -Updated handling of Offset for Rail Fence
' 11/21/2020 Chip Bloch    -Added Shuffle Cipher encShuffle, decShuffle
' 12/17/2020 Chip Bloch    -Added nested transmute (fill, extract)
' 12/26/2020 Chip Bloch    -Added OrderedTraversal
' 01/06/2021 Chip Bloch    -Added Swagman, Swagman key
' 01/23/2021 Chip Bloch    -Added encGrille,decGrille,genGrilleKey
' 01/24/2021 Chip Bloch    -Added grilleQuarter (undocumented, for decoder ring example)
' 01/29/2021 Chip Bloch    -Added textType to encShuffle (filter)
' 02/07/2021 Chip Bloch    -Added encCadenas, decCadenas
' 02/09/2021 Chip Bloch    -Referencing ToText, UnGroupBy, AddSep
' 02/23/2021 Chip Bloch    -Added Myszkowski (encCCol, decCCol)
' 03/23/2021 Chip Bloch    -Trapped zero length string in encCCol, decCCol
' 04/13/2021 Chip Bloch    -Added Seriate,DeSeriate
' 04/17/2021 Chip Bloch    -Added Enum for Seriate_
' 04/19/2021 Chip Bloch    -Changed parms on Seriate_ to ByVal
' 04/28/2021 Chip Bloch    -Updated Transmute to make SwpCh "sticky" - stays with following character
'-------------------------------------------------------------------------------------------

' Enum for Transmute

Public Enum tt_TransType
    tt_Default = 0   ' Will be 2 (Fill Transtype) or 1 (Extract transtype)
    tt_None = 1      ' By Rows (Left->Right, Top->Bottom)
    tt_Trans = 2     ' Transpose = By Columns (Up->Down, Left->Right)
    tt_AltTrans = 3  ' (Rot Left + Mirror)
    tt_Mirror = 4    ' Mirror = By Rows (Right->Left, Up->Down)
    tt_Flip = 5      ' Flip = By rows (Left->Right, Down->Up)
    tt_RotRight = 6  ' Rot Right By rows (Top->Bottom, Left->Right)
    tt_Rot2 = 7      ' 180 (Right->Left, Bottom-> Top)
    tt_RotLeft = 8   ' RotLeft = By Columns (Down->Up, Left->Right)
    tt_AltRow = 9    ' By Alternating rows (left->right, right->left)
    tt_AltCol = 10   ' By Alternating Columns (Up->Down, Down->Up)
    tt_Spiral = 11   ' Clockwise Spiral
    tt_AntiSp = 12   ' CounterClockwise Spiral
    tt_Diagonal = 13 ' Diagonal
    tt_AltDiag = 14  ' Alternating Diagonal
End Enum

' Enum for Seriate_
Public Enum stSeriate
    st_Seriate = 0
    st_Deseriate = 1
End Enum

' Enum for traverse order
Private Enum to_Traverse_
    to_Default = 0  ' 3 = inorder (LNR)
    to_Pre = 1      ' NLR
    to_AltPre = 2   ' NRL
    to_In = 3       ' LNR
    to_Post = 4     ' LRN
    to_AltIn = 5    ' RNL
    to_AltPost = 6  ' RLN
End Enum

' Cadenas Alphabet
Private Const CadenasAlpha_ As String = "BCDEFGHIJKLMNOPQRSTUVXYZA"

' Maximum number of tries to generate a Swagman key
Private Const MAXSWAGKEY_ As Integer = 1000

' Constants for prime functions
Public Enum ci_PrimeType
    ci_AntiNonAnti = -3  ' Anti-prime + Non Anti (right to left)
    ci_NonAnti = -2      ' Non-Anti (right to left)
    ci_Anti = -1         ' Anti-prime (right to left)
    ci_Prime = 0         ' Primes (left to right)
    ci_NonPrime = 1      ' Non-primes (left to right)
    ci_PrimeNonPrime = 2 ' Primes + Non Primes (Left to right)
    ci_nonPrimeAnti = 3  ' Not Prime OR Anti-Prime (Left to right)
    ci_All = 4           ' Primes (Left to right) Anti (right to left) Non (left to right)
End Enum

Dim Primes() As Integer            ' Array or primes up to 1,000
Dim b_primeInit As Boolean         ' True after primes loaded into array

Private Type TextInfo              ' Pseudo-object for handling prime text during encryption
    text As String      ' text
    Ln As Integer       ' len
    Idx As Integer      ' index
    Pad As String       ' pad text
    Pl As Integer       ' pad len
    K As Integer        ' pad index
End Type

' Prime Encryption/decryption methods

' Initialize Primes, array values 2..997, index 1..168
Private Function InitPrimes() As Boolean
Dim I As Integer
Dim Tmp() As Variant
If Not b_primeInit Then ' Do this only once
    Tmp = Array(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, _
    71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, _
    163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, _
    257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, _
    359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, _
    461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, _
    577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, _
    677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, _
    809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, _
    919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997, 1009) ' one extra "buffer" prime
    ' dimension array (shift up by 1)
    ReDim Primes(1 To UBound(Tmp) + 1)
    For I = LBound(Tmp) To UBound(Tmp)
        Primes(I + 1) = Tmp(I)
    Next I
    b_primeInit = True
End If
InitPrimes = True
End Function

' Recursive binary tree traversal function
' Six cases: With a tree of A(1)B(2)C(3)
'    to_Pre = ABC     (NLR)
'    to_AltPre = ACB  (NRL)
'    to_In = BAC      (LNR) (default)
'    to_Post = BCA    (LRN)
'    to_AltIn = CAB   (RNL)
'    to_AltPost = CBA (RLN)
Private Sub Traverse_(ByRef Nodes As Scripting.Dictionary, ByVal Node As Integer, ByVal Max As Integer, ByVal Order As to_Traverse_)
Dim Left As Integer
Dim Right As Integer
If Node > Max Then Exit Sub
Left = Node * 2
Right = Left + 1

' Pre-node processing (Children)
Select Case Abs(Order)
    Case to_AltPost ' RLN
        Call Traverse_(Nodes, Right, Max, Order)
        Call Traverse_(Nodes, Left, Max, Order)
    Case to_Post    ' LRN
        Call Traverse_(Nodes, Left, Max, Order)
        Call Traverse_(Nodes, Right, Max, Order)
    Case to_Pre, to_AltPre  ' NLR, NRL
    Case to_AltIn   ' RNL
        Call Traverse_(Nodes, Right, Max, Order)
    Case Else ' to_In, to_Default LNR
        Call Traverse_(Nodes, Left, Max, Order)
End Select

' Process this node
If Order < 0 Then ' Inverse: Case to_invPre, to_invPost, to_invIn
    Nodes.Add Node, Nodes.Count + 1
Else
    Nodes.Add Nodes.Count + 1, Node
End If

' Post-node processing (Children)
Select Case Abs(Order)
    Case to_Post, to_AltPost ' LRN, RLN
    Case to_AltPre  ' NRL
        Call Traverse_(Nodes, Right, Max, Order)
        Call Traverse_(Nodes, Left, Max, Order)
    Case to_Pre ' NLR
        Call Traverse_(Nodes, Left, Max, Order)
        Call Traverse_(Nodes, Right, Max, Order)
    Case to_AltIn   ' RNL
        Call Traverse_(Nodes, Left, Max, Order)
    Case Else ' to_In, to_Default LNR
        Call Traverse_(Nodes, Right, Max, Order)
End Select
End Sub

' Binary Tree Traversal
Public Function OrderedTraversal(S As String, Optional Order As Integer) As String
Dim Ln As Integer, I As Integer
Dim Nodes As New Scripting.Dictionary
Dim Wrk As String
On Error GoTo Catch
Nodes.CompareMode = BinaryCompare

Ln = Len(S)
If Ln >= 1 Then
    Call Traverse_(Nodes, 1, Ln, Order)
End If

For I = 1 To Nodes.Count
    Wrk = Wrk & Mid(S, Nodes(I), 1)
Next I

OrderedTraversal = Wrk
Exit Function

Catch:
OrderedTraversal = Err.Description
End Function

' Deprecated
Public Function InorderTraversal(S As String) As String
InorderTraversal = OrderedTraversal(S, to_In)
End Function


' Extract Prime Numbered characters
' Act = action.  0 = Primes.  -1 = Anti-prime (backwards).  1 = Non Primes, 3= not prime or anti-prime
' 4 = prime + anti-prime + Non-prime or anti-prime

Public Function decPrimeStr(text As String, Optional Act As ci_PrimeType = ci_Prime) As String
Dim I As Integer
Dim P As Integer
Dim Ln As Integer
Dim Wrk As String
Dim AllPrimes() As Integer

' Handle values that return MULTIPLE parts of the string at once (recursive)
Select Case Act
    Case ci_All ' All three parts
        decPrimeStr = decPrimeStr(text, ci_Prime) & decPrimeStr(text, ci_Anti) & decPrimeStr(text, ci_nonPrimeAnti)
        Exit Function
    Case ci_AntiNonAnti ' Only anti-prime parts
        decPrimeStr = decPrimeStr(text, ci_Anti) & decPrimeStr(text, ci_NonAnti)
        Exit Function
    Case ci_PrimeNonPrime ' Only prime parts
        decPrimeStr = decPrimeStr(text, ci_Prime) & decPrimeStr(text, ci_NonPrime)
        Exit Function
End Select

' Make sure primes are initialized
InitPrimes

' Check source string length (max 1000)
Ln = Len(text)
If Ln > 1000 Then Ln = 1000

' If extracting non-primes (excluding both prime and anti)
If Act = ci_nonPrimeAnti Then
    ReDim AllPrimes(Ln) ' fill array from both ends
    P = 1
    While Primes(P) <= Ln
        I = Primes(P)
        AllPrimes(I) = I
        I = Ln - I + 1
        AllPrimes(I) = I
        P = P + 1
    Wend
End If

' Now extract the information
I = 1
P = 1
While I <= Ln
    Select Case Act
        Case ci_NonAnti ' Non-Anti (text excluding anti, also reversed)
            If I < Primes(P) Then
                Wrk = Wrk & Mid(text, Ln - I + 1, 1)
            Else
                P = P + 1
            End If
        Case ci_Anti ' Backwards (Anti-primes)
            If Primes(I) <= Ln Then
                Wrk = Wrk & Mid(text, Ln - Primes(I) + 1, 1)
            Else
                I = Ln
            End If
        Case ci_Prime ' Forwards (Primes)
            If Primes(I) <= Ln Then
                Wrk = Wrk & Mid(text, Primes(I), 1)
            Else
                I = Ln
            End If
        Case ci_NonPrime ' Non-prime (text excluding prime)
            If I < Primes(P) Then
                Wrk = Wrk & Mid(text, I, 1)
            Else
                P = P + 1
            End If
        Case ci_nonPrimeAnti ' Not prime OR anti-prime (all other)
            If I <> AllPrimes(I) Then ' It's not a prime or an anti-prime
                Wrk = Wrk & Mid(text, I, 1)
            End If
        Case Else ' Error
            Wrk = "Invalid action code: " & Act & ". Should be from -1 to 3"
            I = Ln
    End Select
    I = I + 1
Wend
decPrimeStr = Wrk
End Function

' Init pseudo-object (tracks text and padding strings during encryption)
Private Sub initText(ByRef text As TextInfo, Val As String, Pad As String)
text.text = Val
text.Ln = Len(Val)
text.Idx = 1
text.K = 1
text.Pad = Pad
text.Pl = Len(Pad)
End Sub

' pseudo-object function to return next character in string, with padding
' padding can be a single character or a string (one character at a time)
Private Function getCh(ByRef text As TextInfo)
Dim Ch As String
Ch = Mid(text.text, text.Idx, 1)
text.Idx = text.Idx + 1
If Ch = "" Then
    Ch = Mid(text.Pad, text.K, 1)
    If text.K < text.Pl Then
        text.K = text.K + 1
    Else
        text.K = 1
    End If
End If
getCh = Ch
End Function

' pseudo-object function returns true if characters left in string, exclusive of extra padding
Private Function hasChars(ByRef text As TextInfo) As Boolean
hasChars = text.Idx <= text.Ln
End Function

' Push Prime Numbered characters
' Act = action.  0 = Primes.  -1 = Anti-prime (backwards).  1 = Non Primes, 2= not prime or anti-prime
' 3 = prime + anti-prime + Non-prime or anti-prime

Public Function encPrimeStr(strText As String, Optional strPrime As String = "", Optional strAnti As String = "", _
    Optional Act As ci_PrimeType = ci_Prime, Optional Pad As String = "") As String
Dim Prime As TextInfo
Dim Anti As TextInfo
Dim Other As TextInfo
Dim I As Integer
Dim P As Integer
Dim Lp As Integer ' Prime length
Dim Ln As Integer ' Text Length
Dim AllPrimes() As Integer
'Dim Found As Boolean
Dim Wrk As String
'On Error GoTo itErr
If Pad = "" Then Pad = "X"
InitPrimes

Ln = Len(strText)
If Ln > 1000 Then
    Ln = 1000
    strText = Left(strText, Ln)
End If
If Len(strPrime) >= UBound(Primes) Then
    strPrime = Left(strPrime, UBound(Primes) - 1)
End If
If Len(strAnti) >= UBound(Primes) Then
    strAnti = Left(strAnti, UBound(Primes) - 1)
End If
Lp = 0
P = 1
While Primes(P) <= Ln ' Calculate MAX number of primes for this source length
    Lp = P ' Count of primes
    P = P + 1
Wend
' If default value (ci_prime) and source strings don't match, change default
If Act = ci_Prime Then
    If strAnti <> "" And strPrime = "" Then ' Asked for prime but passed in anti-prime
        Act = ci_Anti
    ElseIf strAnti <> "" And strPrime <> "" Then ' Asked for prime, but passed in all three
        Act = ci_All
    End If
End If
' now handle encryption based on values
Select Case Act
    Case ci_Prime, ci_NonPrime, ci_PrimeNonPrime
        If strPrime = "" Then ' Autofill from text
            Call initText(Prime, Left(strText, Lp), Pad)
            Call initText(Other, Mid(strText, Lp + 1, Ln), Pad)
        Else
            Call initText(Prime, strPrime, Pad)
            Call initText(Other, strText, Pad)
        End If
        Wrk = ""
        I = 1
        While hasChars(Other) Or hasChars(Prime)
            If I = Primes(Prime.Idx) Then
                Wrk = Wrk & getCh(Prime)
            Else
                Wrk = Wrk & getCh(Other)
            End If
            I = I + 1
        Wend
    Case ci_Anti, ci_NonAnti, ci_AntiNonAnti
        If strAnti = "" And strPrime <> "" Then ' Asked for anti but passed in prime
            strAnti = strPrime
        End If
        If strAnti = "" Then ' Autofill from text
            Call initText(Anti, Left(strText, Lp), Pad)
            Call initText(Other, Mid(strText, Lp + 1, Ln), Pad)
        Else
            Call initText(Anti, strAnti, Pad)
            Call initText(Other, strText, Pad)
        End If
        Wrk = ""
        I = 1
        While hasChars(Other) Or hasChars(Anti) ' Build string (right to left)
            If I = Primes(Anti.Idx) Then
                Wrk = getCh(Anti) & Wrk
            Else
                Wrk = getCh(Other) & Wrk
            End If
            I = I + 1
        Wend
    Case ci_nonPrimeAnti, ci_All
        If strPrime = "" And strAnti = "" Then ' Autofill from text
            Call initText(Prime, Left(strText, Lp), Pad)
            Call initText(Anti, Mid(strText, Lp + 1, Lp), Pad)
            Call initText(Other, Mid(strText, Lp * 2 + 1, Ln), Pad)
        Else ' Partial autofill unsupported
            Call initText(Prime, strPrime, Pad)
            Call initText(Anti, strAnti, Pad)
            Call initText(Other, strText, Pad)
            ' compute total lenth of primes + text
            Lp = Application.Max(Len(strAnti), Len(strPrime), Lp)
            Ln = Application.Max(Ln, Primes(Lp) + 2 * Lp)
        End If
        ' compute next likely intersection where primes wont collide
        ' Start with length of text vs. length of primes
        GoTo Continue
Extend:
        ' Primes collided, try next length
        Ln = Ln + 1
        If Ln > 1000 Then
            Wrk = "String too long; no valid solutions without prime/anti-prime collisions"
            Exit Function
        End If
Continue:
        ' compute all primes (backwards and forwards)
        ReDim AllPrimes(Ln)
        P = 1
        While Primes(P) <= Ln
            Lp = P
            I = Primes(P)
            If AllPrimes(I) <> 0 Then ' collision
                GoTo Extend
            End If
            AllPrimes(I) = I
            I = Ln - I + 1  ' Anti-prime
            If AllPrimes(I) <> 0 Then
                GoTo Extend
            End If
            AllPrimes(I) = -I
            P = P + 1
        Wend
        If Other.Ln + 2 * Lp > Ln Then
            GoTo Extend
        End If
        ' Found an intersection that works
        ' Reverse anti-primes
        Wrk = ""
        For I = 1 To Ln
            If AllPrimes(I) < 0 Then
                Wrk = getCh(Anti) & Wrk ' adding to left, not right
            End If
        Next I
        Call initText(Anti, Wrk, Pad) ' re-save anti primes
        ' now append text. AllPrimes has character code for position ( < 0 = anti, > 0 = prime, 0 = non )
        Wrk = ""
        For I = 1 To Ln
            If AllPrimes(I) > 0 Then
                Wrk = Wrk & getCh(Prime)
            ElseIf AllPrimes(I) < 0 Then
                Wrk = Wrk & getCh(Anti)
            Else
                Wrk = Wrk & getCh(Other)
            End If
        Next I
    Case Else
        encPrimeStr = "Invalid action code:" & Act & ", expected -3 to 4"
        Exit Function
End Select
encPrimeStr = Wrk
'Exit Function
'itErr:
'encPrimeStr = "encPrimeStr: " & Err.Number & " " & Err.Description
End Function

' This function decodes repeated (nested) prime strings

Public Function decRepPrimeStr(strText As String, Optional Act As ci_PrimeType = ci_Prime) As String
Dim Lp As Integer
Dim Ln As Integer
Dim P As Integer
Dim Wrk As String

InitPrimes

Select Case Act
    Case ci_Prime, ci_NonPrime
    Case ci_Anti, ci_NonAnti
    Case Else
        decRepPrimeStr = "Invalid Action Code: " & Act & ". must be -2 to 1"
        Exit Function
End Select

Ln = Len(strText)
If Ln > 1000 Then
    Ln = 1000
    strText = Left(strText, Ln)
End If

If Ln = 1 Then
    Wrk = ""
Else
    If Act < ci_Prime Then
        Wrk = decPrimeStr(strText, ci_Anti) & decRepPrimeStr(decPrimeStr(strText, ci_NonAnti), 0)
    Else
        Wrk = decPrimeStr(strText, ci_Prime) & decRepPrimeStr(decPrimeStr(strText, ci_NonPrime), 0)
    End If
End If

decRepPrimeStr = Wrk
End Function

' This sub-function encodes repeated (nested) prime strings <recursive>.
' calling function has all error checking, adds padding and reverses the string if action is anti-prime

Private Function subencRepPrimeStr(strText As String, Optional Act As ci_PrimeType = ci_Prime, Optional Pad As String = "") As String
Dim Lp As Integer
Dim Ln As Integer
Dim P As Integer
Dim Wrk As String
Dim strPrime As String

Ln = Len(strText)

Lp = 0
P = 1
While Primes(P) <= Ln ' Calculate MAX number of primes for this source length
    Lp = P ' Count of primes
    P = P + 1
Wend

If Lp = 0 Then
    Wrk = strText
Else
    strPrime = Mid(strText, 2, Lp)
    strText = Left(strText, 1) & Mid(strText, Lp + 2, Ln)
    Wrk = encPrimeStr(subencRepPrimeStr(strText, ci_Prime, Pad), strPrime, "", ci_Prime, Pad)
End If
subencRepPrimeStr = Wrk
End Function

' This function encodes repeated (nested) prime strings

Public Function encRepPrimeStr(strText As String, Optional Act As ci_PrimeType = ci_Prime, Optional Pad As String = "") As String
Dim Ln As Integer
Dim Wrk As String

Pad = Left(Pad & "X", 1) ' default pad character, also sets length 1

InitPrimes

Select Case Act
    Case ci_Prime, ci_NonPrime
    Case ci_Anti, ci_NonAnti
    Case Else
        encRepPrimeStr = "Invalid Action Code: " & Act & ". must be -2 to 1"
        Exit Function
End Select

Ln = Len(strText)
If Ln > 999 Then ' have to leave room for pad character
    Ln = 999
    strText = Left(strText, Ln)
End If

' always encode forward
Wrk = subencRepPrimeStr(Pad & strText, ci_Prime, Pad)
If Act < ci_Prime Then ' need to reverse for anti
    Wrk = Reverse(Wrk)
End If
encRepPrimeStr = Wrk
End Function

' unused function?
' Return Nth prime (2 = 1st, etc.)
Public Function getPrime(Num As Integer) As Integer
'primeInit = False
InitPrimes
If Num > UBound(Primes) Then
    Num = UBound(Primes)
ElseIf Num < 1 Then
    Num = 1
End If
getPrime = Primes(Num)
End Function


' This function is a complete or incomplete columnar decryption
' Flag for incomplete columnar is that the string is not a multiple of the key length
Public Function decCCol(S As String, Key As String, Optional rCnt As Integer = 0, Optional Pattern As Integer = 0)
Dim Kinfo As New OrderedKey
Dim H As Integer
Dim R As Integer
Dim C As Integer
Dim P As Integer
Dim I As Integer
Dim Ln As Integer

Dim Cv As Integer
Dim Ch As String

Dim rStep As Integer
Dim MinR As Integer
Dim MaxR As Integer

Dim cPat As Integer
Dim Pivot As Integer
Dim rPat As Integer

Dim Myszkowski As Integer

Dim xCols() As Integer
Dim Chars() As String

Dim Wrk As String

On Error GoTo Catch

Kinfo.InitKey = Key ' Will throw error for empty key

' Remove spaces, punctuation from S
'S = ToAlpha(S)
Ln = Len(S)

If Ln = 0 Then
    decCCol = ""
    Exit Function
End If

' Get pattern
If Pattern = -1 Then
    Pattern = 1
    Myszkowski = 1
ElseIf Pattern = 0 Then
    Pattern = 1
ElseIf Pattern > 99 Or Pattern < 0 Then
    decCCol = "Error: invalid pattern, must be in range 1-99 -[" & Pattern & "]"
    Exit Function
End If
cPat = Left(Pattern, 1)
Pivot = cPat + Right(Pattern, 1)
rPat = Pivot - cPat

C = Kinfo.Ln

R = (rPat + cPat) * C ' total character count for two rows
H = (Ln + R - 1) \ R ' Two row blocks (+ partial)

ReDim Cnt(1 To H * 2, 1 To C) ' Maximum rows * cols

' get Pattern count of characters into array
P = Ln
H = 1
While P > 0
    For C = 1 To Kinfo.Ln
        If P >= cPat Then
            Cnt(H, C) = cPat
            P = P - cPat
            cPat = Pivot - cPat
        Else
            Cnt(H, C) = P
            P = 0
            C = Kinfo.Ln
        End If
    Next C
    If P > 0 Then
        cPat = rPat
        rPat = Pivot - cPat
        H = H + 1
    End If
Wend

ReDim Chars(1 To H, 1 To Kinfo.Ln) ' Rows x Cols
ReDim xCols(1 To Kinfo.Ln)

For I = 1 To Kinfo.Ln
    xCols(I) = Kinfo.OrgIdx(I)
Next I

' If Myskowski, change column counts
If Myszkowski <> 0 Then
    For C = Kinfo.Ln - 1 To 1 Step -1
        P = xCols(C)
        I = xCols(C + 1)
        ' if sorted key letters are identical
        If Mid(Key, P, 1) = Mid(Key, I, 1) Then
            xCols(C + 1) = 0 ' flag this column as "empty"
            For R = 1 To H ' for all rows, compact to the previous column
                Cnt(R, P) = Cnt(R, P) + Cnt(R, I)
            Next R
        End If
    Next C
End If

' Peel off characters in column order into array
If rCnt <= 0 Then ' Default to entire column
    rStep = H
Else
    rStep = rCnt ' Only peel off rCnt rows at a time
End If

P = 1

For MinR = 1 To H Step rStep
    MaxR = Application.Min(MinR + rStep - 1, H)
    For C = 1 To Kinfo.Ln
        I = xCols(C)
        If I <> 0 Then
            For R = MinR To MaxR
                Cv = Cnt(R, I)
                Ch = Mid(S, P, Cv)
                Chars(R, I) = Ch
                P = P + Cv
            Next R
        End If
    Next C
Next MinR

' If Myskowski, split columns
If Myszkowski <> 0 Then
    For C = 1 To Kinfo.Ln - 1
        P = Kinfo.OrgIdx(C)
        I = Kinfo.OrgIdx(C + 1)
        ' if sorted key letters are identical
        If Mid(Key, P, 1) = Mid(Key, I, 1) Then
            For R = 1 To H ' for all rows, compact to the previous column
                Ch = Chars(R, P)
                Chars(R, I) = Mid(Ch, 2, Cnt(R, I))
                Chars(R, P) = Left(Ch, 1)
            Next R
        End If
    Next C
End If

' Peel off characters from array into string

For R = 1 To H
    For C = 1 To Kinfo.Ln
        Wrk = Wrk & Chars(R, C)
    Next C
Next R

decCCol = Wrk
Exit Function

Catch:
decCCol = Err.Description
End Function

' This function is a complete columnar encryption
' For incomplete columnar encryption, set the pad character to ""
' For columnar encryption by rows, set rCnt to a number > 0 (rCnt defaults to the entire column)
'dec5Col = decCCol(S, Key, 5)
'enc5Col = encCCol(S, Key, "", Sep, 5)
'decSTrans = decCCol(S, Key, 1)
'encSTrans = encCCol(S, Key, "", , 1)

Public Function encCCol(S As String, Key As String, Optional Pad As String = "X", Optional Sep As String = "", _
    Optional rCnt As Integer = 0, Optional Pattern As Integer = 0)
Dim H As Integer
Dim R As Integer
Dim C As Integer
Dim P As Integer
Dim I As Integer
Dim Ln As Integer

Dim Cv As Integer
Dim Ch As String

Dim rStep As Integer
Dim MinR As Integer
Dim MaxR As Integer

Dim cPat As Integer
Dim Pivot As Integer
Dim rPat As Integer

Dim Myszkowski As Integer

Dim Kinfo As New OrderedKey

Dim xCols() As Integer
Dim Chars() As String ' (Rows x cols)

Dim Wrk As String

Dim ColSep As Boolean

On Error GoTo Catch

Kinfo.InitKey = Key ' Will throw error for empty key

' Remove spaces, punctuation from S
'S = ToAlpha(S)
Ln = Len(S)

If Ln = 0 Then
    encCCol = ""
    Exit Function
End If

' Get pattern
If Pattern = -1 Then
    Pattern = 1
    Myszkowski = 1
ElseIf Pattern = 0 Then
    Pattern = 1
ElseIf Pattern > 99 Or Pattern < 1 Then
    encCCol = "Error: invalid pattern, must be in range 1-99 -[" & Pattern & "]"
    Exit Function
End If
cPat = Left(Pattern, 1)
Pivot = cPat + Right(Pattern, 1)
rPat = Pivot - cPat

C = Kinfo.Ln

R = (rPat + cPat) * C ' total character count for two rows
H = (Ln + R - 1) \ R ' Two row blocks (+ partial)

ReDim Cnt(1 To H * 2, 1 To C) ' Maximum rows * cols

' get Pattern count of characters into array
P = Ln
H = 1
While P > 0 ' Characters left in source string
    For C = 1 To Kinfo.Ln ' for each column
        If P >= cPat Or Pad <> "" Then ' more characters in string than the pattern calls for OR padding needed
            Cnt(H, C) = cPat           ' Count all the characters for the pattern
            P = P - cPat               ' decrement count of characters
            cPat = Pivot - cPat        ' next pattern count
        Else
            Cnt(H, C) = P              ' No pad and ran out of characters, take all remaining characters
            P = 0                      ' Set count to zero (all used up)
            C = Kinfo.Ln               ' skip to end of columns
        End If
    Next C
    If P > 0 Then                      ' Still some characters left
        cPat = rPat                    ' Use pattern for next row
        rPat = Pivot - cPat            ' Swap pattern for next row
        H = H + 1                      ' Add one to total count of rows (H = "height")
    End If
Wend

If P < 0 Then ' add Pad if needed
    S = S & String(-P, Pad) ' Negative number is how many characters were missing
End If

' Check blocking (rCnt)
If rCnt <= 0 Then
    rStep = H    ' Default to entire column
Else
    rStep = rCnt ' Only peel off rCnt rows at a time
End If

ReDim Chars(1 To H, 1 To Kinfo.Ln) ' Rows x Cols
ReDim xCols(1 To Kinfo.Ln)

' Peel off characters from string into Array

P = 1
For R = 1 To H ' For all rows
    For C = 1 To Kinfo.Ln ' for all columns
        I = Cnt(R, C) ' count of characters for this cell
        Chars(R, C) = Mid(S, P, I) ' get characters
        P = P + I     ' increment index by count
    Next C
Next R

For I = 1 To Kinfo.Ln
    xCols(I) = Kinfo.OrgIdx(I)
Next I

' If Myskowski, compact columns
If Myszkowski <> 0 Then
    For C = Kinfo.Ln - 1 To 1 Step -1
        P = xCols(C)
        I = xCols(C + 1)
        ' if sorted key letters are identical
        If Mid(Key, P, 1) = Mid(Key, I, 1) Then
            xCols(C + 1) = 0 ' flag this column as "empty"
            For R = 1 To H ' for all rows, compact to the previous column
                Chars(R, P) = Chars(R, P) & Chars(R, I)
            Next R
        End If
    Next C
End If

' Could be more vigorous
If InStr(S, Sep) > 0 Then Sep = ""

' Peel off characters from array into string (with padding)
' special case - separator at bottom of complete column where no pattern or blocking occurs
ColSep = (Sep <> "") And (rCnt = 0) And (Pattern = 1)

P = 0
For MinR = 1 To H Step rStep ' all blocks of rows (rCnt)
    MaxR = Application.Min(MinR + rStep - 1, H)
    For C = 1 To Kinfo.Ln ' for each column
        I = xCols(C)    ' original column number
        If I <> 0 Then  ' it's not ignored Myskowski
            For R = MinR To MaxR   ' all rows within block
                Ch = Chars(R, I)   ' get text
                If Ch <> "" And Sep <> "" Then ' if text and have a separator
                    Cv = Cnt(R, I)                 ' length of text
                    If P + Cv < 6 Then             ' <= 5 characters
                        P = P + Cv                     ' just increment count
                    Else                           ' > 5 characters, add separator
                        Wrk = Wrk & Left(Ch, 5 - P) & Sep ' peel off any characters up to 5
                        Ch = Mid(Ch, 6 - P, 9)            ' Save any characters after 5th character
                        P = Len(Ch)                       ' Count resets to length of remaining characters
                    End If
                End If
                Wrk = Wrk & Ch      ' append text
            Next R
            If ColSep And C < Kinfo.Ln And MaxR = H Then ' Special case: last row and not final column
                P = 0               ' add extra pad character before starting next column
                Wrk = Wrk & Sep
            End If
        End If
    Next C
Next MinR

encCCol = Wrk
Exit Function

Catch:
encCCol = Err.Description
End Function

' Returns the next character (pair) from a string (combines SwpCh)
Private Function NextCh_(ByVal S As String, ByRef P As Integer) As String
Dim Ch As String
P = P + 1
Ch = Mid(S, P, 1)
If Ch = SwpCh Then
    P = P + 1
    Ch = Ch & Mid(S, P, 1)
End If
NextCh_ = Ch
End Function

' This function returns a string transposition
Public Function Transmute(S As String, Cols As Integer, Optional Pad As String = "X", _
    Optional Fill As tt_TransType = tt_Trans, Optional Extract As tt_TransType = tt_None) As String
Dim R As Integer, C As Integer, K As Integer
Dim Rows As Integer, P As Integer
Dim Ln As Integer
Dim XDir As Integer, YDir As Integer
Dim MinC As Integer, MaxC As Integer, MinR As Integer, MaxR As Integer
Dim Wrk As String
Dim Chars() As String ' Array to hold characters
Dim Ch As String
Dim Turn As Boolean
Dim RevOut As Boolean
On Error GoTo Catch

If S = "" Then      ' Empty string returns empty
    Transmute = ""
    Exit Function
End If

If Cols = 0 Then    ' zero columns
    Transmute = S   ' Return same string
    Exit Function
End If

While Abs(Fill) > 15 Or Abs(Extract) > 15
    S = Transmute(S, Cols, Pad, Fill And 15, Extract And 15)
    Fill = Fill \ 16
    Extract = Extract \ 16
Wend

If Fill = tt_Default Then ' Set defaults
    Fill = tt_Trans
End If
If Extract = tt_Default Then
    Extract = tt_None
End If

If Cols < 0 Then    ' We're undoing a previous transmutation
    Cols = -Cols    ' Change sign and swap transmutations
    R = Fill        ' Yes, it's converting an enum to integer and back
    Fill = Extract
    Extract = R
End If

If Fill < tt_Default Then ' Do this AFTER checking cols or -Cols might not work
    Fill = -Fill
    S = Reverse(S)
End If
If Extract < tt_Default Then
    Extract = -Extract
    RevOut = True
End If

If Pad = "" Then Pad = "X" ' Can't have null pad

P = Len(Replace(S, SwpCh, "")) ' length without swapch
Rows = (P + Cols - 1) \ Cols ' Number of "rows", including partial
Ln = Rows * Cols            ' compute complete block size
If P < Ln Then              ' And pad if needed
    S = S & String(Ln - P, Pad)
End If

ReDim Chars(1 To Rows, 1 To Cols) ' Now dimension array

P = 0
' Fill: put string into array in specified fill pattern
Select Case Fill
    Case tt_Trans ' Transpose = top to bottom, left to right
        For C = 1 To Cols
            For R = 1 To Rows
                Chars(R, C) = NextCh_(S, P)
            Next R
        Next C
    Case tt_RotRight ' Rot Right = Top to Bottom, Right to left
        For C = Cols To 1 Step -1
            For R = 1 To Rows
                Chars(R, C) = NextCh_(S, P)
            Next R
        Next C
    Case tt_AltTrans ' Alternate Transposition = Bottom to Top, Right to left
        For C = Cols To 1 Step -1
            For R = Rows To 1 Step -1
                Chars(R, C) = NextCh_(S, P)
            Next R
        Next C
    Case tt_None ' None = Left to Right, Top to bottom
        For R = 1 To Rows
            For C = 1 To Cols
                Chars(R, C) = NextCh_(S, P)
            Next C
        Next R
    Case tt_Flip ' left to Right, Bottom to Top
        For R = Rows To 1 Step -1
            For C = 1 To Cols
                Chars(R, C) = NextCh_(S, P)
            Next C
        Next R
    Case tt_RotLeft ' RotLeft = Bottom to Top, Left to Right
        For C = 1 To Cols
            For R = Rows To 1 Step -1
                Chars(R, C) = NextCh_(S, P)
            Next R
        Next C
    Case tt_Mirror ' Mirror = Right to Left, top to Bottom
        For R = 1 To Rows
            For C = Cols To 1 Step -1
                Chars(R, C) = NextCh_(S, P)
            Next C
        Next R
    Case tt_Rot2 ' 180 = Right to Left, Bottom to Top
        For R = Rows To 1 Step -1
            For C = Cols To 1 Step -1
                Chars(R, C) = NextCh_(S, P)
            Next C
        Next R
    Case tt_AltCol ' up/down shuttle
        YDir = 1
        R = 1
        C = 1
        While C <= Cols
            Chars(R, C) = NextCh_(S, P)
            If (1 <= R + YDir) And (R + YDir <= Rows) Then
                R = R + YDir
            Else
                YDir = -YDir
                C = C + 1
            End If
        Wend
    Case tt_AltRow ' Right/left shuttle
        XDir = 1
        R = 1
        C = 1
        While R <= Rows
            Chars(R, C) = NextCh_(S, P)
            If (1 <= C + XDir) And (C + XDir <= Cols) Then
                C = C + XDir
            Else
                XDir = -XDir
                R = R + 1
            End If
        Wend
    Case tt_Spiral
        MinC = 1
        MaxC = Cols
        MinR = 1
        MaxR = Rows
        While P < Ln
            ' Top Row
            For C = MinC To MaxC
                Chars(MinR, C) = NextCh_(S, P)
            Next C
            MinR = MinR + 1
            ' Right Column
            If P < Ln Then
                For R = MinR To MaxR
                    Chars(R, MaxC) = NextCh_(S, P)
                Next R
                MaxC = MaxC - 1
            End If
            ' Bottom Row
            If P < Ln Then
                For C = MaxC To MinC Step -1
                    Chars(MaxR, C) = NextCh_(S, P)
                Next C
                MaxR = MaxR - 1
            End If
            ' Left Column
            If P < Ln Then
                For R = MaxR To MinR Step -1
                    Chars(R, MinC) = NextCh_(S, P)
                Next R
                MinC = MinC + 1
            End If
        Wend
    Case tt_AntiSp
        XDir = 0
        YDir = 1
        R = 1
        C = 1
        MinC = 1
        MaxC = Cols
        MinR = 1
        MaxR = Rows
        While P < Ln
            Chars(R, C) = NextCh_(S, P)
            If (C + XDir < MinC) Then ' Top Row
                XDir = 0
                YDir = 1
                MinR = MinR + 1
            ElseIf (R + YDir < MinR) Then ' Right Col
                YDir = 0
                XDir = -1
                MaxC = MaxC - 1
            ElseIf (C + XDir > MaxC) Then ' Bottom Row
                XDir = 0
                YDir = -1
                MaxR = MaxR - 1
            ElseIf (R + YDir > MaxR) Then ' Left Col
                YDir = 0
                XDir = 1
                MinC = MinC + 1
            End If
            C = C + XDir
            R = R + YDir
        Wend
    Case tt_Diagonal ' Diagonal
        For K = 2 To Rows + Cols
            For C = 1 To Cols
                If K > C And K - C <= Rows Then
                    Chars(K - C, C) = NextCh_(S, P)
                End If
            Next C
        Next K
    Case tt_AltDiag ' Alternating Diagonal
        XDir = 0
        For K = 2 To Rows + Cols
            If XDir = 0 Then
                For C = 1 To Cols
                    If K > C And K - C <= Rows Then
                        Chars(K - C, C) = NextCh_(S, P)
                    End If
                Next C
            Else
                For C = Cols To 1 Step -1
                    If K > C And K - C <= Rows Then
                        Chars(K - C, C) = NextCh_(S, P)
                    End If
                Next C
            End If
            XDir = 1 - XDir
        Next K
    Case Else
        Transmute = "Unknown Fill Type " & Fill
        Exit Function
End Select

' Extract: Remove string from array in specified extract pattern
Select Case Extract
    Case tt_Trans
        For C = 1 To Cols
            For R = 1 To Rows
                Wrk = Wrk & Chars(R, C)
            Next R
        Next C
    Case tt_RotRight ' Top to Bottom, Right to left
        For C = Cols To 1 Step -1
            For R = 1 To Rows
                Wrk = Wrk & Chars(R, C)
            Next R
        Next C
    Case tt_AltTrans ' Alternate Transposition = Bottom to Top, Right to left
        For C = Cols To 1 Step -1
            For R = Rows To 1 Step -1
                Wrk = Wrk & Chars(R, C)
            Next R
        Next C
    Case tt_None
        For R = 1 To Rows
            For C = 1 To Cols
                Wrk = Wrk & Chars(R, C)
            Next C
        Next R
    Case tt_Flip
        For R = Rows To 1 Step -1
            For C = 1 To Cols
                Wrk = Wrk & Chars(R, C)
            Next C
        Next R
    Case tt_RotLeft ' Bottom to Top
        For C = 1 To Cols
            For R = Rows To 1 Step -1
                Wrk = Wrk & Chars(R, C)
            Next R
        Next C
    Case tt_Mirror ' Right to Left
        For R = 1 To Rows
            For C = Cols To 1 Step -1
                Wrk = Wrk & Chars(R, C)
            Next C
        Next R
    Case tt_Rot2 ' 180 = Right to Left, Bottom to Top
        For R = Rows To 1 Step -1
            For C = Cols To 1 Step -1
                Wrk = Wrk & Chars(R, C)
            Next C
        Next R
    Case tt_AltCol ' up/down shuttle
        YDir = 1
        R = 1
        C = 1
        While C <= Cols
            Wrk = Wrk & Chars(R, C)
            If (1 <= R + YDir) And (R + YDir <= Rows) Then
                R = R + YDir
            Else
                YDir = -YDir
                C = C + 1
            End If
        Wend
    Case tt_AltRow ' Right/left shuttle
        XDir = 1
        R = 1
        C = 1
        While R <= Rows
            Wrk = Wrk & Chars(R, C)
            If (1 <= C + XDir) And (C + XDir <= Cols) Then
                C = C + XDir
            Else
                XDir = -XDir
                R = R + 1
            End If
        Wend
    Case tt_Spiral
        P = 0
        XDir = 1
        YDir = 0
        R = 1
        C = 1
        While P < Ln
            P = P + 1
            Wrk = Wrk & Chars(R, C)
            Chars(R, C) = ""
            If (1 > C + XDir) Or (C + XDir > Cols) Or (1 > R + YDir) Or (R + YDir > Rows) Then
                Turn = True ' out of bounds
            ElseIf Chars(R + YDir, C + XDir) = "" Then
                Turn = True ' hit filled square
            End If
            If Turn Then
                If XDir = 1 Then
                    XDir = 0
                    YDir = 1
                ElseIf YDir = 1 Then
                    YDir = 0
                    XDir = -1
                ElseIf XDir = -1 Then
                    XDir = 0
                    YDir = -1
                Else ' Ydir = -1
                    YDir = 0
                    XDir = 1
                End If
                Turn = False
            End If
            C = C + XDir
            R = R + YDir
        Wend
    Case tt_AntiSp
        P = 0
        XDir = 0
        YDir = 1
        R = 1
        C = 1
        While P < Ln
            P = P + 1
            Wrk = Wrk & Chars(R, C)
            Chars(R, C) = ""
            If (1 > C + XDir) Or (C + XDir > Cols) Or (1 > R + YDir) Or (R + YDir > Rows) Then
                Turn = True ' out of bounds
            ElseIf Chars(R + YDir, C + XDir) = "" Then
                Turn = True ' hit filled square
            End If
            If Turn Then
                If YDir = 1 Then
                    YDir = 0
                    XDir = 1
                ElseIf XDir = 1 Then
                    XDir = 0
                    YDir = -1
                ElseIf YDir = -1 Then
                    YDir = 0
                    XDir = -1
                Else ' Xdir = -1
                    XDir = 0
                    YDir = 1
                End If
                Turn = False
            End If
            C = C + XDir
            R = R + YDir
        Wend
    Case tt_Diagonal ' Diagonal
        For K = 2 To Rows + Cols
            For C = 1 To Cols
                If K > C And K - C <= Rows Then
                    Wrk = Wrk & Chars(K - C, C)
                End If
            Next C
        Next K
    Case tt_AltDiag ' Alternating Diagonal
        XDir = 0
        For K = 2 To Rows + Cols
            If XDir = 0 Then
                For C = 1 To Cols
                    If K > C And K - C <= Rows Then
                        Wrk = Wrk & Chars(K - C, C)
                    End If
                Next C
            Else
                For C = Cols To 1 Step -1
                    If K > C And K - C <= Rows Then
                        Wrk = Wrk & Chars(K - C, C)
                    End If
                Next C
            End If
            XDir = 1 - XDir
        Next K
    Case Else
        Transmute = "Unknown Extract Type " & Extract
        Exit Function
End Select
If RevOut Then Wrk = Reverse(Wrk)
Transmute = Wrk
Exit Function
Catch:
Transmute = Err.Description
End Function

' Encode Rail Fence
' Rails is desired number of rails.
' If rails = 0 then number of rails is key length
' use key for red fence

Public Function encRailFence(text As String, Optional Rails As Integer, Optional Key As String, Optional Offset As Integer) As String
Dim RailNum() As Integer
Dim I As Integer
Dim Ln As Integer
Dim Dir As Integer ' direction
Dim Rn As Integer
Dim Ofs As Integer
Dim Rail As Integer
Dim Wrk As String
Dim Kinfo As New OrderedKey
On Error GoTo Catch

Ln = Len(text)
ReDim RailNum(1 To Ln)

If Rails = 0 And Key <> "" Then
    ' set rail count from key length
    Rails = Len(Key)
    If Rails < 2 Then
        encRailFence = "Key too short: " + Key + ". Must be at least 2 characters long."
        Exit Function
    End If
    If Rails > Ln Then ' key can't be longer than text
        Rails = Ln
        Key = Left(Key, Ln)
    End If
Else
    ' Set default value, max value for rails
    If Rails = 0 Then Rails = 3
    ' Check at least min value
    If Rails < 2 Then
        encRailFence = "Invalid number of rails:" & Rails
        Exit Function
    End If
    Rails = Application.Min(Rails, Ln)
    Key = String$(Rails, "X")
End If

Kinfo.InitKey = Key
If Rails > Kinfo.Ln Then ' Inverse key
    Rails = Kinfo.Ln
End If

' Compute the rail each character goes on

Rn = Rails - 1

Offset = Offset Mod (2 * Rn)
If Offset < 0 Then
    Offset = Offset + (2 * Rn)
End If
Ofs = Offset Mod Rn

If Offset \ Rn Then ' 0 = up, 1 = down
    Dir = -1
    Rail = (Rn - Ofs) + 1
Else
    Dir = 1
    Rail = Ofs + 1
End If

For I = 1 To Ln ' bounce up and down until end of string is reached
    RailNum(I) = Rail
    Rail = Rail + Dir
    If Rail > Rails Then
        Rail = Rails - 1
        Dir = -1
    ElseIf Rail = 0 Then
        Rail = 2
        Dir = 1
    End If
Next I

' paste charaters in rail order (1..n)
For Rail = 1 To Rails
    For I = 1 To Ln
        If RailNum(I) = Kinfo.OrgIdx(Rail) Then
            Wrk = Wrk & Mid(text, I, 1)
        End If
    Next I
Next Rail

encRailFence = Wrk
Exit Function
Catch:
encRailFence = Err.Description
End Function

' Decode Rail Fence
' Rails is desired number of rails.
' If rails = 0 then number of rails is key length
' use key for red fence

Public Function decRailFence(text As String, Optional Rails As Integer, Optional Key As String, Optional Offset As Integer) As String
Dim RailNum() As Integer
Dim Idx() As Integer
Dim I As Integer
Dim J As Integer
Dim Ln As Integer
Dim Dir As Integer
Dim Rail As Integer
Dim Rn As Integer
Dim Ofs As Integer
Dim Wrk As String
Dim Kinfo As New OrderedKey
On Error GoTo Catch

Ln = Len(text)

If Rails = 0 And Key <> "" Then
    ' set rail count from key length
    Rails = Len(Key)
    If Rails < 2 Then
        decRailFence = "Key too short: " + Key + ". Must be at least 2 characters long."
        Exit Function
    End If
    If Rails > Ln Then ' key can't be longer than text
        Rails = Ln
        Key = Left(Key, Ln)
    End If
Else
    ' Set default value, max value for rails
    If Rails = 0 Then Rails = 3
    ' Check at least min value
    If Rails < 2 Then
        decRailFence = "Invalid number of rails:" & Rails
        Exit Function
    End If
    Rails = Application.Min(Rails, Ln)
    
    Key = String$(Rails, "X")
End If

Kinfo.InitKey = Key
If Rails > Kinfo.Ln Then ' Inverse key
    Rails = Kinfo.Ln
End If

ReDim RailNum(1 To Ln)
ReDim Idx(1 To Rails)

' Compute the rail each character went on
Rn = Rails - 1
Offset = Offset Mod (2 * Rn)
If Offset < 0 Then
    Offset = Offset + (2 * Rn)
End If
Ofs = Offset Mod Rn

If Offset \ Rn Then ' 0 = up, 1 = down
    Dir = -1
    Rail = (Rn - Ofs) + 1
Else
    Dir = 1
    Rail = Ofs + 1
End If

'Dir = 1 ' up ( -1 is down )
'If Offset < 0 Then
'    Dir = -1
'    Offset = Abs(Offset)
'End If

'Rail = (Offset Mod Rails) + 1 ' default 1

For I = 1 To Ln
    RailNum(I) = Rail
    Idx(Rail) = Idx(Rail) + 1 ' Also save counts of characters per rail
    Rail = Rail + Dir ' next rail
    If Rail > Rails Then ' start going down
        Rail = Rails - 1
        Dir = -1
    ElseIf Rail = 0 Then ' start going back up
        Rail = 2
        Dir = 1
    End If
Next I

' each rail is successive characters
' calculate offset of first character for each rail
I = 1
For Rail = 1 To Rails
    J = Idx(Kinfo.OrgIdx(Rail)) ' Save Count
    Idx(Kinfo.OrgIdx(Rail)) = I ' Next Offset
    I = I + J   ' Add count to get next offset
Next Rail

' Build String using rail values and offsets
For I = 1 To Ln
    Rail = RailNum(I)
    Wrk = Wrk & Mid(text, Idx(Rail), 1)
    Idx(Rail) = Idx(Rail) + 1 ' Bump offset for next character on this rail
Next I

decRailFence = Wrk
Exit Function
Catch:
decRailFence = Err.Description
End Function

' Scytale transposition (transmute tt_Transpose)
Public Function encScytale(text As String, turns As Integer, Optional Pad As String) As String

' Validate turns
If turns < 1 Then
    encScytale = "Invalid number of turns: " & turns
    Exit Function
End If

' Set Pad character
If Pad = "" Then
    Pad = Chr(183) ' "�"
Else
    Pad = Left(Pad, 1)
End If

encScytale = Transmute(text, -turns, Pad)
End Function

' Scytale decoder
Public Function decScytale(text As String, turns As Integer) As String
Dim Ln As Integer, Blk As Integer, Rows As Integer

' Validate turns
If turns < 1 Then
    decScytale = "Invalid number of turns: " & turns
    Exit Function
End If

' Compute rows from turns
Ln = Len(text)
Rows = (Ln + turns - 1) \ turns
Blk = turns * Rows

' Validate length
If Blk <> Ln Then
    decScytale = "Not a complete block using " & turns & " turns"
    Exit Function
End If

decScytale = Transmute(text, turns)
End Function

' Custom cipher.  Key is used to seed the random number generator, then text is shuffled with a one-pass algorithm
Public Function encShuffle(text As String, Key As String, Optional tType As textType = textType.ttAlphaUpper) As String
Dim I As Integer
Dim X As Integer
Dim V As Integer
Dim Ln As Integer
Dim Idx() As Integer
Dim Out As String

' Check parms

' filter text, no replacements
text = toText(text, tType)

If text = "" Then
    encShuffle = ""
    Exit Function
End If
If Key = "" Then
    encShuffle = "Empty Key"
    Exit Function
End If
' Seed random number generator
InitRand (Key)

Ln = Len(text)

' Initial Order
ReDim Idx(1 To Ln)
For I = 1 To Ln
    Idx(I) = I
Next I
' One pass shuffle
For I = Ln To 2 Step -1
    X = Int(Rnd() * I) + 1
    V = Idx(X)
    Idx(X) = Idx(I)
    Idx(I) = V
Next I
' Paste together
For I = 1 To Ln
    Out = Out & Mid(text, Idx(I), 1)
Next I
encShuffle = Out
End Function

' Custom cipher.  Key is used to seed the random number generator, then text is (un)shuffled with a one-pass algorithm
Public Function decShuffle(text As String, Key As String) As String
Dim I As Integer
Dim J As Integer
Dim X As Integer
Dim Ln As Integer
Dim Wrk() As Integer
Dim Val() As Integer
Dim Out As String
' Check Parms
If Key = "" Then
    decShuffle = "Empty Key"
    Exit Function
End If
If text = "" Then
    decShuffle = ""
    Exit Function
End If
' Seed random number
InitRand (Key)
' inital order
Ln = Len(text)
ReDim Wrk(1 To Ln)
For I = 1 To Ln
    Wrk(I) = I
Next I
' One pass shuffle
For I = Ln To 2 Step -1
    X = Int(Rnd() * I) + 1
    J = Wrk(X)
    Wrk(X) = Wrk(I)
    Wrk(I) = J
Next I
' Original index for this letter
ReDim Val(1 To Ln)
For I = 1 To Ln
    Val(Wrk(I)) = I
Next I
' Pull back into text form
For I = 1 To Ln
    Out = Out & Mid(text, Val(I), 1)
Next I
decShuffle = Out
End Function

' Recursive function generates a swagman keysquare (kind of like a solved RC x RC sudoku).
' RC is the number of Rows and Columns (3-9).  Caller must trap errors.
' We start by placing all the 1's then move on to 2's, 3's, etc.
Private Function getSwagmanKey_(ByRef Key() As Integer, RC As Integer, MaxRC As Integer, ByRef Pass As Integer) As Boolean
Dim I As Integer
Dim C As Integer
Dim R As Integer
Dim Dir As Integer
Dim Attempts As Integer
Dim Cols() As Integer
Dim Used() As Integer
Dim OK As Boolean

ReDim Cols(1 To MaxRC)

While True ' Forever...
    Pass = Pass + 1
    If Pass > MAXSWAGKEY_ Then ' this has never happened but prevents an infinite loop
        Err.Raise 1044, "RowCol:getSwagmanKey_", "Unable to create unique keysquare in " & MAXSWAGKEY_ & " attempts"
    End If
    ' Generate proposed column map
    ReDim Used(1 To MaxRC) ' make sure to use each column only once
    OK = True
    Dir = 1
    ' For every row, find a column
    For R = 1 To MaxRC
        ' Starting column (random)
        C = Int(Rnd() * MaxRC) + 1
        ' Scan to right (or left) for next empty cell - empty means key(R,C) is zero AND column is not used yet for this row
        ' changing directions tends to break up a stairstep effect if you get the same random number.
        I = 0
        While I < MaxRC And (Key(R, C) <> 0 Or Used(C) = 1)
            C = C + Dir
            If C > MaxRC Then C = 1
            If C < 1 Then C = MaxRC
            I = I + 1 ' bail after checking all columns
        Wend
        If Key(R, C) <> 0 Or Used(C) = 1 Then ' We weren't able to find one
            OK = False
            R = MaxRC
        Else ' remember the column for this row and mark the column as used
            Cols(R) = C
            Used(C) = 1
            Dir = -Dir
        End If
    Next R
    If OK Then ' We found unique available columns for every row
        ' Update the key
        For R = 1 To MaxRC
            Key(R, Cols(R)) = RC
        Next R
        If RC = MaxRC Then ' Woot! This was the last digit
            getSwagmanKey_ = True
            Exit Function
        End If
        ' Recurse for next RC value
        If getSwagmanKey_(Key, RC + 1, MaxRC, Pass) Then ' Again, woot!
            getSwagmanKey_ = True
            Exit Function
        End If
        ' Mark key spaces as empty to try again
        For R = 1 To MaxRC
            Key(R, Cols(R)) = 0
        Next R
    End If
    ' OK some "black voodoo" here...  How many times to retry *THIS* RC before jumping back to
    ' retry an earlier RC?  I don't have a formal proof for this, but my intuition says that the
    ' farther you get down the stack the more likely you might encounter an imposible position
    ' (one that will never work based on what was previously filled in).  So I'm going to give
    ' up after fewer tries the deeper I am in the chain and retry an earlier digit instead.
    ' One supposes I could generate thousands of squares and compare the number of passes, while
    ' varying the maximum number of attempts to see how this affects the overall final value
    ' (fewer passes is better).  If I keep track of the standard deviation, maximum passes, etc.
    ' I could develop a formal proof.  However, the overall number of passes - even for squares
    ' of size nine - has been less than seventy for every attempt so far.  It's generally less
    ' than 20 for smaller squares.  So I like where this is at and see no need to try and
    ' optimize it further.
    Attempts = Attempts + 1
    If Attempts > MaxRC - RC + 1 Then ' Try backing out to previous digit (sooner if at a higher digit)
        getSwagmanKey_ = False
        Exit Function
    End If
Wend
End Function

' This function generates a swagman key (a string of numbers that is a perfect square where no
' digit repeats in the same row or column - kind of like a solved sudoku on rows and columns only)
' Although the cipher that uses this is not a repeatable random cipher, this function is.
Public Function genSwagmanKey(text As String, MaxRC As Integer) As String
Dim R As Integer, C As Integer
Dim Pass As Integer
Dim Key() As Integer
Dim Work As String
On Error GoTo Catch
If MaxRC < 3 Or MaxRC > 9 Then
    Err.Raise 1042, "RowCol:genSwagmanKey", "Square height out of range (" & MaxRC & "), expecting 3-9"
End If
' Text is only used to initialize the random number generator.
InitRand (text)
' Fill in the key square
ReDim Key(1 To MaxRC, 1 To MaxRC)
If Not getSwagmanKey_(Key, 1, MaxRC, Pass) Then ' this should never happen, but...
    Err.Raise 1043, "RowCol:genSwagmanKey", "Unable to generate unique keysquare"
End If
' copy key back to string by rows
For R = 1 To MaxRC
    For C = 1 To MaxRC
        Work = Work & Key(R, C)
    Next C
Next R
genSwagmanKey = Work
Exit Function
Catch:
genSwagmanKey = Err.Description
End Function

' This is the Swagman encryption routine.  This requires a numeric square to transpose the text with.
' This routine does not use a Polybius square, but instead uses an array of Scripting dictionaries.

Public Function encSwagman(text As String, Key As String, Optional tType As textType = textType.ttAlphaUpper, Optional Pad As String = "X", Optional Sep As String = "") As String
Dim R As Integer, C As Integer, P As Integer, Cnt As Integer
Dim Ln As Integer
Dim kLn As Integer
Dim RC As Integer
Dim Cv As Integer
Dim Cols As Integer
Dim Work As String
Dim textArr() As String
Dim KeyMap() As New Scripting.Dictionary

On Error GoTo Catch

' filter text, no replacements
text = toText(text, tType)

If text = "" Then
    encSwagman = ""
    Exit Function
End If

' Remove any non-numeric characters from key
Key = ToDigit(Key)
kLn = Len(Key)

If kLn < 9 Then
    Err.Raise 1050, "RowCol:encSwagman", "Error: Key length is too short; length = " & kLn & ", min length is 9"
End If

' validate key
RC = Int(Sqr(kLn))
If kLn <> RC * RC Then
    Err.Raise 1051, "RowCol:encSwagman", "Error: Key length is not a perfect square; length = " & kLn & ", expecting " & (RC * RC)
End If

' Load key into map
ReDim KeyMap(1 To RC)
For R = 1 To RC
    For C = 1 To RC
        P = P + 1
        Cv = Mid(Key, P, 1)
        If Cv < 0 Or Cv > RC Then
            Err.Raise 1052, "RowCol:encSwagman", "Error: Key contains invalid digit " & Cv & " at position " & P & ", must be in range 1-" & RC
        End If
        If KeyMap(C).Exists(Cv) Then
            Err.Raise 1053, "RowCol:encSwagman", "Error: Key contains duplicate value " & Cv & " in column " & C & " at position " & P
        End If
        ' Map value to row
        KeyMap(C).Add Cv, R
    Next C
Next R

' pad text out to multiple of rows
P = Len(text) Mod RC
If P <> 0 Then
    If Pad = "" Then Pad = "X"
    text = text & String$(RC - P, Pad)
End If

' Compute number of columns
Ln = Len(text)
Cols = Ln \ RC

' Load text into array by rows
ReDim textArr(1 To RC, 1 To Cols)
P = 0
For R = 1 To RC
    For C = 1 To Cols
        P = P + 1
        textArr(R, C) = Mid(text, P, 1)
    Next C
Next R

' Build output by keymap
C = 0
For P = 1 To Cols ' P is column number across text
    C = C + 1   ' C is column number in square
    If C > RC Then C = 1
    For R = 1 To RC
        ' Append text for this column in row order
        Work = Work & textArr(KeyMap(C).Item(R), P)
    Next R
Next P

Work = AddSep(Work, Sep)

encSwagman = Work

Exit Function
Catch:
encSwagman = Err.Description
End Function

' This is the Swagman decryption routine.  This requires a numeric square to transpose the text with.
' This routine does not use a Polybius square, but instead uses an array of Scripting dictionaries.

Public Function decSwagman(text As String, Key As String, Optional Sep As String = "") As String
Dim R As Integer, C As Integer, Col As Integer, P As Integer
Dim Ln As Integer
Dim kLn As Integer
'Dim sLn As Integer ' length of separator
Dim RC As Integer
Dim Cv As Integer
Dim Cols As Integer
Dim Work As String
Dim textArr() As String
Dim KeyMap() As New Scripting.Dictionary

On Error GoTo Catch

' filter text, no replacements (Remove control characters, etc.)
text = Compact(text, ".", "-", textType.ttASCII)
If text = "" Then
    decSwagman = ""
    Exit Function
End If

' Remove any non-numeric characters from key
Key = ToDigit(Key)
kLn = Len(Key)

If kLn < 9 Then
    Err.Raise 1050, "RowCol:decSwagman", "Error: Key length is too short; length = " & kLn & ", min length is 9"
End If

' validate key
RC = Int(Sqr(kLn))
If kLn <> RC * RC Then
    Err.Raise 1051, "RowCol:decSwagman", "Error: Key length is not a perfect square; length = " & kLn & ", expecting " & (RC * RC)
End If

' Load key into map
ReDim KeyMap(1 To RC)
For R = 1 To RC
    For C = 1 To RC
        P = P + 1
        Cv = Mid(Key, P, 1)
        If Cv < 0 Or Cv > RC Then
            Err.Raise 1052, "RowCol:decSwagman", "Error: Key contains invalid digit " & Cv & " at position " & P & ", must be in range 1-" & RC
        End If
        If KeyMap(C).Exists(Cv) Then
            Err.Raise 1053, "RowCol:decSwagman", "Error: Key contains duplicate value " & Cv & " in column " & C & " at position " & P
        End If
        ' Map value to row
        KeyMap(C).Add Cv, R
    Next C
Next R

' Handle Separator if present
text = UngroupBy(text, Sep)

' Compute number of columns
Ln = Len(text)
Cols = Ln \ RC

If Cols * RC <> Ln Then
    Err.Raise 1054, "RowCol:decSwagman", "Error: Text is not a multiple of " & RC & " characters, expected " & (Cols * RC) & " but found " & Ln
End If

' put text into array (by keymap)
ReDim textArr(1 To RC, 1 To Cols)
C = 0
P = 0
For Col = 1 To Cols ' P is column number across text
    C = C + 1   ' C is column number in square
    If C > RC Then C = 1
    For R = 1 To RC
        ' Append text for this column in row order
        P = P + 1
        textArr(KeyMap(C).Item(R), Col) = Mid(text, P, 1)
    Next R
Next Col

' Load text from array (by rows)
Work = ""
For R = 1 To RC
    For C = 1 To Cols
        Work = Work & textArr(R, C)
    Next C
Next R

decSwagman = Work

Exit Function
Catch:
decSwagman = Err.Description
End Function

' Grille cipher Key generation.
' This is a repeatable random function
Public Function genGrilleKey(text As String, Optional tType As textType = textType.ttAlphaUpper, Optional makeSorted As Boolean = True, Optional makePerfect As Boolean = False) As String
Dim Work As String

Dim Grille As New GrilleObj

On Error GoTo Catch

' filter text, no replacements
Work = toText(text, tType)

If Work = "" Then
    genGrilleKey = "Empty source text - unable to generate key"
    Exit Function
End If

' Set some variables (must catch any errors)
Grille.Init (Work)

' Repeatable Random
InitRand (text)

' generate key
Work = Grille.genKey(makeSorted, makePerfect)

genGrilleKey = Work

Exit Function
Catch:
genGrilleKey = Err.Description
End Function

'demo function: Returns 1/4 of encrypted grille
Public Function grilleQuarter(text As String, Key As String, Optional tType As textType = textType.ttAlphaUpper)
Dim QC As Integer
Dim Ch As String
Dim Work As String

Dim Grille As New GrilleObj

On Error GoTo Catch

' filter text, no replacements
text = toText(text, tType)

' empty text string, just return nothing
If text = "" Then
    grilleQuarter = ""
    Exit Function
End If

' Set some variables (must catch any errors)
Grille.Init (text)

' Validate Key (must catch any errors)
Grille.ParseKey (Key)

' Encode text
Work = Grille.encode(text, "_", True, "N")

' decode text
Work = Grille.decode(Work, True, "E")
If Not Grille.HasCenter Then
    QC = Grille.Count
    Work = Left(Work, QC) & String$(QC * 3, "_")
Else
    QC = Grille.Count - 1
    Ch = Mid(Work, QC * 2 + 1, 1)
    Work = Left(Work, QC) & String$(QC, "_") & Ch & String$(QC * 2, "_")
End If

' Encode text
Work = Grille.encode(Work, "_", True, "E")

If Grille.Odd And Not Grille.HasCenter Then
    Work = Left(Work, Grille.Count * 2) & "#" & Right(Work, Grille.Count * 2)
End If

grilleQuarter = Work

Exit Function
Catch:
grilleQuarter = Err.Description
End Function

' Rotating Grille cipher encryption
Public Function encGrille(text As String, Key As String, Optional tType As textType = textType.ttAlphaUpper, _
    Optional Pad As String = "X", Optional Sep As String = "", Optional Clockwise As Boolean = True, Optional Orientation As String) As String
Dim P As Integer
Dim I As Integer
Dim Work As String

Dim Grille As New GrilleObj

On Error GoTo Catch

' filter text, no replacements
text = toText(text, tType)

' empty text string, just return nothing
If text = "" Then
    encGrille = ""
    Exit Function
End If

' Set some variables (must catch any errors)
Grille.Init (text)

' Validate Key (must catch any errors)
Grille.ParseKey (Key)

' Encode text
Work = Grille.encode(text, Pad, Clockwise, Orientation)

' Handle Separator
Work = AddSep(Work, Sep)

encGrille = Work

Exit Function
Catch:
encGrille = Err.Description
End Function

' Rotating Grille cipher decryption
Public Function decGrille(text As String, Key As String, Optional tType As textType = textType.ttAlphaUpper, _
    Optional Sep As String = "", Optional Clockwise As Boolean = True, Optional Orientation As String) As String
Dim P As Integer
Dim I As Integer, Quad As Integer
Dim V As Integer
Dim R As Integer, C As Integer
Dim Work As String

Dim Grille As New GrilleObj
On Error GoTo Catch

' filter text, no replacements (Remove control characters)
' Handle Separator if present
text = UngroupBy(toText(text, ttASCII), Sep)
If text = "" Then
    decGrille = ""
    Exit Function
End If

' Set some variables (must catch any errors)
Grille.Init (text)

' Check text length
If Not ((Grille.Ln = Grille.Square) Or (Grille.Odd And Grille.Ln + 1 = Grille.Square)) Then ' Perfect/imperfect square
    Err.Raise 1109, "Trans:decGrille", "Error: Text length too short, expecting " & Grille.Square & ", found " & Grille.Ln
End If

' Validate Key (must catch any errors)
Grille.ParseKey (Key)

' All checks passed, now build output
Work = Grille.decode(text, Clockwise, Orientation)

decGrille = Work

Exit Function
Catch:
decGrille = Err.Description
End Function

Public Function encCadenas(text As String, Key As String, Optional tType As textType = textType.ttAlphaUpper, Optional Pad As String = "X", Optional Sep As String) As String
Dim R As Integer, C As Integer
Dim P As Integer
Dim Ln As Integer
Dim Ky As New OrderedKey
Dim Wrk As String
Dim defCh As String
Dim Trans() As String
Dim Shift() As Integer

On Error GoTo Catch

' filter text, no replacements
text = toText(text, tType)

If text = "" Then
    encCadenas = ""
    Exit Function
End If

' Default pad
If tType < 0 Then
    defCh = "X"
Else
    defCh = "x"
End If
Pad = Left(Pad & defCh, 1)
If tType < 0 Then
    Pad = UCase(Pad)
ElseIf tType > 0 Then
    Pad = LCase(Pad)
End If

' Round up to a length of 25
Ln = Len(text)
P = Ln Mod 25
If P <> 0 Then
    text = text & String$(25 - P, Pad)
    Ln = Len(text)
End If

' Key - Alpha only, remove W
Key = Compact(Key, "", "W", ttAlpha25Upper)
Ky.InitKey = Key
P = Ln \ 25
If P <> Ky.Ln Then
    encCadenas = "Error: Key = """ & Key & """, Expecting a key length of " & P & ", actual key length  is " & Ky.Ln
    Exit Function
End If

' Define arrays
ReDim Trans(0 To 24, 1 To Ky.Ln)
ReDim Shift(1 To Ky.Ln)

' Compute shift by column
For C = 1 To Ky.Ln
    Shift(C) = InStr(CadenasAlpha_, Mid(Key, C, 1))
Next C

' Parse text into array
P = 0
For R = 0 To 24
    For C = 1 To Ky.Ln
        P = P + 1
        Trans((R + Shift(C)) Mod 25, Ky.Idx(C)) = Mid(text, P, 1)
    Next C
Next R

' Pull text from array
For R = 0 To 24
    For C = 1 To Ky.Ln
        Wrk = Wrk & Trans(R, C)
    Next C
Next R

Wrk = AddSep(Wrk, Sep)

encCadenas = Wrk
Exit Function

Catch:
encCadenas = Err.Description
End Function

Public Function decCadenas(text As String, Key As String, Optional Sep As String) As String
Dim R As Integer, C As Integer
Dim P As Integer
Dim Ln As Integer
'Dim sLn As Integer
Dim Ky As New OrderedKey
Dim Wrk As String
Dim Trans() As String
Dim Shift() As Integer

On Error GoTo Catch

' Handle Separator if present
text = UngroupBy(text, Sep)

' Check that text is length of 25
Ln = Len(text)
P = Ln Mod 25
If P <> 0 Then
    decCadenas = "Error: Text is not a multiple of 25, actual len = " & Ln
    Exit Function
End If

' Key - Alpha only, remove W
Key = Compact(Key, "", "W", ttAlpha25Upper)
Ky.InitKey = Key
P = Ln \ 25
If P <> Ky.Ln Then
    decCadenas = "Error: Key = """ & Key & """, Expecting a key length of " & P & ", actual key length  is " & Ky.Ln
    Exit Function
End If

' Define arrays
ReDim Trans(0 To 24, 1 To Ky.Ln)
ReDim Shift(1 To Ky.Ln)

' Compute shift by column
For C = 1 To Ky.Ln
    Shift(C) = InStr(CadenasAlpha_, Mid(Key, C, 1))
Next C

' Parse text into array
P = 0
For R = 0 To 24
    For C = 1 To Ky.Ln
        P = P + 1
        Trans(R, C) = Mid(text, P, 1)
    Next C
Next R

' Pull text from array
Wrk = ""
For R = 0 To 24
    For C = 1 To Ky.Ln
        Wrk = Wrk & Trans((R + Shift(C)) Mod 25, Ky.Idx(C))
    Next C
Next R

decCadenas = Wrk
Exit Function

Catch:
decCadenas = Err.Description
End Function

' Seriate a text string
' Many functions work on digraphs or trigraphs (or quad...).  These functions generally work on sequential
' characters.  However, some ciphers put characters that are NOT next to each other together,  The distance
' between the two characters is the period, and the height is the count of characters to group (two = digraph,
' three = trigraph...).  This function is designed to be called as part of another cipher, but can also stand
' on its own as a transposition method.

Public Function Seriate_(text As String, ByVal Period As Integer, Optional ST As stSeriate, _
    Optional tType As textType = ttAnum, Optional Repl As Variant, Optional ByVal Height As Integer, Optional ByVal Pad As String = "X") As String
Dim P As Integer
Dim I As Integer
Dim H As Integer
Dim Ln As Integer
Dim BlkLen As Integer
Dim Cmp As New AlphaList
Dim Wrk As String

' Period = 0, don't do anything. Return original string
If Period = 0 Then
    Seriate_ = text
    Exit Function
End If

' Remove Control Characters, do Replacements
text = Cmp.Compact(text, "", Repl, tType)
If text = "" Then Exit Function
Ln = Len(text)

' Period must be positive
If Period < -1 Then
    Err.Raise 707, "Seriate_", "Error: Period less than zero: " & Period
End If

' Height must be >= 2 (default)
If Height = 0 Then Height = 2
If Height < 2 Then
    Err.Raise 708, "Seriate_", "Error: Height less than two : " & Height
End If

' Check length
P = Ln Mod Height
If P <> 0 Then
    If Pad = "" Then
        Err.Raise 709, "Seriate_", "Error: No pad, and text length (" & Len(text) & ") is not a multiple of height: " & Height
    End If
    If Not Cmp.Exists(Pad) Then
        Pad = Cmp.Char(Cmp.Count - 1)
    End If
    If Cmp.ReplExists(Pad) Then Pad = Cmp.Replacement(Pad)
    text = text & String$(Height - P, Pad)
    Ln = Len(text)
End If

' Period one means only pad, return string
If Period = 1 Then ' our work is done
    Seriate_ = text
    Exit Function
End If

' Period -1, calculate maximum period
If Period = -1 Then
    Period = Ln \ Height
End If
BlkLen = Period * Height

' Now regroup
If ST = st_Seriate Then ' join
    For P = 1 To Ln Step BlkLen ' process 1 block at a time
        If P - 1 + BlkLen > Ln Then ' Last block is longer than remaining text
            BlkLen = Ln - P + 1
            Period = BlkLen \ Height
        End If
        For I = P To P + Period - 1
            For H = I To I + BlkLen - 1 Step Period
                Wrk = Wrk & Mid(text, H, 1)
            Next H
        Next I
    Next P
Else ' Deseriate (split)
    For P = 1 To Ln Step BlkLen ' process 1 block at a time
        If P - 1 + BlkLen > Ln Then ' Last block is longer than remaining text
            BlkLen = Ln - P + 1
            Period = BlkLen \ Height
        End If
        For H = P To P + Height - 1
            For I = H To H + BlkLen - 1 Step Height
                Wrk = Wrk & Mid(text, I, 1)
            Next I
        Next H
    Next P
End If

Seriate_ = Wrk
End Function

' Seriate a text string
' Many functions work on digraphs or trigraphs (or quad...).  These functions generally work on sequential
' characters.  However, some ciphers put characters that are NOT next to each other together,  The distance
' between the two characters is the period, and the height is the count of characters to group (two = digraph,
' three = trigraph...).  This function is designed to be called as part of another cipher, but can also stand
' on its own as a transposition method.

Public Function Seriate(text As String, Period As Integer, _
    Optional tType As textType = ttAnum, Optional Repl As Variant, Optional Height As Integer, Optional Pad As String = "X") As String
On Error GoTo Catch

Seriate = Seriate_(text, Period, st_Seriate, tType, Repl, Height, Pad)

Exit Function
Catch:
Seriate = Err.Description
End Function

' Seriate a text string
' Many functions work on digraphs or trigraphs (or quad...).  These functions generally work on sequential
' characters.  However, some ciphers put characters that are NOT next to each other together,  The distance
' between the two characters is the period, and the height is the count of characters to group (two = digraph,
' three = trigraph...).  This function is designed to be called as part of another cipher, but can also stand
' on its own as a transposition method.

Public Function DeSeriate(text As String, Period As Integer, _
    Optional tType As textType = ttAnum, Optional Repl As Variant, Optional Height As Integer, Optional Pad As String = "X") As String
On Error GoTo Catch

DeSeriate = Seriate_(text, Period, st_Deseriate, tType, Repl, Height, Pad)

Exit Function
Catch:
DeSeriate = Err.Description
End Function
