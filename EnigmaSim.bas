Attribute VB_Name = "EnigmaSim"
Option Explicit
' Functions for an Enigma Machine Emulator
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'http://stackoverflow.com/questions/12414168/use-of-custom-data-types-in-vba

'/* Enigma.js
'   Enigma machine simulation.
'
'   Copyright (c) 2009, Mike Koss
'
'References:
'          Paper Enigma - http://mckoss.com/Crypto/Enigma.htm
'          Enigma Simulator - http://bit.ly/enigma-machine
'          Enigma History - http://en.wikipedia.org/wiki/Enigma_machine
'
'Usage:
'
'           var enigma = global_namespace.Import('startpad.enigma');
'           var machine = new enigma.Enigma();
'           var cipher = machine.Encode("plain text");
'
'           machine.Init();
'           var plain = machine.Encode(cipher);  -> "PLAIN TEXT"
'
'        Rotor settings from:
'             http://homepages.tesco.net/~andycarlson/enigma/simulating_enigma.html
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 08/14/2020 Chip Bloch    -Copied (but did not implement) reflector A, rotors VI, VII, VIII
'*/

Private Type tRotor
    Name As String
    Wires As String
    Map() As Integer
    MapRev() As Integer
    Notch As Integer
End Type

Private Type tNS
    Alpha As String ' "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    mRotors() As tRotor
                'I: {wires: "EKMFLGDQVZNTOWYHXUSPAIBRCJ", notch: 'Q'},
                'II: {wires: "AJDKSIRUXBLHWTMCQGZNPYFVOE", notch: 'E'},
                'III: {wires: "BDFHJLCPRTXVZNYEIWGAKMUSQO", notch: 'V'},
                'IV: {wires: "ESOVPZJAYQUIRHXLNFTGKDCMWB", notch: 'J'},
                'V: {wires: "VZBRGITYUPSDNHLXAWMJQOFECK", notch: 'Z'}
    mReflectors() As tRotor
        'B: {wires: "YRUHQSLDPXNGOKMIEBFZCWVJAT"},
        'C: {wires: "FVPJIAOYEDRZXWGCTKUQSBNMHL"}
    codeA As Integer ' ASC("A")
    Init As Boolean
End Type

Private NS As tNS

Private Type tState
    rotors() As Integer
    position() As Integer
    rings() As Integer
    reflector As Integer
    plugs As String
    mPlugs() As Integer
    OK As Boolean
    error As String
    trace As String
End Type

Private Function IFromCh(Ch As String) As Integer
    IFromCh = Asc(UCase(Left(Ch, 1))) - NS.codeA
End Function

Private Function ChFromI(I As Integer) As String
    ChFromI = Chr(I + NS.codeA)
End Function

Private Function wrap(I As Integer) As Integer
wrap = ((I + 25) Mod 26) + 1
End Function

Private Function initRotor(Name As String, Wires As String, Optional Notch As String = "") As tRotor
Dim rotor As tRotor
Dim iFrom As Integer, iTo As Integer
With rotor
    .Name = Name
    .Wires = Wires
    ' Compute forward and reverse mappings for rotors and reflectors
    ReDim .Map(26)
    ReDim .MapRev(26)
    For iFrom = 1 To 26
        iTo = IFromCh(Mid(.Wires, iFrom, 1))
        .Map(iFrom) = wrap(iTo - iFrom)
        .MapRev(iTo) = wrap(iFrom - iTo)
    Next iFrom
    If Notch <> "" Then
        .Notch = IFromCh(Notch)
    End If
End With
initRotor = rotor
End Function

Private Function initNS()
    With NS
        If .Init Then
            Exit Function
        End If
        
        .Alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        .codeA = Asc("A") - 1
        
        ReDim .mRotors(5)
                
        .mRotors(1) = initRotor("I", "EKMFLGDQVZNTOWYHXUSPAIBRCJ", "Q")
        .mRotors(2) = initRotor("II", "AJDKSIRUXBLHWTMCQGZNPYFVOE", "E")
        .mRotors(3) = initRotor("III", "BDFHJLCPRTXVZNYEIWGAKMUSQO", "V")
        .mRotors(4) = initRotor("IV", "ESOVPZJAYQUIRHXLNFTGKDCMWB", "J")
        .mRotors(5) = initRotor("V", "VZBRGITYUPSDNHLXAWMJQOFECK", "Z")
        '.mRotors(6) = initRotor("VI", "JPGVOUMFYQBENHZRDKASXLICTW", "ZM")
        '.mRotors(7) = initRotor("VII", "NZJHGRCXMYSWBOUFAIVLPEKQDT", "ZM")
        '.mRotors(8) = initRotor("VIII", "FKQHTLXOCBJSPDZRAMEWNIUYGV", "ZM")
        
        ReDim .mReflectors(2)
        
        '.mReflectors(0) = initRotor("A", "EJMZALYXVBWFCRQUONTSPIKHGD")
        .mReflectors(1) = initRotor("B", "YRUHQSLDPXNGOKMIEBFZCWVJAT")
        .mReflectors(2) = initRotor("C", "FVPJIAOYEDRZXWGCTKUQSBNMHL")
        .Init = True
    End With
End Function

Private Function CheckRotor(rotor As String) As Integer
Select Case rotor
    Case "I"
        CheckRotor = 1
    Case "II"
        CheckRotor = 2
    Case "III"
        CheckRotor = 3
    Case "IV"
        CheckRotor = 4
    Case "V"
        CheckRotor = 5
    Case Else
        CheckRotor = 0
End Select
End Function

Private Sub setState(ByRef state As tState, rotors As String, reflector As String, position As String, Optional rings As String = "", Optional plugs As String = "")
Dim I As Integer, J As Integer
Dim W As String
Dim iFrom As Integer, iTo As Integer
initNS
With state
    ReDim .rotors(3)
    ReDim .position(3)
    ReDim .rings(3)
    ReDim .mPlugs(26)
    
    .OK = True
    W = ToAlpha(rotors, "-")
    
    I = InStr(W, "-")
    .rotors(1) = CheckRotor(Left(W, I - 1))
    J = InStr(I + 1, W, "-")
    .rotors(2) = CheckRotor(Mid(W, I + 1, J - I - 1))
    .rotors(3) = CheckRotor(Mid(W, J + 1, 255))
    
    For I = 1 To 3
        If (.rotors(I) = 0) Then
            .error = "Invalid Rotor String:" & rotors
            .OK = False
            Exit Sub
        End If
    Next I
    
    W = ToAlpha(reflector)
    Select Case W
    Case "B"
        .reflector = 1
    Case "C"
        .reflector = 2
    Case Else
        .OK = False
        .error = "Invalid reflector string:" & reflector
    End Select
    
    W = ToAlpha(position)
    If Len(W) >= 3 Then
        For I = 1 To 3
            .position(I) = IFromCh(Mid(W, I, 1))
        Next I
    Else
        .OK = False
        .error = "Invalid position string:" & position
    End If
    
    W = ToAlpha(rings)
    If W = "" Then
        W = "AAA"
    End If
    If Len(W) >= 3 Then
        For I = 1 To 3
            .rings(I) = IFromCh(Mid(W, I, 1))
        Next I
    Else
        .OK = False
        .error = "Invalid ring string:" & rings
    End If
    
    W = ToAlpha(plugs)
    For I = 1 To 26
        .mPlugs(I) = I
    Next I

    If Len(W) Mod 2 = 1 Then
        .error = "Invalid plug definition- must have an even number of characters:" & plugs
        .OK = False
        Exit Sub
    End If
    
    For I = 1 To Len(W) Step 2
        iFrom = IFromCh(Mid(W, I, 1))
        iTo = IFromCh(Mid(W, I + 1, 1))
        
        If .mPlugs(iFrom) <> iFrom Then
            .OK = False
            .error = "Invalid Plug Redefinition at " & Mid(W, I, 1) & ":" & plugs
            Exit Sub
        End If
        If .mPlugs(iTo) <> iTo Then
            .OK = False
            .error = "Invalid Plug Redefinition at " & Mid(W, I + 1, 1) & ":" & plugs
            Exit Sub
        End If
        
        .mPlugs(iFrom) = iTo
        .mPlugs(iTo) = iFrom
    Next I
End With
End Sub

Private Sub defstate(ByRef state As tState)
setState state, "I-II-III", "B", "MCK"
End Sub

Private Sub mstate(ByRef state As tState, rotors As String, reflector As String, position As String, Optional rings As String = "", Optional plugs As String = "")
setState state, rotors, reflector, position, rings, plugs
End Sub

Private Function Bump(I As Integer) As Integer
I = I + 1
If I > 26 Then
    I = 1
End If
Bump = I
End Function

Private Function encodeCh(ByRef state As tState, Ch As String) As String
Dim R As Integer, V As Integer
Ch = UCase(Ch)
If Ch < "A" Or Ch > "Z" Then
    encodeCh = Ch
    Exit Function
End If
' Note that notches are components of the outer rings.  So wheel
' motion is tied to the visible rotor position (letter or number)
' NOT the wiring position - which is dictated by the rings settings
' (or offset from the 'A' position).
With state
    ' Middle notch - all rotors rotate
    If .position(2) = NS.mRotors(.rotors(2)).Notch Then
        .position(1) = Bump(.position(1))
        .position(2) = Bump(.position(2))
    Else
        If .position(3) = NS.mRotors(.rotors(3)).Notch Then
            .position(2) = Bump(.position(2))
        End If
    End If
    .position(3) = Bump(.position(3))
    
    .trace = Ch
    V = IFromCh(Ch)
    V = .mPlugs(V)
    .trace = .trace & ChFromI(V)
    For R = 3 To 1 Step -1
        V = wrap(V + NS.mRotors(.rotors(R)).Map(wrap(V + .position(R) - .rings(R))))
        .trace = .trace & ChFromI(V)
    Next R
    V = wrap(V + NS.mReflectors(.reflector).Map(V))
    .trace = .trace & ChFromI(V)
    For R = 1 To 3
        V = wrap(V + NS.mRotors(.rotors(R)).MapRev(wrap(V + .position(R) - .rings(R))))
        .trace = .trace & ChFromI(V)
    Next R
    V = .mPlugs(V)
    .trace = .trace & ChFromI(V)
    encodeCh = ChFromI(V)
End With
End Function

Private Sub mState2(ByRef state As tState, rotors As String, reflector As String, position As String, Optional rings As String = "", Optional plugs As String = "")
setState state, rotors, reflector, position, rings, plugs
End Sub

Public Function Enigma(S As String, rotors As String, position As String, Optional rings As String = "", Optional plugs As String = "", Optional reflector As String = "B") As String
Dim state As tState
Dim I As Integer
Dim W As String
mstate state, rotors, reflector, position, rings, plugs
If state.error <> "" Then
    Enigma = state.error
    Exit Function
End If
For I = 1 To Len(S)
    W = W & encodeCh(state, Mid(S, I, 1))
Next I
Enigma = W
End Function

Private Function RingStr(Ints() As Integer) As String
Dim I As Integer
Dim W As String
For I = 1 To 3
    W = W & ChFromI(Ints(I))
Next I
RingStr = W
End Function

Private Sub Bumpit(ByRef state As tState, ByRef Posn() As Integer, ByRef Ring() As Integer)
Dim I As Integer
Ring(3) = Bump(Ring(3))
If Ring(3) = 1 Then
    Ring(2) = Bump(Ring(2))
    If Ring(2) = 1 Then
        Ring(1) = Bump(Ring(1))
        If Ring(1) = 1 Then
            Posn(3) = Bump(Posn(3))
            If Posn(3) = 1 Then
                Posn(2) = Bump(Posn(2))
                If Posn(2) = 1 Then
                    Posn(1) = Bump(Posn(1))
                End If
            End If
        End If
    End If
End If
For I = 1 To 3
    state.position(I) = Posn(I)
    state.rings(I) = Ring(I)
Next I
End Sub


' Searches for a starting rotor and ring position givin a string to match
Public Function EnigmaTest(Srch As String, S As String, rotors As String, position As String, Optional rings As String = "", Optional plugs As String = "", Optional reflector As String = "B") As String
Dim state As tState
Dim I As Integer

Dim Posn() As Integer
Dim Ring() As Integer

ReDim Posn(3)
ReDim Ring(3)

'Initialize the enigma simulator
mstate state, rotors, reflector, position, rings, plugs
If state.error <> "" Then
    EnigmaTest = state.error
    Exit Function
End If

' save the current position
For I = 1 To 3
    Posn(I) = state.position(I)
    Ring(I) = state.rings(I)
Next I

Bumpit state, Posn, Ring

' Begin the search
Do
    ' try to match the string
    Do
        For I = 1 To Len(Srch)
            If Mid(Srch, I, 1) <> encodeCh(state, Mid(S, I, 1)) Then
                Exit Do
            End If
        Next I
        ' found a possible match - return the ring and rotor position
        EnigmaTest = RingStr(Posn) & ":" & RingStr(Ring)
        Exit Function
    Loop While True
    
    ' Didn't find the string, bump the rings then the rotors
    Bumpit state, Posn, Ring
    
    ' exit if we get all the way around - it'll never be found
    If RingStr(Posn) = position And RingStr(Ring) = rings Then
        EnigmaTest = "Not found"
        Exit Function
    End If
Loop While True
End Function
