VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GrilleObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       GrilleObj - rotating Grille Cipher
'
'USAGE:
'       Binds information about multiple holes in a grille
'
'GROUPS:
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 01/21/2021 Chip Bloch    -Original Version
'-------------------------------------------------------------------------------------------

Public Ln As Integer ' Length of text
Public RC As Integer ' Rows/Cols
Public MidLn As Integer ' "Middle" row/col
Public Square As Integer ' Length of perfect square (RC*RC)
Public Count As Integer ' Count of holes
Public Odd As Boolean ' True if odd number of rows/cols
Public Sorted As Boolean ' True if sorted key
Public HasCenter As Boolean ' True if has a center
Private Grille_() As New HoleObj ' List of holes
Private Hole As HoleObj

'Private Sub Class_Initialize()
'End Sub

Public Sub Init(text As String)
Ln = Len(text)
RC = Int(Sqr(Ln) + 0.999)

If RC < 3 Then
    Err.Raise 1041, "GrilleObj.init", "Text too short, must be at least 5 characters long"
End If

' Set some variables
MidLn = (RC + 1) \ 2
Odd = RC Mod 2 = 1
Square = RC * RC
HasCenter = Odd And (Ln = Square) ' odd number of rows and perfect square, include center
End Sub

'Property Get GrilleX() As HoleObj()
'GrilleX = Grille_
'End Property

' subsort (insertion)
Private Sub insertHole_(ByVal P As Integer)
If Sorted And P >= 2 Then ' sorted flag is set
    While P >= 2 And Hole.Idx < Grille_(P - 1).Idx
        Set Grille_(P) = Grille_(P - 1)
        P = P - 1
    Wend
End If
Set Grille_(P) = Hole
End Sub

' Insert center hole
Private Sub insertCenter()
Set Hole = New HoleObj
Call Hole.InitRC(MidLn, MidLn, RC)
Count = Count + 1
Call insertHole_(Count)
End Sub

' Grille cipher key key generation
Public Function genKey(Optional makeSorted As Boolean = True, Optional makePerfect As Boolean = False) As String
Dim I As Integer
Dim R As Integer, C As Integer
Dim Idx As Integer
Dim Quad As Integer
Dim Work As String

' Set some variables
Sorted = makeSorted
HasCenter = (Odd) And ((Ln = Square) Or makePerfect) ' odd number of rows and perfect square, include center

I = Square \ 4
If HasCenter Then
    I = I + 1  ' Center will insert after this count (zero to I minus 1) - unsorted
    Idx = Int(Rnd() * I)
End If

' Initialize empty grille - index zero is null hole (backstops sort)
ReDim Grille_(I)
Set Grille_(0) = New HoleObj

' cover top left quadrant
For R = 1 To RC \ 2
    For C = 1 To MidLn
        If HasCenter And Count = Idx Then
            insertCenter
        End If
        ' Create hole at this row, column
        Set Hole = New HoleObj
        Call Hole.InitRC(R, C, RC)
        ' Rotate to random quadrant
        Quad = Int(Rnd() * 4)
        Select Case Quad
            Case 3
                Hole.PrevQuad
            Case 2
                Hole.FarQuad
            Case 1
                Hole.nextQuad
            ' case else (0)
        End Select
        ' Insert into grille
        Count = Count + 1
        Call insertHole_(Count)
    Next C
Next R

If HasCenter And Count = Idx Then ' center was not already inserted, must be last
    insertCenter
End If

' Return <maybe sorted> list of indexes
Work = Grille_(1).Idx
For I = 2 To Count
    Work = Work & " " & Grille_(I).Idx
Next I

genKey = Work

End Function

' Grille cipher - Parse key into Grille
Public Sub ParseKey(key As String)
Dim I As Integer
Dim R As Integer, C As Integer
Dim V As Integer
Dim Quad As Integer
Dim textArr() As String
Dim keyVals() As String

ReDim textArr(1 To RC, 1 To RC)

' Maximum Number of holes
I = Square \ 4
If Odd Then I = I + 1

' Initialize "empty" grille - zero entry used for sort (lowest)
ReDim Grille_(I)
Set Grille_(0) = New HoleObj

' Validate Key
keyVals = Split(key, " ")

If UBound(keyVals) >= I Then ' too many holes
    Err.Raise 1109, "GrilleObj:parseKey", "Too many key items: " & UBound(keyVals) + 1 & " expecting no more than " & I
End If

' Iterate over key array
For I = 0 To UBound(keyVals)
    If Not VBA.IsNumeric(keyVals(I)) Then
        Err.Raise 1110, "GrilleObj:parseKey", "Key item number " & I & " is not numeric: [" & keyVals(I) & "]"
    End If
    ' Get value and create new grille object
    V = keyVals(I)
    Set Hole = New HoleObj
    Call Hole.InitIdx(V, RC)
    ' Now fill in textArr with value
    If textArr(Hole.Row, Hole.Col) <> "" Then ' already filled previously
        Err.Raise 1111, "Trans:encGrille", "Error: Key item number " & I & " (" & V & ") is in the same orbit as key item " & textArr(Hole.Row, Hole.Col)
    End If
    textArr(Hole.Row, Hole.Col) = V
    ' Insert Hole into grille
    Count = Count + 1
    Call insertHole_(Count)
    ' Now fill in array for remaining rotations
    If Hole.Center Then ' Center doesn't have multiples
        HasCenter = True ' but if not already set
    Else
        ' Clone object
        Set Hole = New HoleObj
        Call Hole.InitIdx(V, RC)
        ' rotate 3 times
        For Quad = 1 To 3
            Hole.nextQuad
            textArr(Hole.Row, Hole.Col) = V
        Next Quad
    End If
Next I

' Now validate that all holes were covered
For R = 1 To RC
    For C = 1 To RC
        If textArr(R, C) = "" Then ' No punched hole
            If (R = MidLn And C = MidLn And Not HasCenter) Then ' no need to cover center
            Else
                Err.Raise 1112, "Trans:encGrille", "Error: Not all orbits covered by key, missing (" & Hole.Row & "," & Hole.Col & ")"
            End If
        End If
    Next C
Next R

' Set sorted flag (each entry greater than the previous one)
Sorted = True
For I = 2 To Count
    If Grille_(I).Idx < Grille_(I - 1).Idx Then ' it wasn't
        Sorted = False
        I = Count
    End If
Next I
End Sub

' Translate Oriention
Private Sub Offset(Orientation As String)
Dim I As Integer
Dim Ofs As Integer
If VBA.IsNumeric(Orientation) Then
    Ofs = Orientation
    Ofs = Ofs Mod 3
Else
    Select Case Orientation
        Case "N", "U", ""
            Ofs = 0
        Case "E", "R"
            Ofs = 1
        Case "S", "D"
            Ofs = 2
        Case "W", "L"
            Ofs = 3
        Case Else
            Err.Raise 1120, "GrilleObj:Offset", "Error: Unknown orientation [" & Orientation & "], expecting N, E, S, W"
    End Select
End If
If Ofs <> 0 Then
    For I = 1 To Count
        Select Case Ofs
            Case 1
                Set Hole = Grille_(I).nextQuad
            Case 2
                Set Hole = Grille_(I).FarQuad
            Case 3
                Set Hole = Grille_(I).PrevQuad
        End Select
        If Sorted Then Call insertHole_(I)
    Next I
End If
End Sub


' Turning grille cipher - encode text string using grille
Public Function encode(text As String, Pad As String, Optional Clockwise As Boolean, Optional Orientation As String) As String
Dim P As Integer, I As Integer
Dim R As Integer, C As Integer
Dim Quad As Integer
Dim Work As String
Dim Ch As String
Dim textArr() As String

ReDim textArr(1 To RC, 1 To RC)

' pad text out to multiple of rows/columns
P = Square - Ln
If Odd And Not HasCenter Then ' Center is not needed
    P = P - 1   ' Pad out to 1 character less
End If
If P > 0 Then
    If Pad = "" Then Pad = "X"
    text = text & String$(P, Pad)
End If

Offset (Orientation)

' All checks passed, now build output
P = 0
' all four quadrants
For Quad = 1 To 4
    ' all holes in grille
    For I = 1 To Count
        Set Hole = Grille_(I)
        ' Center hole appears in 2nd quadrant.  Sorted - appears at end, unsorted appears in key order
        If Not Hole.Center Or (Not Sorted And Quad = 2) Then
            P = P + 1
            textArr(Hole.Row, Hole.Col) = Mid(text, P, 1)
        End If
    Next I
    If Sorted And HasCenter And Quad = 2 Then ' Handle middle - sorted - always exact middle of string
        P = P + 1
        textArr(MidLn, MidLn) = Mid(text, P, 1)
    End If
    ' Rotate for next pass
    If Quad < 4 Then
        For I = 1 To Count
            If Clockwise Then
                Set Hole = Grille_(I).nextQuad
            Else
                Set Hole = Grille_(I).PrevQuad
            End If
            If Sorted Then Call insertHole_(I) ' resort
        Next I
    End If
Next Quad

' now peel text out of array
For R = 1 To RC
    For C = 1 To RC
        Ch = textArr(R, C)
        If Ch <> "" Then
            Work = Work & Ch
        End If
    Next C
Next R

encode = Work
End Function

' Turning grille cipher - decode text string using grille
Function decode(text As String, Optional Clockwise As Boolean, Optional Orientation As String) As String
Dim I As Integer
Dim lt As Integer
Dim Quad As Integer
Dim Work As String
' if there *could* have been a center, but is not, then add one character exactly in the middle (after 2nd quadrant)
If Odd And Not HasCenter Then
    lt = Count * 2
    text = Left(text, lt) & " " & Right(text, lt)
End If

Offset (Orientation)

' for each quadrant
For Quad = 1 To 4
    ' For each hole
    For I = 1 To Count
        Set Hole = Grille_(I)
        If Not Hole.Center Or (Not Sorted And Quad = 2) Then ' Center when unsorted is in 2nd quadrant
            Work = Work & Mid(text, Hole.Idx, 1)
        End If
    Next I
    If Sorted And HasCenter And Quad = 2 Then ' Handle middle - sorted - appears after 2nd quadrant
        Work = Work & Mid(text, Len(Work) + 1, 1)
    End If
    ' Rotate for next pass
    If Quad < 4 Then
        For I = 1 To Count
            If Clockwise Then
                Set Hole = Grille_(I).nextQuad
            Else
                Set Hole = Grille_(I).PrevQuad
            End If
            If Sorted Then Call insertHole_(I)
        Next I
    End If
Next Quad

decode = Work
End Function
