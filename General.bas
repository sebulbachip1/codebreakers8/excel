Attribute VB_Name = "General"
Option Explicit
Option Compare Binary
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       General
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime"
'
' USAGE:
'       General purpose functions, not necessarly specific to any kind of cipher
'
' GROUPS:
'       Range
'            Spread, Koncat (Partially deprecated in 2016)
'            strRange, range2str
'
'       String
'            Reverse, MCase, ToAlpha, ToDigit, ToAnum, XLAT, GroupBy, UnGroup, ToText,
'            Compact, Dedup, Word, Alphabet, Alpha25, Rotate, RMID, AltKey, multiSub, AddSep
'            GoldBug, toOffset, toChars
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 01/28/2019 Chip Bloch    -Added HashStr
' 01/29/2019 Chip Bloch    -Added StrRange
' 06/03/2019 Chip Bloch    -Added MCase
' 06/16/2019 Chip Bloch    -Added Base64, Case flag for XLAT
' 06/18/2019 Chip Bloch    -Migrated Base 64 functions to base module
' 06/19/2019 Chip Bloch    -Added Alphabet, Alpha25, Rotate
' 06/24/2019 Chip Bloch    -Moved Affine to Cipher
' 10/12/2019 Chip Bloch    -Added RMID
' 06/27/2020 Chip Bloch    -Updated COMPACT replace
' 06/28/2020 Chip Bloch    -Added Pad to GroupBy
' 07/07/2020 Chip Bloch    -Added AltKey
' 07/12/2020 Chip Bloch    -Added range2str, textType option for dedup
' 07/13/2020 Chip Bloch    -Added textType 0 for dedup (Alpha preserve case)
' 07/16/2020 Chip Bloch    -Added Enum for textType, keyType
'                          -Default mask for GetFullIC to entire string
' 07/18/2020 Chip Bloch    -Added multiSub
' 07/19/2020 Chip Bloch    -Default to space on null split character for Word
' 07/21/2020 Chip Bloch    -Suppressed trailing nulls on multiSub target list
' 07/22/2020 Chip Bloch    -Added "-" option for pad on compact, ttAlphaDigit
' 07/26/2020 Chip Bloch    -Using Application.Min
'                          -Migrated Statistical functions to Stats (Including HashStr)
'                          -Migrated Binary function(s) to Base
' 07/28/2020 Chip Bloch    -Changed Pad to Sep in ToAlpha, ToDigit, ToAnum, GroupBy
' 07/31/2020 Chip Bloch    -Extended MCase to pass punctuation from Match through
' 08/15/2020 Chip Bloch    -Added xlType (case translation) for XLAT
' 08/18/2020 Chip Bloch    -Added SkipPunct to MCase
'                          -Migrated RunType from Cipher
' 08/19/2020 Chip Bloch    -Added pass through for digits on dedup with ttAlphaDigit
' 08/25/2020 Chip Bloch    -Added additional dedup options
' 09/06/2020 Chip Bloch    -Added punctiation option to Compact
' 10/04/2020 Chip Bloch    -Added Public modifer to range2str
' 10/11/2020 Chip Bloch    -Added toDwarf
' 11/28/2020 Chip Bloch    -Made cs_Alphabet public
' 12/04/2020 Chip Bloch    -Added Repl to Dedup
' 12/05/2020 Chip Bloch    -Added textType to Compact
' 12/10/2020 Chip Bloch    -Migrated some variables to Globals
' 12/12/2020 Chip Bloch    -Updated dedup (got rid of goto, using lists)
'                          -Updated GroupBy: 0 count = len, <1 = sqr(len)
' 12/13/2020 Chip Bloch    -Corrected ASCII Filter in Dedup (Case sensitive)
' 12/28/2020 Chip Bloch    -Converted Dedup and Compact to accept a range as well as a
'                           string for the replacement character (24 character ciphers)
' 01/01/2021 Chip Bloch    -Added bonus characters to Compact()
' 01/06/2021 Chip Bloch    -Compact now respects case translation (upper/lower)
' 02/10/2021 Chip Bloch    -Added ToText, Ungroup, AddSep
' 02/21/2021 Chip Bloch    -Added Pad to ToText, Compact Final Replacement Character
'                          -Added GoldBug
'                          -Not folding "f" to "F" in toDwarf()
' 03/13/2021 Chip Bloch    -Added "^" (not) option to XLAT
' 03/29/2021 Chip Bloch    -Migrated core Dedup and Compact to AlphaList()
' 04/11/2021 Chip Bloch    -Added AlphaVal, textVal
' 04/21/2021 Chip Bloch    -Renamed AlphaVal as toOffset, textVal as toChars
' 04/22/2021 Chip Bloch    -Added AsHex option to toOffset, toChars
' 04/25/2021 Chip Bloch    -Migrated Alphabet, Alpha25, Goldbug to Globals
' 04/28/2021 Chip Bloch    -Updated Reverse to make SwpCh "sticky" - stays with following character
'-------------------------------------------------------------------------------------------

'Private Const pList As String = ",;-:"".?' !/()"

Public Enum xlType
    xlLower = -1    ' translate to lower case
    xlNone = 0      ' no case translation
    xlBoth = 1      ' Case Insensitive translation
    xlupper = 2     ' Upper case translation
End Enum

Public Enum RunType
    rtProgressive = -2
    rtPosttext = -1
    rtWrap = 0
    rtAutokey = 1
    rtInterrupt = 2
End Enum

' ******************** Range Functions **********************

' This "strrange" function appends multiple cells into a single string, with optional field and record separators.
' field and record separators default to empty string ("")
' If nulls is false, will skip empty cells.
' Will raise an error if the variant input is not a string or a range
Public Function StrRange(Rng As Variant, Optional Fld As String = "", Optional Rec As String = "", Optional Nulls As Boolean = True) As String
Dim lastRow As Integer
Dim lastCol As Integer
Dim Cel As Range
Dim Val As String

If IsMissing(Rng) Or IsEmpty(Rng) Then ' Val = ""
ElseIf VarType(Rng) = vbString Then
    Val = Rng
ElseIf TypeName(Rng) = "Range" Then
    For Each Cel In Rng
        If Nulls Or Cel.Value <> "" Then ' skip empty cells if nulls = false
            If lastRow <> Cel.Row Then ' reached end of row, append record separator if needed
                lastRow = Cel.Row
                If Rec <> "" Then ' Skip field separator if record separator is specified
                    lastCol = Cel.Column
                End If
                If Val <> "" Then
                    Val = Val & Rec
                End If
            End If
            If lastCol <> Cel.Column Then ' next column, append field separator if needed
                If Val <> "" Then
                    Val = Val & Fld
                End If
            End If
            Val = Val & Cel.Value
        End If
    Next
Else
    Err.Raise 513, "General:strRange", "Unknown parm type for range (expecting string/range): " & TypeName(Rng)
End If

StrRange = Val
End Function

' This is a public function, similar to strRange.  However, it has less overhead and is intended to be called internally
' by other functions.  Note, the calling function must trap errors
Public Function range2str(SR As Variant) As String
Dim Str As String
Dim Cel As Range

' Process SR Parameter - must be a String or a Range of characters
If IsMissing(SR) Or IsEmpty(SR) Then ' Str = ""
'ElseIf VBA.IsNumeric(SR) Then
'    ' It's a Number, convert to string
'    Str = SR
ElseIf VarType(SR) = vbString Then
    ' It's a String
    Str = SR
ElseIf TypeName(SR) = "Range" Then
    For Each Cel In SR
        Str = Str & Cel.Value2
    Next Cel
Else
    Err.Raise 584, "General:range2str", "Unknown parm type for range (expecting string/range): " & TypeName(SR)
End If
range2str = Str
End Function

' This is the MultiSubstitute function
' This function is similar to Substitute, but replaces multiple strings in one pass
' Src contains a range of values to be replaced
' Tgt contains a 1 to 1 list of replacement values
' if there is only one value in the tgt list, all values in the source list will be replaced with the target value
Public Function multiSub(Str As String, src As Range, tgt As Variant) As String
Dim R As Integer
Dim tCnt As Integer
Dim tStr() As String
Dim Cel As Range
' Build target list
If VarType(tgt) = vbString Then
    ReDim tStr(1 To 1)
    tStr(1) = tgt
    tCnt = 1
ElseIf TypeName(tgt) = "Range" Then
    ReDim tStr(1 To tgt.Count)
    For Each Cel In tgt
        tCnt = tCnt + 1
        tStr(tCnt) = Cel.Value2
    Next Cel
    While tCnt > 1 And tStr(tCnt) = ""
        tCnt = tCnt - 1
    Wend
Else
    Err.Raise 584, "General:multiSub", "Unknown parm type for target (expecting string/range): " & TypeName(tgt)
End If
' Process source list
For Each Cel In src
    R = R + 1
    If tCnt = 1 Then
        Str = Application.WorksheetFunction.Substitute(Str, Cel.Value2, tStr(1))
    Else
        If R <= tCnt Then
            Str = Application.WorksheetFunction.Substitute(Str, Cel.Value2, tStr(R))
        Else
            Str = Application.WorksheetFunction.Substitute(Str, Cel.Value2, "")
        End If
    End If
Next Cel
multiSub = Str
End Function

' Function to convert a long into english text for the number
Public Function NumToText(L As Long) As String
Dim Word1() As String
Dim Word2() As String
Dim Word3() As String
Dim Number As String
Dim Space As String
Dim Hyphen As String
Dim text As String
Dim To_Go As Integer
Dim Index As Integer
Dim Digit As Integer
If L >= 1000000 Or L <= 0 Then
    NumToText = "Error: Number must be in range 1 to 999,999 :[" & L & "]"
    Exit Function
End If
Word1 = Split("one two three four five six seven eight nine")
Word2 = Split("ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen")
Word3 = Split("twenty thirty forty fifty sixty seventy eighty ninety")
Number = L
To_Go = Len(Number)
Index = 1
While To_Go > 0
    Digit = Mid(Number, Index, 1)
    Select Case To_Go
        Case 6, 3
            If Digit <> 0 Then
                text = text & Space & Word1(Digit - 1) + " hundred"
            End If
            To_Go = To_Go - 1
            Index = Index + 1
        Case 5, 2
            Select Case Digit
                Case 0
                    Hyphen = ""
                    To_Go = To_Go - 1
                    Index = Index + 1
                Case 1
                    text = text & Space & Word2(Mid(Number, Index + 1, 1))
                    If To_Go = 5 Then
                        text = text & " thousand"
                    End If
                    Hyphen = ""
                    To_Go = To_Go - 2
                    Index = Index + 2
                Case Else
                    text = text & Space & Word3(Digit - 2)
                    Hyphen = "-"
                    To_Go = To_Go - 1
                    Index = Index + 1
            End Select
        Case 4, 1
            If Digit <> 0 Then
                If Hyphen = "" Then
                    text = text & Space
                Else
                    text = text & Hyphen
                End If
                text = text & Word1(Digit - 1)
            End If
            If To_Go = 4 Then
                text = text + " thousand"
            End If
            To_Go = To_Go - 1
            Index = Index + 1
    End Select
    Space = " "
Wend
NumToText = text
End Function

' This function concatenates a range of cells into a single string (2016)
Public Function Koncat(R As Range, Optional Sep As String = "") As String ' Deprecated due to Microsoft writing the same function
Dim Cel As Range
Dim lastRow As Integer
Dim S As String
For Each Cel In R
    If lastRow <> Cel.Row Then
        lastRow = Cel.Row
        If S <> "" Then
            S = S & Sep
        End If
    End If
    S = S & Cel.Value
Next Cel
Koncat = S
End Function

' This function returns a character in a string based on an offset of the cell from another cell
Public Function Spread(Anchor As Range, S As String, Optional Cols As Integer = 0, Optional Count As Integer = 1) As String
Dim R As Integer, C As Integer, Rows As Integer
Dim Transpose As Boolean
Transpose = False
If Count = 0 Then
  Count = 1
ElseIf Count < 0 Then
  Transpose = True
  Count = -Count
End If
If Not Transpose Then
    If Cols = 0 Then ' default to number of columns in the range
        Cols = Anchor.Columns.Count
    End If
    ' Compute offset into range for this column
    C = Application.Caller.Column - Anchor.Item(1).Column
    If C >= Cols Then ' outside range, return empty string
        Spread = ""
    Else ' compute offset into range for this row, return character(s) from string
        R = Application.Caller.Row - Anchor.Item(1).Row
        Spread = Mid(S, Count * (R * Cols + C) + 1, Count)
    End If
Else
    Rows = Cols
    If Rows = 0 Then ' default to number of rows in the range
        Rows = Anchor.Rows.Count
    End If
    ' Compute offset into range for this column
    R = Application.Caller.Row - Anchor.Item(1).Row
    If R >= Rows Then ' outside range, return empty string
        Spread = ""
    Else ' compute offset into range for this row, return character(s) from string
        C = Application.Caller.Column - Anchor.Item(1).Column
        Spread = Mid(S, Count * (C * Rows + R) + 1, Count)
    End If
End If
End Function

' ******************** AlphaList Functions **********************

' return filtered text (pre-processing text)
Public Function toText(text As String, Optional tType As textType = textType.ttAlphaUpper, Optional Repl As String = "-", Optional Pad As String) As String
Dim Punct As String

' Set ASCII Pass through if needed
Select Case tType
    ' ASCII
    Case textType.ttASCII, textType.ttASCIIUpper, textType.ttASCIIAny
        Punct = "." ' Pass any character (Except control characters)
        Repl = "-" ' No replacement
    Case textType.ttASCIIPunct, textType.ttASCIIAll ' only pass characters for these filters
        Repl = "-" ' No replacement
    Case textType.ttAlpha25, textType.ttAlpha25Upper
        ' Replacement happens here if specified (default none if "-", "J"->"I" if empty)
    Case Else
        Repl = "-" ' No replacement
End Select

If Pad <> "" Then Punct = "+" & Punct & Pad

' filter text, usually no replacements
toText = Compact(text, Punct, Repl, tType)
End Function

' This function strips punctuation and spaces from a string
' it also optionally inserts a padding character between doubled letters (default behavior).
' If pad = "+", then do not insert pad between doubled letters, but still pad final "X" if needed
' If pad = "", then do not insert pad at all
' if pad = "-X" then replace rather than insert between double letters
' Finally, it replaces a specific character with its predecessor, e.g. J becomes I (always does this)
' Compact ("Oh, Hello World", "X") = "OHHELXLOWORLDX"

Public Function Compact(text As String, Optional Pad As String = "X", Optional Repl As Variant, _
    Optional tType As Variant, Optional Bonus As String = "") As String
Dim Cmp As New AlphaList
On Error GoTo Catch
Cmp.defType = ttAlpha25Upper
Compact = Cmp.Compact(text, Pad, Repl, tType, Bonus)
Exit Function
Catch:
Compact = Err.Description
End Function

' This function takes a key and an alphabet and returns the deduped letters in key/alphabet order
' e.g. Dedup ("Blue", "ABCDEFGHIJKLMNOPQRSTUVWXYZ") = "BLUEACDFGHIJKMNOPQRSTVWXYZ"
' Useful for keyword code or playfair code

' If textType is negative or ttAlpha25, the alpha comparison and dedup is *case insensitive* ('A' = 'a')
' The default is uppercase alpha

' If TextType is ttAlpha25 or ttAlpha25Upper, will replace characters before deduping (J->I default)

Public Function Dedup(Key As String, Optional text As String = "", _
    Optional tType As Variant, Optional Repl As Variant, Optional Bonus As String = "") As String

Dim Dup As New AlphaList
On Error GoTo Catch

Dup.defType = ttAlphaUpper
Dedup = Dup.Dedup(Key, text, tType, Repl, Bonus)

Exit Function
Catch:
Dedup = Err.Description
End Function

' Returns Alphabet values (index of letter in alphabet)
' Offset of 2048 (oASCII) will return ASCII values
Public Function toOffset(text As String, Optional tType As Variant, Optional Repl As Variant, Optional Offset As Integer, Optional AsHex As Boolean) As String
Dim AlphaDigits As New AlphaList
On Error GoTo Catch
If Offset < 0 Then
    Err.Raise 901, "AlphaVal", "Negative offsets not allowed: " & Offset
End If
AlphaDigits.Rep = "-" ' default no replacements
AlphaDigits.Init tType, Repl, Offset
toOffset = AlphaDigits.toOffset(text, AsHex)
Exit Function
Catch:
toOffset = Err.Description
End Function

' Returns letter values for Alphabet values
' Offset of 2048 (oASCII) will return ASCII characters
Public Function toChars(text As String, Optional tType As Variant, Optional Repl As Variant, Optional Offset As Integer, Optional AsHex As Boolean) As String
Dim AlphaDigits As New AlphaList
On Error GoTo Catch
AlphaDigits.Rep = "-" ' default no replacements
AlphaDigits.Init tType, Repl, Offset
toChars = AlphaDigits.toChars(text, AsHex)
Exit Function
Catch:
toChars = Err.Description
End Function

' ******************** String Functions **********************

' Function to reverse a string
Public Function Reverse(S As String) As String
Dim I As Integer
Dim W As String
Dim Ch As String
For I = 1 To Len(S)
    Ch = Mid(S, I, 1)
    If Ch = SwpCh Then
        I = I + 1
        Ch = Ch & Mid(S, I, 1)
    End If
    W = Ch & W
Next I
Reverse = W
End Function

Public Function ReverseWords(S As String, Optional SplitCh As String = " ") As String
Dim I As Integer
Dim Words() As String
Dim Wrk As String
If SplitCh = "" Then SplitCh = " "
Words = Split(S, SplitCh)
For I = 0 To UBound(Words)
    If Wrk <> "" Then Wrk = Wrk & SplitCh
    Wrk = Wrk & Reverse(Words(I))
Next I
ReverseWords = Wrk
End Function

' Function to prep a string for display in the Dwarven Rune font
Public Function toDwarf(S As String, Optional Extra As Boolean = False) As String
Dim I As Integer
Dim Wrk As String
Dim Ch As String

For I = 1 To Len(S)
    Ch = Mid(S, I, 1)
    ' Pass "F" through with it's original case
    ' Uppercase F is straight, lowercase F is curly
    Select Case UCase(Ch)
        Case "F", "U", "V", "P"
        Case Else
            Ch = LCase(Ch)
    End Select
    Wrk = Wrk & Ch
Next I

'S = LCase(S)
S = Wrk

' Consecutive punctuation
S = Replace(S, "  ", " ") ' Double space => space
S = Replace(S, ", ", ",") ' consecutive space+comma => comma
S = Replace(S, " ,", ",")
S = Replace(S, ". ", ".") ' Consecutive space+period => period
S = Replace(S, " .", ".")
S = Replace(S, ",.", ".") ' Consecutive comma+period => period
S = Replace(S, ".,", ".")
' Punctuation
S = Replace(S, " ", "1") ' space (one dot)
S = Replace(S, ",", "2") ' "comma" (two dots)
S = Replace(S, ".", "3") ' "period" (three dots)

S = Replace(S, "d1", "D1") ' boxed D
S = Replace(S, "d2", "D2") ' boxed D
S = Replace(S, "d3", "D3") ' boxed D
If Right(S, 1) = "d" Then ' boxed D
    S = Left(S, Len(S) - 1) & "D"
End If

'S = Replace(S, "f", "F") ' curly F => Straight F
S = Replace(S, "th", "T") ' Th
S = Replace(S, "ng", "N") ' ng
S = Replace(S, "oo", "o") ' oo -> o
S = Replace(S, "ee", "E") ' ee
S = Replace(S, "1wh", "1hw") ' hwen, hwy, hwere...
S = Replace(S, "2wh", "2hw") ' hwen, hwy, hwere...
S = Replace(S, "3wh", "3hw") ' hwen, hwy, hwere...
If Left(S, 2) = "wh" Then
    S = "hw" & Mid(S, 3, Len(S))
End If
S = Replace(S, "q", "cw") ' q...
If Extra Then
    S = Replace(S, "st", "S")
    S = Replace(S, "ea", "q")
    S = Replace(S, "ae", "A")
    S = Replace(S, "oe", "O")
    S = Replace(S, "oa", "O")
End If
toDwarf = S
End Function

' Function to return mid rotation

' returns Ln characters from the source string, starting at postion Idx
' Ln defaults to the entire string
' Idx defailts to 1 (first character)
' Unlike MID, RMID wraps the source string.  So, RMID("ABC",2) returns "BCA" and RMID("ABC",0) returns "CAB"
' Never returns more characters than were in the original string

Public Function RMID(S As String, Optional Idx As Integer = 1, Optional Ln As Integer = 0) As String
Dim Val As Integer
Val = Len(S)
Select Case Val
    Case 0 ' empty string
        RMID = ""
    Case 1 ' length 1
        RMID = S
    Case Else
        If Ln = 0 Then
            Ln = Val
        End If
        Idx = Idx Mod Val
        If Idx <= 0 Then Idx = Val + Idx
        RMID = Mid(S & S, Idx, Ln)
End Select
End Function

' Function to rotate a string by a count
Public Function Rotate(S As String, Count As Integer) As String
Dim Ln As Integer
Dim Idx As Integer
Ln = Len(S)
If Ln > 0 Then
    ' Make sure count is in range of 0..Ln - 1
    Idx = Count Mod Ln
    If Idx < 0 Then Idx = Idx + Ln
    ' Now return the string starting at the index (wrap around as needed)
    Rotate = Left(Mid(S, Idx + 1, Ln) & S, Ln)
End If
End Function

' General function to match the case of a string to a target string

Public Function MCase(text As String, Match As String, Optional SkipPunct As Boolean) As String
Dim T As Integer
Dim M As Integer
Dim Ch As String
Dim MCh As String
Dim Wrk As String
T = 1
M = 1
While M <= Len(Match) Or T <= Len(text)
    MCh = Mid(Match, M, 1)
    Ch = Mid(text, T, 1)
    If MCh >= "A" And MCh <= "Z" Then ' Match has upper case
        Ch = UCase(Ch)
    ElseIf MCh >= "a" And MCh <= "z" Then ' Match has lower case
        Ch = LCase(Ch)
    ElseIf MCh <> Ch And MCh <> "" Then ' Pass punctuation and other characters from match through
        If Not SkipPunct Then
            Wrk = Wrk & MCh
        End If
        GoTo Continue
    'Else ' same punctuation in both strings...
    End If
    Wrk = Wrk & Ch ' Source character from text in same case as match
    T = T + 1 ' Next character in text
Continue:
    M = M + 1 ' Next character in Match
Wend
MCase = Wrk
End Function

' Folds a string to upper case Alpha and removes spaces and other characters
' Repeated Sep characters will be deduped

' ToAlpha("ABC 1+3 XYZ") = "ABCXYZ"
' ToAlpha("ABC 1+3 XYZ","+") = "ABC+XYZ"
' ToAlpha("ABC 1+3 XYZ"," ") = "ABC XYZ"
Public Function ToAlpha(S As String, Optional Sep As String = "") As String
Dim I As Integer
Dim W As String, Ch As String
S = Trim(UCase(S))
For I = 1 To Len(S)
    Ch = Mid(S, I, 1)
    If (Ch >= "A" And Ch <= "Z") Or (Ch = Sep And Ch <> Right(W, 1)) Then
        W = W & Ch
    End If
Next I
ToAlpha = W
End Function

' Folds a string to Digits and removes spaces and other characters
' Repeated Sep characters will be deduped

' ToDigit("ABC 1+3 XYZ") = "13"
' ToDigit("ABC 1+3 XYZ","+") = "1+3"
' ToDigit("ABC 1+3 XYZ"," ") = " 13 "
Public Function ToDigit(S As String, Optional Sep As String = "") As String
Dim I As Integer
Dim W As String, Ch As String
S = Trim(S)
For I = 1 To Len(S)
    Ch = Mid(S, I, 1)
    If (Ch >= "0" And Ch <= "9") Or (Ch = Sep And Ch <> Right(W, 1)) Then
        W = W & Ch
    End If
Next I
ToDigit = W
End Function

' Folds a string to Digits+Letters and removes spaces and other characters
' Repeated Sep characters will be deduped

' ToAnum("ABC 1+3 XYZ") = "ABC13XYZ"
' ToAnum("ABC 1+3 XYZ","+") = "ABC1+3XYZ"
' ToAnum("ABC 1+3 XYZ"," ") = "ABC 13 XYZ"
Public Function ToANum(S As String, Optional Sep As String = "") As String
Dim I As Integer
Dim W As String, Ch As String
S = UCase(Trim(S))
For I = 1 To Len(S)
    Ch = Mid(S, I, 1)
    If (Ch >= "0" And Ch <= "9") Or (Ch >= "A" And Ch <= "Z") Or (Ch = Sep And Ch <> Right(W, 1)) Then
        W = W & Ch
    End If
Next I
ToANum = W
End Function

' Replace individual characters in a string, case sensitive
' ChFrom is the list of characters to replace
' ChTo is the 1-for-1 replacement list
' If ChTo is shorter than ChFrom, the source characters without destination characters will be deleted
Public Function XLAT(S As String, ChFrom As String, Optional ChTo As String = "", Optional intCase As xlType) As String
Dim X As Integer
Dim I As Integer
Dim W As String
Dim Ch As String
Dim CT As Integer
Dim Invert As Boolean
CT = vbBinaryCompare ' Default is case sensitive

If Left(ChFrom, 1) = "^" Then ' "Not - translate everything not in From to Null"
    Invert = True
    ChFrom = Mid(ChFrom, 2, Len(ChFrom))
ElseIf Left(ChFrom, 1) = "\" Then ' Escape - treat next character as literal
    ChFrom = Mid(ChFrom, 2, Len(ChFrom))
End If

Select Case intCase
    Case xlLower, xlBoth, xlupper
        CT = vbTextCompare
        Select Case intCase
            Case xlLower
                ChTo = LCase(ChTo) ' Case insensitive comparison, lower case translation
            'Case xlBoth           ' Case insensitive comparison only
            Case xlupper
                ChTo = UCase(ChTo) ' Case insensitive compare, upper case translation
        End Select
End Select

For I = 1 To Len(S)
    Ch = Mid(S, I, 1)
    X = InStr(1, ChFrom, Ch, CT)
    If X <> 0 Then ' found this character
        If Invert Then
            Ch = ""
        Else
            Ch = Mid(ChTo, X, 1) ' translate
        End If
    End If
    W = W & Ch
Next I
XLAT = W
End Function

' Adds spaces to a string after every Cnt Characters (default 5)
Public Function GroupBy(S As String, Optional Cnt As Integer = 5, Optional Sep As String = " ") As String
Dim I As Integer
Dim W As String, Ch As String
If Sep = "" Then
    Sep = " "
End If
S = Replace(S, Sep, "")
If Cnt = 0 Then
    Cnt = Len(S)
ElseIf Cnt < 0 Then
    Cnt = Sqr(Len(S))
End If
For I = 1 To Len(S) Step Cnt
    W = W & Ch & Mid(S, I, Cnt)
    Ch = Sep
Next I
GroupBy = W
End Function

' Add Separator, only if no character in the separator is present in the text
Public Function AddSep(text As String, Optional Sep As String = " ", Optional Cnt As Integer = 5) As String
Dim P As Integer
AddSep = text
If Sep <> "" Then
    For P = 1 To Len(Sep) ' For every character in the separator
        If InStr(1, text, Mid(Sep, P, 1), vbTextCompare) <> 0 Then ' a character in the separator is in the text, bail
            Exit Function
        End If
    Next P
    AddSep = GroupBy(text, Cnt, Sep)
End If
End Function

' Remove Separator, IF it is present every Cnt characters
' Otherwise, return original text
Public Function UngroupBy(text As String, Optional Sep As String = " ", Optional Cnt As Integer = 5) As String
Dim P As Integer
Dim sLn As Integer
Dim Wrk As String

UngroupBy = text ' Default to original text

' Handle Separator if present
sLn = Len(Sep)
If sLn <> 0 And Cnt > 0 Then
    Wrk = Left(text, Cnt)
    
    ' Scan text to make sure there is a separator every 5 (Cnt) characters.  Build string without separators
    For P = Cnt + 1 To Len(text) Step sLn + Cnt
        If Mid(text, P, sLn) <> Sep Then ' Missed separator, time to bail
            Exit Function
        End If
        Wrk = Wrk & Mid(text, P + sLn, Cnt)
    Next P
    
    ' We found separators at every instance
    UngroupBy = Wrk
End If
End Function

' This function takes an ASCII key and returns an equivalent or inverted key
' keyType-
' -2 : Inverse space separated numbers
' -1 : Inverse alpha - all characters replaced with UNIQUE characters (default)
'  0 : Standard Alpha - All characters replaced with UNIQUE characters
'  1 : All characters replaces with space separated numbers

' AltKey("BAA", -2) = 2 3 1
' AltKey("BAA", -1) = BCA
' AltKey("BAA", 0)  = CAB
' AltKey("BAA", 1)  = 3 1 2

Public Function AltKey(Key As String, Optional kType As keyType = keyType.ktAlphaInv) As String
Dim OKey As New OrderedKey
On Error GoTo Catch
AltKey = OKey.Invert(kType, Key)
Exit Function
Catch:
AltKey = Err.Description
End Function

' Splits a string on SplitCh (default " ") into an array of words; returns the Nth word in the string
Public Function Word(text As String, Index As Integer, Optional SplitCh As String) As String
Dim Words() As String
On Error GoTo Catch
If SplitCh = "" Then SplitCh = " "
Words = Split(text, SplitCh)
Word = Words(Index - 1)
Exit Function
Catch:
Word = ""
End Function

