Attribute VB_Name = "Base"
Option Explicit
Option Compare Binary
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Base
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime", "Microsoft XML, V6.0"
'
' USAGE:
'
'       Translation Functions that perform binary operations
'
' GROUPS:
'
'       Binary
'            BitNot
'            Deprecated 2013: BitAnd, BitOr, BitXor (Microsoft added)
'
'       Base
'            EncodeBase64, DecodeBase64
'            EncodeBase32, DecodeBase32
'
'       ASCII
'            strASCII
'
'       XOR
'            XorStr, XorEncode
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 06/18/2019 Chip Bloch    -Added Base64
' 06/20/2019 Chip Bloch    -Added Base32
' 07/12/2020 Chip Bloch    -Prevalidate Base64 Decode
' 07/16/2020 Chip Bloch    -Filter CR/LF from invalid characters in base64 decode
' 07/26/2020 Chip Bloch    -Migrated BitNot from General
' 08/16/2020 Chip Bloch    -Added Xalphabet functions
' 08/18/2020 Chip Bloch    -Migrated XORStr, XorEncode from Cipher
' 08/19/2020 Chip Bloch    -Added additional parms for XorEncode (Alphabets, filter)
' 08/20/2020 Chip Bloch    -Fixed bug in LoadAlpha when duplicate characters were passed
' 10/03/2020 Chip Bloch    -Added Public modifier to EncodeBase64
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
' 01/31/2021 Chip Bloch    -Changed XOREncode default for non-alphabetic characters to "Omit"
' 04/25/2021 Chip Bloch    -Migrated xAlpha32, xAlphaXOR, xAlpha32Hex, xAlphaPunct to Globals
'-------------------------------------------------------------------------------------------

Public Enum xa_Alpha
    xa_Standard = 0
    xa_Base32 = 1
    xa_Hex = 2
    xa_Punct = 3
End Enum

' ******************** Binary Functions **********************

' Binary AND (2013)
'Public Function BitAnd(A As Long, B As Long) As Long
'BitAnd = A And B
'End Function

' Binary OR (2013)
'Public Function BitOr(A As Long, B As Long) As Long
'BitOr = A Or B
'End Function

' Binary XOR (2013)
'Public Function BitXor(A As Long, B As Long) As Long
'BitXor = A Xor B
'End Function

' Binary NOT
Public Function BitNot(A As Long) As Long
BitNot = Not A
End Function

' ******************** Base Functions **********************
' Encode a base 64 string
' Requires a reference to Microsoft.XML, v6.0 (Tools, References)
' Microsoft DOC
' https://docs.microsoft.com/en-us/previous-versions/windows/desktop/ms764730(v=vs.85)

Public Function EncodeBase64(text As String) As String
  Dim ArrData() As Byte
  On Error GoTo Catch
  ArrData = StrConv(text, vbFromUnicode)

  Dim objXML As MSXML2.DOMDocument60
  Dim objNode As MSXML2.IXMLDOMElement

  Set objXML = New MSXML2.DOMDocument60
  Set objNode = objXML.createElement("b64")

  objNode.DataType = "bin.base64"
  objNode.nodeTypedValue = ArrData
  EncodeBase64 = objNode.text

  Set objNode = Nothing
  Set objXML = Nothing
  Exit Function
Catch:
  EncodeBase64 = Err.Description
End Function

Public Function DecodeBase64(strData As String) As String
    Dim objXML As MSXML2.DOMDocument60
    Dim objNode As MSXML2.IXMLDOMElement
    Dim I As Integer
    Dim skip As Integer
    Dim Pad As Integer
    Dim Ln As Integer
    Dim Ch As String
    Dim EOS As Boolean
    On Error GoTo Catch
    
    ' Prevalidate to catch errors
    Ln = Len(strData)
    For I = 1 To Ln
        Ch = Mid(strData, I, 1)
        Select Case Ch
            Case Chr(10), Chr(13), " " ' ignore characters
                skip = skip + 1
            Case "A" To "Z", "a" To "z", "0" To "9", "+", "-" ' valid character
                If EOS Then
                    DecodeBase64 = "Source character [" & Ch & "] found after padding character at position " & I & " in text"
                    Exit Function
                End If
            Case "=" ' Padding character, only at end
                Pad = Pad + 1
                If Pad > 3 Then
                    DecodeBase64 = "Too many padding characters [=] found before end of string at position " & I & " in text"
                    Exit Function
                End If
                EOS = True
            Case Else
                DecodeBase64 = "Invalid Character [" & Ch & "] found at position " & I & " in text"
                Exit Function
        End Select
    Next I
    
    ' Auto pad out to four characters
    Ln = Ln - skip
    I = Ln Mod 4
    If I <> 0 Then
        I = 4 - I
        If I + Pad > 3 Then
            DecodeBase64 = "Unable to add padding characters at end of string due to existing pad"
            Exit Function
        End If
        strData = strData & String$(I, "=")
    End If
    
    ' help from MSXML
    Set objXML = New MSXML2.DOMDocument60
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.text = strData
    'DecodeBase64 = objNode.nodeTypedValue
    DecodeBase64 = StrConv(objNode.nodeTypedValue, vbUnicode)
  
    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing
    Exit Function
Catch:
    DecodeBase64 = Err.Description
End Function

' Loads the alphabet into a dictionary; returns true if the alphabet is the default alphabet

Private Function loadAlpha_(ByRef Dict As Scripting.Dictionary, Alpha As Variant, Default As String) As Boolean
Dim I As Integer, J As Integer
Dim strAlpha As String
Dim Ch As String

If IsMissing(Alpha) Or IsEmpty(Alpha) Then
    strAlpha = Default
ElseIf VarType(Alpha) = vbString Then
    If Alpha = "" Then
        strAlpha = Default
    Else
        strAlpha = Alpha
    End If
ElseIf VBA.IsNumeric(Alpha) Then
    I = Alpha
    Select Case I
        Case xa_Standard
            strAlpha = xa_Alphabet
        Case xa_Base32
            strAlpha = xa_Alphabet32
        Case xa_Hex
            strAlpha = xa_AlphabetTri
        Case xa_Punct
            strAlpha = xa_AlphabetTxt
        Case Else
            Err.Raise 629, "Base:loadAlpha", "Unknown value for default alphabet: " & Alpha
    End Select
Else
    Err.Raise 630, "Base:loadAlpha", "Unknown Variable type for Alphabet :" & VarType(Alpha)
End If

loadAlpha_ = strAlpha = Default

' Load Dictionary
Dict.CompareMode = TextCompare

For I = 1 To Len(strAlpha)
    Ch = Mid(strAlpha, I, 1)
    If Dict.Exists(Ch) Then
        Err.Raise 631, "Base:loadAlpha", "Duplicate character """ & Ch & """ in: [" & strAlpha & "]"
    End If
    Dict.Add Ch, I - 1
Next I

If Dict.Count <> 32 Then
    Err.Raise 632, "Base:loadAlpha", "Alphabet is not 32 characters long: [" & strAlpha & "]"
End If
End Function

Public Function EncodeBase32(strData As String, Optional Alpha As Variant) As String
Dim Binary As String
Dim I As Integer
Dim J As Integer
Dim Ln As Integer
Dim V As Integer
Dim XORAlpha As New Scripting.Dictionary
Dim Bloc As String
Dim Chr As String
Dim Wrk As String
Dim defPad As String

On Error GoTo Catch

If loadAlpha_(XORAlpha, Alpha, xa_Alphabet32) Then
    defPad = "="
End If

For I = 1 To Len(strData) Step 5
    ' Peel off 5 characters at a time
    Bloc = Mid(strData, I, 5)
    Ln = Len(Bloc)
    ' Build a 40 bit binary string
    Binary = ""
    For J = 1 To 5
        Chr = Mid(Bloc, J, 1)
        If Chr = "" Then
            V = 0
        Else
            V = Asc(Chr)
        End If
        Binary = Binary & Application.WorksheetFunction.Dec2Bin(V, 8)
    Next J
    ' convert 40 bits to eight 5 bit characters
    For J = 0 To 7
        If Ln = 1 And J >= 2 Then
            Chr = defPad
        ElseIf Ln = 2 And J >= 4 Then
            Chr = defPad
        ElseIf Ln = 3 And J >= 5 Then
            Chr = defPad
        ElseIf Ln = 4 And J >= 7 Then
            Chr = defPad
        Else
            V = Application.WorksheetFunction.Bin2Dec(Mid(Binary, J * 5 + 1, 5))
            Chr = XORAlpha.Keys(V)
        End If
        Wrk = Wrk & Chr
    Next J
Next I
EncodeBase32 = Wrk
Exit Function
Catch:
EncodeBase32 = Err.Description
End Function

Public Function DecodeBase32(strData As String, Optional Alpha As Variant) As String
Dim I As Integer
Dim Ln As Integer
Dim V As Integer
Dim Bits As Integer
Dim ActiveBits As Integer
Dim hasPadding As Boolean
Dim Binary As String
Dim Ch As String
Dim Wrk As String
Dim XORAlpha As New Scripting.Dictionary
Dim isDefault As Boolean
Ln = Len(strData)

On Error GoTo Catch

loadAlpha_ XORAlpha, Alpha, xa_Alphabet32

I = 1
While I <= Ln
    ' Peel off 8 characters (40 bits)
    ActiveBits = 0
    Bits = 0
    hasPadding = False
    Binary = ""
    While Bits < 40 And I <= Ln
        Ch = Mid(strData, I, 1)
        If Not XORAlpha.Exists(Ch) Then
            If Ch <= " " Then GoTo NextBits
            If Ch = "=" Then
                hasPadding = True
                V = 0
                GoTo Padding
            End If
            DecodeBase32 = "Invalid Character [" & Ch & "] encountered at position " & I & " in source."
            Exit Function
        End If
        If hasPadding Then
            DecodeBase32 = "Character [" & Ch & "] follows padding [=] at position " & I & " in source."
            Exit Function
        End If
        V = XORAlpha(Ch)
        ActiveBits = ActiveBits + 5
Padding:
        Binary = Binary & Application.WorksheetFunction.Dec2Bin(V, 5)
        Bits = Bits + 5
NextBits:
        I = I + 1
    Wend
    ' round up to 40 bits if needed
    If Bits < 40 Then
        Binary = Binary & String$(40 - Bits, "0")
    End If
    ' now process
    Bits = 0
    While Bits < ActiveBits
        Wrk = Wrk & Chr(Application.WorksheetFunction.Bin2Dec(Mid(Binary, Bits + 1, 8)))
        Bits = Bits + 8
    Wend
Wend
DecodeBase32 = Wrk
Exit Function
Catch:
DecodeBase32 = Err.Description
End Function

' This xors a key to a plaintext string
' A-Z = 0..25, 0-5 = 26-31
' The key text "wraps" if it is shorter than the plaintext
' Does not encipher punctuation
' Runtype parm is currently not used
Public Function XorEncode(Plaintext As String, Key As String, Optional Run As RunType = RunType.rtWrap, Optional Alpha As Variant, Optional AllowNonAlpha As Boolean) As String
Dim I As Integer
Dim J As Integer
Dim X As Integer
Dim Ch As String
Dim W As String
Dim XORAlpha As New Scripting.Dictionary
Dim KeyLen As Integer
Dim StrLen As Integer
Dim Vals() As Integer

On Error GoTo Catch

loadAlpha_ XORAlpha, Alpha, xa_Alphabet ' Default for XOREncode

StrLen = Len(Plaintext)

' Array to hold key
ReDim Vals(1 To Application.Max(Len(Key), StrLen))

' Parse key into array (ignores characters not in alphabet)
For J = 1 To Len(Key)
    Ch = Mid(Key, J, 1)
    If XORAlpha.Exists(Ch) Then
        KeyLen = KeyLen + 1
        Vals(KeyLen) = XORAlpha(Ch)
    End If
Next J

' Now parse string
If KeyLen > 0 Then
    J = 1
    For I = 1 To Len(Plaintext)
        Ch = Mid(Plaintext, I, 1)
        If Not XORAlpha.Exists(Ch) Then
            If AllowNonAlpha Then
                W = W & Ch ' Other characters pass through Unchanged
            End If
        Else
            X = XORAlpha(Ch) ' Index of character
            If Run >= rtAutokey And KeyLen < StrLen Then
                KeyLen = KeyLen + 1
                Vals(KeyLen) = X
            End If
            X = X Xor Vals(J)
            If Run <= rtPosttext And KeyLen < StrLen Then
                KeyLen = KeyLen + 1
                Vals(KeyLen) = X
            End If
            W = W & XORAlpha.Keys(X) ' Key for character
            J = J + 1
            If J > KeyLen Then
                J = 1
            End If
        End If
    Next I
End If

Set XORAlpha = Nothing

XorEncode = W
Exit Function
Catch:
XorEncode = Err.Description
End Function

' Converts a range of Bytes (ASCII values) to text (not NUL character)

Public Function StrAscii(Rng As Range) As String
Dim Cells() As Byte
Dim Cnt As Integer
Dim Cel As Range

'ReDim Cells(1 To Rng.Rows.Count * Rng.Columns.Count)
ReDim Cells(1 To Rng.Count)

For Each Cel In Rng
    If VBA.IsNumeric(Cel) Then ' It's a number
        If Cel.Value2 >= 1 And Cel.Value2 <= 255 Then ' It's a byte
            Cnt = Cnt + 1
            Cells(Cnt) = Cel.Value2
        End If
    End If
Next

StrAscii = StrConv(Cells, vbUnicode)
End Function

' This xors an ASCII string to a plaintext string
' The key text "wraps" if it is shorter than the plaintext
' Runtype parm is currently not used
Public Function XorStr(Plaintext As String, ASCII As String, Optional Run As RunType = RunType.rtWrap) As String
Dim I As Integer
Dim J As Integer
Dim Ch As String
Dim W As String
Dim KeyLen As Integer

If ASCII = "" Then
    XorStr = Plaintext
    Exit Function
End If
' Now parse string

KeyLen = Len(ASCII)
J = 1
For I = 1 To Len(Plaintext)
    Ch = Chr(Asc(Mid(Plaintext, I, 1)) Xor Asc(Mid(ASCII, J, 1)))
    If Ch <> Chr(0) Then
        W = W & Ch
        J = J + 1
        If J > KeyLen Then
            J = 1
        End If
    End If
Next I
XorStr = W
End Function
