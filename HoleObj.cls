VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "HoleObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       HoleObj
'
'USAGE:
'       Binds information about a hole (row, column, index)
'
'GROUPS:
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 01/09/2021 Chip Bloch    -Original Version
'-------------------------------------------------------------------------------------------

Public MaxRC As Integer ' Max row or column
Public Idx As Integer ' Index of hole in the Grille (1-based)
Public Row As Integer ' Row to find Hole
Public Col As Integer ' Column to find Hole
Public Center As Boolean

'Private Sub Class_Initialize()
'End Sub

' Initialize based on row/column
Public Function InitRC(R, C, RC) As HoleObj
Dim MidLn As Integer
If RC < 3 Then
    Err.Raise 1101, "HoleObj.InitRC", "Max Row/Col out of range, must be be >= 3 :" & RC
End If
MaxRC = RC
Row = R
Col = C
If Row < 1 Or Row > RC Then
    Err.Raise 1102, "HoleObj.InitRC", "Row out of range, must be between 1 and " & RC & " :" & Row
End If
If Col < 1 Or Col > RC Then
    Err.Raise 1103, "HoleObj.InitRC", "Col out of range, must be between 1 and " & RC & " :" & Col
End If
Idx = (Row - 1) * MaxRC + Col
MidLn = (RC + 1) \ 2
Center = RC Mod 2 = 1 And Row = MidLn And Col = MidLn
Set InitRC = Me
End Function

' Initialize based on index
Public Function InitIdx(I, RC) As HoleObj
Dim MidLn As Integer
If RC < 3 Then
    Err.Raise 1101, "HoleObj.InitIdx", "Max Row/Col out of range, must be be >= 3 :" & RC
End If
MaxRC = RC
If I > RC * RC Then
    Err.Raise 1104, "HoleObj.InitIdx", "Index out of range, must be between 1 and " & RC * RC
End If
Idx = I
Row = (Idx - 1) \ MaxRC + 1
Col = (Idx - 1) Mod MaxRC + 1
MidLn = (RC + 1) \ 2
Center = RC Mod 2 = 1 And Row = MidLn And Col = MidLn
Set InitIdx = Me
End Function

'Public Function toQuad(Q As Integer) As HoleObj
'toQuad = Me
'End Function

' Rotate right
Public Function nextQuad() As HoleObj

If Not Center Then
    Idx = Idx - 1
    Row = Idx Mod MaxRC + 1
    Col = MaxRC - (Idx \ MaxRC)
    'Quad = (Quad + 1) Mod 4
    Idx = (Row - 1) * MaxRC + Col
End If

Set nextQuad = Me
End Function

' Rotate Left
Public Function PrevQuad() As HoleObj

If Not Center Then
    Idx = Idx - 1
    Row = MaxRC - (Idx Mod MaxRC)
    Col = Idx \ MaxRC + 1
    'Quad = (Quad + 3) Mod 4
    Idx = (Row - 1) * MaxRC + Col
End If

Set PrevQuad = Me
End Function

' Rotate x2 (Far Quadrant)
Public Function FarQuad() As HoleObj

If Not Center Then
    Idx = Idx - 1
    Row = MaxRC - (Idx \ MaxRC)
    Col = MaxRC - (Idx Mod MaxRC)
    'Quad = (Quad + 2) Mod 4
    Idx = (Row - 1) * MaxRC + Col
End If

Set FarQuad = Me
End Function
