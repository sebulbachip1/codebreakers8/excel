Attribute VB_Name = "Pontifex"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

' Pontifex (Solitaire) Code routines

'-------------------------------------------------------------------------------------------
'CLASS:
'       Pontifex
'
'USAGE:
'       Routines for Solitaire Cipher, AKA Pontifex
'
'GROUPS:
'       Pontifex
'            Newdeck
'            Keygen - returns Keyed Deck
'            Solitaire - returns keystream
'            Cards2Arr, Arr2Cards - Converts deck from text to csv
'            encSolitaire, decSolitaire
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 10/??/2012 Chip Bloch    -Original version
' 10/04/2020 Chip Bloch    -Added encSolitaire, decSolitaire, optional A/B for card arrays
'-------------------------------------------------------------------------------------------

' Generates a "deck" of cards from a string
' A deck is defined as...
' Rank ordering of cards in suits (A=1, K=13)
' Bridge ordering of suits (clubs, Diamonds, Hearts, Spades)
' Add 13 for each higher suit over clubs
' Ten (10) is coded as "T"
' Low joker(53) is LO, High joker(54) is HI
' All cards numbered from 1-54

Private Function InitDeck_(cards As String) As String
Dim I As Integer
Dim V As Integer
Dim W As String
Dim Ch As String
I = 0
cards = UCase(cards)
Do While I < Len(cards)
    I = I + 1
    Do
        Ch = Mid(cards, I, 1)
        Select Case Ch
            Case "A"
                V = 1
            Case "2" To "9"
                V = Asc(Ch) - Asc("0")
            Case "T"
                V = 10
            Case "J"
                V = 11
            Case "Q"
                V = 12
            Case "K"
                V = 13
            Case "L"
                V = 53
            Case "H"
                V = 54
            Case Else
                Exit Do
        End Select
        I = I + 1
        Ch = Mid(cards, I, 1)
        Select Case Ch
            Case "S"
                V = V + 39
            Case "H"
                V = V + 26
            Case "D"
                V = V + 13
            Case "C", "O", "I"
                V = V + 0
            Case Else
                Exit Do
        End Select
        W = W & Chr(V)
        'Exit Do
    Loop While False
Loop
If Len(W) <> 54 Then
    Err.Raise 721, "InitDeck_", "Error: Invalid deck string"
End If
InitDeck_ = W
End Function

' Generates a card stream from a deck
Private Function Cardstream_(Deck As String) As String
Dim I As Integer
Dim V As Integer
Dim S As Integer
Dim Ch As String
Dim W As String
For I = 1 To Len(Deck)
    If W <> "" Then W = W & " "
    Ch = Mid(Deck, I, 1)
    V = Asc(Ch)
    Select Case V
        Case 54:
            W = W & "HI"
        Case 53:
            W = W & "LO"
        Case Else
            V = V - 1
            S = Int(V / 13)
            V = (V Mod 13) + 1
            Select Case V
                Case 1:
                    W = W & "A"
                Case 10:
                    W = W & "T"
                Case 11:
                    W = W & "J"
                Case 12:
                    W = W & "Q"
                Case 13:
                    W = W & "K"
                Case Else
                    W = W & Chr(V + Asc("0"))
            End Select
            Select Case S
                Case 0:
                    W = W & "C"
                Case 1:
                    W = W & "D"
                Case 2:
                    W = W & "H"
                Case 3:
                    W = W & "S"
            End Select
    End Select
Next I
Cardstream_ = W
End Function

' Generates a single character from the deck

Private Function Step_(ByRef Deck As String) As String
Dim I As Integer
Dim J As Integer
Dim T As Integer
Dim LO As String
Dim Hi As String
Dim Ch As String
LO = Chr(53)
Hi = Chr(54)
One:    ' Move Lo Joker
    I = InStr(Deck, LO)
    If I = 54 Then
        Deck = Mid(Deck, 1, 1) & LO & Mid(Deck, 2, 52)
    Else
        Deck = Mid(Deck, 1, I - 1) & Mid(Deck, I + 1, 1) & LO & Mid(Deck, I + 2, 54)
    End If
two:    ' Move Hi Joker
    J = InStr(Deck, Hi)
    If J = 53 Then
        Deck = Mid(Deck, 1, 1) & Hi & Mid(Deck, 2, 51) & Mid(Deck, 54, 1)
    ElseIf J = 54 Then
        Deck = Mid(Deck, 1, 2) & Hi & Mid(Deck, 3, 51)
    Else
        Deck = Mid(Deck, 1, J - 1) & Mid(Deck, J + 1, 2) & Hi & Mid(Deck, J + 3, 54)
    End If
Three:  ' Triple Cut
    I = InStr(Deck, LO)
    J = InStr(Deck, Hi)
    If J < I Then
        T = J
        J = I
        I = T
    End If
    Deck = Mid(Deck, J + 1, 54) & Mid(Deck, I, J - I + 1) & Mid(Deck, 1, I - 1)
Four:   ' Count Cut
    Ch = Mid(Deck, 54, 1)
    I = Asc(Ch)
    If I < 53 Then ' Not a joker
        Deck = Mid(Deck, I + 1, 53 - I) & Mid(Deck, 1, I) & Ch
    End If
Five:   '
    Ch = Mid(Deck, 1, 1)
    I = Asc(Ch)
    If I > 52 Then
        Ch = Mid(Deck, 54, 1)
    Else
        Ch = Mid(Deck, I + 1, 1)
    End If
Output:
    I = Asc(Ch)
    If I > 52 Then
        GoTo One
    End If
    If I > 26 Then
        I = I - 26
    End If
    Step_ = Chr(I + Asc("@"))
End Function

' Returns a card stream in default order (1-54)
Public Function NewDeck() As String
Dim I As Integer
Dim Deck As String
For I = 1 To 54
    Deck = Deck & Chr(I)
Next I
NewDeck = Cardstream_(Deck)
End Function

' Takes an initial deck ordering and "Keys" it
Public Function KeyGen(cards As String, Key As String) As String
Dim I, J As Integer
Dim Deck As String
Dim Ch As String
Dim W As String
On Error GoTo Catch
Deck = InitDeck_(cards)
Key = UCase(Key)
For I = 1 To Len(Key)
    Ch = Mid(Key, I, 1)
    If Ch >= "A" And Ch <= "Z" Then
        ' Ignore return result from this...
        Step_ Deck
        ' Extra count cut
        J = Asc(Ch) - Asc("@")
        Ch = Mid(Deck, 54, 1)
        Deck = Mid(Deck, J + 1, 53 - J) & Mid(Deck, 1, J) & Ch
    End If
Next I
KeyGen = Cardstream_(Deck)
Exit Function
Catch:
KeyGen = Err.Description
End Function

' Generates a keystream for the solitaire cipher (can throw error)
Private Function Solitaire_(cards As String, Count As Integer) As String
Dim I As Integer
Dim Deck As String
Dim W As String
Deck = InitDeck_(cards)
For I = 1 To Count
    W = W & Step_(Deck)
Next I
Solitaire_ = W
End Function

' Generates a keystream for the solitaire cipher
Public Function Solitaire(cards As String, Count As Integer) As String
On Error GoTo Catch
Solitaire = Solitaire_(cards, Count)
Exit Function
Catch:
Solitaire = Err.Description
End Function

' Returns a card stream as csv
Public Function Cards2Arr(cards As String, Optional dcode As Boolean = False) As String
Dim I
Dim V
Dim Deck As String
Dim W As String
On Error GoTo Catch
Deck = InitDeck_(cards)
For I = 1 To Len(Deck)
    If W <> "" Then
        W = W & ","
    End If
    V = Asc(Mid(Deck, I, 1))
    If dcode And V >= 53 Then
        W = W & Chr(V + 12) ' A or B
    Else
        W = W & V
    End If
Next I
Cards2Arr = W
Exit Function
Catch:
Cards2Arr = Err.Description
End Function

' Returns a csv as a card stream
Public Function Arr2Cards(Arr As String) As String
Dim I As Integer
Dim Ch As String
Dim J As Integer
Dim Deck As String
Dim CVal() As String
CVal = Split(Arr, ",")
For I = 0 To UBound(CVal)
    Ch = CVal(I)
    Select Case Ch
        Case "A", "B"
            J = Asc(Ch) - 12
        Case Else
            J = Ch
    End Select
    If J > 0 And J < 55 Then
        Deck = Deck & Chr(J)
    End If
Next I
Arr2Cards = Cardstream_(Deck)
End Function

' Solitaire encryption.  Will pad to multiple of 5 with "X" and optionally insert separator every 5 characters
Public Function encSolitaire(text As String, Deck As String, Optional Sep As String = "") As String
Dim ln As Integer
Dim Wrk As String
On Error GoTo Catch
text = ToAlpha(text)
ln = Len(text) Mod 5
If ln <> 0 Then
    text = text & String(5 - ln, "X")
End If
Wrk = Solitaire_(Deck, Len(text))
Wrk = Encrypt(text, Wrk)
If Sep <> "" Then
    Wrk = GroupBy(Wrk, , Sep)
End If
encSolitaire = Wrk
Exit Function
Catch:
encSolitaire = Err.Description
End Function

' Solitaire decryption.
Public Function decSolitaire(text As String, Deck As String) As String
Dim ln As Integer
Dim Wrk As String
On Error GoTo Catch
text = ToAlpha(text)
Wrk = Solitaire_(Deck, Len(text))
Wrk = Decrypt(text, Wrk)
decSolitaire = Wrk
Exit Function
Catch:
decSolitaire = Err.Description
End Function
