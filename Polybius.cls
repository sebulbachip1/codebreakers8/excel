VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Polybius"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       Polybius (base for a number of the "square" ciphers)
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime"
'
'USAGE:
'       Translates a string or a range into a Polybius Square.  Provides routines for
'       searching the square for a given character, returning the row and column
'
'       Advantage is that square is processed only one time and the cached version is used
'       after that
'
'       Caller must trap errors.
'
'GROUPS:
'       Polybius
'            Make, Row, Col, chrVal, RC, encString, decString, Rows, Cols, Exists, Filter
'            NullRowCh, Alphabet, RUp, RDown, CUp, CDown
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 06/27/2020 Chip Bloch    -Original Version
' 07/12/2020 Chip Bloch    -Calling range2str (externalized)
'                          -Created LetterInfo type
'                          -Renamed Variables
' 07/21/2020 Chip Bloch    -Extended to allow 6x6 square
' 07/31/2020 Chip Bloch    -Added encString (need to write decString)
' 08/02/2020 Chip Bloch    -Added decString
' 08/06/2020 Chip Bloch    -Modified polybius make method (prep for moving column names)
' 08/08/2020 Chip Bloch    -Reordered parms for Polybius Make
' 08/23/2020 Chip Bloch    -Modified tList.Make to allow column/row names a multiple of length
' 08/25/2020 Chip Bloch    -Corrected encstring to not emit output for punctuation
'                          -Corrected bug with space in square
' 09/18/2020 Chip Bloch    -Added vtUpperASCII... Need to move to Scripting.Dictionary
'                          -Added Exists, Strip
'                          -Converted to Scripting.Dictionary
' 09/19/2020 Chip Bloch    -Made disctionary case insensitive for all but ASCII
'                          -Folded Strip into Filter
' 09/21/2020 Chip Bloch    -Added null row name ('�') for MONDI, hasNull flag
' 09/25/2020 Chip Bloch    -Added # escape to encString, decString
' 09/26/2020 Chip Bloch    -Added / escape to encString, decString
' 09/29/2020 Chip Bloch    -Disallowed <escape><digits...><escape> when / appears on row 1
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
' 11/23/2020 Chip Bloch    -Added RUp/RDown/CUp/CDown
' 12/04/2020 Chip Bloch    -Added Dynamic Square Size based on key (make)
' 12/05/2020 Chip Bloch    -Added Values property
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType
'                          -Added PolyVals
' 12/31/2020 Chip Bloch    -Added error check for slash escape having identical row/column
'                           name in encString/decString
' 01/03/2021 Chip Bloch    -Added ttASCIIAny (ASCII, allow dups in square)
' 02/06/2021 Chip Bloch    -Added Alternating Row/Columns for encString, decString
' 02/07/2021 Chip Bloch    -Added Alternating Row/Columns for Filter
' 03/25/2021 Chip Bloch    -Added OutCh to decString, make
' 04/27/2021 Chip Bloch    -Added AlphaPlus
' 04/28/2021 Chip Bloch    -Added SwpCh
'-------------------------------------------------------------------------------------------

'Const Null_ As String = "�"
Const Octothorpe_ As String = "#"
Const NumEscape_ As String = "/"

Const defRCNames As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

' Alphabet
Private Alpha As String
Private bValid As Boolean       ' True after Make is called
Private sChars() As String      ' Characters in square (5x5)
Private pLetter As New Scripting.Dictionary
Private Rowz As Integer         ' Number of rows
Private Colz As Integer         ' Number of columns
Private MP As Integer           ' Max Page
Private Swp() As String         ' Page swap characters
Private VT As textType          ' Expected values of text for square
Private tRowNames As New Scripting.Dictionary  ' Optional row names
Private sRowNames As String
Private iRowCnt As Integer      ' Row Header Count
Private tColNames As New Scripting.Dictionary  ' Optional column names
Private sColNames As String
Private iColCnt As Integer      ' Col Header Count
Private HasNull As Boolean      ' Null Row or Column (must only be one)

' Set arrays (all empty)
Private Sub Class_Initialize()
bValid = False
'ReDim pLetter(96)
End Sub

Private Sub Class_Terminate()
Set pLetter = Nothing
Set tRowNames = Nothing
Set tColNames = Nothing
End Sub

' Row/Column Name Routines ****************************************************************************************************

Private Sub MakeList_(ByRef tList As Scripting.Dictionary, ByRef Names As String, S As String, Max As Integer, iCnt As Integer)
Dim I As Integer
Dim Ln As Integer
Dim Chr As String
Dim Cnt As Integer
Ln = Len(S)
While I < Ln
    I = I + 1
    Chr = Mid(S, I, 1)
    If Not tList.Exists(Chr) Then
        If Chr <> NullCh Or Not HasNull Then
            Names = Names & Chr
            Cnt = Cnt + 1
            ' Column number is index + 1
            If Chr = NullCh Then
                HasNull = True
                Chr = ""
            End If
            tList.Add Chr, Cnt
        End If
    End If
Wend
If Cnt Mod Max <> 0 Then
    Err.Raise 553, "Polybius:Makelist", "Error: Invalid column names [" & Names & "] not multiple of length " & Max
End If
iCnt = Cnt / Max
End Sub

Private Function ListCh_(ByRef tList As Scripting.Dictionary, I As Integer) As String
'If tList.Count = 0 Then
'    Err.Raise 550, "TList:Ch", "List not initialized, call Make(List) before Ch()"
'End If
If I < 1 Or I > tList.Count Then
    Err.Raise 551, "Polybius:ListCh", "Index out of range" & I & ", expecting 1.." & tList.Count
End If
ListCh_ = tList.Keys(I - 1)
End Function

Private Function ListIdx_(ByRef tList As Scripting.Dictionary, Chr As String) As Integer
'If tList.Count = 0 Then
'    Err.Raise 550, "TList:Idx", "List not initialized, call Make(List) before Idx()"
'End If
If tList.Exists(Chr) Then
    ListIdx_ = tList(Chr)
End If
End Function

Private Sub InitDefCols_()
If tColNames.Count = 0 Then
    MakeList_ tColNames, sColNames, defRCNames, Colz, iColCnt
'    tColNames.Make defRCNames, Colz
End If
End Sub

Private Sub InitDefRows_()
If tRowNames.Count = 0 Then
    If Rowz <> Colz Then
        MakeList_ tRowNames, sRowNames, defRCNames, Rowz, iRowCnt
        'Call tRowNames.Make(defRCNames, Rowz)
    Else
        Set tRowNames = tColNames
        iRowCnt = iColCnt
        'sRowNames = sColNames
    End If
End If
End Sub

' Returns next higher row (wraps)
Property Get RUp(I As Integer) As Integer
I = I + 1
If I > Rowz Then I = 1
RUp = I
End Property

' Returns next lower row (wraps)
Property Get RDown(I As Integer) As Integer
I = I - 1
If I = 0 Then I = Rowz
RDown = I
End Property

' Returns next higher col (wraps)
Property Get CUp(I As Integer) As Integer
I = I + 1
If I > Colz Then I = 1
CUp = I
End Property

' Returns next lower col (wraps)
Property Get CDown(I As Integer) As Integer
I = I - 1
If I = 0 Then I = Colz
CDown = I
End Property

'Property Get NullCh() As String
'NullCh = NullCh
'End Property

Property Let Colnames(S As String)
If Not bValid Then
    Err.Raise 528, "Polybius:ColNames", "Square not initialized, call Make(values...) before ColNames()"
End If
MakeList_ tColNames, sColNames, S, Colz, iColCnt
End Property

Property Get Colnames() As String
If Not bValid Then
    Err.Raise 528, "Polybius:ColNames", "Square not initialized, call Make(values...) before ColNames()"
End If
' InitCols
Colnames = sColNames
End Property

Property Let RowNames(S As String)
If Not bValid Then
    Err.Raise 528, "Polybius:RowNames", "Square not initialized, call Make(values...) before RowNames()"
End If
MakeList_ tRowNames, sRowNames, S, Rowz, iRowCnt
'tRowNames.Make S, Rowz
End Property

Property Get RowNames() As String
If Not bValid Then
    Err.Raise 528, "Polybius:RowNames", "Square not initialized, call Make(values...) before RowNames()"
End If
'InitRows
If tColNames Is tRowNames Then
    RowNames = sColNames
Else
    RowNames = sRowNames
End If
'RowNames = tRowNames.toString
End Property

'Encode a string to row and column names
Public Function encString(text As String, Optional FT As fltType = ftSquare) As String
Dim I As Integer
Dim P As Integer
Dim BlkLen As Integer
Dim R As Integer, C As Integer
Dim Shift As Integer
Dim Ln As Integer
Dim CP As Integer
Dim LI As LetterObj
Dim Ch As String
Dim DI As String
Dim Work As String
Dim EscCh As String
Dim Tail As String
Dim InEscape As Boolean
Dim Escapeable As Boolean
Dim SlashEscape As Boolean
Dim HashEscape As Boolean
Dim AnyEscape As Boolean
Dim OutCh As Boolean
Dim RCFirst As Integer

If Not bValid Then
    Err.Raise 528, "Polybius:encString", "Square not initialized, call Make(values...) before encString()"
End If

' Initialize row/column names to default values if needed
InitDefCols_
InitDefRows_

CP = 0

Select Case FT
    Case ftSqplus
        SlashEscape = pLetter.Exists(NumEscape_)
        HashEscape = pLetter.Exists(Octothorpe_) And Not SlashEscape
        AnyEscape = HashEscape Or SlashEscape
        If SlashEscape Then
            EscCh = NumEscape_
            Set LI = pLetter(EscCh)
            R = LI.Row
            C = LI.Col
            ' Can this use double letters?
            Escapeable = ListCh_(tRowNames, R) <> ListCh_(tColNames, C) And R > 1
        ElseIf HashEscape Then
            EscCh = Octothorpe_
        End If
    Case ftSqRC
        RCFirst = 1
    Case ftSqCR
        RCFirst = 2
    Case ftColRows
        RCFirst = 3
End Select

Ln = Len(text)
For I = 1 To Ln
    Ch = Mid(text, I, 1)
    If Not pLetter.Exists(Ch) Then
        If Not (AnyEscape And Ch >= "0" And Ch <= "9") Then
            Err.Raise 529, "Polybius:encString", "Character not in Square [" & Ch & "]"
        End If
        If HashEscape Or (SlashEscape And Not InEscape) Then ' Append Escape Character
            Set LI = pLetter(EscCh)
            R = LI.Row
            C = LI.Col
            Work = Work & ListCh_(tRowNames, R) & ListCh_(tColNames, C)
        End If
        If Escapeable Then ' need to double number
            InEscape = True
            Work = Work & Ch
        End If
        Work = Work & Ch
    Else
        If InEscape Then
            Set LI = pLetter(EscCh)
            R = LI.Row
            C = LI.Col
            Work = Work & ListCh_(tRowNames, R) & ListCh_(tColNames, C)
            InEscape = False
        End If
        Set LI = pLetter(Ch)
        If LI.Idx <> CP Then ' insert swap character
            Ch = Swp(CP)
            CP = 1 - CP
            Set LI = pLetter(Ch)
            I = I - 1
        End If
        R = LI.Row
        If R = 0 Then ' could be escape character; check if final position
            If I = Ln - 1 Then ' it was (ignore escape characters before last character)
                OutCh = True
            End If
        Else
            C = LI.Col
            If iRowCnt > 1 Then
                R = R + Int(Rnd() * iRowCnt) * Rowz
            End If
            If iColCnt > 1 Then
                C = C + Int(Rnd() * iColCnt) * Colz
            End If
            Select Case RCFirst
                Case 0
                    DI = ListCh_(tRowNames, R) & ListCh_(tColNames, C)
                Case 1
                    DI = ListCh_(tRowNames, R) & ListCh_(tColNames, C)
                    RCFirst = 2
                Case 2
                    DI = ListCh_(tColNames, C) & ListCh_(tRowNames, R)
                    RCFirst = 1
                Case 3
                    DI = ListCh_(tColNames, C) & ListCh_(tRowNames, R)
            End Select
            If OutCh Then DI = Left(DI, 1) ' drop final digit
            Work = Work & DI
        End If
    End If
Next I
encString = Work
End Function

' Decode a string function from row and column names

Public Function decString(text As String, Optional FT As fltType = fltType.ftRowCols, Optional OutCh As String) As String
Dim I As Integer
Dim P As Integer
Dim BlkLen As Integer
Dim lt As Integer
Dim RC As String
Dim CC As String
Dim Ch As String
Dim CP As Integer
Dim Work As String
Dim EscCh As String
Dim LI As LetterObj
Dim SingleEscape As Boolean
Dim DoubleEscape As Boolean
Dim AnyEscape As Boolean
Dim InEscape As Boolean
Dim LastCh As Boolean
Dim RCFirst As Integer
If Not bValid Then
    Err.Raise 528, "Polybius:decString", "Square not initialized, call Make(values...) before decString()"
End If

InitDefCols_
InitDefRows_

lt = Len(text)

'NumEscape = pLetter.Exists(Octothorpe_) And FT = ftSqplus
Select Case FT
    Case ftSqplus
        If pLetter.Exists(NumEscape_) Then
            EscCh = NumEscape_
            Set LI = pLetter(EscCh)
            DoubleEscape = ListCh_(tRowNames, LI.Row) <> ListCh_(tColNames, LI.Col) And LI.Row > 1
            SingleEscape = Not DoubleEscape
        ElseIf pLetter.Exists(Octothorpe_) Then
            EscCh = Octothorpe_
            SingleEscape = True
        End If
        AnyEscape = SingleEscape Or DoubleEscape
    Case ftColRows
        RCFirst = 3
    Case ftSqRC
        RCFirst = 1
    Case ftSqCR
        RCFirst = 2
End Select

CP = 0

' parse text out by column
If HasNull Then
    I = 1
    While I <= lt
        RC = Mid(text, I, 1) ' Left character (Row)
        If InEscape Then
            If I = lt Then
                Err.Raise 540, "Polybius:decString", "Error: missing numeric second digit at position (" & I & ") '" & RC & "'"
            Else
                I = I + 1
                CC = Mid(text, I, 1)
            End If
            If RC = CC Then ' It's a digit
                Work = Work & RC
            Else ' should be numeric exit code
                Ch = Me.chrVal((ListIdx_(tRowNames, RC) - 1) Mod Rowz + 1, (ListIdx_(tColNames, CC) - 1) Mod Colz + 1)
                If Ch <> EscCh Then
                    Err.Raise 540, "Polybius:decString", "Error: missing numeric escape position (" & I & ") '" & Ch & "'"
                End If
                InEscape = False
            End If
            I = I + 1
        Else
            If Not tRowNames.Exists(RC) Then ' null row name
                CC = RC
                RC = ""
            Else
                I = I + 1
                If I > lt Then
                    If OutCh = "" Then
                        Err.Raise 540, "Polybius:decString", "Error: no corresponding column character for final row character '" & RC & "'"
                    Else
                        OutCh = Left(OutCh, 1)
                        If Me.Exists(OutCh) Or VBA.IsNumeric(OutCh) Then
                            Err.Raise 540, "Polybius:decString", "Error: Out Character exists in square '" & OutCh & _
                                "' and no column character for final row character '" & RC & "'"
                        End If
                        Work = Work & OutCh
                        CC = 0
                        LastCh = True
                    End If
                Else
                    CC = Mid(text, I, 1)
                End If
            End If
            Ch = Me.chrVal((ListIdx_(tRowNames, RC) - 1) Mod Rowz + 1, (ListIdx_(tColNames, CC) - 1) Mod Colz + 1)
            If (Ch <> EscCh) Or LastCh Then
                Work = Work & Ch
            ElseIf SingleEscape Then
                I = I + 1
                If I > lt Then
                    Err.Raise 540, "Polybius:decString", "Error: no corresponding number for final numeric escape"
                End If
                Ch = Mid(text, I, 1)
                Work = Work & Ch
            Else
                InEscape = True
            End If
            I = I + 1
        End If
    Wend
Else ' no nulls
    If lt Mod 2 = 1 Then
        Err.Raise 540, "Polybius:decString", "Error: Odd number of characters in Ciphertext"
    End If
    For I = 1 To lt - 1 Step 2
        Select Case RCFirst
            Case 0
                RC = Mid(text, I, 1) ' Left character (row)
                CC = Mid(text, I + 1, 1) ' Right character (column)
            Case 1
                RC = Mid(text, I, 1) ' Left character (row)
                CC = Mid(text, I + 1, 1) ' Right character (column)
                RCFirst = 2
            Case 2
                CC = Mid(text, I, 1) ' Left character (Col)
                RC = Mid(text, I + 1, 1) ' Right character (Row)
                RCFirst = 1
            Case 3
                CC = Mid(text, I, 1) ' Left character (Column)
                RC = Mid(text, I + 1, 1) ' Right character (Row)
        End Select
        Ch = sChars(CP, (ListIdx_(tRowNames, RC) - 1) Mod Rowz + 1, (ListIdx_(tColNames, CC) - 1) Mod Colz + 1)
        If Ch = Swp(CP) Then ' page swap flag
            CP = 1 - CP ' don't emit swap character
        Else
            Work = Work & Ch
        End If
    Next I
End If
decString = Work
End Function

' Base Polybius Routines ******************************************************************************************************

' Get string into arrays, set bValid flag
' This is procesed only once.  Arrays speed searches after that.
Public Sub Make(SR As Variant, CCnt As Integer, Optional Vals As textType, _
    Optional rCnt As Integer, Optional cNames As String, Optional rNames As String, _
    Optional OutCh As String)
Dim R As Integer
Dim C As Integer
Dim I As Integer
Dim Cnt As Integer
Dim Ch As String
Dim V As Integer
Dim Page As Integer
Dim CP As Integer
Dim Str As String
Dim Cel As Range
Dim TotCnt As Integer

Dim LObj As LetterObj

' Process SR Parameter - must be a String with 25 letters or a Range of characters (hopefully 5x5)
Str = range2str(SR)

If CCnt = -1 Then ' Dynamic: get square size from input alphabet
    Select Case (Len(Str))
        Case Is >= 100  ' 10x10, ASCII #32 - #255 (case sensitive) - Not used yet
            CCnt = 10
            Vals = textType.ttASCII
        Case Is >= 81   ' 9x9, ASCII (case sensitive) - Not used yet
            CCnt = 9
            Vals = textType.ttASCII
        Case Is >= 64   ' 8x8, ASCII (case sensitive) - Not used yet
            CCnt = 8
            Vals = textType.ttASCII
        Case Is = 52    ' 5x5 by 2
            CCnt = 5
            Vals = textType.ttAlphaPlus
        Case Is >= 49   ' 7x7, A-Z (case insensitive); 0-9; Punctuation
            CCnt = 7
            Vals = textType.ttASCIIUpper
        Case Is >= 36   ' 6x6, A-Z (case insensitive); 0-9
            CCnt = 6
            If XLAT(UCase(Str), "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", "") = "" Then
                Vals = textType.ttAnumUpper
            Else
                Vals = textType.ttASCIIUpper
            End If
        Case Else
            CCnt = 5    ' 5x5 A-Z less one letter (default)
            Vals = textType.ttAlphaUpper
    End Select
End If

VT = Vals
Select Case VT
    Case ttASCII
        pLetter.CompareMode = BinaryCompare
    Case ttAnumUpper, ttAlphaUpper, ttASCIIUpper
        pLetter.CompareMode = TextCompare
    Case ttAlphaPlus
        MP = 1
        pLetter.CompareMode = TextCompare
    Case ttASCIIAny
        pLetter.CompareMode = BinaryCompare
    Case Else
        Err.Raise 1121, "Polybius.Make", "Invalid textType: " & Vals
End Select

If rCnt = 0 Then rCnt = CCnt
Colz = CCnt
Rowz = rCnt
ReDim sChars(0 To MP, 1 To Rowz, 1 To Colz)
ReDim Swp(0 To MP)

tColNames.CompareMode = TextCompare
tRowNames.CompareMode = TextCompare

If cNames <> "" Then
    'tColNames.Make cNames, Colz
    MakeList_ tColNames, sColNames, cNames, Colz, iColCnt
    If rNames <> "" Then
        MakeList_ tRowNames, sRowNames, rNames, Rowz, iRowCnt
        'tRowNames.Make rNames, Rowz
    Else
        InitDefRows_
    End If
End If

TotCnt = Rowz * Colz
If VT = ttAlphaPlus Then
    TotCnt = TotCnt * 2 + 2 ' 5x5 * 2
End If

For I = 1 To Len(Str)
    Ch = Mid(Str, I, 1)
    If Ch < " " Then GoTo Continue ' Ignore CR, LF, etc.
    Select Case Vals
        ' Case ValType.vtASCII, ValType.vtUpperASCII
        Case textType.ttAnumUpper, textType.ttAnum
            If Not ((Ch >= "0" And Ch <= "9") Or (UCase(Ch) >= "A" And UCase(Ch) <= "Z")) Then
                GoTo Continue
            End If
        Case textType.ttAlphaUpper, textType.ttAlpha
            If Not (UCase(Ch) >= "A" And UCase(Ch) <= "Z") Then
                GoTo Continue
            End If
    End Select
    Alpha = Alpha & Ch
Continue:
Next I

' Now parse String into pArrays()
R = 1
C = 0
For I = 1 To Len(Alpha)
    Ch = Mid(Alpha, I, 1)
    Cnt = Cnt + 1
    C = C + 1 ' Next column
    If C > Colz Then
        R = R + 1 ' Next row
        If R > Rowz Then
            Page = Page + 1
            R = 1
        End If
        C = 1
    End If
    If Ch <> SwpCh Then
        CP = Page
    Else
        I = I + 1
        Ch = Mid(Alpha, I, 1)
        Cnt = Cnt + 1
        If Ch = "" Then
            Err.Raise 524, "Polybius:Make", "Error: String ends with '" & SwpCh & "' " & Str
        End If
        Swp(CP) = Ch
        CP = 1 - Page
    End If
    Set LObj = New LetterObj
    With LObj
        .Ch = Ch
        .Idx = Page
        .Row = R
        .Col = C
    End With
    If Ch <> NullCh Then
        sChars(Page, R, C) = Ch
    Else
        Ch = Ch & Cnt
        LObj.Ch = ""
    End If
    If Vals = ttASCIIAny Then ' allow dups
        Ch = pLetter.Count
    Else
        If pLetter.Exists(Ch) Then ' Already procesed this letter, error
            Err.Raise 525, "Polybius:Make", "Duplicate Character [" & Ch & "]: " & Str
        End If
    End If
    If Cnt > TotCnt Then ' More than 25 letters
        Err.Raise 526, "Polybius:Make", "String too long, expecting " & TotCnt & " letters: " & Str
    End If
    pLetter.Add Ch, LObj
Next I

If Cnt <> TotCnt Then ' Not enough letters
    Err.Raise 527, "Polybius:Make", "String too short, expecting " & TotCnt & " letters: " & Str
End If

' Bonus characters pass filter...
For I = 1 To Len(OutCh)
    Ch = Mid(OutCh, I, 1)
    If Not pLetter.Exists(Ch) And Not VBA.IsNumeric(Ch) Then
        Set LObj = New LetterObj
        With LObj
            .Ch = Ch
    '        .Idx = Cnt
    '        .Row = R
    '        .Col = C
        End With
        pLetter.Add Ch, LObj
    End If
Next I
bValid = True
End Sub

' This returns the type of characters in the square
Property Get Values() As textType
Values = VT
End Property

' This returns the parameter for Compact() to filter the plain text, based on the types of characters in the square
Property Get PolyVals() As textType
Dim PV As textType
If Not bValid Then
    Err.Raise 528, "Polybius:PolyVals", "Square not initialized, call Make(values...) before PolyVals()"
End If
Select Case VT
    Case textType.ttAlphaPlus
        PV = ttAlphaPlus
    Case textType.ttAnumUpper
        PV = ttAlphaDigit
    Case textType.ttAlphaUpper
        PV = ttAlpha25
    Case textType.ttASCIIUpper
        If Colz = 6 Then
            PV = ttASCIIPunct
        Else ' Colz = 7
            PV = ttASCIIAll
        End If
    Case Else ' textType.ttASCII - Compact doesn't do any filtering for this
        PV = ttASCII
End Select
PolyVals = PV
End Property

' Returns number of columns
Property Get Cols() As Integer
If Not bValid Then
    Err.Raise 528, "Polybius:Col", "Square not initialized, call Make(values...) before Col()"
End If
Cols = Colz
End Property

' Returns Number of Rows
Property Get Rows() As Integer
If Not bValid Then
    Err.Raise 528, "Polybius:Row", "Square not initialized, call Make(values...) before Row()"
End If
Rows = Rowz
End Property

' Returns corresponding row for a given character
Property Get Row(Ch As String) As Integer
If Not bValid Then
    Err.Raise 528, "Polybius:Row", "Square not initialized, call Make(values...) before Row()"
End If
If Not pLetter.Exists(Ch) Then
    Err.Raise 529, "Polybius:Row", "Character not in Square [" & Ch & "]"
End If
Row = pLetter(Ch).Row
End Property

' Returns corresponding column for a given character
Property Get Col(Ch As String) As Integer
'Dim LObj As LetterObj
If Not bValid Then
    Err.Raise 528, "Polybius:Col", "Square not initialized, call Make(values...) before Col()"
End If
If Not pLetter.Exists(Ch) Then
    Err.Raise 529, "Polybius:Col", "Character not in Square [" & Ch & "]"
End If
Col = pLetter(Ch).Col
End Property

' Returns Row AND Col for a given character (Row before Col)
Sub RC(Ch As String, ByRef R As Integer, ByRef C As Integer)
Dim LI As LetterObj
If Not bValid Then
    Err.Raise 528, "Polybius:RC", "Square not initialized, call Make(values...) before RC()"
End If
If Not pLetter.Exists(Ch) Then
    Err.Raise 529, "Polybius:RC", "Character not in Square [" & Ch & "]"
End If
Set LI = pLetter(Ch)
R = LI.Row
C = LI.Col
End Sub

' Returns Corresponding character for a row and column
Property Get chrVal(R As Integer, C As Integer) As String
If Not bValid Then
    Err.Raise 528, "Polybius:chrVal", "Square not initialized, call Make(values...) before chrVal()"
End If
If C < 1 Or C > Colz Then
    Err.Raise 531, "Polybius:chrVal", "Col out of range, must be 1-" & Colz & ": " & C
End If
If R < 1 Or R > Rowz Then
    Err.Raise 531, "Polybius:chrVal", "Row out of range, must be 1-" & Rowz & ": " & R
End If
chrVal = sChars(0, R, C)
End Property

' returns characters in square
Property Get Alphabet() As String
If Not bValid Then
    Err.Raise 528, "Polybius:Alphabet", "Square not initialized, call Make(values...) before Alphabet()"
End If
Alphabet = Alpha
End Property

' Combo Routines ******************************************************************************************************

' returns true if character exists in square
Public Function Exists(Ch As String, Optional FT As fltType) As Boolean
If Not bValid Then
    Err.Raise 528, "Polybius:Exists", "Square not initialized, call Make(values...) before Exists()"
End If
Select Case FT
Case fltType.ftCols
    Exists = tColNames.Exists(Ch)
Case fltType.ftRows
    Exists = tRowNames.Exists(Ch)
Case Else
    Exists = pLetter.Exists(Ch)
End Select
End Function

' filters string for characters in square or rows & columns
Public Function Filter(S As String, Optional FT As fltType = ftRowCols) As String
Dim I As Integer
Dim Pivot As Integer
Dim lt As Integer
Dim AltPivot As Integer
Dim Chr As String
Dim Work As String
If Not bValid Then
    Err.Raise 528, "Polybius:Filter", "Square not initialized, call Make(values...) before Filter()"
End If

' Validate names
Select Case FT
    Case fltType.ftRowOrCols ' Both, no default for rows
        If tColNames.Count = 0 Then
            Err.Raise 550, "Polybius:Filter", "List not initialized, call ColNames(List) before Filter()"
        End If
        If tRowNames.Count = 0 Then
            Err.Raise 550, "Polybius:Filter", "List not initialized, call RowNames(List) before Filter()"
        End If
    Case fltType.ftRowCols, fltType.ftColRows, fltType.ftSqRC, fltType.ftSqCR ' Both, default rows to cols
        If tColNames.Count = 0 Then
            Err.Raise 550, "Polybius:Filter", "List not initialized, call ColNames(List) before Filter()"
        End If
        If tRowNames.Count = 0 Then
            Set tRowNames = tColNames
        End If
    Case fltType.ftCols ' Cols only
        If tColNames.Count = 0 Then
            Err.Raise 550, "Polybius:Filter", "List not initialized, call ColNames(List) before Filter()"
        End If
    Case fltType.ftRows ' Rows only, but default to cols
        If tRowNames.Count = 0 Then
            If tColNames.Count = 0 Then
                Err.Raise 550, "Polybius:Filter", "List not initialized, call RowNames(List) before Filter()"
            End If
            Set tRowNames = tColNames
        End If
End Select

Pivot = 2 * FT
Select Case FT
    Case fltType.ftSqplus
        If Not (pLetter.Exists(Octothorpe_) Or pLetter.Exists(NumEscape_)) Then ' escape character not in square, ignore
            FT = ftSquare
            Pivot = 2 * FT
        End If
    Case fltType.ftRowOrCols ' Monome-Dinome, Straddling Checkerboard (row optional)
        Pivot = fltType.ftRowOrCols + fltType.ftCols
    Case fltType.ftRowCols
        Pivot = fltType.ftRows + fltType.ftCols
        FT = ftRows
    Case fltType.ftColRows
        Pivot = fltType.ftRows + fltType.ftCols
        FT = ftCols
    Case fltType.ftSqRC
        Pivot = fltType.ftRows + fltType.ftCols
        AltPivot = fltType.ftRows
        FT = ftRows
    Case fltType.ftSqCR
        Pivot = fltType.ftRows + fltType.ftCols
        AltPivot = fltType.ftCols
        FT = ftCols
End Select

I = 0
lt = Len(S)
While I < lt
    I = I + 1
    Chr = Mid(S, I, 1)
    Select Case FT
        Case fltType.ftSqplus
            If Chr = Octothorpe_ Or Chr = NumEscape_ Then ' can't use this, in square or not
                GoTo Continue
            ElseIf Not pLetter.Exists(Chr) Then
                If (Chr < "0") Or (Chr > "9") Then ' Or (Not tColNames.Exists(Chr)) Then
                    GoTo Continue ' can't use anything not in square except numbers
                End If
            End If
        Case fltType.ftSquare
            If Not pLetter.Exists(Chr) Then
                GoTo Continue
            End If
        Case fltType.ftRowOrCols
            If tRowNames.Exists(Chr) Then
                FT = Pivot - FT
            Else
                If Not tColNames.Exists(Chr) Then
                    GoTo Continue
                End If
            End If
        Case fltType.ftRows
            If Not tRowNames.Exists(Chr) Then
                GoTo Continue
            End If
            If AltPivot = 0 Then
                FT = Pivot - FT
            Else
                If AltPivot = fltType.ftRows Then
                    FT = Pivot - FT
                Else
                    AltPivot = Pivot - AltPivot
                End If
            End If
        Case fltType.ftCols
            If Not tColNames.Exists(Chr) Then
                GoTo Continue
            End If
            If AltPivot = 0 Then
                FT = Pivot - FT
            Else
                If AltPivot = fltType.ftCols Then
                    FT = Pivot - FT
                Else
                    AltPivot = Pivot - AltPivot
                End If
            End If
    End Select
    Work = Work & Chr
Continue:
Wend
' Limit to only the 5 characters in the column names
lt = Len(Work)
If (FT = fltType.ftRowCols) And (lt Mod 2 = 1) Then
    Err.Raise 550, "Polybius:Filter", "Error: Odd number of characters in Ciphertext"
End If
Filter = Work
End Function
