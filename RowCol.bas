Attribute VB_Name = "RowCol"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       RowCol
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime"
'
' USAGE:
'       Polibius row/column codes
'
' GROUPS:
'       BIFID, CM BIFID
'            EncBifid, DecBifid
'
'       Checkerboard (5x5, 6x6, 7x7)
'            encChecker, decChecker
'
'       MONDI (Monome Dinome)
'            encMONDI, decMONDI
'
'       Nihilist Substitution
'            encNihilistSub, decNihilistSub
'
'       Non-Indicator Cipher (AKA: Doppelkastenschlilssel)
'            encTwoBox, decTwoBox
'
'       Nihilist Transposition
'            encNihilistTrans, decNihilistTrans
'
'       Phillips, PhillipsRC [PhillipsC]
'            encPhillips, decPhillips
'
'       Straddling Checkerboard
'            encStraddle, decStraddle
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 08/06/2020 Chip Bloch    -Modified polybius make method (prep for moving column names)
' 08/08/2020 Chip Bloch    -Reordered parms for Polybius
' 08/08/2020 Chip Bloch    -Calling Filter in Polybius for decChecker functions
' 08/23/2020 Chip Bloch    -Updating encChecker to allow for multiple column names
' 08/25/2020 Chip Bloch    -Added enc6Checker/dec6Checker
' 09/06/2020 Chip Bloch    -Added BIFID (& CM BIFID) - encBIFID, decBIFID
' 09/16/2020 Chip Bloch    -Added Nihilist Substitution
' 09/17/2020 Chip Bloch    -Renamed module, migrated functions from Playfair/Digraphic
' 09/21/2020 Chip Bloch    -Added encMONDI, decMONDI
' 09/26/2020 Chip Bloch    -Added encStraddle, decStraddle
' 09/26/2020 Chip Bloch    -Updated displayBlock to change spaces to non-breaking spaces
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
' 12/05/2020 Chip Bloch    -Made Checkerboard Dynamic
' 12/07/2020 Chip Bloch    -Made BIFID Dynamic
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType (pSquare.Make)
' 12/12/2020 Chip Bloch    -Made DisplayBlock allow dynamic count (0) for rows
' 12/12/2020 Chip Bloch    -Added KeySquare to generate a Polybius key
' 12/13/2020 Chip Bloch    -Made Rows and strRows optional (displayBlock)
' 12/28/2020 Chip Bloch    -Converted KeySquare to accept a range as well as a
'                           string for the replacement character (24 character ciphers)
' 12/28/2020 Chip Bloch    -Converted encMondi to pass an array of replacement strings to Compact()
' 12/31/2020 Chip Bloch    -Changed KeySquare to pass wildcard for Alphabet
' 01/03/2021 Chip Bloch    -Added Nihilist Transposition
' 01/05/2021 Chip Bloch    -Added Phillips, PhillipsRC
' 02/07/2021 Chip Bloch    -Added alternating Row/Col to Checkerboard
' 02/09/2021 Chip Bloch    -Added reference to AddSep
' 03/25/2021 Chip Bloch    -Added OutCh to decStraddle, encStraddle
' 03/30/2021 Chip Bloch    -Updated encPhillips to check key for pad before adding
' 04/03/2021 Chip Bloch    -Calling recompact in encNihilistSub
' 04/05/2021 Chip Bloch    -Added NI(twoBox) cipher
' 04/11/2021 Chip Bloch    -Added period -1 to NI, BIFID ciphers
' 04/15/2021 Chip Bloch    -Added Seriation to encChecker/decChecker (RRCC, CCRR)
' 04/19/2021 Chip Bloch    -Added call to Seriate_ in NI.
' 04/28/2021 Chip Bloch    -Updated DisplayBlock, KeySquare for pages in ttAlphaPlus
'                          -Added Paginate for ttAlphaPlus
' 05/01/2021 Chiop Bloch   -Made Repl a variant for encChecker (used with ttAlphaPlus)
'-------------------------------------------------------------------------------------------

Const Colnames_     As String = "12345"
Const Chk7Colnames_ As String = "01234567"

Public Enum RCMethod
    rcRows = 0
    rcCols = 1
    rcRC = 2
End Enum

' This function is the 5x5 Checkerboard encryption method
Public Function encChecker(text As String, Rng As Variant, Optional Repl As Variant, Optional strColNames As String, Optional strRowNames As String, _
    Optional RCO As String) As String
Dim pSquare As New Polybius
Dim FT As fltType
Dim Ser As Boolean
Dim Work As String
On Error GoTo Catch

' Initialize Square (dynamic)
Call pSquare.Make(Rng, -1)

' Check RCO (RowCol Order)
Select Case UCase(RCO)
    Case "", "RC"
        FT = ftSquare
    Case "RRCC"
        FT = ftSquare
        Ser = True
    Case "RCCR"
        FT = ftSqRC
    Case "CRRC"
        FT = ftSqCR
    Case "CR"
        FT = ftColRows
    Case "CCRR"
        FT = ftColRows
        Ser = True
    Case Else
        Err.Raise 302, "encChecker", "Error: Unknown RowCol Order: " & RCO & ", expecting RC, RCCR, RRCC, CCRR, CRRC or CR"
End Select

' Get Column Names
If strColNames = "" Then strColNames = Left(Chk7Colnames_, pSquare.Cols)
If strRowNames = "" Then strRowNames = strColNames

If Len(strColNames) > pSquare.Cols Or Len(strRowNames) > pSquare.Rows Then
    InitRand text
End If

pSquare.Colnames = strColNames
pSquare.RowNames = strRowNames

' Remove non-alphabetic characters and replace substitution charater
text = Compact(text, "", Repl, pSquare.PolyVals)

' Encode to row/column names
Work = pSquare.encString(text, FT)

' If going to Deseriate (split rows/columns)
If Ser Then
    Work = Seriate_(Work, -1, st_Deseriate, ttASCIIAny)
End If

encChecker = Work
Exit Function
Catch:
encChecker = Err.Description
End Function

' This function is the Checkerboard decryption method
Public Function decChecker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String, _
    Optional RCO As String) As String
Dim lt As Integer
Dim FT As fltType
Dim FTRC As fltType
Dim pSquare As New Polybius
Dim Flt As New AlphaList
Dim Work As String
Dim Ser As Boolean
On Error GoTo Catch

' Initialize square (Dynamic)
Call pSquare.Make(Rng, -1)

Select Case UCase(RCO)
    Case "", "RC"
        FT = ftSquare
        FTRC = ftRowCols
    Case "RRCC"
        FT = ftSquare
        FTRC = ftRowCols
        Ser = True
    Case "RCCR"
        FT = ftSqRC
        FTRC = ftSqRC
    Case "CRRC"
        FT = ftSqCR
        FTRC = ftSqCR
    Case "CCRR"
        FT = ftColRows
        FTRC = ftColRows
        Ser = True
    Case "CR"
        FT = ftColRows
        FTRC = ftColRows
    Case Else
        Err.Raise 302, "decChecker", "Error: Unknown RowCol Order: " & RCO & ", expecting RC, RCCR, RRCC, CCRR, CRRC or CR"
End Select

' Get Column Names
If strColNames = "" Then strColNames = Left(Chk7Colnames_, pSquare.Cols)
If strRowNames = "" Then strRowNames = strColNames

pSquare.Colnames = strColNames
pSquare.RowNames = strRowNames

' If going to seriate (join rows/columns)
If Ser Then
    ' first filter out non row/col characters
    Flt.Init Dedup(strColNames, strRowNames, ttASCIIAny)
    text = Flt.Filter(text)

    text = Seriate_(text, -1, st_Seriate, ttASCIIAny)
End If

' Limit to only the characters in the column/row names
text = pSquare.Filter(text, FTRC)

' Now decode with Polybius Square
Work = pSquare.decString(text, FT)

decChecker = Work
Exit Function

Catch:
decChecker = Err.Description
End Function

' This function is the 6x6 Checkerboard encryption method (deprecated)
Public Function enc6Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
enc6Checker = encChecker(text, Rng, "", strColNames, strRowNames)
End Function

' This function is the 6x6 Checkerboard decryption method (deprecated)
Public Function dec6Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
dec6Checker = decChecker(text, Rng, strColNames, strRowNames)
End Function

' This function is the 7x7 Checkerboard encryption method (deprecated)
Public Function enc7Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
enc7Checker = encChecker(text, Rng, "", strColNames, strRowNames)
End Function

' This function is the Checkerboard decryption method (deprecated)
Public Function dec7Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
dec7Checker = decChecker(text, Rng, strColNames, strRowNames)
End Function

' This function is the BIFID encryption method
Public Function encBIFID(text As String, Period As Integer, S1 As Variant, Optional S2 As Variant = Nothing, Optional Repl As String = "J") As String
Dim I As Integer, J As Integer
Dim Ln As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim Work As String
Dim Tmp As String
Dim Blk As String
Dim Ch As String
'Dim VT As textType

On Error GoTo Catch

' Initialize squares
Call pSquare1.Make(S1, -1)
'Get Column Names
pSquare1.Colnames = Left("1234567", pSquare1.Cols)

If S2 Is Nothing Then
    Set pSquare2 = pSquare1
Else
    If S2 = "" Then
        Set pSquare2 = pSquare1
    Else
        Call pSquare2.Make(S2, pSquare1.Cols, pSquare1.Values, , pSquare1.Colnames)
    End If
End If

Select Case pSquare1.Values
    Case textType.ttASCII, textType.ttASCIIUpper
        If Period = 0 Then
            Err.Raise 1021, "RowCol.encBIFID", "Period 0 specified with ASCII square"
        End If
End Select

' Remove non-alphabetic characters and replace substitution charater
If Repl = "" Then Repl = "J"
If Period = 0 Then Tmp = "."
text = Compact(text, Tmp, Repl, pSquare1.PolyVals)
Ln = Len(text)

If Period = -1 Then Period = Ln
If Period < 0 Then
    encBIFID = "ERROR: Period < 0 [" & Period & "]"
    Exit Function
End If

I = 1
While I <= Ln
    J = 0
    Ch = ""
    Blk = ""
    While (I <= Ln) And ((Period = 0 And Ch <> " ") Or (J < Period))
        Ch = Mid(text, I, 1)
        I = I + 1
        If pSquare1.Exists(Ch, ftSquare) Then
            J = J + 1
            Blk = Blk & Ch
        End If
    Wend
    If J > 0 Then
        Tmp = ""
        Blk = pSquare1.encString(Blk)
        
        Blk = Blk & Mid(Blk, 2, J * 2)
        For J = 1 To Len(Blk) Step 2
            Tmp = Tmp & Mid(Blk, J, 1)
        Next J
    
        Work = Work & pSquare2.decString(Tmp)
    End If
Wend

If Period = 0 Then ' insert punctuation
    Tmp = Work
    Work = ""
    J = 1
    For I = 1 To Len(text)
        Ch = Mid(text, I, 1)
        If pSquare1.Exists(Ch, ftSquare) Then
        'If Ch >= "A" And Ch <= "Z" Then
            Work = Work & Mid(Tmp, J, 1)
            J = J + 1
        Else
            Work = Work & Ch
        End If
    Next I
End If

encBIFID = Work
Exit Function
Catch:
encBIFID = Err.Description
End Function


' This function is the BIFID decryption method
Public Function decBIFID(text As String, Period As Integer, S1 As Variant, Optional S2 As Variant = Nothing) As String
Dim I As Integer, J As Integer
Dim P2 As Integer
Dim Ln As Integer, BlkLn As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim Work As String
Dim Tmp As String
Dim Blk As String
Dim Ch As String
'Dim VT As textType
On Error GoTo Catch

' Initialize squares
Call pSquare1.Make(S1, -1)
'Get Column Names
pSquare1.Colnames = Left("1234567", pSquare1.Cols)

'Call pSquare1.Make(S1, ValType.vtUpperAlpha, 5, , Colnames_)
If S2 Is Nothing Then
    Set pSquare2 = pSquare1
Else
    If S2 = "" Then
        Set pSquare2 = pSquare1
    Else
        Call pSquare2.Make(S2, pSquare1.Cols, pSquare1.Values, , pSquare1.Colnames)
    End If
End If

'VT = toFilter_(pSquare1.Values, pSquare1.Cols)
Select Case pSquare1.Values
    Case textType.ttASCII, textType.ttASCIIUpper
        If Period = 0 Then
            Err.Raise 1021, "RowCol.decBIFID", "Period 0 specified with ASCII square"
        End If
End Select

' Remove non-alphabetic characters and replace substitution charater
If Period <> 0 Then text = Compact(text, ".", "-", pSquare1.PolyVals)
If Period = -1 Then Period = Len(text)
If Period < 0 Then
    decBIFID = "ERROR: Period < 0 [" & Period & "]"
    Exit Function
End If

I = 1
Ln = Len(text)
While I <= Ln
    J = 0
    Ch = ""
    Blk = ""
    While (I <= Ln) And ((Period = 0 And Ch <> " ") Or (J < Period))
        Ch = Mid(text, I, 1)
        I = I + 1
        If pSquare2.Exists(Ch, ftSquare) Then
            J = J + 1
            Blk = Blk & Ch
        End If
    Wend
    If J > 0 Then
        Blk = pSquare2.encString(Blk)
        
        Tmp = ""
        BlkLn = Len(Blk) \ 2
        For J = 1 To BlkLn
            Tmp = Tmp & Mid(Blk, J, 1) & Mid(Blk, J + BlkLn, 1)
        Next J
    
        Work = Work & pSquare1.decString(Tmp)
    End If
Wend

If Period = 0 Then ' insert punctuation
    Tmp = Work
    Work = ""
    J = 1
    For I = 1 To Len(text)
        Ch = Mid(text, I, 1)
        If pSquare2.Exists(Ch, ftSquare) Then
            Work = Work & Mid(Tmp, J, 1)
            J = J + 1
        Else
            Work = Work & Ch
        End If
    Next I
End If

decBIFID = Work
Exit Function
Catch:
decBIFID = Err.Description
End Function

' This function is the Nihilist Substitution encryption method
Public Function encNihilistSub(text As String, S1 As Variant, Key As String, Optional Repl As String = "J") As String
Dim K As Integer, P As Integer
Dim kLn As Integer
Dim pSquare As New Polybius
Dim Work As String
Dim Kv() As Integer
Dim Ch As String
Dim Cv As Integer
Dim Cmp As New AlphaList
On Error GoTo Catch

If Repl = "" Then Repl = "J" ' Default

' Remove non-alphabetic characters and replace substitution charater in source text
text = Cmp.Compact(text, "", Repl)

If text = "" Then
    encNihilistSub = "" ' empty source text...
    Exit Function
End If

' Remove non-alphabetic characters and replace substitution character in key
Key = Cmp.Recompact(Key)

If Key = "" Then
    encNihilistSub = "Error: Empty key"
    Exit Function
End If

' Initialize square
Call pSquare.Make(S1, 5, textType.ttAlphaUpper, , Colnames_)

kLn = Len(Key)
ReDim Kv(1 To kLn)

' Encode keyword
Key = pSquare.encString(Key)

P = 1
For K = 1 To kLn
    Ch = Mid(Key, P, 2)
    Kv(K) = Ch
    P = P + 2
Next K

' Encode String
text = pSquare.encString(text)

' Add Keyword to string
K = 1
For P = 1 To Len(text) Step 2
    Ch = Mid(text, P, 2)
    Cv = Kv(K) + Ch
    If Cv > 100 Then
        Work = Work & Right(Cv, 2)
    Else
        Work = Work & Cv
    End If
    K = K + 1
    If K > kLn Then
        K = 1
    End If
Next P

encNihilistSub = Work
Exit Function
Catch:
encNihilistSub = Err.Description
End Function

' This function is the Nihilist Substitution decryption method
Public Function decNihilistSub(text As String, S1 As Variant, Key As String, Optional Repl As String = "J") As String
Dim K As Integer, P As Integer
Dim Ln As Integer
Dim kLn As Integer
Dim pSquare As New Polybius
Dim Work As String
Dim Kv() As Integer
Dim Ch As String
Dim Cv As Integer
On Error GoTo Catch

If Repl = "" Then Repl = "J" ' Default

' Remove non-numeric characters
text = ToDigit(text)

If text = "" Then
    decNihilistSub = "" ' empty source text...
    Exit Function
End If

Ln = Len(text)
If Ln Mod 2 = 1 Then
    decNihilistSub = "Error: Odd number of digits in source text (" & Ln & ") [" & text & "]"
    Exit Function
End If

' Remove non-alphabetic characters and replace substitution charater in key
Key = Compact(Key, "", Repl)

If Key = "" Then
    decNihilistSub = "Error: Empty key"
    Exit Function
End If

' Initialize square
Call pSquare.Make(S1, 5, textType.ttAlphaUpper, , Colnames_)
    
kLn = Len(Key)
ReDim Kv(1 To kLn)

' Encode keyword
Key = pSquare.encString(Key)

P = 1
For K = 1 To kLn
    Ch = Mid(Key, P, 2)
    Kv(K) = Ch
    P = P + 2
Next K

' Subtract keyword from string
K = 1
For P = 1 To Len(text) Step 2
    Cv = Mid(text, P, 2)
    If Cv < 11 Then Cv = Cv + 100
    Cv = Cv - Kv(K)
    Work = Work & Cv
    K = K + 1
    If K > kLn Then
        K = 1
    End If
Next P

' Decode String
Work = pSquare.decString(Work)

decNihilistSub = Work
Exit Function
Catch:
decNihilistSub = Err.Description
End Function

' Monome/Dinome encryption
Public Function encMONDI(text As String, Key As String, strRowCols As String, _
    Optional Repl1 As String, Optional Repl2 As String, Optional Sep As String) As String
Dim lRC As Integer
Dim rowStr As String
Dim colStr As String
Dim rect As New Polybius
Dim Wrk As String
Dim RepArr() As String
On Error GoTo Catch

'Only alphabetic
Repl1 = ToAlpha(Repl1)
Repl2 = ToAlpha(Repl2)

If Repl1 = "" Then Repl1 = "J" ' Combine I/J (default)
If Repl2 = "" Then Repl2 = "Z" ' Combine Y/Z (default)

ReDim RepArr(1 To 2)
RepArr(1) = Repl1
RepArr(2) = Repl2

' Row/Column name parameter - 10 character string (generally digits)
lRC = Len(strRowCols)
If lRC <> 10 Then
    encMONDI = "Error: Colnames are invalid length (" & lRC & "), must be 10 [" & strRowCols & "]"
    Exit Function
End If

rowStr = NullCh & Left(strRowCols, 2)  ' Rownames (Null + left two characters)
colStr = Right(strRowCols, 8)       ' Colnames (right eight characters)

' Make encryption rectangle 3x8
Call rect.Make(Key, 8, textType.ttAlphaUpper, 3, colStr, rowStr)

If Len(ToAlpha(Key)) <> 24 Then
    encMONDI = "Error: Key is invalid, expecting 24 letters [" & Key & "]"
    Exit Function
End If

If Left(Repl1, 1) = Left(Repl2, 1) Then
    encMONDI = "Error: duplicate replacement characters [" & Repl1 & "," & Repl2 & "]"
    Exit Function
End If

If rect.Exists(Left(Repl1, 1), ftSquare) Or rect.Exists(Left(Repl2, 1), ftSquare) Then
    encMONDI = "Error: replacement characters found in square (" & Repl1 & "," & Repl2 & ") [" & Key & "]"
    Exit Function
End If

' Replace any characters in source text, as needed
text = Compact(text, "", RepArr)

' encode
Wrk = rect.encString(text)

' Optional group by
Wrk = AddSep(Wrk, Sep)

encMONDI = Wrk
Exit Function
Catch:
encMONDI = Err.Description
End Function

' Monome/Dinome decryption
Public Function decMONDI(text As String, Key As String, strRowCols As String) As String
Dim lRC As Integer
Dim rowStr As String
Dim colStr As String
Dim rect As New Polybius
Dim Wrk As String
On Error GoTo Catch

' Row/Column name parameter - 10 character string (generally digits)
lRC = Len(strRowCols)
If lRC <> 10 Then
    decMONDI = "Error: Colnames are invalid length (" & lRC & "), must be 10 [" & strRowCols & "]"
    Exit Function
End If

rowStr = NullCh & Left(strRowCols, 2)   ' Rownames (Null + left two characters)
colStr = Right(strRowCols, 8)                   ' Colnames (right eight characters)

' Make encryption rectangle 3x8
Call rect.Make(Key, 8, textType.ttAlphaUpper, 3, colStr, rowStr)

If Len(ToAlpha(Key)) <> 24 Then
    decMONDI = "Error: Key is invalid, expecting 24 letters [" & Key & "]"
    Exit Function
End If

' Remove Control Characters, separators
text = rect.Filter(text, ftRowOrCols)

' encode
Wrk = rect.decString(text)

decMONDI = Wrk
Exit Function
Catch:
decMONDI = Err.Description
End Function

Private Function Paginate_(Key As String, Optional Page As Integer) As String
Dim I As Integer, P As Integer
Dim Pg() As String
Dim Ch As String
If InStr(1, Key, SwpCh) = 0 Then ' it's not a paged type
    Paginate_ = Key ' return original value
    Exit Function
End If
Page = Sgn(Page)
ReDim Pg(0 To 1)
For I = 1 To Len(Key)
    Ch = Mid(Key, I, 1)
    If Ch = SwpCh Then ' get next character (it's a swap)
        If Page = -1 Then Pg(P) = Pg(P) & SwpCh
        I = I + 1
        Ch = Mid(Key, I, 1)
        Pg(P) = Pg(P) & Ch
        P = P + 1 ' next Swp will be new page
    Else
        If UCase(Ch) >= "A" And UCase(Ch) <= "Z" Then
            Pg(0) = Pg(0) & Ch
        Else
            Pg(1) = Pg(1) & Ch
        End If
    End If
Next I
Select Case Page
    Case -1 ' both (52)
        Ch = Pg(0) & Pg(1)
    Case 0 ' Page 0 (25)
        Ch = Pg(0)
    Case 1 ' Page 1 (25)
        Ch = Pg(1)
End Select
Paginate_ = Ch
End Function

' Returns the page coresponding to the page number
' -1 = all
' 0 = first
' 1 = second...
Public Function Paginate(Key As String, Optional Page As Integer) As String
Dim I As Integer, P As Integer
Dim Pg() As String
Dim Ch As String
On Error GoTo Catch
Paginate = Paginate_(Key, Page)
Exit Function
Catch:
Paginate = Err.Description
End Function

' This function generates a key square for use with a Polybius based cipher
Public Function KeySquare(Key As String, tType As textType, _
    Optional Repl As Variant, Optional Fill As tt_TransType = tt_None, Optional tCase As Integer = 0, Optional Cols As Integer = 0) As String
'Dim Cols As Integer
Dim Work As String
On Error GoTo Catch
' tType should be one of these
Select Case tType
    Case textType.ttAlphaDigit, textType.ttASCIIAll, textType.ttASCIIPunct, textType.ttAlpha25, textType.ttAlpha25Upper, textType.ttAlphaPlus
    Case Else
        KeySquare = "Error: Invalid textType " & tType
End Select
' First dedup key with full alphabet
Work = Dedup(Key, "*", tType, Repl)
' Now compute column/row width & height
If tType = ttAlphaPlus Then
    Cols = 5
    Work = Paginate_(Work, -1)
    If Fill <> tt_None Then
        Work = Transmute(Left(Work, 26), Cols, , Fill) & Transmute(Right(Work, 26), Cols, , Fill)
    End If
Else
    If Cols < 1 Then
        Cols = Sqr(Len(Work))
    End If
    ' Transmute
    If Fill <> tt_None Then
        Work = Transmute(Work, Cols, , Fill)
    End If
End If
Select Case tCase
    Case Is < 0
        Work = UCase(Work)
    Case Is > 0
        Work = LCase(Work)
End Select
KeySquare = Work
Exit Function
Catch:
KeySquare = Err.Description
End Function

' This function formats a key from a Polybius based cipher for printing
Public Function displayBlock(Key As String, Optional Rows As Integer, Optional strRows As String, Optional strCols As String, Optional Page As Integer) As String
Dim R As Integer
Dim C As Integer
Dim I As Integer
Dim Wrk As String
Dim Ch As String
On Error GoTo Catch
If Key = "" Then Exit Function
If Page <> 0 Then Page = 1
If Len(Key) = 52 Then
    Rows = 5
    Key = Paginate_(Key, Page)
Else
    If Rows < 1 Then Rows = Sqr(Len(Key))
End If
Key = Transmute(Key, Rows, , tt_RotRight)
C = Len(Key) \ Rows
strRows = Transmute(strRows, Rows, , tt_Mirror)
R = Len(strRows) \ Rows + 1
If strCols <> "" Then
    Wrk = strCols
    strCols = ""
    For I = 1 To Len(Wrk) Step C
        strCols = strCols & String(R, " ") & Mid(Wrk, I, C)
    Next I
End If
Wrk = Transmute(strRows & String(Rows, " ") & Key, -Rows, , tt_RotRight)
displayBlock = Replace(GroupBy(strCols & Wrk, C + R, Chr(10)), " ", Chr(160))
Exit Function
Catch:
displayBlock = Err.Description
End Function

Public Function encStraddle(text As String, Key As String, strRows As String, _
    Optional strCols As String, Optional Bonus As String, Optional Sep As String, Optional OutCh As String) As String
Dim Rows As Integer
Dim I As Integer
Dim P As Integer
Dim kLn As Integer
Dim tot As Integer
Dim FT As fltType
Dim chrCols As New Scripting.Dictionary
Dim chrRows As New Scripting.Dictionary
Dim rect As New Polybius
Dim Ch As String
Dim Wrk As String
Const Digits As String = "0123456789"
On Error GoTo Catch

If text = "" Then Exit Function ' nothing to do

' check column names
chrCols.CompareMode = TextCompare
If strCols = "" Then
    strCols = Digits
ElseIf Len(strCols) <> 10 Then
    Err.Raise 608, "encStraddle", "Error: Column names too short, must be length 10 [" & strCols & "]"
End If
For I = 1 To 10
    Ch = Mid(strCols, I, 1)
    If chrCols.Exists(Ch) Then Err.Raise 608, "encStraddle", "Error: duplicate character '" & Ch & "' in column names [" & strCols & "]"
    chrCols.Add Ch, I
Next I

' check rows
chrRows.CompareMode = TextCompare
Rows = Len(strRows)
Select Case Rows
    Case Is < 2
        Err.Raise 608, "encStraddle", "Error: Row names too short, must be at least 2 [" & strRows & "]"
    Case 2
        FT = ftSqplus ' Allow for "#" compression
    Case 3
        FT = ftSquare
    Case Is > 3
        Err.Raise 608, "encStraddle", "Error: Row names too long, must be no more than 3 [" & strRows & "]"
End Select
For I = 1 To Rows
    Ch = Mid(strRows, I, 1)
    If chrRows.Exists(Ch) Then Err.Raise 608, "encStraddle", "Error: duplicate character '" & Ch & "' in row names [" & strRows & "]"
    chrRows.Add Ch, I
Next I

' check key
kLn = Len(Key)
Wrk = Key
If kLn = Len(ToAlpha(Wrk)) Then ' use default bonus characters
    If Bonus = "" Then
        If Rows = 2 Then
            Bonus = "# "
            I = 2
        Else
            Bonus = " " & Digits
            I = 11
        End If
    End If
    Key = Key & Left(Bonus, I)
    kLn = kLn + I
End If
tot = (Rows + 1) * 10 - Rows ' expected key length
If tot < kLn Then
    Err.Raise 608, "encStraddle", "Error: key too long (" & kLn & "), expecting length " & tot & " [" & Key & "]"
ElseIf tot > kLn Then
    Err.Raise 608, "encStraddle", "Error: key too short (" & kLn & "), expecting length " & tot & " [" & Key & "]"
End If

' insert nulls in key
P = 0
I = 0
Wrk = ""
While P < 10
    P = P + 1
    Ch = Mid(strCols, P, 1)
    If chrRows.Exists(Ch) Then
        Wrk = Wrk & NullCh
    Else
        I = I + 1
        Wrk = Wrk & Mid(Key, I, 1)
    End If
Wend

' validate all rows in cols
If I + Rows > 10 Then
    Err.Raise 608, "encStraddle", "Error: some row names (" & strRows & ") are missing from column names [" & strCols & "]"
End If

' append rest of key
Key = Wrk & Mid(Key, I + 1, kLn + 1)

' insert null in strRows
Rows = Rows + 1
strRows = NullCh & strRows

' Make Polybius square
rect.Make Key, 10, textType.ttASCIIUpper, Rows, strCols, strRows, OutCh

' Filter for characters in square
text = rect.Filter(text, FT)

' And encode
Wrk = rect.encString(text, FT)

' Optional group by
Wrk = AddSep(Wrk, XLAT(Sep, Digits, ""))

encStraddle = Wrk
Exit Function
Catch:
encStraddle = Err.Description
End Function

Public Function decStraddle(text As String, Key As String, strRows As String, Optional strCols As String, Optional Bonus As String, Optional OutCh As String) As String
Dim Rows As Integer
Dim I As Integer
Dim P As Integer
Dim kLn As Integer
Dim tot As Integer
Dim FT As fltType
Dim chrCols As New Scripting.Dictionary
Dim chrRows As New Scripting.Dictionary
Dim rect As New Polybius
Dim Ch As String
Dim Wrk As String
Const Digits As String = "0123456789"
On Error GoTo Catch

If text = "" Then Exit Function ' nothing to do

' check column names
chrCols.CompareMode = TextCompare
If strCols = "" Then
    strCols = Digits
ElseIf Len(strCols) <> 10 Then
    Err.Raise 608, "decStraddle", "Error: Column names too short, must be length 10 [" & strCols & "]"
End If
For I = 1 To 10
    Ch = Mid(strCols, I, 1)
    If chrCols.Exists(Ch) Then Err.Raise 608, "decStraddle", "Error: duplicate character '" & Ch & "' in column names [" & strCols & "]"
    chrCols.Add Ch, I
Next I

' check rows
chrRows.CompareMode = TextCompare
Rows = Len(strRows)
Select Case Rows
    Case Is < 2
        Err.Raise 608, "decStraddle", "Error: Row names too short, must be at least 2 [" & strRows & "]"
    Case 2
        FT = ftSqplus ' Allow for "#" compression
    Case 3
        FT = ftSquare
    Case Is > 3
        Err.Raise 608, "decStraddle", "Error: Row names too long, must be no more than 3 [" & strRows & "]"
End Select
For I = 1 To Rows
    Ch = Mid(strRows, I, 1)
    If chrRows.Exists(Ch) Then Err.Raise 608, "decStraddle", "Error: duplicate character '" & Ch & "' in row names [" & strRows & "]"
    chrRows.Add Ch, I
Next I

' check key
kLn = Len(Key)
Wrk = Key
If kLn = Len(ToAlpha(Wrk)) Then ' use default bonus characters
    If Bonus = "" Then
        If Rows = 2 Then
            Bonus = "# "
            I = 2
        Else
            Bonus = " " & Digits
            I = 11
        End If
    End If
    Key = Key & Left(Bonus, I)
    kLn = kLn + I
End If
tot = (Rows + 1) * 10 - Rows ' expected key length
If tot < kLn Then
    Err.Raise 608, "decStraddle", "Error: key too long (" & kLn & "), expecting length " & tot & " [" & Key & "]"
ElseIf tot > kLn Then
    Err.Raise 608, "decStraddle", "Error: key too short (" & kLn & "), expecting length " & tot & " [" & Key & "]"
End If

' insert nulls in key
P = 0
I = 0
Wrk = ""
While P < 10
    P = P + 1
    Ch = Mid(strCols, P, 1)
    If chrRows.Exists(Ch) Then
        Wrk = Wrk & NullCh
    Else
        I = I + 1
        Wrk = Wrk & Mid(Key, I, 1)
    End If
Wend

' validate all rows in cols
If I + Rows > 10 Then
    Err.Raise 608, "decStraddle", "Error: some row names (" & strRows & ") are missing from column names [" & strCols & "]"
End If

' append rest of key
Key = Wrk & Mid(Key, I + 1, kLn + 1)

' insert null in strRows
Rows = Rows + 1
strRows = NullCh & strRows

' Make Polybius square
rect.Make Key, 10, textType.ttASCIIUpper, Rows, strCols, strRows

' Filter for characters in rows/cols
text = rect.Filter(text, ftRowOrCols)

' And decode
Wrk = rect.decString(text, ftSqplus, OutCh)

decStraddle = Wrk
Exit Function
Catch:
decStraddle = Err.Description
End Function

' Nihilist Transposition encryption.
' Write text into a square, peel off by scrambled rows and columns

Public Function encNihilistTrans(text As String, Key As String, Optional ByRow As Boolean = False, Optional Pad As String = "X", _
    Optional Sep As String = "") As String
Dim Ln As Integer
Dim RC As Integer
Dim R As Integer, C As Integer
Dim Wrk As String
Dim Kinfo As New OrderedKey
Dim pSquare As New Polybius
On Error GoTo Catch
If Pad = "" Then Pad = "X"
' Fold to uppercase, remove control characters, pass everything else through
text = Compact(text, ".", "-")

' Pad to square boundary: 1,4,9,16,25,...
Ln = Len(text)
RC = Int(Sqr(Ln) + 0.9999)
text = text & String$((RC * RC) - Ln, Pad)

Kinfo.InitKey = Key
If Kinfo.Ln <> RC Then
    Err.Raise 722, "RowCol:encNihilistTrans", "Incorrect Key length - " & Key & "(" & Kinfo.Ln & "), expecting length " & RC
End If

Call pSquare.Make(text, RC, ttASCIIAny)

If ByRow Then
    For R = 1 To RC
        For C = 1 To RC
            Wrk = Wrk & pSquare.chrVal(Kinfo.Idx(R), Kinfo.Idx(C))
        Next C
    Next R
Else
    For C = 1 To RC
        For R = 1 To RC
            Wrk = Wrk & pSquare.chrVal(Kinfo.Idx(R), Kinfo.Idx(C))
        Next R
    Next C
End If

Wrk = AddSep(Wrk, Sep)

encNihilistTrans = Wrk

Exit Function
Catch:
encNihilistTrans = Err.Description
End Function

' Nihilist Transposition decryption.
' Write text into a square, peel off by scrambled rows and columns

Public Function decNihilistTrans(text As String, Key As String, Optional ByRow As Boolean = False, Optional Sep As String = "") As String
Dim Ln As Integer
Dim RC As Integer
Dim R As Integer, C As Integer
Dim Wrk As String
Dim Kinfo As New OrderedKey
Dim pSquare As New Polybius
On Error GoTo Catch
' Fold to uppercase, remove control characters, pass everything else through
text = Compact(text, ".", "-")

' See if need to remove padding
Ln = Len(text)
RC = Int(Sqr(Ln))

If Ln <> RC * RC Then
    If Sep <> "" Then
        text = UngroupBy(text, Sep)
        Ln = Len(text)
        RC = Int(Sqr(Ln))
    End If
End If

If Ln <> RC * RC Then
    Err.Raise 721, "RowCol:decNihilistTrans", "Unexpected text length (" & Ln & "), expecting length " & (RC * RC)
End If

Kinfo.InitKey = Key
If Kinfo.Ln <> RC Then
    Err.Raise 722, "RowCol:decNihilistTrans", "Incorrect Key length - " & Key & "(" & Kinfo.Ln & "), expecting length " & RC
End If

Call pSquare.Make(text, RC, ttASCIIAny)

If ByRow Then
    For R = 1 To RC
        For C = 1 To RC
            Wrk = Wrk & pSquare.chrVal(Kinfo.OrgIdx(R), Kinfo.OrgIdx(C))
        Next C
    Next R
Else
    For C = 1 To RC
        For R = 1 To RC
            Wrk = Wrk & pSquare.chrVal(Kinfo.OrgIdx(R), Kinfo.OrgIdx(C))
        Next R
    Next C
End If

decNihilistTrans = Wrk

Exit Function
Catch:
decNihilistTrans = Err.Description
End Function

' This function is the Phillips encryption method
Public Function encPhillips(text As String, Square As Variant, RC As RCMethod, Optional Repl As Variant, Optional Sep As String = "") As String
Dim Ln As Integer   ' Length of source text (after compact)
Dim P As Integer    ' Interate over source text with P, I
Dim I As Integer
Dim S As Integer    ' Square number
Dim R As Integer    ' Row/Column
Dim C As Integer
Dim MaxRows As Integer
'Dim Suppress As Boolean
Dim Swp As Integer
Dim Work As String
Dim numSquares As Integer
Dim pSquare As New Polybius
Dim Ch As String
Dim RowMap() As New OrderedKey
On Error GoTo Catch

' Initialize square
Call pSquare.Make(Square, -1)
MaxRows = pSquare.Rows

If MaxRows > 9 Then
    Err.Raise 1011, "RowCol:encPhillips", "Error: too many rows/columns in square (" & MaxRows & ")"
End If

' Remove non-alphabetic characters and replace substitution charater in source text
text = Compact(text, "", Repl, pSquare.PolyVals)

If text = "" Then
    encPhillips = "" ' empty source text...
    Exit Function
End If
Ln = Len(text)

' Rather than create multiple Polybius squares, we will use the same square BUT build
' a virtual map of the rows (one per square)

' Number of squares will stop when the 2 and 1 have been sorted to the last positions
numSquares = 2 * (MaxRows - 1)
ReDim RowMap(1 To numSquares)

' First square will always have rows 1234... (physical/virtual will match)
Work = Left("123456789", MaxRows)
RowMap(1).InitKey = Work

' We will swap this row with its' successor each pass
Swp = 1

' Build map for squares from two to numSquares
For S = 2 To numSquares
    ' Initialize rows for this square from the previous one
    '      Chars < Swp         +   Chars(Swp + 1)      + Chars(Swp)        + Chars > Swp + 1
    Work = Left(Work, Swp - 1) & Mid(Work, Swp + 1, 1) & Mid(Work, Swp, 1) & Mid(Work, Swp + 2, MaxRows - Swp - 1)
    RowMap(S).InitKey = Work
    ' Bump Swp to next position
    Swp = Swp + 1
    If Swp = MaxRows Then Swp = 1
Next S

' Now encrypt text
Work = ""
S = 1
For P = 1 To Ln Step 5 ' In blocks of five
    For I = P To Application.Min(P + 4, Ln)
        ' Get Row/Column for character in square
        Ch = Mid(text, I, 1)
        Call pSquare.RC(Ch, R, C)
        ' Next check if using physical or virtual rows
        Select Case RC
            Case rcRows, rcRC
                ' Convert row to virtual, add 1, map back to Physical
                R = RowMap(S).NxtIdx(R)
            Case Else
                R = pSquare.RUp(R)
        End Select
        ' Now do Columns
        Select Case RC
            Case rcCols, rcRC
                ' Convert row to virtual, add 1, map back to Physical
                C = RowMap(S).NxtIdx(C)
            Case Else
                C = pSquare.CUp(C)
        End Select
        ' Pull value from original square
        Work = Work & pSquare.chrVal(R, C)
    Next I
    ' finished with this block, move on to next square
    S = S + 1
    If S > numSquares Then S = 1
Next P

' If a separator is passed and values are not in the square
If Not pSquare.Exists(Sep) Then
    Work = AddSep(Work, Sep)
End If

encPhillips = Work
Exit Function
Catch:
encPhillips = Err.Description
End Function

' This function is the Phillips encryption method
Public Function decPhillips(text As String, Square As Variant, RC As RCMethod) As String
Dim Ln As Integer   ' Length of source text (after compact)
Dim P As Integer    ' Interate over source text with P, I
Dim I As Integer
Dim S As Integer    ' Square number
Dim R As Integer    ' Row/Column
Dim C As Integer
Dim MaxRows As Integer
Dim Swp As Integer
Dim Work As String
Dim numSquares As Integer
Dim pSquare As New Polybius
Dim Ch As String
Dim RowMap() As New OrderedKey
On Error GoTo Catch

' Initialize square
Call pSquare.Make(Square, -1)
MaxRows = pSquare.Rows

If MaxRows > 9 Then
    Err.Raise 1011, "RowCol:decPhillips", "Error: too many rows/columns in square (" & MaxRows & ")"
End If

' Remove non-alphabetic characters and replace substitution charater in source text
text = pSquare.Filter(text, ftSquare)

If text = "" Then
    decPhillips = "" ' empty source text...
    Exit Function
End If
Ln = Len(text)

' Rather than create multiple Polybius squares, we will use the same square BUT build
' a virtual map of the rows (one per square)

' Number of squares will stop when the 2 and 1 have been sorted to the last positions
numSquares = 2 * (MaxRows - 1)
ReDim RowMap(1 To numSquares)

' First square will always have rows 1234... (physical/virtual will match)
Work = Left("123456789", MaxRows)
RowMap(1).InitKey = Work

' We will swap this row with its' successor each pass
Swp = 1

' Build map for squares from two to numSquares
For S = 2 To numSquares
    ' Initialize rows for this square from the previous one
    '      Chars < Swp         +   Chars(Swp + 1)      + Chars(Swp)        + Chars > Swp + 1
    Work = Left(Work, Swp - 1) & Mid(Work, Swp + 1, 1) & Mid(Work, Swp, 1) & Mid(Work, Swp + 2, MaxRows - Swp - 1)
    RowMap(S).InitKey = Work
    ' Bump Swp to next position
    Swp = Swp + 1
    If Swp = MaxRows Then Swp = 1
Next S

Work = ""
S = 1
For P = 1 To Ln Step 5
    For I = P To Application.Min(P + 4, Ln)
        ' Get Row/Column for character in square
        Ch = Mid(text, I, 1)
        If pSquare.Exists(Ch, ftSquare) Then
            Call pSquare.RC(Ch, R, C)
            Select Case RC
                Case rcRows, rcRC
                    ' Convert row to virtual, subtract 1, map back to Physical
                    R = RowMap(S).PrvIdx(R)
                Case Else
                    R = pSquare.RDown(R)
            End Select
            ' Now do Column
            Select Case RC
                Case rcCols, rcRC
                    ' Convert row to virtual, subtract 1, map back to Physical
                    C = RowMap(S).PrvIdx(C)
                Case Else
                    C = pSquare.CDown(C)
            End Select
            Work = Work & pSquare.chrVal(R, C)
        End If
    Next I
    S = S + 1
    If S > numSquares Then S = 1
Next P

decPhillips = Work
Exit Function
Catch:
decPhillips = Err.Description
End Function

' Two Box Cipher ("Doppelkastenschlilssel") or Non-Indicator cipher
' German WWII field cipher (declassified NSA document)

Public Function encTwoBox(text As String, square1 As Variant, square2 As Variant, Optional Repl As Variant, _
    Optional Pad As String = "X", Optional Sep As String = "", Optional Period As Integer = 21) As String
Dim I As Integer
Dim Pass As Integer
Dim P As Integer
Dim LR As Integer, RR As Integer
Dim LC As Integer, RC As Integer
Dim Ln As Integer
Dim LCh As String
Dim RCh As String
Dim S1 As New Polybius
Dim S2 As New Polybius
Dim Wrk As String
On Error GoTo Catch

' Make the squares
Call S1.Make(square1, -1)
Call S2.Make(square2, -1)

' Check that they have the same alphabet
If S2.Alphabet <> S1.Filter(S2.Alphabet, ftSquare) Then
    encTwoBox = "Error: square1 and square2 do not contain identical alphabets"
    Exit Function
End If

' Validate period (default to 21)
If Period = -1 Then Period = Ln \ 2
If Period = 0 Then Period = 21
If Period < 1 Then
    encTwoBox = "Invalid period: " & Period & ". Must be > 0"
    Exit Function
End If

' filter control characters, replace substitution character and pad to even length
text = Seriate_(text, Period, st_Seriate, S1.PolyVals, Repl, 2, Pad)
If text = "" Then
    Exit Function
End If
Ln = Len(text)

' Iterate over text
For I = 1 To Ln Step 2
    LCh = Mid(text, I, 1) ' left character
    RCh = Mid(text, I + 1, 1) ' right character

    For Pass = 1 To 2 ' Repeat encryption 2x
        S1.RC LCh, LR, LC ' get left character row/col
        S2.RC RCh, RR, RC ' get right character row/col

        If LR = RR Then ' Line, move one character left
            LCh = S2.chrVal(LR, S2.CDown(RC))
            RCh = S1.chrVal(RR, S1.CDown(LC))
        Else ' Box, take opposite corners
            LCh = S2.chrVal(LR, RC)
            RCh = S1.chrVal(RR, LC)
        End If
    Next Pass

    Wrk = Wrk & LCh & RCh ' append left/right character
Next I

Wrk = Seriate_(Wrk, Period, st_Deseriate, S1.PolyVals, Repl)

Wrk = AddSep(Wrk, Sep) ' Add separator if provided (and not present in ciphertext)

encTwoBox = Wrk
Exit Function
Catch:
encTwoBox = Err.Description
End Function

' Two Box Cipher ("Doppelkastenschlilssel") or Non-Indicator cipher
' German WWII field cipher

Public Function decTwoBox(text As String, square1 As Variant, square2 As Variant, _
    Optional Sep As String = "", Optional Period As Integer = 21) As String
Dim I As Integer
Dim Pass As Integer
Dim P As Integer
Dim RR As Integer, LR As Integer
Dim RC As Integer, LC As Integer
Dim Ln As Integer
Dim RCh As String
Dim LCh As String
Dim S1 As New Polybius
Dim S2 As New Polybius
Dim Wrk As String
On Error GoTo Catch

' Make the squares
Call S1.Make(square1, -1)
Call S2.Make(square2, -1)

' Check that they have the same alphabet
If S2.Alphabet <> S1.Filter(S2.Alphabet, ftSquare) Then
    decTwoBox = "Error: square1 and square2 do not contain identical alphabets"
    Exit Function
End If

' Validate period (default to 21)
If Period = -1 Then Period = Ln / 2
If Period = 0 Then Period = 21
If Period < 1 Then
    decTwoBox = "Invalid period: " & Period & ". Must be > 0"
    Exit Function
End If

' filter control characters, remove separator if present, check for even length, join period distance
text = Seriate_(UngroupBy(text), Period, st_Seriate, S1.PolyVals, "-", 2, "")
If text = "" Then
    Exit Function
End If

' Check for even length
Ln = Len(text)

' Iterate over ciphertext
For I = 1 To Ln Step 2
    LCh = Mid(text, I, 1)   ' left character
    RCh = Mid(text, I + 1, 1) ' right character
    
    For Pass = 1 To 2 ' Repeat 2x
        S2.RC LCh, LR, LC ' get left character row/col
        S1.RC RCh, RR, RC ' get right character row/col
        
        If RR = LR Then ' Line, move 1 character right
            LCh = S1.chrVal(LR, S1.CUp(RC))
            RCh = S2.chrVal(RR, S2.CUp(LC))
        Else ' Box, swap corners
            LCh = S1.chrVal(LR, RC)
            RCh = S2.chrVal(RR, LC)
        End If
    Next Pass
    
    Wrk = Wrk & LCh & RCh ' Append left/right characters
Next I

' Split period back out
Wrk = Seriate_(Wrk, Period, st_Deseriate, S1.PolyVals)

decTwoBox = Wrk
Exit Function

Catch:
decTwoBox = Err.Description
End Function
