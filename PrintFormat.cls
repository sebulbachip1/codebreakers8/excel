VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PrintFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       PrintFormat
'
'USAGE:
'       Formats a number into a string, dynamically.  Can format as Number/Hex (fixed or
'       variable), Roman Numerals or Wildcard (List: A, C, F, B)
'
'       Caller must trap errors
'
'GROUPS:
'       PrintFormat
'            Value , Format, isValid, Length, Sep, hasSep, Wildcards, Count, toString,
'            Zero, hasZero
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 01/28/2019 Chip Bloch    -Original Version
' 09/07/2020 Chip Bloch    -Added support for 'zero' codes (0 = 0 or 10)
'-------------------------------------------------------------------------------------------

Public Enum Display
    None = 0
    Number = 1
    Hex = 2
    Roman = 3
    Wild = 4
End Enum

Public Enum ModifierType
    mt_Zero = 0
    mt_Fixed = 1
    mt_Variable = 2
    mt_Extend = 3
End Enum

Private fmt   As Display ' Code Type
Private Ln    As Integer ' Target length
Private Val   As Integer ' Value
Private Nmf   As String  ' Numeric format code
Private Cnt   As Integer ' Maximum number of codes
Private Wld() As String  ' Wildcard Display values
Private sSep  As String  ' Code separator
Private FCnt  As Integer ' Extended code count (+0)
Private Shft  As Integer ' Zero based shift
Private Fixed As Boolean ' Fixed width indicator
Private FixCheck As Boolean

Private Const Maxcount As Integer = 8192

Private Sub Class_Initialize()
    fmt = None
    Nmf = "0"
    Cnt = 0
    ReDim Wld(0)
End Sub

' Need Public Enum
' Extend codes to include zero
Property Let Extend(M As ModifierType)
Select Case M
    Case mt_Zero
        FCnt = 1
        Shft = -1 ' Zero Based
    Case mt_Fixed
        Fixed = True
    Case mt_Variable
        FCnt = 1 ' 1-0, 01-00, ...
    Case mt_Extend
        FCnt = 1 ' 1-0, 01-00, ...
        Fixed = True
End Select
End Property

Property Get isExtended() As Boolean
isExtended = FCnt = 1
End Property

' Code value
Property Get Value() As Integer
    Value = Val
End Property

Property Let Value(V As Integer)
    Val = V
End Property

' Format: Decimal, Hex, Roman, Wild
Property Get Format() As Display
    Format = fmt
End Property

Property Let Format(F As Display)
    fmt = F
End Property

Property Get isValid() As Boolean
    isValid = fmt <> None
End Property

' Code format length (width)
Property Get Length() As Integer
    Length = Ln
End Property

Property Let Length(L As Integer)
    Ln = Application.Max(L, 1)
    Nmf = String$(Ln, "0")
    
    If Ln > 1 And ((fmt = Number) Or (fmt = Hex)) Then
        Fixed = True
    End If
End Property

' Code separator

Property Get Sep() As String
Sep = sSep
End Property

Property Let Sep(Char As String)
sSep = Char
End Property

Property Get hasSep() As Boolean
hasSep = sSep <> ""
End Property

' Wildcard values
Property Get Wildcards() As String()
    Wildcards = Wld
End Property

Property Let Wildcards(strs() As String)
    Wld = strs
End Property

' returns maximum count of codes
Property Get Count() As Integer
    If Cnt = 0 Then ' it's not initialized, calculate it
        Select Case fmt
            Case Display.Number
                If Not isFixed Then ' Variable width
                    Cnt = Maxcount
                Else
                    Select Case Ln
                        Case Is < 2
                            Cnt = 9 + FCnt
                        Case 2
                            Cnt = 99 + FCnt
                        Case 3
                            Cnt = 999 + FCnt
                        Case Else
                            Cnt = Maxcount
                    End Select
                End If
            Case Display.Hex
                If Not isFixed Then ' Variable Width
                    Cnt = Maxcount
                Else
                    Select Case Ln
                        Case Is < 2
                            Cnt = 15 + FCnt
                        Case 2
                            Cnt = 255 + FCnt
                        Case 3
                            Cnt = 4095 + FCnt
                        Case Else
                            Cnt = Maxcount
                    End Select
                End If
            Case Display.Roman
                Cnt = Maxcount
            Case Display.Wild
                Cnt = UBound(Wld) + 1
        End Select
    End If
    Count = Cnt
End Property

Private Function isFixed() As Boolean
If Not FixCheck Then
    FixCheck = True
    Fixed = Fixed Or Ln >= 2 Or Sep = ""
End If
isFixed = Fixed
End Function

' Note: This is the default method for this class.  Added the following line in the exported class function and re-imported it
' Attribute toString.VB_UserMemId = 0
' See http://www.cpearson.com/excel/DefaultMember.aspx

Public Function toString() As String
Attribute toString.VB_UserMemId = 0
Dim SV As Integer
Dim VLn As Integer
'    On Error GoTo Out
    Select Case fmt
        Case Display.Number
            SV = Val + Shft
            If isFixed Then
                toString = Right(Application.WorksheetFunction.text(SV, Nmf), Ln)
            Else
                toString = Application.WorksheetFunction.text(SV, Nmf)
            End If
        Case Display.Hex
            SV = Val + Shft
            If isFixed Then ' (fixed width)
                toString = Right(Application.WorksheetFunction.Dec2Hex(SV, Ln + 1), Ln)
            Else
                Select Case SV
                    Case Is < 16
                        VLn = 1
                    Case Is < 256
                        VLn = 2
                    Case Is < 4096
                        VLn = 3
                    Case Else
                        VLn = 4
                End Select
                toString = Application.WorksheetFunction.Dec2Hex(SV, VLn)
            End If
        Case Display.Roman
            toString = Application.WorksheetFunction.Roman(Val)
        Case Display.Wild
            toString = Wld(Val - 1)
    End Select
    Exit Function
'Out: ' "Catch" subscript out of range
' must catch this in calling class
'    toString = "toString: Err: " & Err.Number & " " & Err.Description
End Function

' Set code format

Public Sub AsNum(L As Integer)
    fmt = Number
    Val = 0
    Length = L
End Sub

Public Sub AsHex(L As Integer)
    fmt = Hex
    Val = 0
    Length = L
End Sub

Public Sub AsRoman()
    fmt = Roman
    Val = 0
    Ln = 1
End Sub

Public Sub AsWild(L As Integer)
    fmt = Wild
    Length = L
End Sub
