VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "OrderedKey"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       OrderedKey
'
'USAGE:
'       Provides routines for handling Ordered Keys where the key is used only to change the
'       order of columns and not used for encryption (columnar transposition, et. al.)
'
'       Caller must trap errors.
'
'GROUPS:
'       OrderedKey
'            InitKey, Ln, OrgIdx, Idx, Invert, NxtIdx, PrvIdx
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 06/28/2020 Chip Bloch    -Original Version (migrated from pseudo-object in trans)
' 07/05/2020 Chip Bloch    -Added Invert
' 07/16/2020 Chip Bloch    -Added keyType enum
' 09/06/2020 Chip Bloch    -Changed from Idx(I) in AltKey to iIdx(I)
' 09/11/2020 Chip Bloch    -Added ktInvKey, ktKey
' 01/04/2021 Chip Bloch    -Added NxtIdx, PrvIdx
' 04/10/2021 Chip Bloch    -Added Inverse order for equal characters ("=")
'-------------------------------------------------------------------------------------------

Public Enum keyType
    ktInvKey = -3
    ktNumInv = -2
    ktAlphaInv = -1
    ktAlpha = 0
    ktNumber = 1
    ktKey = 2
End Enum

Private Const ci_AT = 32 ' ASCII value for space (used for positional key)

' Positional key data
Private iKey As String  ' Initialized with key value
Dim sKey As String      ' Initialized with Sorted Key Value
Dim iIdx() As Integer   ' New Index
Dim OIdx() As Integer   ' Initial Index
Dim kLn As Integer      ' Key Length

' Set key (empty)
Private Sub Class_Initialize()
'iKey = ""
End Sub

' Get string into arrays, set bValid flag
' This is procesed only once.  Arrays speed searches after that.
Property Let InitKey(Key As String)
Dim I As Integer
Dim P As Integer
Dim Ch As String
Dim SCh() As String
Dim Reverse As Boolean
Dim Inverse As Boolean

Ch = Left(Key, 1)
Do While Ch = "=" Or Ch = "-"
    If Ch = "-" Then
        Reverse = True
        Key = Mid(Key, 2, Len(Key))
    ElseIf Ch = "=" Then
        Inverse = True
        Key = Mid(Key, 2, Len(Key))
    End If
    Ch = Left(Key, 1)
Loop

If Key = "" Then
    Err.Raise 549, "OrderedKey:InitKey", "Empty Key"
End If

' Get Key Length
kLn = Len(Key)

ReDim iIdx(1 To kLn)
ReDim OIdx(1 To kLn)
ReDim SCh(kLn)

' SCh is sort array
' Initialize key; duplicate characters allowed - appends column order to provide a stable sort
For I = 1 To kLn
    ' Every character in the key becomes two.
    If Inverse Then ' Identical characters sort inverted order
        P = kLn - I
    Else
        P = I
    End If
    SCh(I) = Mid(Key, I, 1) & Chr(P + ci_AT)
Next I

' Sort key - insertion sort, but key likely won't be hundreds of characters long
' Also is STABLE sort
For I = 1 To kLn - 1
    P = I
    While P > 0 And SCh(P) > SCh(P + 1)
        Ch = SCh(P)
        SCh(P) = SCh(P + 1)
        SCh(P + 1) = Ch
        P = P - 1
    Wend
Next I

' Now split sorted key into arrays
For I = 1 To kLn
    sKey = sKey & Left(SCh(I), 1)
    If Reverse Then
        If Inverse Then
            iIdx(I) = kLn - (Asc(Right(SCh(I), 1)) - ci_AT)
        Else
            iIdx(I) = Asc(Right(SCh(I), 1)) - ci_AT
        End If
        OIdx(iIdx(I)) = I
    Else
        If Inverse Then
            OIdx(I) = kLn - (Asc(Right(SCh(I), 1)) - ci_AT)
        Else
            OIdx(I) = Asc(Right(SCh(I), 1)) - ci_AT
        End If
        iIdx(OIdx(I)) = I
    End If
Next I

iKey = Key
End Property

' Returns Length of Key
Property Get Ln() As Integer
If iKey = "" Then
    Err.Raise 550, "OrderedKey:Ln", "Key not initialized, call InitKey(Key) before Ln()"
End If
Ln = kLn
End Property

' This function returns the original column number for the character at position I in the key
Property Get OrgIdx(I As Integer) As Integer
If iKey = "" Then
    Err.Raise 550, "OrderedKey:OrgIdx", "Key not initialized, call InitKey(Key) before OrgIdx()"
End If
OrgIdx = OIdx(I)
End Property

' This function returns the current column number for the character at position I in the key
Property Get Idx(I As Integer) As Integer
If iKey = "" Then
    Err.Raise 550, "OrderedKey:Idx", "Key not initialized, call InitKey(Key) before Idx()"
End If
Idx = iIdx(I)
End Property

' This Function returns the original index of the next (sorted) column
Function NxtIdx(I As Integer) As Integer
    I = OIdx(I) + 1
    If I > kLn Then I = 1
    NxtIdx = iIdx(I)
End Function

' This Function returns the original index of the Previous (sorted) column
Function PrvIdx(I As Integer) As Integer
    I = OIdx(I) - 1
    If I = 0 Then I = kLn
    PrvIdx = iIdx(I)
End Function

' Returns alpha or numeric key, straight or inverted
Function Invert(Optional kType As keyType = keyType.ktAlphaInv, Optional Key As String = "") As String
Dim I As Integer
Dim Base As Integer
Dim Wrk As String, Ch As String, Space As String
If iKey = "" Then
    Me.InitKey = Key
End If
Select Case kType
    Case keyType.ktInvKey, keyType.ktKey ' Original Key
    Case keyType.ktNumInv, keyType.ktNumber ' numeric
    Case keyType.ktAlphaInv, keyType.ktAlpha ' Alpha
        I = 127 - Len(Key)
            If I < 32 Then
                Err.Raise 551, "OrderedKey:Invert", "Error: Key too long to invert (" & Len(Key) & "): " & iKey
            ElseIf I > 64 Then
                Base = 64
            ElseIf I > 47 Then
                Base = 47
            Else
                Base = 32
            End If
    Case Else
        Err.Raise 552, "OrderedKey:Invert", "Error: Unknown key type: " & kType
End Select
For I = 1 To kLn
    Select Case kType
        Case keyType.ktInvKey ' Inverted Key
            Ch = Mid(sKey, I, 1)
        Case keyType.ktNumInv ' Inverted numeric
            Ch = Space & OIdx(I)
            Space = " "
        Case keyType.ktAlphaInv ' Inverted Alpha
            Ch = Chr(Base + OIdx(I))
        Case keyType.ktAlpha ' Alpha
            Ch = Chr(Base + iIdx(I))
        Case keyType.ktNumber ' Numeric
            Ch = Space & iIdx(I)
            Space = " "
        Case keyType.ktKey ' Forward Key
            Ch = Mid(iKey, I, 1)
    End Select
    Wrk = Wrk & Ch
Next I
Invert = Wrk
End Function
