Attribute VB_Name = "Pollux"
Option Explicit
' Functions involving morse code
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       Pollux
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime"
'
'USAGE:
'       Provides routines to deal with morse code
'
'GROUPS:
'       Pollux
'            Alpha2Morse, Morse2Alpha
'            encFracMorse, decFracMorse
'            encMorbit, decMorbit
'            encPollux, decPollux
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 07/28/2020 Chip Bloch    -Changed Pad to Sep in Alpha2Morse
' 08/01/2020 Chip Bloch    -Caught bug in SetDigits (overflowed integer value)
'                          -Added encFracMorse, decFracMorse, encPollux, decPollux, encMorbit, decMorbit
'                          -Corrected some bugs in Pollux
' 08/02/2020 Chip Bloch    -Matched default key for Pollux, filtering out spaces in code
'                          -Added sep to enc functions for Pollux, Morbit, FracMorse
' 08/06/2020 Chip Bloch    -Calling InitRand in encPollux
' 10/04/2020 Chip Bloch    -Added Public modifer to Morbit2Morse, FracMorse2Morse
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
'-------------------------------------------------------------------------------------------

Type tChar
    Digits As String
    Count As Integer
End Type

' Global Variables - note we can't initialize the arrays like in VB, and we don't want to use variants...
Dim CharVals As New Scripting.Dictionary
Dim TextVals As New Scripting.Dictionary
Dim morseInit As Boolean

' Initialize arrays
Private Function InitMorse()
Dim morseChars() As String
Dim morseText() As String
Dim I As Integer
'morseInit = False
If Not morseInit Then
    morseChars = Split(" |A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|" & _
            "0|1|2|3|4|5|6|7|8|9|.|,|?|'|!|/|(|)|&|:|;|=|+|-|_|""|$|@", "|")
    morseText = Split("X|.-|-...|-.-.|-..|.|..-.|--.|....|..|.---|-.-|.-..|--|-.|---|" & _
            ".--.|--.-|.-.|...|-|..-|...-|.--|-..-|-.--|--..|-----|.----|..---|...--|" & _
            "....-|.....|-....|--...|---..|----.|.-.-.-|--..--|..--..|.----.|-.-.--|" & _
            "-..-.|-.--.|-.--.-|.-...|---...|-.-.-.|-...-|.-.-.|-....-|..--.-|.-..-.|" & _
            "...-..-|.--.-.", "|")
    CharVals.CompareMode = TextCompare
    TextVals.CompareMode = TextCompare
    For I = 0 To UBound(morseChars)
        CharVals.Add morseChars(I), morseText(I)
        TextVals.Add morseText(I), morseChars(I)
    Next I
    morseInit = True
End If
End Function

' For Pollux, translate aliases to Dot, Dash or Space
Private Function PolluxKey(Key As String) As String
PolluxKey = XLAT(Key, "012+xX", ".-    ")
End Function

' Returns the morse equivalent for a character
Public Function Ch2Morse(Val As String) As String
Dim text As String
InitMorse

text = Val

If CharVals.Exists(Val) Then
    text = CharVals(Val)
End If

Ch2Morse = text
End Function

' Returns the character equivalent of a morse string

Public Function Morse2Ch(Val As String) As String
Dim text As String
InitMorse

text = Val
If TextVals.Exists(Val) Then
    text = TextVals(Val)
End If

Morse2Ch = text
End Function

' returns the text equivalent of a morse string (multiple morse characters separated by a space)

Public Function Morse2Alpha(Code As String, Optional Sep As String = " ") As String
Dim V As String
Dim Ch As String
Dim Wrk As String
Dim I As Integer
V = ""
Wrk = ""
If Sep = "" Then
    Sep = " "
End If

For I = 1 To Len(Code)
    Ch = Mid(Code, I, 1)
    If Ch <> Sep Then
        V = V & Ch
    Else
        If V = "" Then
            Wrk = Wrk & " "
        Else
            Wrk = Wrk & Morse2Ch(V)
        End If
        V = ""
    End If
Next I
If V <> "" Then
    Wrk = Wrk & Morse2Ch(V)
End If
Morse2Alpha = Wrk
End Function

' Returns the morse string equivalent of a text string - dots and dashes separated by the Sep character

Public Function Alpha2Morse(Code As String, Optional Sep As String = " ", Optional ln As Integer) As String
Dim Wrk As String
Dim Ch As String
Dim LastCh As String
Dim I As Integer
Dim X As Integer
Ch = ""
Wrk = ""

If Sep = "" Then
    Sep = " "
End If

For I = 1 To Len(Code)
    Ch = Mid(Code, I, 1)
    If Ch <> " " Or LastCh <> " " Then
        If Wrk <> "" Then
            Wrk = Wrk & Sep
        End If
        If Ch <> " " Then
            Wrk = Wrk & Ch2Morse(Ch)
        End If
    End If
    LastCh = Ch
Next I

' Add additional padding at end if need to round to a multiple of Ln
If ln > 1 Then
    X = Len(Wrk) Mod ln
    If X <> 0 Then ' should use Wrk = Wrk & String$(Sep, <something with x>)
        For I = X To ln - 1
            Wrk = Wrk & Sep
        Next I
    End If
End If
Alpha2Morse = Wrk
End Function

' Morbit encryption
' Morse text, separated by "X", padded to length of 2
Public Function Morse2Morbit(Key As String, Code As String, Optional Sep As String = "") As String
Dim I As Integer
Dim J As Integer
Dim cnt As Integer
Dim Wrk As String
Dim X As Integer
Dim Kinfo As New OrderedKey
On Error GoTo Catch

' Get an ordered (sorted) key
Kinfo.InitKey = Key
If Kinfo.ln <> 9 Then
    Morse2Morbit = "Key must be of length nine :[" & Key & "]"
    Exit Function
End If

' Grab pairs of characters from Code and translate to index
Wrk = ""
For I = 1 To Len(Code) Step 2 ' Because we're jumping by 2, we may cut off a trailing "X" character.  OK.
    If Sep <> "" Then
        cnt = cnt + 1
        If cnt = 6 Then
            Wrk = Wrk & Sep
            cnt = 1
        End If
    End If
    X = 0
    For J = 0 To 1
        X = X * 3
        Select Case Mid(Code, I + J, 1)
            Case "X"
                X = X + 2
            Case "-"
                X = X + 1
        End Select
    Next J
    Wrk = Wrk & Kinfo.Idx(X + 1)
Next I
Morse2Morbit = Wrk
Exit Function
Catch:
Morse2Morbit = Err.Description
End Function

' Also does proper Morse Translation
Public Function encMorbit(text As String, Key As String, Optional Sep As String = "") As String
encMorbit = Morse2Morbit(Key, Alpha2Morse(text, "X", 2), Sep)
End Function

' Translates Morbit to Morse code
Public Function Morbit2Morse(Key As String, Code As String) As String
Dim I As Integer
Dim J As Integer
Dim Wrk As String
Dim Ch As String
Dim toDitDah() As String
Dim Kinfo As New OrderedKey
On Error GoTo Catch
' Ordered Key
Kinfo.InitKey = Key

toDitDah = Split(".,-,X", ",")
For I = 1 To Len(Code)
    Ch = Mid(Code, I, 1)
    If Ch >= "1" And Ch <= "9" Then
        ' Original index
        J = Kinfo.OrgIdx(CInt(Ch)) - 1
        Wrk = Wrk & toDitDah((J \ 3) Mod 3) & toDitDah(J Mod 3)
    End If
Next I
Morbit2Morse = Wrk
Exit Function
Catch:
Morbit2Morse = Err.Description
End Function

Public Function decMorbit(text As String, Key As String) As String
decMorbit = Morse2Alpha(Morbit2Morse(Key, text), "X")
End Function

' Converts a morse string to fractionated morse
' Morse text, separated by "X", padded to length of 3
Public Function Morse2FracMorse(Key As String, Code As String, Optional Sep As String = "") As String
Dim I As Integer
Dim J As Integer
Dim cnt As Integer
Dim Wrk As String
Dim X As Integer
Wrk = ""
For I = 1 To Len(Code) Step 3 ' Because we're jumping by 3, we may cut off 1-2 of the trailing "X" characters.  OK.
    If Sep <> "" Then
        cnt = cnt + 1
        If cnt = 6 Then
            Wrk = Wrk & Sep
            cnt = 1
        End If
    End If
    X = 0
    For J = 0 To 2
        X = X * 3
        Select Case Mid(Code, I + J, 1)
            Case "X"
                X = X + 2
            Case "-"
                X = X + 1
        End Select
    Next J
    Wrk = Wrk & Mid(Key, X + 1, 1)
Next I
Morse2FracMorse = Wrk
End Function

Public Function encFracMorse(text As String, Key As String, Optional Sep As String = "") As String
encFracMorse = Morse2FracMorse(Key, Alpha2Morse(text, "X", 3), Sep)
End Function

Public Function FracMorse2Morse(Key As String, Code As String) As String
Dim I As Integer
Dim J As Integer
Dim Wrk As String
Dim Ch As String
Dim toDitDah() As String
toDitDah = Split(".,-,X", ",")
On Error GoTo Catch
For I = 1 To Len(Code)
    Ch = Mid(Code, I, 1)
    J = InStr(1, Key, Ch, vbTextCompare)
    If J <> 0 Then ' ignore characters not in key
        J = J - 1
        Wrk = Wrk & toDitDah(J \ 9) & toDitDah((J \ 3) Mod 3) & toDitDah(J Mod 3)
    End If
Next I
FracMorse2Morse = Wrk
Exit Function
Catch:
FracMorse2Morse = Err.Description
End Function

Public Function decFracMorse(text As String, Key As String) As String
decFracMorse = Morse2Alpha(FracMorse2Morse(Key, text), "X")
End Function

' Converts a Pollux string to straight Morse
' Filtering out spaces, Pad in source code
Public Function Pollux2Morse(Key As String, Code As String, Optional Sep As String = "") As String
Pollux2Morse = XLAT(Code, "1234567890 " & Sep, PolluxKey(Key))
End Function

Public Function decPollux(text As String, Key As String, Optional Sep As String = "") As String
decPollux = Morse2Alpha(Pollux2Morse(Key, text, Sep))
End Function

' Converts a Morse string to Pollux

Public Function Morse2Pollux(Key As String, Code As String, Optional Sep As String = "") As String

Dim I As Integer
Dim cnt As Integer
Dim Space As tChar
Dim Dot As tChar
Dim Dash As tChar
'Dim lSpace As Integer
'Dim lDot As Integer
'Dim lDash As Integer
Dim Work As String
Dim Ch As String
'Dim RandVal As Long

' Validate key. Must be 10 characters long, characters must be in "012.- xX"
Work = PolluxKey(Key)
For I = 1 To 10
  Ch = Mid(Work, I, 1)
  Select Case Ch
    Case "."
      Dot.Digits = Dot.Digits & Right(I, 1)
    Case "-"
      Dash.Digits = Dash.Digits & Right(I, 1)
    Case " "
      Space.Digits = Space.Digits & Right(I, 1)
    Case Else
      Morse2Pollux = "Invalid key at position " & I & ":" & Key
      Exit Function
  End Select
Next I

' Make sure we got some of each type
Space.Count = Len(Space.Digits)
If Space.Count = 0 Then
  Morse2Pollux = "Invalid key; no spaces :" & Key
End If
Dot.Count = Len(Dot.Digits)
If Dot.Count = 0 Then
  Morse2Pollux = "Invalid key; no periods :" & Key
End If
Dash.Count = Len(Dash.Digits)
If Dash.Count = 0 Then
  Morse2Pollux = "Invalid key; no dashes :" & Key
End If

Key = Work
Work = ""

' Initialize the random number generator based on hash for string (reproduces the same output for a given string)
InitRand (Code)

' translate space, dot, dash charaters to random digits
For I = 1 To Len(Code)
  If Sep <> "" Then
    cnt = cnt + 1
    If cnt = 6 Then
        Work = Work & Sep
        cnt = 1
    End If
  End If
  Ch = Mid(Code, I, 1)
  Select Case Ch
    Case " "
      Work = Work & Mid(Space.Digits, Int(Rnd() * Space.Count) + 1, 1)
    Case "."
      Work = Work & Mid(Dot.Digits, Int(Rnd() * Dot.Count) + 1, 1)
    Case "-"
      Work = Work & Mid(Dash.Digits, Int(Rnd() * Dash.Count) + 1, 1)
    'Case Else Ignore anything but a space, dot or dash
    '  Morse2Pollux = "Invalid Code at position " & I & ":" & Ch
    '  Exit Function
  End Select
Next I
Morse2Pollux = Work
End Function

Public Function encPollux(text As String, Key As String, Optional Sep As String = "") As String
encPollux = Morse2Pollux(Key, Alpha2Morse(text), Sep)
End Function

' For testing
Private Function test(Table() As Integer, Code As String) As Boolean
Dim Work As String
Dim Ch As String
Dim I As Integer
Dim J As Integer
Dim V As String
Dim cnt As Integer
Dim DitDah() As Integer
ReDim DitDah(3)
test = False
Work = Code
For I = 1 To 10
    Ch = (I - 1)
    Select Case Table(I)
        Case 0
            V = " "
            DitDah(1) = DitDah(1) + 1
        Case 1
            V = "."
            DitDah(2) = DitDah(2) + 1
        Case Else ' 2
            V = "-"
            DitDah(3) = DitDah(3) + 1
    End Select
    Work = Replace(Work, Ch, V)
Next I
If DitDah(1) < 2 Or DitDah(2) < 2 Or DitDah(3) < 2 Then
    Exit Function
End If
cnt = 0
V = ""
For I = 1 To Len(Work)
    Ch = Mid(Work, I, 1)
    If Ch = " " Then
        V = ""
        cnt = cnt + 1
        If cnt > 2 Then
            Exit Function
        End If
    Else
        cnt = 0
        V = V + Ch
        If Len(V) > 6 Then
            Exit Function
        End If
        If Len(V) > 4 Then
            If TextVals.Exists(V) Then
                test = True
                Exit Function
            End If
        End If
        If Len(V) = 4 Then
            Select Case V
                Case "..--", ".-.-", "---.", "----"
                    Exit Function
            End Select
        End If
    End If
Next I
test = True
End Function

Private Sub Bump(Table() As Integer)
Dim I As Integer
Dim V As Integer
For I = 10 To 1 Step -1
    V = Table(I) + 1
    If V < 3 Then
        Table(I) = V
        Exit Sub
    End If
    Table(I) = 0
Next I
End Sub

' Only used by test function now
Private Function SetDigits(Key As String, Table() As Integer) As Long
Dim I As Integer
Dim Ch As String
Dim Value As Long
Dim Curr As Integer
Key = Right("0000000000" & Key, 10)
Value = 0
For I = 1 To 10
    Value = Value * 3
    Ch = Mid(Key, I, 1)
    Select Case Ch
        Case "1"
            Curr = 1
        Case "2"
            Curr = 2
        Case Else
            Curr = 0
    End Select
    Table(I) = Curr
    Value = Value + Curr
Next I
SetDigits = Value
End Function

' searches for a solution to a Pollux string

Public Function PolluxTest(Key As String, Code As String) As String
Dim Translate() As Integer
Dim I As Long
Dim Wrk As String
Dim X As Integer
Dim Strt As Integer
ReDim Translate(10)
Wrk = ""
Strt = SetDigits(Key, Translate) + 1
Call Bump(Translate)
For I = Strt To 59050
    If test(Translate, Code) Then
        For X = 1 To 10
            Wrk = Wrk & Translate(X)
        Next X
        PolluxTest = Wrk
        Exit Function
    End If
    Call Bump(Translate)
Next I
PolluxTest = "Not Found"
End Function
