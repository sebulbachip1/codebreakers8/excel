VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LetterObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       LetterObj
'
'USAGE:
'       Binds information about a letter (row, column, index)
'
'GROUPS:
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 09/18/2020 Chip Bloch    -Original Version (migrated from pseudo-object in polybius)
'-------------------------------------------------------------------------------------------

Public Idx As Integer ' Index of letter in alphabet (A = 1)
Public Row As Integer ' Row to find letter
Public Col As Integer ' Column to find letter
Public Ch As String   ' Value of character
