This project contains VBA source code for creating and solving Ciphers in Microsoft Excel (Google docs does not support VBA).  There are many examples of solving ciphers in Python, Java, C and other languages on the web.  This is the only VBA repository that I know of.

Although a few of the algorithms can be traced back to other places, they all had to be ported to VBA. The code for the Enigma machine is one such example - even though the link to the original C code is now a dead link.

However, most of the ciphers I wrote from scratch using the documentation for the cipher as requirements.  The DecoderRing spreadsheet provides examples for using the various functions, as well as links to pages on the web for a number of the ciphers.  I have tried to verify the operation of the functions against external links where possible for accuracy.

Please refer to Cipherlist.txt for the complete list of known ciphers as of this release.

A number of ciphers are unique to this repository.  The Index ciphers are one such example - these routines can be used to implement many different word/letter translations (including a few standard ciphers).  Other ciphers were created by myself or my co-workers, such as the prime ciphers.  These are likely not going to be found anywhere else.  Other cool features are things like dynamic switching from 5x5 Polybius squares to 6x6 or 7x7 with ciphers like Playfair or Checkerboard.

You are free to use the ciphers in this repository as you see fit.  See MIT_License.txt.

Although many of the ciphers in this repository were once considered "state of the art", none of the ciphers here are enterprise level security today.  I would suggest that they are used for educational purposes only.

Please see Using_Excel_Macros.docx for instructions if you need help setting things up.

You can import all the .cls and .bas files into an Excel file as needed.
Importing into an .xlsm macro enabled file will make the code available within that spread sheet.
Importing into an .xlam macro enabled add-in will make the Ciphers available in every excel spreadsheet you open, if the xlam file is added to your start-up folder

Chip Bloch
