Attribute VB_Name = "Globals"
Option Explicit
Option Compare Binary
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Globals
'
' USAGE:
'
'       Global constants: Variables and functions
'
' GROUPS:
'       String
'            Alphabet, Alpha25, GoldBug, NullChr, xAlpha32, xAlphaXOR, xAlpha32Hex,
'            xAlphaPunct
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 12/10/2020 Chip Bloch    -Migrated textType, cs_Alphabet from General
' 01/03/2021 Chip Bloch    -Added ttASCIIAny
' 03/13/2021 Chip Bloch    -Added ttNone, ttString
' 04/25/2021 Chip Bloch    -Migrated NullCh from PolyBius
'                          -Migrated Alphabet, Alpha25 and GoldBug from General
'                          -Added NullChr()
'                          -Migrated xAlpha32, xAlphaXOR, xAlpha32Hex, xAlphaPunct from base
' 04/27/2021 Chip Bloch    -Added ttAlphaPlus (50 character alphabet, 2x25)
' 04/28/2021 Chip Bloch    -Added SwpCh
'-------------------------------------------------------------------------------------------

Public Const cs_Alphabet As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
Private Const cs_Alphabet25 As String = "ABCDEFGHIKLMNOPQRSTUVWXYZ" ' no "J"

Public Const NullCh As String = "�"
Public Const SwpCh As String = "~"

Public Enum textType
    ttNone = -128       ' Not initialized
    ttString = -127     ' Initialized from string
    ttAlphaPlus = -8    ' Letters, numbers, punct (25 letter alphabet, remaining letter + ASCIIAll + *)
    ttAlpha25Upper = -7 ' Letters (25 letter alphabet, uppercase)
    ttASCIIAll = -6     ' Letters only and insert Digits and Punctuation
    ttASCIIPunct = -5   ' Letters only and insert punctuation after L-Z
    ttASCIIUpper = -4   ' All characters, fold alpha to upper
    ttAnumUpper = -3    ' Letters and number, fold alpha to upper
    ttAlphaDigit = -2   ' Letters only and insert digits after "A"-"J"
    ttAlphaUpper = -1   ' Letters only, uppercase (default)
    ttAlphaKeep = 0     ' Letters, preserve case from source (1st character)
    ttAlpha = 1         ' Letters (Upper & Lower)
    ttNumber = 2        ' Digits only
    ttAnum = 3          ' Digits and Letters (Upper & Lower)
    ttASCII = 4         ' All characters
    ttAlpha25 = 5       ' Letters (25 letter alphabet)
    ttASCIIAny = 6      ' ASCII, allow dups
End Enum

Public Enum fltType ' Filter Type
    ftSqCR = -4     ' Characters in square and alternate column/Row names
    ftSqRC = -3     ' Characters in square and alternate row/column names
    ftSqplus = -2   ' Characters in square plus numbers if # in square
    ftSquare = -1   ' Characters in Square
    ftRowCols = 0   ' Characters in Rows and Columns
    ftCols = 1      ' Characters in column names
    ftRows = 2      ' Characters in row names
    ftRowOrCols = 3 ' Characters in Row and/or column
    ftColRows       ' Characters in Columns and Rows
End Enum


' Deprecated these, they used to be used for Polybius.  Now using textType instead
'Public Enum ValType_ ' Types of characters stored in square
'    vtUpperAlpha = 0 ' Case insensitive Alpha - ttAlpha, ttAlphaKeep, ttAlphaUpper, ttAlpha25Upper, ttAlpha25
'    vtAnum = 1       ' Case Insensitive Alpha+digits - ttAnumUpper, ttAnum (ttAlphaDigit)
'    vtAscii = 2      ' ASCII - ttASCII
'    vtUpperASCII = 3 ' Case insensitive ASCII - ttASCIIUpper (ttASCIIAll, ttASCIIPunct)
'   none             ' ttNumber
'End Enum

'       String Constants
'            Alpha32 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
'            AlphaXOR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"
'            AlphaTxt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ !,-.?"
'            TriContaKaiDecimal = "0123456789ABCDEFGHIJKLMNOPQRSTUV"
'
Public Const xa_Alphabet As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"
Public Const xa_Alphabet32 As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
Public Const xa_AlphabetTri As String = "0123456789ABCDEFGHIJKLMNOPQRSTUV"
Public Const xa_AlphabetTxt As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ !,-.?"

' ******************** String Functions **********************

Public Function XAlpha32() As String
XAlpha32 = xa_Alphabet32
End Function

Public Function XAlphaXor() As String
XAlphaXor = xa_Alphabet
End Function

Public Function XAlpha32Hex()
'Tricontakaidecimal = xa_AlphabetTri
XAlpha32Hex = xa_AlphabetTri
End Function

Public Function XAlphaPunct()
XAlphaPunct = xa_AlphabetTxt
End Function

Public Function NullChr() As String
NullChr = NullCh
End Function

Public Function Alphabet() As String
Alphabet = cs_Alphabet
End Function

Public Function Alpha25() As String
Alpha25 = cs_Alphabet25
End Function

Public Function GoldBug() As String
' letters j, k, q, w, x and z were not part of the cipher from the book
'         "ABCDEFGHIjkLMNOPqRSTUVwxYz"
GoldBug = "52-�81346,709*�.$();?�]�:["
End Function


