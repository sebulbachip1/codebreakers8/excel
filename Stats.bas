Attribute VB_Name = "Stats"
Option Explicit
Option Compare Binary
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Stats
'
' USAGE:
'
'       Statistical functions
'
' GROUPS:
'
'       Statistical
'            CountCh, GetIC, GetFullIC, GetCS, ChiSquare, HashStr, InitRand, GetDIC
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 07/26/2020 Chip Bloch    -Initial version; Migrated functions from General
' 08/06/2020 Chip Bloch    -Created InitRand
' 11/06/2020 Chip Bloch    -Added Random()
' 01/30/2021 Chip Bloch    -Adjusted Lower Dim() to (1 To N) in some functions
' 03/21/2021 Chip Bloch    -Using AlphaList in GetIC (Dynamic Alphabet)
'                          -Added GetDIC
' 04/21/2021 Chip Bloch    -Renamed Alphalist.Items to AlphaList.Item
'-------------------------------------------------------------------------------------------

' ******************** Statistical Functions **********************

' Counts occurrences of a character in a string
' ignores case by default (vbTextCompare = 1), set compare to 0 to do a binary comparison
Public Function CountCh(S As String, Ch As String, Optional Compare As Integer = vbTextCompare) As Integer
Dim I As Integer, tot As Integer
If Compare = vbTextCompare Then
    Ch = UCase(Ch)
    S = UCase(S)
End If
For I = 1 To Len(S)
    If (Ch = Mid(S, I, 1)) Then
        tot = tot + 1
    End If
Next I
CountCh = tot
End Function

' Function to hash a string to a number
' https://stackoverflow.com/questions/7666509/hash-function-for-string
Public Function HashStr(ByRef S As String) As Long
Dim L As Long
Dim I As Integer
If Len(S) = 0 Then
  HashStr = 0
  Exit Function
End If
L = 5581
For I = 1 To Len(S)
    L = (L * 33 + Asc(Mid(S, I, 1))) And 33554431 ' 0x1FFFFFF
Next I
HashStr = L
End Function

Public Function InitRand(ByRef S As String) As Single
' Initialize the random number generator based on hash for string (reproduces the same output for a given string)
InitRand = Rnd(-HashStr(S))
End Function

Public Function Random() As Single
Random = Rnd()
End Function

' Index of Coincidence
' A measure of how "Random" a string of characters is
Public Function GetIC(Rng As Range, Optional Filt As Variant, Optional Repl As Variant) As Double
Dim Plaintext As String
Dim Counts As New AlphaList
Dim Cel As Range
Dim totCount As Long
Dim I As Integer
Dim V As Integer
Dim sum As Long
Dim Ch As String
On Error GoTo Catch

For Each Cel In Rng
    Plaintext = Plaintext & Cel.Value
Next Cel

Counts.Init Filt, Repl, oType.oZero

'Plaintext = UCase(Plaintext)
For I = 1 To Len(Plaintext)
    Ch = Mid(Plaintext, I, 1)
    If Counts.Exists(Ch) Then
        Counts(Ch) = Counts(Ch) + 1
        totCount = totCount + 1
    End If
Next I

For I = 0 To Counts.Count - 1
    V = Counts.Item(I)
    sum = sum + (V * (V - 1))
Next I
GetIC = 1000 * sum / (totCount * (totCount - 1))
Exit Function
Catch:
GetIC = -1
End Function

' Digraphic Index of Coincidence
' A measure of how "Random" a string of characters is
Public Function GetDIC(Rng As Range, Optional Filt As Variant, Optional Repl As Variant) As Double
Dim Plaintext As String
Dim Counts As New AlphaList
Dim Cel As Range
Dim totCount As Long
Dim I As Integer
Dim V As Integer
Dim sum As Long
Dim Ch As String
On Error GoTo Catch

For Each Cel In Rng
    Plaintext = Plaintext & Cel.Value
Next Cel

Counts.Init Filt, Repl, oType.oZero

Plaintext = Counts.Filter(Plaintext)

If Len(Plaintext) Mod 2 = 1 Then
    GetDIC = -2
    Exit Function
End If

Set Counts = New AlphaList

Counts.Init2 Filt, Repl, oType.oZero

For I = 1 To Len(Plaintext) Step 2
    Ch = Mid(Plaintext, I, 2)
    If Counts.Exists(Ch) Then
        Counts(Ch) = Counts(Ch) + 1
        totCount = totCount + 1
    End If
Next I

For I = 0 To Counts.Count - 1
    V = Counts.Item(I)
    sum = sum + (V * (V - 1))
Next I
GetDIC = 1000 * sum / (totCount * (totCount - 1))
Exit Function
Catch:
GetDIC = -1
End Function

' Index of Coincidence
' A measure of how "Random" a string of characters is
Public Function GetFullIC(Rng As Range, Optional Mask As String = "") As Double
Dim Plaintext As String
Dim Counts() As Integer
Dim Chars() As Integer
Dim totCount As Long
Dim I As Integer
Dim V As Integer
Dim sum As Long
Dim Out As Double
ReDim Counts(256)
ReDim Chars(256)
For I = 1 To Rng.Count
    Plaintext = Plaintext & Rng.Item(I).Value2
Next I
If Mask = "" Then Mask = Plaintext
For I = 1 To Len(Mask)
    V = Asc(Mid(Mask, I, 1))
    If V <= 256 Then
        Chars(V) = 1
    End If
Next I
For I = 1 To Len(Plaintext)
    V = Asc(Mid(Plaintext, I, 1))
    If V <= 256 And Chars(V) = 1 Then
        Counts(V) = Counts(V) + 1
        totCount = totCount + 1
    End If
Next I
For I = 0 To 256
    sum = sum + (Counts(I) * (Counts(I) - 1))
Next I
Out = 1000 * sum / (totCount * (totCount - 1))
GetFullIC = Out
End Function

' Chi Squared Statistic
' A measure of how "english-like" a string is
Public Function GetCS(Rng As Range) As Double
Dim Plaintext As String
Dim Counts() As Integer
Dim Expected() As Double
Dim ecnt As Double
Dim totCount As Long
Dim I As Integer
Dim V As Integer
Dim sum As Double
'Dim ec As Double
ReDim Counts(1 To 26)
ReDim Expected(1 To 26)
Expected(1) = 0.08167
Expected(2) = 0.01492
Expected(3) = 0.02782
Expected(4) = 0.04253
Expected(5) = 0.12702
Expected(6) = 0.02228
Expected(7) = 0.02015
Expected(8) = 0.06094
Expected(9) = 0.06966
Expected(10) = 0.00153
Expected(11) = 0.00772
Expected(12) = 0.04025
Expected(13) = 0.02406
Expected(14) = 0.06749
Expected(15) = 0.07507
Expected(16) = 0.01929
Expected(17) = 0.00095
Expected(18) = 0.05987
Expected(19) = 0.06327
Expected(20) = 0.09056
Expected(21) = 0.02758
Expected(22) = 0.00978
Expected(23) = 0.0236
Expected(24) = 0.0015
Expected(25) = 0.01974
Expected(26) = 0.00074
For I = 1 To Rng.Count
    Plaintext = Plaintext & Rng.Item(I).Value2
Next I
Plaintext = UCase(Plaintext)
For I = 1 To Len(Plaintext)
    V = Asc(Mid(Plaintext, I, 1)) - 64
    If V > 0 And V < 27 Then
        Counts(V) = Counts(V) + 1
        totCount = totCount + 1
    End If
Next I
For I = 1 To 26
    ecnt = Expected(I) * totCount
    sum = sum + Application.WorksheetFunction.Power((Counts(I) - ecnt), 2) / ecnt
    'ec = ec + Application.WorksheetFunction.Power((Counts(I) - totCount / 26#), 2) / (totCount / 26#)
Next I
GetCS = sum
End Function

' This function returns a ChiSquare computation of counts versus an expected random distribution
Public Function ChiSquare(R As Range, Optional Filt As Variant, Optional Repl As Variant) As Double
Dim Cel As Range
Dim S As String
Dim I As Integer
Dim Idx As Integer
Dim tot As Integer
Dim RDist As Double
Dim RTot As Double
Dim Ch As String
Dim Cnt As New AlphaList
On Error GoTo Catch

For Each Cel In R
    S = S & Cel.Value
Next Cel

Cnt.Init Filt, Repl, oType.oZero

For I = 1 To Len(S)
    Ch = Mid(S, I, 1)
    If Cnt.Exists(Ch) Then
        Cnt(Ch) = Cnt(Ch) + 1
        tot = tot + 1
    End If
Next I
If tot = 0 Then
    ChiSquare = Null
    Exit Function
End If
RDist = tot / Cnt.Count
For I = 0 To Cnt.Count - 1
    RTot = RTot + ((RDist - Cnt.Item(I)) ^ 2) / RDist
Next I
ChiSquare = RTot
Exit Function
Catch:
ChiSquare = -1
End Function

' This function returns a ChiSquare computation of digraph counts versus an expected random distribution
Public Function DIChiSquare(R As Range, Optional Filt As Variant, Optional Repl As Variant) As Double
Dim Cel As Range
Dim S As String
Dim I As Integer
Dim Idx As Integer
Dim tot As Integer
Dim RDist As Double
Dim RTot As Double
Dim Ch As String
Dim Cnt As New AlphaList
On Error GoTo Catch

For Each Cel In R
    S = S & Cel.Value
Next Cel

Cnt.Init Filt, Repl, oType.oZero

S = Cnt.Filter(S)

If Len(S) Mod 2 = 1 Then
    DIChiSquare = -2
    Exit Function
End If

Set Cnt = New AlphaList

Cnt.Init2 Filt, Repl, oType.oZero

For I = 1 To Len(S) Step 2
    Ch = Mid(S, I, 2)
    If Cnt.Exists(Ch) Then
        Cnt(Ch) = Cnt(Ch) + 1
        tot = tot + 1
    End If
Next I
If tot = 0 Then
    DIChiSquare = Null
    Exit Function
End If
RDist = tot / Cnt.Count
For I = 0 To Cnt.Count - 1
    RTot = RTot + ((RDist - Cnt.Item(I)) ^ 2) / RDist
Next I
DIChiSquare = RTot
Exit Function
Catch:
DIChiSquare = -1
End Function
