Attribute VB_Name = "Index"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Index
'
' USAGE:
'       Word Letter index cipher routines
'
' GROUPS:
'       Word/Letter
'            encLLookup, decLLookup
'
'       Keys
'            CreateNumberedKey, CreateHomophonicKey
'
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 09/07/2020 Chip Bloch    -Migrated encLLookup, decLLookup from Cipher
'                          -Added support for codes with zeros, either 0 = zero or ten
' 09/07/2020 Chip Bloch    -Added CreateNumberedKey, CreateHomophonicKey
' 11/22/2020 Chip Bloch    -Added substType.stMultiFib for Fibbonaci randomization
' 01/31/2021 Chip Bloch    -Updated documentation block
'                          -Corrected bug with single random values in encLLookup
' 02/01/2021 Chip Bloch    -Only split wildcards on separator when it is present in the list
'                           (allows codes like "? " not just "?")
' 03/30/2021 Chip Bloch    -Updated CreateHomophonicKey using Dedup("", "*", ttAlpha25Upper, Repl)
'-------------------------------------------------------------------------------------------

' https://powerspreadsheets.com/declare-variables-vba-excel/

Private Const cf_LWild_  As String = "?" ' Wildcard
Private Const cf_WWild_  As String = "*"

Private Const cf_LDigit_ As String = "0" ' Decimal
Private Const cf_WDigit_ As String = "9"

Private Const cf_LHex_   As String = "#" ' Hex
Private Const cf_WHex_   As String = "$"

Private Const cf_LRoman_ As String = "V" ' Roman numeral
Private Const cf_WRoman_ As String = "X"

Private Const cf_Null_   As String = "~" ' Null separator

' Modifiers

Private Const cf_Zero_   As String = "<" ' Zero based codes
Private Const cf_Fix_    As String = "=" ' Fixed width 1-9
Private Const cf_Var_    As String = ">" ' 1 based variable width, 10 may be 0 or 10, 100 may be 00 or 100, etc.
Private Const cf_Extend_ As String = "+" ' 1 based fixed width, 0 is 10, 00 is 100, etc.

Private Const WordWarning_ As Integer = 26

Private Enum ActionType_
    decode = 0
    encode = 1
End Enum

Private Type FormatInfo_
    Width As Integer
    Variable As Boolean
    Sep As String
    Head() As Integer
    Tail() As Integer
End Type

Private Enum FormatType_
    fLetter = 0
    fWord = 1
    fSep = 2
    fNull = 3
End Enum

Public Enum substType   ' Type of substitution
    stAny = 0           ' Random
    stOne = 1           ' Any one (select one)
    stEarly = 2         ' Favor early codes (select one)
    stMultiFib = 3      ' Favor early codes
End Enum

' This function translates the wildcard range
Private Function parseWild_(ByRef fmt As PrintFormat, Rng As Range) As String
Dim Val As String
Dim Cv As String
Dim Sep As String
Dim Cel As Range
Dim Wilds() As String
Dim Count As Integer
Dim I As Integer
Dim L As Integer
Dim Idx As Integer
    If fmt.Format <> Wild Then
        parseWild_ = "Err: Not a wildcard format"
        Exit Function
    End If
    If Rng Is Nothing Then
        parseWild_ = "ERR: Wildcard format with no range"
        Exit Function
    End If
    
    Val = ""
    
    Sep = Chr(0)

    Count = 0
    For Each Cel In Rng
        Cv = Cel.Value
        If Cv <> "" Then
            If Val <> "" Then
                Val = Val & Sep
            End If
            Val = Val & Cv
            Count = Count + 1
        End If
    Next Cel
    
    If Count > 1 Then
        Wilds = Split(Val, Sep)
    Else
        If fmt.hasSep And InStr(Val, fmt.Sep) > 0 Then
            Wilds = Split(Val, fmt.Sep)
        Else
            L = fmt.Length
            Count = Len(Val) \ L
            ReDim Wilds(Count - 1)
            Idx = 0
            For I = 1 To Len(Val) Step L
                Wilds(Idx) = Mid(Val, I, L)
                Idx = Idx + 1
            Next I
            'parseFormat = "ERR: Single cell wildcard and no separator specified"
            'Exit Function
        End If
    End If
    fmt.Wildcards = Wilds
End Function

' This function is the basis for word/letter lookup ciphers
' It parses out the format string and returns a list of formatted codes based on that string and the key.
' The list of codes is returned in Letters().
' the Info type is also passed back with format specifiers (trailing separator, variable width flag, etc.)
' This return string is empty on success, otherwise contains an error code from the parse, e.g.:
' ERR: Format string does not contain any letter specifiers
' ERR: Roman formats must use separators
' ERR: Variable with format and no separators
' Action is either Encrypt or Decrypt
' For encrypt, returns an array of 26 collections (one per letter)
' For decrypt, returns an array with a single collection, but the collection is keyed by the different codes.
Private Function parseFormat_(ByRef Letters() As Collection, ByRef Warnings() As String, key As String, sFmt As String, _
    ByRef Info As FormatInfo_, Action As ActionType_, Rng As Range, Rng2 As Range) As String
Dim I As Integer, J As Integer
Dim L As Integer
Dim M As Integer
Dim WordNum As Integer
Dim LetNum As Integer
Dim Ch As Integer
Dim Char As String
Dim Str As Variant
Dim strVal As String
Dim Wrk As String
Dim Val As String
Dim Chr As String
Dim Rep As String
Dim InWord As Boolean

Dim minl As Integer
Dim maxl As Integer

Dim lastFType As FormatType_

Dim Word As New PrintFormat
Dim Letter As New PrintFormat

Dim FormStr As New Collection

Dim DupKey As Collection

Dim WRng As Range
Dim LRng As Range

On Error GoTo Catch

' default format
If sFmt = "" Then
    sFmt = "9-0,"
End If

' initializations
Info.Sep = ""
Info.Width = 0
Info.Variable = False
ReDim Info.Head(255)
ReDim Info.Tail(255)

minl = 8192

Wrk = cf_Zero_ & cf_Fix_ & cf_Var_ & cf_Extend_  ' Zero, Fixed, 10 Variable, 10 Fixed width

' Pre-process Letter Modifiers

Val = cf_LDigit_ & cf_LHex_
For I = 1 To Len(Wrk) ' For each modifier
    For J = 1 To Len(Val) ' For each letter type
        Chr = Mid(Val, J, 1) ' get letter type
        Rep = Chr & Mid(Wrk, I, 1) ' build string, e.g. "0+"
        If InStr(sFmt, Rep) > 0 Then ' scan for string
            Letter.Extend = (I - 1) ' Found, set modifier
            sFmt = Replace(sFmt, Rep, Chr) ' And remove modifer from format string
        End If
    Next J
Next I

' Pre-process Word Modifiers

Val = cf_WDigit_ & cf_WHex_
For I = 1 To Len(Wrk) ' For each modifier
    For J = 1 To Len(Val) ' For each word type
        Chr = Mid(Val, J, 1) ' get word type
        Rep = Chr & Mid(Wrk, I, 1) ' build string, e.g. "9+"
        If InStr(sFmt, Rep) > 0 Then ' scan for string
            Word.Extend = (I - 1) ' Found, set modifier
            sFmt = Replace(sFmt, Rep, Chr) ' And remove modifer from format string
        End If
    Next J
Next I

' Parse format string
I = 1
M = Len(sFmt)
Do While I <= M
    Char = Mid(sFmt, I, 1)
    Select Case UCase(Char)
        Case cf_LDigit_ ' Decimal Letter
            L = 0
            Do While Mid(sFmt, I, 1) = Char
                L = L + 1
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fLetter
            Letter.AsNum (L)
            FormStr.Add Letter
        Case cf_WDigit_ ' Decimal Word
            L = 0
            Do While Mid(sFmt, I, 1) = Char
                L = L + 1
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fWord
            Word.AsNum (L)
            FormStr.Add Word
        Case cf_LRoman_ ' Roman Letter
            Do While UCase(Mid(sFmt, I, 1)) = UCase(Char)
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fLetter
            Letter.AsRoman
            FormStr.Add Letter
        Case cf_WRoman_ ' Roman Word
            Do While UCase(Mid(sFmt, I, 1)) = UCase(Char)
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fWord
            Word.AsRoman
            FormStr.Add Word
        Case cf_LHex_ ' Hex Letter
            L = 0
            Do While Mid(sFmt, I, 1) = Char
                L = L + 1
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fLetter
            Letter.AsHex (L)
            FormStr.Add Letter
        Case cf_WHex_ ' Hex Word
            L = 0
            Do While Mid(sFmt, I, 1) = Char
                L = L + 1
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fWord
            Word.AsHex (L)
            FormStr.Add Word
        Case cf_LWild_ ' Wildcard letter
            L = 0
            Do While Mid(sFmt, I, 1) = Char
                L = L + 1
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fLetter
            Letter.AsWild (L)
            FormStr.Add Letter
        Case cf_WWild_ ' Wildcard word
            L = 0
            Do While Mid(sFmt, I, 1) = Char
                L = L + 1
                I = I + 1
                If I > M Then Exit Do
            Loop
            lastFType = fWord
            Word.AsWild (L)
            FormStr.Add Word
        Case cf_Null_ ' Null
            Do While Mid(sFmt, I, 1) = Char
                I = I + 1
                If I > M Then Exit Do
            Loop
            If lastFType = fLetter Then
                Letter.Sep = Char
            ElseIf lastFType = fWord Then
                Word.Sep = Char
            End If
            lastFType = fNull
        Case Else ' it's a separator
            Val = ""
            Do
                Char = Mid(sFmt, I, 1)
                Select Case UCase(Char)
                    Case cf_LDigit_, cf_WDigit_, cf_LRoman_, cf_WRoman_, cf_LHex_, cf_WHex_, cf_LWild_, cf_WWild_, cf_Null_
                        Exit Do
                    Case Else
                        I = I + 1
                        Val = Val + Char
                End Select
            Loop Until I > M
            FormStr.Add Val
            If lastFType = fLetter Then ' follows letter
                Letter.Sep = Val
            ElseIf lastFType = fWord Then ' follows word
                Word.Sep = Val
            End If
            lastFType = fSep
    End Select
Loop

' some extra validation
If Not Letter.isValid Then
    parseFormat_ = "ERR: No letter code in format string (" & cf_LDigit_ & ", " & cf_LHex_ & ", " & cf_LRoman_ & ", " & cf_LWild_ & ")"
    Exit Function
End If
'If Word.isValid And Word.Sep = "" And Word.Format = Roman Then ' check for potential problems
'    ' can't use roman word (variable width) with no separator
'    parseFormat = "ERR: Roman style word must use separator"
'    Exit Function
'End If
'If Letter.Sep = "" And Letter.Format = Roman Then ' check for potential problems
'    ' can't use roman letter (variable width) with no separator
'    parseFormat = "ERR: Roman style letter must use separator"
'    Exit Function
'End If

If lastFType = fSep Then ' remove trailing separator
    Info.Sep = FormStr.Item(FormStr.Count)
    FormStr.Remove FormStr.Count
End If

' Read wildcard values if specified
If Letter.Format = Wild Or Word.Format = Wild Then
    If Word.Format = Wild Then
        Val = parseWild_(Word, Rng)
        If Val <> "" Then
            parseFormat_ = Val
            Exit Function
        End If
        
        If Letter.Format = Wild Then
            Val = parseWild_(Letter, Rng2)
            If Val <> "" Then
                parseFormat_ = Val
                Exit Function
            End If
        End If
    Else
        Val = parseWild_(Letter, Rng)
        If Val <> "" Then
            parseFormat_ = Val
            Exit Function
        End If
    End If
End If

' parse key
WordNum = 0
LetNum = 0

ReDim Warnings(WordWarning_)
If Action = encode Then
    ReDim Letters(25)
Else
    ReDim Letters(0)
End If
For I = 0 To UBound(Letters)
    Set Letters(I) = New Collection
Next I

Set DupKey = New Collection

For I = 1 To Len(key)
    Ch = Asc(UCase(Mid(key, I, 1))) - 65
    If Ch < 0 Or Ch > 25 Then
        InWord = False
        GoTo ContinueFor
    End If
    If Word.isValid And Not InWord Then
        InWord = True
        WordNum = WordNum + 1
        If WordNum > Word.Count Then
            ' exit if max word count exceeded for format, but log potential error
            If Word.Format = Wild Then
                Warnings(WordWarning_) = "Maximum dynamic word count (" & Word.Count & ") exceeded at position " & I & " in key."
            Else
                Warnings(WordWarning_) = "Maximum fixed word count (" & Word.Count & ") exceeded at position " & I & " in key."
            End If
            Exit For
        End If
        Word.Value = WordNum
        LetNum = 0 ' restart letter count when using words
    End If
    LetNum = LetNum + 1
    ' don't add letters beyond 9th letter for fixed with
    ' (15 for hex format)
    ' variable for wild
    If LetNum > Letter.Count Then
        ' continue if letter word count exceeded for format, log warning
        If Warnings(Ch) = "" Then
            If Letter.Format = Wild Then
                Warnings(Ch) = "Maximum dynamic letter count (" & Letter.Count & ") exceeded at position " & I & " in key."
            Else
                Warnings(Ch) = "Maximum fixed letter count (" & Letter.Count & ") exceeded at position " & I & " in key."
            End If
        End If
        GoTo ContinueFor
    End If
    
    Letter.Value = LetNum
    
    ' Build code
    Val = ""
    For Each Str In FormStr
        strVal = CStr(Str)
        Val = Val & strVal
    Next
    
    ' Check for variable length
    L = Len(Val)
    If minl > L Then minl = L
    If maxl < L Then maxl = L
    
    'Debug.Print Val
    If Action = encode Then
        Letters(Ch).Add Val
    Else
        Letters(0).Add Ch, Val
    End If
    
    ' Will cause error for duplicate key
    DupKey.Add Ch, Val
    
    ' track first & last characters (in case we need to parse)
    If Info.Sep = "" Then
        Ch = Asc(Left(Val, 1))
        Info.Head(Ch) = 1
        Ch = Asc(Right(Val, 1))
        Info.Tail(Ch) = 1
    End If

ContinueFor:
Next I

Info.Width = maxl
Info.Variable = maxl <> minl

If Info.Variable And Info.Sep = "" Then
    For I = 0 To 255
        ' If duplicate characters are used at both ends, there is no way to parse this code
        If Info.Head(I) = 1 And Info.Tail(I) = 1 Then
            parseFormat_ = "Err: Variable width codes and no separator, cannot parse"
            Exit Function
        End If
    Next I
End If

parseFormat_ = ""

Exit Function
Catch:
If Err.Number = 457 Then
    parseFormat_ = "Err: Duplicate generated code '" & Val & "', please check format string"
Else
    parseFormat_ = "ParseFormat: " & Err.Number & " " & Err.Description
End If
End Function


' This function is the word/letter encryption function.
' Calls parseformat to parse out the format string and key before encrypting.
' default CType is to use all letters (Possible homophonic/polyalphabetic cipher)
' CType 1 to 2 will be simple substitution (only one code per letter)
' will return the encrypted string or an error (potentially from parseformat)
' errors from this routine include plaintext letter not found in key
Public Function encLLookup(Plaintext As String, key As Variant, Optional sFmt As String, Optional cType As substType = substType.stAny, _
    Optional Rng As Range = Nothing, Optional Rng2 As Range = Nothing) As String
Dim Letters() As Collection
Dim Warnings() As String

Dim Info As FormatInfo_

Dim I As Integer
Dim J As Integer
Dim K As Integer
Dim M As Integer
Dim Cnt As Integer
Dim Ch As Integer
Dim Char As String
Dim Val As String

'Dim Sel As Collection

Dim F As Long
Dim FibArr() As Long
Dim FibSum() As Long

Dim Cel As Range

On Error GoTo Catch

Val = parseFormat_(Letters, Warnings, StrRange(key, " ", , False), sFmt, Info, encode, Rng, Rng2)
If Val <> "" Then
    encLLookup = Val
    Exit Function
End If

' Letters() now contains the codes parsed out from the key string
' Letters() array is 26 long (0-25), one per letter of the alphabet

' Initialize the random number stream
InitRand (Plaintext)

If cType = substType.stOne Then ' only use one random code per letter
    For Ch = 0 To 25
        If Letters(Ch).Count > 1 Then
            ' Collection index is one based
            M = Int(Rnd * Letters(Ch).Count) + 1
            Val = Letters(Ch).Item(M)
            Set Letters(Ch) = New Collection
            Letters(Ch).Add Val
        End If
    Next Ch
End If

' Multi Fibbonaci
If cType = substType.stMultiFib Or cType = substType.stEarly Then ' Build Fibbonaci arrays
    M = 0 ' Find Max Count
    For I = 0 To 25
        If Letters(I).Count > M Then
            M = Letters(I).Count
        End If
    Next I
    ReDim FibArr(M) ' Redim Arrays
    ReDim FibSum(M)
    FibArr(0) = 1 ' First entries are 1, 1
    FibArr(1) = 1
    FibSum(1) = 1 ' Sum starts at 2nd fibbonaci number
    For I = 2 To M
        FibArr(I) = FibArr(I - 1) + FibArr(I - 2)
        FibSum(I) = FibSum(I - 1) + FibArr(I)
    Next I
End If

' Fix to use Fibbonacci sequence
If cType = substType.stEarly Then ' only use one random code per letter, favor early codes
    For Ch = 0 To 25
        Cnt = Letters(Ch).Count
        If Cnt > 1 Then ' need to randomize
            F = Int(Rnd * FibSum(Cnt)) + 1
            ' Binary search
            J = 1
            K = Cnt
            M = (J + K) \ 2
            Do While FibSum(M - 1) >= F Or FibSum(M) < F
                If FibSum(M) < F Then
                    J = M + 1
                Else
                    K = M
                End If
                M = (J + K) \ 2
            Loop
            M = Cnt - M + 1 ' Have to flip index to opposite end of queue
            ' Save M'th item, re-add to new collection
            Val = Letters(Ch).Item(M)
            Set Letters(Ch) = New Collection
            Letters(Ch).Add Val
        End If
    Next Ch
End If

' now to the cipher text
Val = ""
For I = 1 To Len(Plaintext)
    Char = Mid(Plaintext, I, 1)
    Ch = Asc(UCase(Char)) - 65
    If Ch >= 0 And Ch <= 25 Then
        Cnt = Letters(Ch).Count
        If Cnt = 0 Then
            If Warnings(Ch) = "" Then
                If Warnings(WordWarning_) = "" Then
                    encLLookup = "Err: Character '" & Char & "' at position " & I & " not found in key"
                Else
                    encLLookup = "Err: Character '" & Char & "' at position " & I & " not found in key before " & Warnings(WordWarning_)
                End If
            Else
                encLLookup = "Err: Character '" & Char & "' is at position " & I & ", but skipped when " & Warnings(Ch)
            End If
            Exit Function
        ElseIf Cnt = 1 Then
            M = 1
        Else ' Cnt > 1
            If cType <> substType.stMultiFib Then
                M = Int(Rnd * Cnt) + 1
            Else ' random code per letter, but favor early codes
                F = Int(Rnd * FibSum(Cnt)) + 1
                ' Binary search
                J = 1
                K = Cnt
                M = (J + K) \ 2
                Do While FibSum(M - 1) >= F Or FibSum(M) < F
                    If FibSum(M) < F Then
                        J = M + 1
                    Else
                        K = M
                    End If
                    M = (J + K) \ 2
                Loop
                M = Cnt - M + 1 ' Have to flip index to opposite end of queue
'                M = M
            End If
        End If
        If Val <> "" Then
            Val = Val & Info.Sep
        End If
        Val = Val & Letters(Ch).Item(M)
    End If
Next I

encLLookup = Val

Exit Function
Catch:
encLLookup = "encLookup: " & Err.Number & " " & Err.Description
End Function

' This function is the word/letter decryption function.
' Calls parseformat to parse out the format string and key before decrypting.
' will return the decrypted string or an error (potentially from parseformat)
' errors from this routine include code not found in key
Public Function decLLookup(Code As String, key As Variant, Optional sFmt As String, Optional Rng As Range = Nothing, Optional Rng2 As Range = Nothing) As String
Dim Letters() As Collection
Dim Warnings() As String
Dim Codes() As String
Dim Info As FormatInfo_
Dim I As Integer
'Dim J As Integer
Dim L As Integer
Dim M As Integer
Dim Ch As Integer
Dim Cv As String
Dim Val As String

Dim Sel As Collection

On Error GoTo Catch

Val = parseFormat_(Letters, Warnings, StrRange(key, " ", , False), sFmt, Info, decode, Rng, Rng2)
If Val <> "" Then
    decLLookup = Val
    Exit Function
End If

Set Sel = Letters(0)

' now to the cipher text
If Info.Sep <> "" Then ' we can use the separator character
    Codes = Split(Code, Info.Sep)
    M = UBound(Codes)
ElseIf Info.Variable = False Then ' they're fixed width codes
    L = Info.Width
    ReDim Codes(Len(Code) \ L)
    M = -1
    For I = 1 To Len(Code) Step L
        M = M + 1
        Codes(M) = Mid(Code, I, L)
    Next I
Else ' They're variable width, parse using head/tail characters
    M = -1
    L = Len(Code)
    Val = ""
    I = 1
    Do While I <= L
        ' look for character at tail of code
        Do While I <= L
            Cv = Mid(Code, I, 1)
            If Info.Tail(Asc(Cv)) = 1 Then
                Exit Do
            End If
            Val = Val & Cv
            I = I + 1
        Loop
        ' look for character at head of code
        Do While I <= L
            Cv = Mid(Code, I, 1)
            If Info.Head(Asc(Cv)) = 1 Then
                Exit Do
            End If
            Val = Val & Cv
            I = I + 1
        Loop
        ' Add code to array
        M = M + 1
        ReDim Preserve Codes(M)
        Codes(M) = Val
        Val = ""
    Loop
End If

' codes array now contains all the codes in the source string
' M contains the number of codes
' Sel collection contains the codes parsed from the key string

Val = ""
For I = 0 To M
    Ch = Sel.Item(Codes(I))
    Val = Val & Chr(Ch + 65)
Next I

decLLookup = Val

Exit Function
Catch:
If Err.Number = 5 Then
    decLLookup = "decLLookup: Err: Translation for code number " & I & " '" & Codes(I) & "' not found!"
Else
    decLLookup = "decLLookup: " & Err.Number & " " & Err.Description
End If
End Function

Public Function CreateNumberedKey(Phrase As String, Shift As Integer) As String
Dim Used As String
'Dim UnUsed As String
Phrase = ToAlpha(Phrase)
Used = Dedup(Phrase)
'UnUsed = Mid(Dedup(Used, Alphabet()), Len(Used) + 1, 26)
CreateNumberedKey = Rotate(Phrase & Mid(Dedup(Used, Alphabet()), Len(Used) + 1, 26), Shift)
End Function

Public Function CreateHomophonicKey(key As String, Optional Repl As String = "") As String
Dim I As Integer
Dim Alpha As String
Dim Wrk As String

Alpha = Dedup("", "*", ttAlpha25Upper, Repl)

key = Compact(key, "", Repl)
If Len(key) <> 4 Then
    CreateHomophonicKey = "Error: Key not length 4: [" & key & "]"
    Exit Function
End If

For I = 1 To 4
    Wrk = Wrk & Rotate(Alpha, InStr(Alpha, (Mid(key, I, 1))) - 1)
Next I
CreateHomophonicKey = Wrk
End Function

