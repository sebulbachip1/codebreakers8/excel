Attribute VB_Name = "Cipher"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Cipher
'
'DEPENDENCIES:
'       External References: "Microsoft Scripting Runtime"
'
' USAGE:
'       General cipher routines
'
' GROUPS:
'       CONDI
'            encCONDI, decCONDI
'
'       Gromark
'            encGromark, decGromark
'
'       Hill
'            encHill, decHill, genHillKey, MatrixSquare
'
'       Portax
'            encPortax, decPortax
'
'       Quagmires (I, II, III, IV)
'            QuagEncode, QuagDecode
'
'       Ragbaby
'            encRagbaby, decRagbaby
'
'       Shift
'            encShift, decShift
'
'       Slidefair
'            decryptSlide, encryptSlide
'
'       TRIFID
'            encTRIFID, decTRIFID
'
'       Vigenere (zero-based, A=0)
'            VigEncode, VigDecode
'''''''''''''''''''''''''''''''''''''''''''''''''''''
'       String
'            AlphaEncode, Affine
'
'       Numeric Key (Zero Based)
'            NumEncode, NumDecode
'
'       Alternate (one-based, A=1)
'            Encrypt, Decrypt
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 01/28/2019 Chip Bloch    -Added encLLookup, decLLookup
' 01/29/2019 Chip Bloch    -Calling StrRange on Key for encLLookup/decLLookup
' 05/03/2019 Chip Bloch    -Converting Variant to String in ParseFormat
' 06/03/2019 Chip Bloch    -Added Interupted Key to Vigenere routines
' 06/09/2019 Chip Bloch    -Migrated encCol/decCol to trans module (transposition ciphers)
' 06/24/2019 Chip Bloch    -Added Affine
' 10/12/2019 Chip Bloch    -Updated Vigenere documentation, Added Progressive
'                          -Added Variant/Beaufort
' 07/12/2020 Chip Bloch    -Added XorStr
' 07/16/2020 Chip Bloch    -Added CipherType, Porta
'                          -Added runType
'                          -Corrected bug in Vigenaire Autokey
' 08/06/2020 Chip Bloch    -Calling InitRand in encLLookup
' 08/14/2020 Chip Bloch    -Using Vig runtype in XorEncode (Autokey)
' 08/16/2020 Chip Bloch    -Using Scripting.Dictionary in XOREncode (prep for adding
'                           Additional alphabets)
' 08/17/2020 Chip Bloch    -XORStr dropping source text that would produce a NUL
' 08/18/2020 Chip Bloch    -Migrated XORStr to Base, RunType to General
' 09/05/2020 Chip Bloch    -Renamed private types/functions with trailing "_"
'                          -Added encrypSlide, added ciphertype parm to decryptslide
' 09/06/2020 Chip Bloch    -Added encGromark, decGromark
' 09/07/2020 Chip Bloch    -Migrated encLLookup, decLLookup to Index
' 09/16/2020 Chip Bloch    -Added encTRIFID, decTRIFID
' 09/20/2020 Chip Bloch    -Added encCONDI, decCONDI
' 10/03/2020 Chip Bloch    -Added PUBLIC modifer to Affine
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
' 11/28/2020 Chip Bloch    -Added Portax
' 11/29/2020 Chip Bloch    -Added Quagmires
' 12/31/2020 Chip Bloch    -Made TRIFID Ignore separator in key
' 01/01/2021 Chip Bloch    -Added Ragbaby
' 03/08/2021 Chip Bloch    -Added Shift Cipher
' 03/10/2021 Chip Bloch    -Added encHill, decHill, genHillKey, MatrixSquare
' 03/13/2021 Chip Bloch    -Converted Gromark, CONDI, Ragbaby, Shift to AlphaList
' 03/30/2021 Chip Bloch    -Quagmire using AlphaList
'                          -Slidefair not using replacements
' 04/11/2021 Chip Bloch    -Altered hill cipher handling of period for 2x2 matrices
'                          -Added default for period -1 (length of string \ matrix.RC)
' 04/17/2021 Chip Bloch    -Calling Seriate_ in encHill,decHill
'                          -Moved MaxAttempts_ to top of module
' 04/23/2021 Chip Bloch    -Calling Seriate_ in encTRIFID, decTRIFID
'-------------------------------------------------------------------------------------------

' https://powerspreadsheets.com/declare-variables-vba-excel/

Public Enum CipherType
    ctVigenaire = 0
    ctVariant = 1
    ctBeufort = 2
    ctPorta = 3
End Enum

Private Const MaxAttempts_ As Integer = 128 ' Max Attempts for genHillKey to loop

' Function Affine
' The affine cipher exchanges letters according to a mathematical formula Ct = (A * Pt + B) % 26
' Letters are converted to numbers form 0-25 (A-Z).
Public Function Affine(A As Integer, b As Integer) As String
Dim I As Integer
Dim Wrk As String
For I = 0 To 25 ' Build translation string
    Wrk = Wrk & Chr(((A * I + b) Mod 26) + 65)
Next I
Wrk = Dedup(Wrk) ' Validate exactly 26 letters generated
If Len(Wrk) < 26 Then
    Affine = "Error, A and B are not coprime"
Else
    Affine = Wrk
End If
End Function

' This function is the "Slidefair" decryption algorithm

Public Function decryptSlide(Plaintext As String, Key As String, Optional Method As CipherType = CipherType.ctVigenaire) As String
Dim I As Integer

Dim K As Integer
Dim Kv() As Integer
Dim KeyLen As Integer

Dim Pv1 As Integer, Pv2 As Integer
Dim Cv1 As Integer, Cv2 As Integer

Dim Ch As String

Dim W As String
Dim StrLen As Integer

' Fold to uppercase
Plaintext = ToAlpha(Plaintext)
StrLen = Len(Plaintext)
If StrLen Mod 2 = 1 Then ' wasn't encrypted properly, return error
    decryptSlide = "Error: Source string had odd length [" & Plaintext & "]"
    Exit Function
End If

ReDim Kv(1 To Len(Key))
' Parse key into array
For K = 1 To Len(Key)
    Ch = UCase(Mid(Key, K, 1))
    If Ch >= "A" And Ch <= "Z" Then
        KeyLen = KeyLen + 1
        Kv(KeyLen) = Asc(Ch) - 65
    End If
Next K

If KeyLen = 0 Then ' return plaintext string if no key
    decryptSlide = Plaintext
    Exit Function
End If

K = 1
For I = 1 To StrLen Step 2
    ' decryption is digraphs, same key value for both letters in the pair
    
    ' encrypt char 1 (becomes char 2)
    Pv1 = Asc(Mid(Plaintext, I, 1)) - 65
    Select Case Method
        Case CipherType.ctVariant
            Cv2 = (Pv1 - Kv(K) + 26) Mod 26
        Case CipherType.ctBeufort
            Cv2 = (Kv(K) - Pv1 + 26) Mod 26
        Case Else ' Default: CipherType.ctVigenaire
            Cv2 = (Pv1 + Kv(K)) Mod 26
    End Select
    
    ' decrypt char 2 (becomes char 1)
    Pv2 = Asc(Mid(Plaintext, I + 1, 1)) - 65
    Select Case Method
        Case CipherType.ctVariant
            Cv1 = (Pv2 + Kv(K)) Mod 26
        Case CipherType.ctBeufort
            Cv1 = (Kv(K) - Pv2 + 26) Mod 26
        Case Else ' CipherType.ctVigenaire
            Cv1 = (Pv2 - Kv(K) + 26) Mod 26
    End Select
    
    ' function is nearly symmetrical with encryptslide, except for this step
    ' If digraph is equal after encryption, bump to left
    If (Pv1 = Cv1) And (Pv2 = Cv2) Then
        Cv1 = (Cv1 + 25) Mod 26
        If Method = ctBeufort Then
            Cv2 = (Cv2 + 1) Mod 26 ' Beaufort ciphertext is descending
        Else
            Cv2 = (Cv2 + 25) Mod 26
        End If
    End If
    
    ' Next key value
    K = K + 1
    If K > KeyLen Then
        K = 1
    End If
    W = W & Chr(Cv1 + 65) & Chr(Cv2 + 65)
Next I
decryptSlide = W
End Function

' This function is the "Slidefair" encryption algorithm

Public Function encryptSlide(Plaintext As String, Key As String, Optional Method As CipherType = CipherType.ctVigenaire) As String
Dim I As Integer

Dim K As Integer
Dim Kv() As Integer
Dim KeyLen As Integer

Dim Pv1 As Integer, Pv2 As Integer
Dim Cv1 As Integer, Cv2 As Integer

Dim Ch As String

Dim W As String
Dim StrLen As Integer

' Fold to uppercase, add PAD("X") at end if needed
Plaintext = Compact(Plaintext, "+", "-")
StrLen = Len(Plaintext)

ReDim Kv(1 To Len(Key))
' Parse key into array
For K = 1 To Len(Key)
    Ch = UCase(Mid(Key, K, 1))
    If Ch >= "A" And Ch <= "Z" Then
        KeyLen = KeyLen + 1
        Kv(KeyLen) = Asc(Ch) - 65
    End If
Next K

If KeyLen = 0 Then ' return plaintext string if no key
    encryptSlide = Plaintext
    Exit Function
End If

K = 1
For I = 1 To StrLen Step 2
    ' encryption is digraphs, same key value for both letters in the pair

    ' encrypt char 1 (becomes char 2)
    Pv1 = Asc(Mid(Plaintext, I, 1)) - 65
    Select Case Method
        Case CipherType.ctVariant
            Cv2 = (Pv1 - Kv(K) + 26) Mod 26
        Case CipherType.ctBeufort
            Cv2 = (Kv(K) - Pv1 + 26) Mod 26
        Case Else ' Default: CipherType.ctVigenaire
            Cv2 = (Pv1 + Kv(K)) Mod 26
    End Select
    
    ' Decrypt char 2 (becomes char 1)
    Pv2 = Asc(Mid(Plaintext, I + 1, 1)) - 65
    Select Case Method
        Case CipherType.ctVariant
            Cv1 = (Pv2 + Kv(K)) Mod 26
        Case CipherType.ctBeufort
            Cv1 = (Kv(K) - Pv2 + 26) Mod 26
        Case Else ' CipherType.ctVigenaire
            Cv1 = (Pv2 - Kv(K) + 26) Mod 26
    End Select
    
    ' function is nearly symmetrical with encryptSlide, except for this step
    ' If digraph is equal after encryption, bump to right
    If (Pv1 = Cv1) And (Pv2 = Cv2) Then
        Cv1 = (Cv1 + 1) Mod 26
        If Method = ctBeufort Then ' beufort ciphertext is descending
            Cv2 = (Cv2 + 25) Mod 26
        Else
            Cv2 = (Cv2 + 1) Mod 26
        End If
    End If
    
    ' next key value
    K = K + 1
    If K > KeyLen Then
        K = 1
    End If
    W = W & Chr(Cv1 + 65) & Chr(Cv2 + 65)
Next I
encryptSlide = W
End Function

' This is the general Quagmire encode function - a variation on Vigenere with keyed cipher or plaintext alphabets
' Quagmire I uses PlainKey to key the plaintext alphabet
' Quagmire II uses CipherKey to key the ciphertext alphabet
' Quagmire III uses PlainKey and Cipherkey, both identical
' Quagmire IV uses PlainKey and Cipherkey, both different
' IndicKey is the actual cipher key
' IndicLetter is the plaintext letter it appears under (defaults to "A")
' Runtype is the key type (as per Vigenere), but only rtWrap is standard for Quagmires.
' Optional Sep will be inserted every five characters if specified

Public Function QuagEncode(Plaintext As String, PlainKey As String, CipherKey As String, IndicKey As String, _
    Optional IndicLetter As String, Optional Run As RunType = RunType.rtWrap, Optional Sep As String = "") As String
Dim I As Integer

Dim K As Integer
Dim Kv() As Integer
Dim KOfs As Integer
Dim KeyLen As Integer

Dim PV As Integer
Dim Cv As Integer

Dim Ch As String
Dim Alpha As String

Dim IndicOfs As Integer

Dim W As String
Dim StrLen As Integer

' Set Alphabets
Dim PlainAlpha As New AlphaList
Dim CipherAlpha As New AlphaList
Dim KeyAlpha As New Scripting.Dictionary

Dim Dup As New AlphaList

On Error GoTo Catch

'PlainAlpha.CompareMode = TextCompare
'CipherAlpha.CompareMode = TextCompare
'KeyAlpha.CompareMode = TextCompare

IndicLetter = Left(ToAlpha(IndicLetter & "A"), 1)

' Plain Alphabet (Letter lookup to number)
Alpha = Dup.Dedup(PlainKey, "*")
PlainAlpha.Init (Alpha)

IndicOfs = PlainAlpha(IndicLetter)

' Cipher Alphabet (Number lookup to letter)
Alpha = Dup.Undup(CipherKey, "*")
CipherAlpha.Init (Alpha)
'CipherAlpha.AltIndex
For I = 0 To 25
    Ch = Mid(Alpha, I + 1, 1)
'    CipherAlpha.Add I, Ch
    KeyAlpha.Add Asc(Ch) - 65, I
Next I

StrLen = Len(Plaintext)
ReDim Kv(1 To Application.Max(Len(IndicKey), StrLen))

' Parse key into array
For K = 1 To Len(IndicKey)
    Ch = UCase(Mid(IndicKey, K, 1))
    If Ch >= "A" And Ch <= "Z" Then
        KeyLen = KeyLen + 1
        Kv(KeyLen) = Asc(Ch) - 65
    End If
Next K

K = 1
For I = 1 To Len(Plaintext)
    Ch = UCase(Mid(Plaintext, I, 1))
    If Ch >= "A" And Ch <= "Z" Then
        If K <= KeyLen Then
            PV = PlainAlpha(Ch)
            If Run = RunType.rtAutokey And KeyLen < StrLen Then ' Auto key (pre)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = PV
            End If
            KOfs = KeyAlpha.Item(Kv(K))
            Cv = (PV + KOfs - IndicOfs + 26) Mod 26
            
            Ch = CipherAlpha.Char(Cv)
            If Run = RunType.rtPosttext And KeyLen < StrLen Then ' Auto key (post)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = Cv
            End If
            K = K + 1
            If K > KeyLen Then
                If Run = RunType.rtProgressive Then ' progressive
                    For K = 1 To KeyLen
                        Kv(K) = Kv(K) + 1
                        If Kv(K) > 25 Then ' Z
                            Kv(K) = 0  ' A
                        End If
                    Next K
                End If
                K = 1
            End If
        End If
    Else
        If Run = RunType.rtInterrupt And Ch = " " Then ' Interrupted Key
            K = 1
        End If
    End If
    W = W & Ch
Next I
If Sep <> "" Then
    W = GroupBy(W, , Sep)
End If
QuagEncode = W
Exit Function
Catch:
QuagEncode = Err.Description
End Function

' This is the general Quagmire decode function - a variation on Vigenere with keyed cipher or plaintext alphabets
' Quagmire I uses PlainKey to key the plaintext alphabet
' Quagmire II uses CipherKey to key the ciphertext alphabet
' Quagmire III uses PlainKey and Cipherkey, both identical
' Quagmire IV uses PlainKey and Cipherkey, both different
' IndicKey is the actual cipher key
' IndicLetter is the plaintext letter it appears under (defaults to "A")
' Runtype is the key type (as per Vigenere), but only rtWrap is standard for Quagmires.
' Optional Sep will remove any inserted spaces from encode
Public Function QuagDecode(CipherText As String, PlainKey As String, CipherKey As String, IndicKey As String, _
    Optional IndicLetter As String, Optional Run As RunType = RunType.rtWrap, Optional Sep As String = "") As String
Dim I As Integer
Dim K As Integer
Dim PV As Integer
Dim Cv As Integer
Dim Ch As String
Dim W As String

Dim KeyLen As Integer
Dim StrLen As Integer
Dim Kv() As Integer
Dim KOfs As Integer

Dim Alpha As String

Dim IndicOfs As Integer

Dim Dup As New AlphaList
' Set Alphabets
Dim PlainAlpha As New AlphaList
Dim CipherAlpha As New AlphaList
Dim KeyAlpha As New Scripting.Dictionary

On Error GoTo Catch

IndicLetter = Left(ToAlpha(IndicLetter & "A"), 1)

' Plain Alphabet (Number to Letter lookup)
Alpha = Dup.Dedup(PlainKey, "*")
PlainAlpha.Init (Alpha)
IndicOfs = PlainAlpha(IndicLetter)

' Cipher Alphabet (Letter to Number lookup)
Alpha = Dup.Undup(CipherKey, "*")
CipherAlpha.Init (Alpha)
For I = 0 To 25
    Ch = Mid(Alpha, I + 1, 1)
    'CipherAlpha.Add Ch, I
    KeyAlpha.Add Asc(Ch) - 65, I
Next I

If Sep <> "" Then
    CipherText = Replace(CipherText, Sep, "")
End If

StrLen = Len(CipherText)
ReDim Kv(Application.Max(Len(IndicKey), StrLen))

'Key = ToAlpha(Key)
' Parse key into array
For K = 1 To Len(IndicKey)
    Ch = UCase(Mid(IndicKey, K, 1))
    If Ch >= "A" And Ch <= "Z" Then
        KeyLen = KeyLen + 1
        Kv(KeyLen) = Asc(Ch) - 65
    End If
Next K

K = 1
For I = 1 To Len(CipherText)
    Ch = UCase(Mid(CipherText, I, 1))
    If Ch >= "A" And Ch <= "Z" Then
        If K <= KeyLen Then
            Cv = CipherAlpha.Idx(Ch)
            If Run = RunType.rtPosttext And KeyLen < StrLen Then ' Auto key (pre-text)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = Cv
            End If
            KOfs = KeyAlpha.Item(Kv(K))
            PV = (Cv - KOfs + IndicOfs + 26) Mod 26
            Ch = PlainAlpha.Char(PV)
            If Run = RunType.rtAutokey And KeyLen < StrLen Then ' Auto key (post-text)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = PV
            End If
            K = K + 1
            If K > KeyLen Then
                If Run = RunType.rtProgressive Then
                    For K = 1 To KeyLen
                        Kv(K) = Kv(K) + 1
                        If Kv(K) > 25 Then ' Z
                            Kv(K) = 0  ' A
                        End If
                    Next K
                End If
                K = 1
            End If
        End If
    Else
        If Run = RunType.rtInterrupt And Ch = " " Then ' Interrupted Key
            K = 1
        End If
    End If
    W = W & Ch
Next I
QuagDecode = W
Exit Function
Catch:
QuagDecode = Err.Description
End Function

' This function adds a "0 based" key to a plaintext string
' Letters that go above "Z" roll around to "A"
' Adding "B" to a character will make it become the next letter, e.g. "I" + "B" => "J"
' Adding "A" is the null operation
' Does not encipher punctuation

Public Function VigEncode(Plaintext As String, Key As String, Optional Run As RunType = RunType.rtWrap, _
    Optional Method As CipherType = CipherType.ctVigenaire) As String
Dim I As Integer

Dim K As Integer
Dim Kv() As Integer
Dim KeyLen As Integer

Dim PV As Integer
Dim Cv As Integer

Dim Ch As String

Dim W As String
Dim StrLen As Integer

StrLen = Len(Plaintext)
ReDim Kv(1 To Application.Max(Len(Key), StrLen))

' Parse key into array
For K = 1 To Len(Key)
    Ch = UCase(Mid(Key, K, 1))
    If Ch >= "A" And Ch <= "Z" Then
        KeyLen = KeyLen + 1
        Kv(KeyLen) = Asc(Ch) - 65
    End If
Next K

K = 1
For I = 1 To Len(Plaintext)
    Ch = UCase(Mid(Plaintext, I, 1))
    If Ch >= "A" And Ch <= "Z" Then
        If K <= KeyLen Then
            PV = Asc(Ch) - 65
            If Run = RunType.rtAutokey And KeyLen < StrLen Then ' Auto key (pre)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = PV
            End If
            Select Case Method
                Case CipherType.ctVariant
                    ' C = P - K (Variant)
                    Cv = (PV - Kv(K) + 26) Mod 26
                Case CipherType.ctBeufort
                    ' C = K - P (Beaufort)
                    Cv = (Kv(K) - PV + 26) Mod 26
                Case CipherType.ctPorta ' Porta
                    If PV < 13 Then ' < "N"
                        Cv = ((PV + (Kv(K) \ 2)) Mod 13) + 13
                    Else
                        Cv = ((PV - (Kv(K) \ 2)) Mod 13)
                    End If
                Case Else ' CipherType.ctVigenaire
                    ' C = P + K (Vigenere)
                    Cv = (PV + Kv(K)) Mod 26
            End Select
            Ch = Chr(Cv + 65)
            If Run = RunType.rtPosttext And KeyLen < StrLen Then ' Auto key (post)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = Cv
            End If
            K = K + 1
            If K > KeyLen Then
                If Run = RunType.rtProgressive Then ' progressive
                    For K = 1 To KeyLen
                        Kv(K) = Kv(K) + 1
                        If Kv(K) > 25 Then ' Z
                            Kv(K) = 0  ' A
                        End If
                    Next K
                End If
                K = 1
            End If
        End If
    Else
        If Run = RunType.rtInterrupt And Ch = " " Then ' Interrupted Key
            K = 1
        End If
    End If
    W = W & Ch
Next I
VigEncode = W
End Function

' This function Subtracts a "0 based" key from an encyphered string
' Letters that go below "A" roll around to "Z"
' Subtracting "A" is the null operation
' Subtracting "Z" from a character will make it become the next letter, e.g. "I" - "Z" => "H"
Public Function VigDecode(CipherText As String, Key As String, Optional Run As RunType = RunType.rtWrap, _
    Optional Method As CipherType = CipherType.ctVigenaire) As String
Dim I As Integer
Dim K As Integer
Dim PV As Integer
Dim Cv As Integer
Dim Ch As String
Dim W As String

Dim KeyLen As Integer
Dim StrLen As Integer
Dim Kv() As Integer

StrLen = Len(CipherText)
ReDim Kv(Application.Max(Len(Key), StrLen))

'Key = ToAlpha(Key)
' Parse key into array
For K = 1 To Len(Key)
    Ch = UCase(Mid(Key, K, 1))
    If Ch >= "A" And Ch <= "Z" Then
        KeyLen = KeyLen + 1
        Kv(KeyLen) = Asc(Ch) - 65
    End If
Next K

K = 1
For I = 1 To Len(CipherText)
    Ch = UCase(Mid(CipherText, I, 1))
    If Ch >= "A" And Ch <= "Z" Then
        If K <= KeyLen Then
            Cv = Asc(Ch) - 65
            If Run = RunType.rtPosttext And KeyLen < StrLen Then ' Auto key (pre-text)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = Cv
            End If
            Select Case Method
                Case CipherType.ctVariant
                    ' P = C + K
                    PV = (Cv + Kv(K)) Mod 26
                Case CipherType.ctBeufort
                    ' P = K - C
                    PV = (Kv(K) - Cv + 26) Mod 26
                Case CipherType.ctPorta
                    If Ch < "N" Then
                        PV = ((Cv + (Kv(K) \ 2)) Mod 13) + 13
                    Else
                        PV = ((Cv - (Kv(K) \ 2)) Mod 13)
                    End If
                Case Else ' CipherType.ctVigenaire
                    ' P = C - K
                    PV = (Cv - Kv(K) + 26) Mod 26
            End Select
            Ch = Chr(PV + 65)
            If Run = RunType.rtAutokey And KeyLen < StrLen Then ' Auto key (post-text)
                KeyLen = KeyLen + 1
                Kv(KeyLen) = PV
            End If
            K = K + 1
            If K > KeyLen Then
                If Run = RunType.rtProgressive Then
                    For K = 1 To KeyLen
                        Kv(K) = Kv(K) + 1
                        If Kv(K) > 25 Then ' Z
                            Kv(K) = 0  ' A
                        End If
                    Next K
                End If
                K = 1
            End If
        End If
    Else
        If Run = RunType.rtInterrupt And Ch = " " Then ' Interrupted Key
            K = 1
        End If
    End If
    W = W & Ch
Next I
VigDecode = W
End Function

' This function adds a "1 based" key to a plaintext string
' Letters that go above "Z" roll around to "A"
' Adding "A" to a character will make it become the next letter, e.g. "I" + "A" => "J"
' Adding "Z" is the null operation
' The cipher text "wraps" if it is shorter than the plaintext
' Does not encipher punctuation
Public Function Encrypt(Plaintext As String, Key As String) As String
Dim I As Integer
Dim J As Integer
Dim X As Integer
Dim Ofs As Integer
Dim Ch As String
Dim W As String
Ofs = Asc("A") + Asc("@")
J = 1
Key = ToAlpha(Key)
For I = 1 To Len(Plaintext)
    Ch = UCase(Mid(Plaintext, I, 1))
    If Ch >= "A" And Ch <= "Z" Then
        If J <= Len(Key) Then
            X = Asc(Ch) + Asc(Mid(Key, J, 1)) - Ofs
            If X > 25 Then
                X = X - 26
            End If
            Ch = Chr(X + Asc("A"))
            J = J + 1
            If J > Len(Key) Then
                J = 1
            End If
        End If
    End If
    W = W & Ch
Next I
Encrypt = W
End Function

' This function Subtracts a "1 based" key from an encyphered string
' Letters that go below "A" roll around to "Z"
' Subtracting "A" from a character will make it become the next letter, e.g. "I" - "A" => "H"
' Subtracting "Z" is the null operation
Public Function Decrypt(CipherText As String, Key As String) As String
Dim M As Integer
Dim I As Integer
Dim J As Integer
Dim X As Integer
Dim Ofs As Integer
Dim Ch As String
Dim W As String
Ofs = Asc("A") - Asc("@")
J = 1
Key = ToAlpha(Key)
For I = 1 To Len(CipherText)
    Ch = UCase(Mid(CipherText, I, 1))
    If Ch >= "A" And Ch <= "Z" Then
        If J <= Len(Key) Then
            X = Asc(Ch) - Asc(Mid(Key, J, 1)) - Ofs
            If X < 0 Then
                X = X + 26
            End If
            Ch = Chr(X + Asc("A"))
            J = J + 1
            If J > Len(Key) Then
                J = 1
            End If
        End If
    End If
    W = W & Ch
Next I
Decrypt = W
End Function

' This function converts a numeric key to an Alpha String
' Drops characters other than numbers in the key
' Changes numbers 0-9 into A-J
' Equivalent to Xlat(ToDigit(NumKey),"0123456789","ABCDEFGHIJ")
Public Function AlphaEncode(NumKey As String) As String
Dim I As Integer
Dim Ofs As Integer
Dim AKey As String
Dim Ch As String

Ofs = Asc("A") - Asc("0")

' Translate numeric key to alpha
For I = 1 To Len(NumKey)
    Ch = Mid(NumKey, I, 1)
    If Ch >= "0" And Ch <= "9" Then
        Ch = Chr(Asc(Ch) + Ofs)
        AKey = AKey & Ch
    End If
Next I

' Call VigEncode
AlphaEncode = AKey
End Function

' This function adds a numeric key to a plaintext string
' Letters that go above "Z" roll around to "A"
' Adding "1" to a character will make it become the next letter, e.g. "I" + "B" => "J"
' Adding "0" is the null operation
' The cipher text "wraps" if it is shorter than the plaintext
' Does not encipher punctuation
Public Function NumEncode(Plaintext As String, Key As String) As String
NumEncode = VigEncode(Plaintext, AlphaEncode(Key))
End Function

' This function Subtracts a numeric key from an encyphered string
' Letters that go below "A" roll around to "Z"
' Subtracting "0" is the null operation
' Subtracting "1" from a character will make it become the previous letter, e.g. "I" - "1" => "H"
Public Function NumDecode(CipherText As String, Key As String) As String
NumDecode = VigDecode(CipherText, AlphaEncode(Key))
End Function

' This is the Gromark encryption function
Public Function encGromark(text As String, Key As String, Optional Primer As String = "") As String
Dim I As Integer, J As Integer, K As Integer
Dim V As Integer
Dim Ln As Integer
Dim kLn As Integer
Dim CT As String
Dim Dup As New AlphaList
Dim CipherText As New AlphaList
Dim OKey As New OrderedKey
Dim Ch As String
Dim Work As String
Dim Kv() As Integer
Dim P() As Integer
Dim Progressive As Boolean

On Error GoTo Catch

Progressive = Primer = ""

' Compute Ciphertext
Key = Dup.Dedup(Key)
If Key = "" Then
    encGromark = "Error: Empty Key"
    Exit Function
End If

If Progressive Then
    OKey.InitKey = Key
    kLn = OKey.Ln
    If OKey.Ln > 9 Then
        encGromark = "Error: Key > length 9 - [" & Key & "]"
        Exit Function
    End If
Else
    kLn = 5
    ' Primer should be five digits
    If Len(Primer) <> kLn Then
        encGromark = "Error: Primer not 5 digit value - [" & Primer & "]"
        Exit Function
    End If
End If

' Compute Ciphertext Alphabet
CT = encCCol(Dup.Undup(Key, "*"), Key, "")
CipherText.Init (CT)

' Filter text for alphabet only
text = ToAlpha(text)
Ln = Len(text)

' Primer matches key order & Length.  Compute primer & Chain add key
ReDim P(1 To kLn) ' Chain add key
ReDim Kv(1 To Application.Max(Ln, kLn)) ' Key
For K = 1 To kLn
    If Progressive Then
        P(K) = CipherText(Mid(Key, K, 1)) ' Chain add key
        Kv(K) = OKey.Idx(K)
        Primer = Primer & Kv(K)
    Else
        Ch = Mid(Primer, K, 1)
        If Ch < "0" Or Ch > "9" Then
            encGromark = "Error: Primer not 5 digit value - [" & Primer & "]"
            Exit Function
        End If
        Kv(K) = Ch
    End If
Next K

'Get rest of offset values
For I = kLn + 1 To Ln
    K = I - kLn
    Kv(I) = Right(Kv(K) + Kv(K + 1), 1)
Next I

' Output starts with Primer
Work = Primer

' Now compute Cipher
K = 1
For I = 1 To Ln Step kLn
    Work = Work & " "
    ' Iterate over next block
    For J = I To I + Len(Mid(text, I, kLn)) - 1
        ' Shift PT value right by (chain add key + key)
        V = Asc(Mid(text, J, 1)) - 65
        V = (V + P(K) + Kv(J)) Mod 26
        ' And look up CT value
        Work = Work & CipherText.Char(V)
    Next J
    ' Next progressive offset index
    K = K + 1
    If K > kLn Then K = 1
Next I

' Add Check digit at end
Work = Work & " " & Kv(Ln)

encGromark = Work
Exit Function

Catch:
encGromark = Err.Description
End Function

' This is the Gromark decryption function
Public Function decGromark(text As String, Key As String, Optional Progressive As Boolean = True) As String
Dim I As Integer, J As Integer, K As Integer
Dim V As Integer
Dim Ln As Integer
Dim kLn As Integer
Dim CT As String
Dim CipherText As New AlphaList
Dim OKey As New OrderedKey
Dim Chk As String
Dim Ch As String
Dim Work As String
Dim Primer As String
Dim Kv() As Integer
Dim P() As Integer

On Error GoTo Catch

' Compute Transposition Block Key
Key = Dedup(Key)
If Key = "" Then
    decGromark = "Error: Empty Key"
    Exit Function
End If

' Save Check value
Chk = ToDigit(text)
If Left(text, 1) < "0" Or Left(text, 1) > "9" Or Right(text, 1) < "0" Or Right(text, 1) > "9" Then ' can't be valid
    decGromark = "Error: missing primer/check digit"
    Exit Function
ElseIf Len(Chk) <> 6 Then ' force progressive if primer + check digit not length 6
    Progressive = True
ElseIf Len(Key) <> 5 Then ' Force non-progressive if Key length <> 5 and primer = 5
    Progressive = False
ElseIf ToDigit(AltKey(Key, ktNumber)) <> Left(text, 5) Then ' Force non-progressive if Key <> primer
    Progressive = False
'Else
    ' leave progressive flag alone
End If

If Progressive Then
    OKey.InitKey = Key
    kLn = OKey.Ln
    If kLn > 9 Then
        decGromark = "Error: Key > length 9 -> [" & Key & "]"
        Exit Function
    End If
Else
    kLn = 5
    Primer = Left(text, kLn)
End If
Chk = Right(Chk, 1)

' Compute Ciphertext Alphabet
CT = encCCol(Dedup(Key, "*"), Key, "")
CipherText.Init (CT)

' Filter text for alphabet only and get length
text = ToAlpha(text)
Ln = Len(text)

' Primer matches key order & Length.  Compute primer & Chain add key
ReDim P(1 To kLn) ' Chain add key
ReDim Kv(1 To Application.Max(Ln, kLn)) ' Key
For K = 1 To kLn
    If Progressive Then
        P(K) = CipherText(Mid(Key, K, 1))
        Kv(K) = OKey.Idx(K)
        Primer = Primer & Kv(K)
    Else
        Ch = Mid(Primer, K, 1)
        If Ch < "0" Or Ch > "9" Then
            decGromark = "Error: Primer not 5 digit value - [" & Primer & "]"
            Exit Function
        End If
        Kv(K) = Ch
    End If
Next K

'Get rest of offset values
For I = kLn + 1 To Ln
    K = I - kLn
    Kv(I) = Right(Kv(K) + Kv(K + 1), 1)
Next I

' Validate check value
If Chk <> Kv(Ln) Then
    decGromark = "Error: Checksum does not match - [" & Kv(I) & "]"
    Exit Function
End If

' Now compute Cipher

K = 1
For I = 1 To Ln Step kLn
    ' Iterate over next block
    For J = I To I + Len(Mid(text, I, kLn)) - 1
        ' Shift CT value left by (chain add key + key)
        V = (CipherText(Mid(text, J, 1)) + 52 - (P(K) + Kv(J))) Mod 26
        ' And look up PT value
        Work = Work & Chr(V + 65)
    Next J
    ' Next progressive offset index
    K = K + 1
    If K > kLn Then K = 1
Next I

decGromark = Work
Exit Function

Catch:
decGromark = Err.Description
End Function

' This is the TRIFID encryption function
Public Function encTRIFID(text As String, Key As String, Period As Integer, Optional Sep As String = "") As String
Dim I As Integer, J As Integer, K As Integer, P As Integer
Dim Keys As New Scripting.Dictionary
Dim Items As New Scripting.Dictionary
Dim Ch As String, Cd As String
Dim Work As String
Dim Block As String
On Error GoTo Catch

Keys.CompareMode = TextCompare ' alphabet search is case insensitive
If Len(Key) = 26 Then Key = Key & "#" ' default to '#' for 27th character

If Len(Key) <> 27 Then ' We don't have a valid key
    encTRIFID = "Error: Key not length 27 [" & Key & "]"
    Exit Function
End If

If Period < 5 Or Period > 15 Then ' period should be 5-15
    encTRIFID = "Error: Period must be between 5 and 15 [" & Period & "]"
    Exit Function
End If

' parse key (111 to 333)
P = 0
For I = 1 To 3 ' top row
    For J = 1 To 3 ' middle row
        For K = 1 To 3 ' bottom row
            P = P + 1 ' next position for key
            Ch = Mid(Key, P, 1)
            If Keys.Exists(Ch) Then ' we alread found one of these
                encTRIFID = "Error: Duplicate character '" & Ch & "' at position " & P & " in key [" & Key & "]"
                Exit Function
            End If
            Cd = I & J & K ' 3 digit code
            Keys.Add Ch, Cd
            Items.Add Cd, Ch
        Next K
    Next J
Next I

If Sep <> "" Then ' make sure separator is not in key
    If Keys.Exists(Sep) Then
        Sep = ""
        'encTRIFID = "Error: Separator character '" & Sep & "' found in key [" & Key & "]"
        'Exit Function
    End If
End If

' encrypt text
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Keys.Exists(Ch) Then
        Block = Block & Keys(Ch)
    End If
Next I

' Deseriate - height 3, no pad
Block = Seriate_(Block, Period, st_Deseriate, ttASCII, , 3, "")

' decrypt deseriated text
For I = 1 To Len(Block) Step 3
    Ch = Mid(Block, I, 3)
    Work = Work & Items(Ch)
Next I

' Add separator
If Sep <> "" Then
    If Not (InStr(1, Key, Sep) > 0) Then ' make sure it's not in the key first
        Work = AddSep(Work, Sep)
    End If
End If

encTRIFID = Work
Exit Function

Catch:
encTRIFID = Err.Description
End Function

' This is the TRIFID decryption function
Public Function decTRIFID(text As String, Key As String, Period As Integer) As String
Dim I As Integer, J As Integer, K As Integer, P As Integer
Dim Keys As New Scripting.Dictionary
Dim Items As New Scripting.Dictionary
Dim Ch As String, Cd As String
Dim Work As String
Dim Blk(1 To 3) As String
Dim Block As String
On Error GoTo Catch
Keys.CompareMode = TextCompare
If Len(Key) = 26 Then Key = Key & "#"
If Len(Key) <> 27 Then
    decTRIFID = "Error: Key not length 27 [" & Key & "]"
    Exit Function
End If

If Period < 5 Or Period > 15 Then
    decTRIFID = "Error: Period must be between 5 and 15 [" & Period & "]"
    Exit Function
End If

' Build translation keys (111 to 333)
P = 0
For I = 1 To 3
    For J = 1 To 3
        For K = 1 To 3
            P = P + 1
            Ch = Mid(Key, P, 1)
            If Keys.Exists(Ch) Then
                decTRIFID = "Error: Duplicate character '" & Ch & "' at position " & P & " in key [" & Key & "]"
                Exit Function
            End If
            Cd = I & J & K ' three digit code
            Keys.Add Ch, Cd
            Items.Add Cd, Ch
        Next K
    Next J
Next I

' encrypt text (will also strip separator)
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Keys.Exists(Ch) Then
        Block = Block & Keys(Ch)
    End If
Next I

' Deseriate - height 3, no pad
Block = Seriate_(Block, Period, st_Seriate, ttASCII, , 3, "")

' decrypt Seriated text
For I = 1 To Len(Block) Step 3
    Ch = Mid(Block, I, 3)
    Work = Work & Items(Ch)
Next I

decTRIFID = Work
Exit Function

Catch:
decTRIFID = Err.Description
End Function

' CONDI (Consecutive Digraphs) encryption
Public Function encCONDI(text As String, Key As String, Init As Variant) As String
Dim I As Integer
Dim Prev As Integer
Dim Curr As Integer
Dim CT As New AlphaList
Dim Ch As String
Dim Work As String
On Error GoTo Catch

' Ciphertext Key (scrambled 26 letter alphabet)
CT.Init ToAlpha(Key)

If CT.Count <> 26 Then
    encCONDI = "Error: Key not length 26 [" & Key & "]"
    Exit Function
End If

' Get starting offset
If IsNumeric(Init) Then ' It's a number, should be 1-26
    Prev = Init
    If Prev < 1 Or Prev > CT.Count Then
        encCONDI = "Error: Initial index must be a number from 1-26 (" & Prev & ")"
        Exit Function
    End If
    Prev = Prev - 1 ' Change to 0 based
ElseIf VarType(Init) = vbString Then ' It's a character
    Ch = Left(Init, 1)
    If Not CT.Exists(Ch) Then ' must be in key
        encCONDI = "Error: Initial character is not a letter '" & Ch & "'"
        Exit Function
    Else
        Prev = CT(Ch) ' get starting offset (0 based)
    End If
Else
    encCONDI = "Error: Initial character must be a letter or a number, vartype = " & VarType(Init)
    Exit Function
End If

' Encipher string
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If CT.Exists(Ch) Then
        Curr = CT(Ch) ' Current Index
        Work = Work & CT.Char((Curr + Prev + 1) Mod 26)  ' Get next ciphertext character
        Prev = Curr ' remember offset of this plaintext character
    Else
        Work = Work & Ch ' other characters pass through
    End If
Next I

encCONDI = Work
Exit Function
Catch:
encCONDI = Err.Description
End Function

' CONDI (Consecutive Digraphs) decryption
Public Function decCONDI(text As String, Key As String, Init As Variant) As String
Dim I As Integer
Dim Prev As Integer
Dim CT As New AlphaList

Dim Ch As String
Dim Work As String
Dim Result As String
On Error GoTo Catch

' Ciphertext Key (scrambled 26 letter alphabet)
CT.Init ToAlpha(Key)

If CT.Count <> 26 Then
    decCONDI = "Error: Key not length 26 [" & Key & "]"
    Exit Function
End If

' Get starting offset
If IsNumeric(Init) Then ' It's a number, should be 1-26
    Prev = Init
    If Prev < 1 Or Prev > 26 Then
        decCONDI = "Error: Initial index must be a number from 1-26 (" & Prev & ")"
        Exit Function
    End If
    Prev = Prev - 1 ' Switch to zero based
ElseIf VarType(Init) = vbString Then ' It's a character
    Ch = Left(Init, 1)
    If Not CT.Exists(Ch) Then ' must be in key
        decCONDI = "Error: Initial character is not a letter '" & Ch & "'"
        Exit Function
    Else
        Prev = CT(Ch) ' get starting offset
    End If
Else
    decCONDI = "Error: Initial character must be a letter or a number, vartype = " & VarType(Init)
    Exit Function
End If

' Decipher string
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If CT.Exists(Ch) Then
        Ch = CT.Char((CT(Ch) - Prev + 25) Mod 26) ' Get next plaintext character
        Work = Work & Ch
        Prev = CT(Ch) ' remember offset of this new character
    Else
        Work = Work & Ch ' other characters pass through
    End If
Next I

decCONDI = Work
Exit Function

Catch:
decCONDI = Err.Description
End Function

' This is the Portax encryption/decryption routine.  Text is assumed to be even length and all alpha.
' Caller must trap errors (empty key)
' Symmetric Function
Private Function Portax_(text As String, Key As String) As String
Dim Ln As Integer
Dim kLn As Integer
Dim I As Integer
Dim J As Integer
Dim K As Integer

Dim Blk As Integer
Dim BlkLn As Integer

Dim Idx As Integer

Dim Ch1 As String, Ch2 As String
Dim V1 As Integer, V2 As Integer
Dim C1 As Integer, C2 As Integer
Dim R1 As Integer, R2 As Integer

Dim Wrk As String
Dim Tail As String

Key = ToAlpha(Key)
kLn = Len(Key)

If kLn = 0 Then
    Err.Raise 971, "Portax_", "Error: Key is empty"
End If

K = 1

'text = ToAlpha(text)
Ln = Len(text)
For I = 0 To Ln - 1 Step kLn * 2
    ' compute current block
    Blk = Application.Min(I + kLn * 2, Ln)
    BlkLn = (Blk - I + 1) \ 2
    ' iterate over block
    For J = I + 1 To I + BlkLn
        ' Grab two characters
        Ch1 = Mid(text, J, 1)
        Ch2 = Mid(text, J + BlkLn, 1)
        
        ' If Ch2 = "" Then Ch2 = "X" ' Only needed for encryption, caller is expected to have done a pad
        
        ' Compute index of shift from key
        Idx = (Asc(Mid(Key, K, 1)) - 65) \ 2
        
        ' Compute numeric value (0-25) of two characters
        V1 = Asc(Ch1) - 65
        V2 = Asc(Ch2) - 65
        
        ' Compute row/column for character 1
        If V1 < 13 Then ' A-M
            R1 = 0
            C1 = (V1 + Idx) Mod 13
        Else ' M-Z
            R1 = 1
            C1 = V1 - 13
        End If
        
        ' Compute row/column for character 2
        R2 = V2 Mod 2
        C2 = V2 \ 2
        
        ' Encrypt
        If C1 = C2 Then ' Same column, select opposite row characters
            ' Upper character
            If R1 = 1 Then ' Row 0
                Wrk = Wrk & Chr(((C1 - Idx + 13) Mod 13) + 65)
            Else ' row 1
                Wrk = Wrk & Chr(C1 + 78) ' 65+13
            End If
            ' Lower character; flips row (1-R2)
            Tail = Tail & Chr(C2 * 2 + (1 - R2) + 65)
        Else ' different columns, look in rectangle
            If R1 = 1 Then ' Upper bottom row (1)
                Wrk = Wrk & Chr(C2 + 78) ' 65+13
            Else ' Upper top row (compute shift)
                Wrk = Wrk & Chr(((C2 - Idx + 13) Mod 13) + 65)
            End If
            ' Lower rows; add bottom row number (R2)
            Tail = Tail & Chr(C1 * 2 + R2 + 65)
        End If
        
        ' Move on to next key value
        K = K + 1
        If K > kLn Then
            K = 1
            Wrk = Wrk & Tail
            Tail = ""
        End If
    Next J
Next I

Wrk = Wrk & Tail

Portax_ = Wrk
End Function

Public Function EncPortax(text As String, Key As String, Optional Sep As String) As String
Dim Wrk As String

On Error GoTo Catch

text = ToAlpha(text)
If Len(text) Mod 2 <> 0 Then
    text = text & "X"
End If

Wrk = Portax_(text, Key)

If Sep <> "" Then
    Wrk = GroupBy(Wrk, 5, Sep)
End If

EncPortax = Wrk
Exit Function
Catch:
EncPortax = Err.Description
End Function

Public Function DecPortax(text As String, Key As String) As String
On Error GoTo Catch
text = ToAlpha(text)
If Len(text) Mod 2 <> 0 Then
    DecPortax = "Error: Text is odd length"
    Exit Function
End If
DecPortax = Portax_(text, Key)
Exit Function
Catch:
DecPortax = Err.Description
End Function

' Ragbaby Encryption

Public Function encRagbaby(text As String, Key As String, Optional Repl As Variant) As String
Dim kLn As Integer
Dim Word As Integer
Dim I As Integer
Dim P As Integer
Dim Idx As Integer
Dim Ch As String
Dim Wrk As String
Dim InWord As Boolean
Dim tType As textType
Dim cList As New AlphaList

On Error GoTo Catch

' Add key to character lists
cList.Init Replace(Key, " ", "")

kLn = cList.Count
' compact based on key
If kLn < 26 Then
    tType = ttAlpha25Upper
Else
    tType = ttASCIIAll ' Don't use Repl
End If
' Adds bonus character (space) to pass through (regardless of tType)
text = Compact(text, "", Repl, tType, " ")
' Zero based shift (allows use of MOD)
Word = 0
InWord = False
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Ch = " " Then ' will skip to next word
        InWord = False
    Else
        If cList.Exists(Ch) Then ' can encrypt this character
            If Not InWord Then ' It's a new word
                InWord = True
                If Wrk <> "" Then ' Add a space if needed
                    Wrk = Wrk & " "
                End If
                Idx = Word ' Index starts at word number
                Word = (Word + 1) Mod kLn ' next word number
            End If
            P = cList(Ch) ' get current character index
            P = (P + Idx + 1) Mod kLn ' compute shift (index + 1)
            Wrk = Wrk & cList.Char(P)
            Idx = (Idx + 1) Mod kLn ' next character shift
        End If
    End If
Next I
encRagbaby = Wrk
Exit Function
Catch:
encRagbaby = Err.Description
End Function

' Ragbaby Decryption

Public Function decRagbaby(text As String, Key As String) As String
Dim kLn As Integer
Dim Word As Integer
Dim I As Integer
Dim P As Integer
Dim Idx As Integer
Dim Ch As String
Dim Wrk As String
Dim InWord As Boolean
Dim cList As New AlphaList
On Error GoTo Catch

' Add key to character lists
cList.Init Replace(Key, " ", "")
kLn = cList.Count

' Zero based shift (allows use of MOD)
Word = 0
InWord = False
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Ch = " " Then ' will skip to next word
        InWord = False
    Else
        If cList.Exists(Ch) Then ' can decrypt this character
            If Not InWord Then ' It's a new word
                InWord = True
                If Wrk <> "" Then ' Add a space if needed
                    Wrk = Wrk & " "
                End If
                Idx = Word ' Index starts at word number
                Word = (Word + 1) Mod kLn ' next word number
            End If
            P = cList(Ch) ' get current character index
            P = (P - Idx - 1 + cList.Count) Mod kLn ' compute shift (index - 1)
            Wrk = Wrk & cList.Char(P)
            Idx = (Idx + 1) Mod kLn ' next character shift
        End If
    End If
Next I
decRagbaby = Wrk
Exit Function
Catch:
decRagbaby = Err.Description
End Function

' Shift Encryption (Gronesfeld++)
' For double encrypting ciphers

Private Function Shift_(text As String, Key As String, Up As Boolean, Optional Filter As Variant, Optional Repl As String) As String
Dim kLn As Integer ' length of key
Dim I As Integer
Dim Idx As Integer
Dim K As Integer

Dim Kv() As Integer

Dim Ch As String
'Dim kCh As String
Dim Wrk As String

Dim tList As New AlphaList  ' filter for text
Dim kList As New AlphaList  ' filter for key

On Error GoTo Catch

' If empty source text
If text = "" Then
    Shift_ = text
    Exit Function
End If

tList.Init Filter, Repl

' Allow numbers and/or letters in key
Key = toText(Key, ttAlphaDigit)

' empty key
If Key = "" Then ' nothing to do
    Shift_ = "Empty Key"
    Exit Function
End If

' Create key dictionary Kv()
' Add key values - 0=0, 1=1, ...
' Alpha key values - A=0, B=1, ...
kList.Init
kList.Init "0123456789", , -kList.Count

' Adjust shifts to be within filter count (tlist)
kLn = Len(Key)  ' length of key
ReDim Kv(kLn - 1)
For K = 0 To kLn - 1
    Ch = Mid(Key, K + 1, 1)
    If Not kList.Exists(Ch) Then ' should never happen because we filtered the key for letter + digits
        Err.Raise 714, "Cipher:Shift_", "Unknown key value at position " & K & " [" & Ch & "]"
    End If
    Idx = kList(Ch) Mod tList.Count
    Kv(K) = Idx
Next K

' Iterate over source text, add or subtract key values
K = kLn
For I = 1 To Len(text) ' for every character in the string
    Ch = Mid(text, I, 1)
    If tList.Exists(Ch) Then ' if we found the input character in the text filter
        ' move to next position in key (wrap as needed)
        K = K + 1
        If K >= kLn Then K = 0
        ' look up index in alphabet; shift by key value
        If Up Then ' Add
            Idx = (tList(Ch) + Kv(K)) Mod tList.Count
        Else ' subtract
            Idx = (tList.Count + tList(Ch) - Kv(K)) Mod tList.Count
        End If
        ' add to output string
        Wrk = Wrk & tList.Char(Idx)
    End If
Next I

' return shifted text
Shift_ = Wrk
Exit Function

Catch:
Shift_ = Err.Description
End Function

' Shift Up (add key to text)

Public Function encShift(text As String, Key As String, Optional Filter As Variant, Optional Repl As String) As String
encShift = Shift_(text, Key, True, Filter, Repl)
End Function

' Shift Down (subtract key from text)

Public Function decShift(text As String, Key As String, Optional Filter As Variant, Optional Repl As String) As String
decShift = Shift_(text, Key, False, Filter, Repl)
End Function

' Generates a string representing a matrix, suitable for display as wrapped text in a cell

Public Function MatrixSquare(Key As String, Optional Filter As Variant, Optional Repl As String) As String
Dim MatrixObj As New MatrixObj

On Error GoTo Catch

' Filter determines characters to encode (usually a 26 character alphabet), but can include others.
MatrixObj.Init Filter, Repl

' Pads the key out to length 4 or 9...
' if the determinant shares cofactors with the filter length (like 2 or 13 for 26 letters), then the matrix cannot be inverted
' If the key is not invertible, will raise an error
' A prime filter length (e.g. 26 letters plus 3 punctuation - space, period, maybe comma) is the easiest way to eliminate cofactors
MatrixObj.Parse (Key)

MatrixSquare = MatrixObj.Block
Exit Function

Catch:
MatrixSquare = Err.Description
End Function

' Generate an invertible matrix key using the source cipher text as a seed
' Size is either 4 or 9

Public Function genHillKey(text As String, Size As Integer, Optional Filter As Variant, Optional Repl As String) As String
Dim I As Integer
Dim V As Integer
Dim Tries As Integer

Dim Matrix As New MatrixObj

Dim Key As String

On Error GoTo Catch
InitRand (text)

' must be a 2x2 or 3x3 matrix
If Size <= 4 Then
    Size = 4
Else
    Size = 9
End If

' Filter determines characters to encode (usually a 26 character alphabet), but can include others.
' if the determinant shares cofactors with the filter length (like 2 or 13 for 26 letters), then the matrix cannot be inverted
' A prime filter length (e.g. 26 letters plus 3 punctuation - space, period, maybe comma) is the easiest way to eliminate cofactors
Matrix.Init Filter, Repl

' we will loop back here if the determinant is zero or shares factors with the filter length
TryAgain:
Tries = Tries + 1
If Tries > MaxAttempts_ Then ' didn't test to see how many times this loops on average... just picked a number to guarantee no infinite loop
    genHillKey = "Error: Exceeded Max number of attempts " & MaxAttempts_
End If

' Generate a random key from the filter list
Key = ""
For I = 1 To Size
    V = Int(Rnd() * Matrix.Count)
    Key = Key & Matrix.Char(V)
Next I

' if this raises an error, the catch will loop back to try again
' if not, this is an invertible matrix; return the key
Matrix.Parse Key

genHillKey = Key ' & " " & Tries
Exit Function

Catch:
' Determinant 0 or Common Factors
If Err.Number = 722 Or Err.Number = 723 Then Resume TryAgain

' All other errors
genHillKey = Err.Description
End Function

' Hill Cipher (2x2 or 3x3, depends on key size)
' Key is either a string of text (characters in the filter) or a series of numbers separated by spaces
' Pad defaults to the character 23/26 of the way through the filter ("X" for a 26 letter alphabet)
' Repl is only used for 25 character alphabets
' Filter is a text string or one of the tType constants
' Period is only used for seriated version (custom)

Public Function encHill(text As String, Key As String, Optional Pad As String = "", Optional Repl As String, Optional Filter As Variant, _
    Optional Period As Integer, Optional Sep As String) As String
Dim C As Integer
Dim I As Integer
Dim P As Integer

Dim Ln As Integer

Dim BlkLen As Integer

Dim Wrk As String

Dim Matrix As New MatrixObj

On Error GoTo Catch

' Filter determines characters to encode (usually a 26 character alphabet), but can include others.
' if the determinant shares cofactors with the filter length (like 2 or 13 for 26 letters), then the matrix cannot be inverted
' A prime filter length (e.g. 26 letters plus 3 punctuation - space, period, maybe comma) is the easiest way to eliminate cofactors
Matrix.Init Filter, Repl

' no need to capture return DI for encrypt
Matrix.Parse Key

' Filter text
text = Matrix.Filter(text)

If text = "" Then
    encHill = ""
    Exit Function
End If

' For encryption, Pad to height of matrix
P = Len(text) Mod Matrix.RC
If P <> 0 Then
    If Pad = "" Then Pad = Matrix.Char(Int((23 / 26) * Matrix.Count))
    Pad = Left(Pad, 1)
    If Not Matrix.Exists(Pad) Then
        encHill = "Pad character [" & Pad & "] required but does not exist in filter alphabet"
        Exit Function
    End If
    text = text & String$(Matrix.RC - P, Pad)
End If

Ln = Len(text)

' For seriated, transform the string (Write in rows of period length, matrix height, read back in columns)
' ABCDEFGHIJKLMNO, period 4: becomes
' ABCD MN
' EFGH OP
' IJKL QR
' becomes
' AEIBFJCGKDHL MOQNPR
If Period = -1 Then Period = Ln \ Matrix.RC
If Period > 1 Then
    text = Seriate_(text, Period, st_Seriate, ttASCIIAny, , Matrix.RC, "")
End If

' For encryption, now do the multiplication

Wrk = Matrix.Multiply(text)

Wrk = AddSep(Wrk, Sep)

encHill = Wrk

Exit Function
Catch:
encHill = Err.Description
End Function

' Hill Cipher (2x2 or 3x3, depends on key size)

Public Function decHill(text As String, Key As String, Optional Repl As String, Optional Filter As Variant, _
    Optional Period As Integer, Optional Sep As String) As String
Dim R As Integer, C As Integer
Dim P As Integer

Dim Ln As Integer

Dim BlkLen As Integer

Dim Wrk As String

Dim Matrix As New MatrixObj

On Error GoTo Catch

' Handle Separator if present
text = UngroupBy(text, Sep)

' Filter determines characters to decode (usually a 26 character alphabet), but can include others.
' if the determinant shares cofactors with the filter length (like 2 or 13 for 26 letters), then the matrix cannot be inverted
' A prime filter length (e.g. 26 letters plus 3 punctuation - space, period, maybe comma) is the easiest way to eliminate cofactors
Matrix.Init Filter, Repl

' Parse key into matrix
Matrix.Parse (Key)

' Filter text
text = Matrix.Filter(text)

If text = "" Then
    decHill = ""
    Exit Function
End If

' For decryption, fail if the length is not correct
Ln = Len(text)
P = Ln Mod Matrix.RC
If P <> 0 Then
    Err.Raise 732, "Cipher:decHill", "Error: text length is not a multiple of " & Matrix.RC
End If

' For decryption, here there be inverting the matrix
Matrix.Invert

' now do the multiplication
Wrk = Matrix.Multiply(text)

' For seriated, transform the string (read in columns, write in rows - columns are Matrix height wide, period high)
' AEIBFJCGKDHLMOQNPR, period 4: becomes
' AEI
' BFJ
' CGK
' DHL
'
' MOQ
' NPR
' becomes
' ABCDEFGHIJKLMNOPQR

If Period = -1 Then Period = Ln \ Matrix.RC
If Period > 1 Then
    Wrk = Seriate_(Wrk, Period, st_Deseriate, ttASCIIAny, , Matrix.RC, "")
End If

decHill = Wrk

Exit Function
Catch:
decHill = Err.Description
End Function
