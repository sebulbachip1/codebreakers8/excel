Attribute VB_Name = "Playfair"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save


'-------------------------------------------------------------------------------------------
' MODULE:
'       Playfair
'
' USAGE:
'       Foursquare/Twosquare/Playfair codes (digraphic)
'
' GROUPS:
'       BIFID, CM BIFID
'            EncBifid, DecBifid
'
'       Foursquare
'            EncodeFour, DecodeFour (single digraph)
'            EncFoursquare, DecFoursquare
'
'       Twosquare
'            EncodeTwo, DecodeTwo (single digraph)
'            EncTwosquare, DecTwosquare
'
'       Trisquare
'            encTrisquare, decTrisquare
'
'       Playfair
'            Encode, Decode (single digraph)
'            EncPlayfair, DecPlayfair
'
'       Dynamic
'            EncodeSquare, DecodeSquare
'
'       Checkerboard
'            encChecker, decChecker
'            enc6Checker, dec6Checker
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 01/28/2019 Chip Bloch    -Added Twosquare, Foursquare routines
' 06/03/2019 Chip Bloch    -Added pass-through for punctuation on playfair
' 06/13/2019 Chip Bloch    -Made pad character on Playfair optional
' 06/25/2020 Chip Bloch    -Enc/DecPlayFair using Polybius
'                          -Removed MCase in Enc/DecPlayfair
' 06/30/2020 Chip Bloch    -Added ADFGX
' 07/26/2020 Chip Bloch    -Migrated ADFGX to Combo
' 08/06/2020 Chip Bloch    -Modified polybius make method (prep for moving column names)
' 08/08/2020 Chip Bloch    -Reordered parms for Polybius
' 08/08/2020 Chip Bloch    -Calling Filter in Polybius for decChecker functions
' 08/21/2020 Chip Bloch    -Updated Encode, Decode, EncTwoSquare, DecTwoSquare to use Polybius
' 08/23/2020 Chip Bloch    -Updated EncodeFour, EncFoursquare, DecodeFour, DecFoursquare to us Polybius
'                          -Updating encChecker to allow for multiple column names
' 08/25/2020 Chip Bloch    -Added enc6Checker/dec6Checker
' 09/05/2020 Chip Bloch    -Added Trisquare
' 09/06/2020 Chip Bloch    -Added BIFID (& CM BIFID) - encBIFID, decBIFID
'-------------------------------------------------------------------------------------------

Private Const iPolyCols As Integer = 5

' Dynamic Decryption

Public Function DecodeSquare(Digraph As String, R1 As Range, Optional R2 As Range = Nothing, Optional R3 As Range = Nothing, Optional R4 As Range = Nothing) As String
If R2 Is Nothing Then
    DecodeSquare = DecPlayfair(Digraph, R1)
ElseIf R3 Is Nothing Then
    DecodeSquare = DecTwosquare(Digraph, R1, R2)
ElseIf R4 Is Nothing Then
    DecodeSquare = DecFoursquare(Digraph, R1, R2, R3, R1)
Else
    DecodeSquare = DecFoursquare(Digraph, R1, R2, R3, R4)
End If
End Function

' Dynamic Encryption

Public Function EncodeSquare(Digraph As String, R1 As Range, Optional R2 As Range = Nothing, Optional R3 As Range = Nothing, Optional R4 As Range = Nothing) As String
If R2 Is Nothing Then
    EncodeSquare = EncPlayfair(Digraph, R1)
ElseIf R3 Is Nothing Then
    EncodeSquare = EncTwosquare(Digraph, R1, R2)
ElseIf R4 Is Nothing Then
    EncodeSquare = EncFoursquare(Digraph, R1, R2, R3, R1)
Else
    EncodeSquare = EncFoursquare(Digraph, R1, R2, R3, R4)
End If
End Function

' Foursquare Digraph Decryption (Deprecated)

Public Function DecodeFour(Digraph As String, TL As Variant, TR As Variant, BL As Variant, Optional BR As Variant) As String
Dim Char1 As String
Dim Char2 As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim pTL As New Polybius
Dim pTR As New Polybius
Dim pBL As New Polybius
Dim pBR As New Polybius
On Error GoTo Catch
Call pTL.Make(TL, vtUpperAlpha, 5)
Call pTR.Make(TR, vtUpperAlpha, 5)
Call pBL.Make(BL, vtUpperAlpha, 5)
If IsMissing(BR) Or IsEmpty(BR) Then
    Call pBR.Make(TL, vtUpperAlpha, 5)
Else
    Call pBR.Make(BR, vtUpperAlpha, 5)
End If
Char1 = Left(Digraph, 1)
Char2 = Mid(Digraph, 2, 1)
If Char2 = "" Then Char2 = "X"
pTR.RC Char1, R, C
pBL.RC Char2, Y, X
DecodeFour = pTL.chrVal(R, X) & pBR.chrVal(Y, C)
Exit Function
Catch:
DecodeFour = Err.Description
End Function

' Foursquare Digraph Encryption (deprecated)

Public Function EncodeFour(Digraph As String, TL As Variant, TR As Variant, BL As Variant, Optional BR As Variant) As String
Dim Char1 As String
Dim Char2 As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim pTL As New Polybius
Dim pTR As New Polybius
Dim pBL As New Polybius
Dim pBR As New Polybius
On Error GoTo Catch
Call pTL.Make(TL, vtUpperAlpha, 5)
Call pTR.Make(TR, vtUpperAlpha, 5)
Call pBL.Make(BL, vtUpperAlpha, 5)
If IsMissing(BR) Or IsEmpty(BR) Then
    Call pBR.Make(TL, vtUpperAlpha, 5)
Else
    Call pBR.Make(BR, vtUpperAlpha, 5)
End If
Char1 = Left(Digraph, 1)
Char2 = Mid(Digraph, 2, 1)
If Char2 = "" Then Char2 = "X"
pTL.RC Char1, R, C
pBR.RC Char2, Y, X
EncodeFour = pTR.chrVal(R, X) & pBL.chrVal(Y, C)
Exit Function
Catch:
EncodeFour = Err.Description
End Function

' Twosquare Digraph Decryption (Deprecated)

Public Function DecodeTwo(Digraph As String, LR As Variant, RR As Variant) As String
Dim Char1 As String
Dim Char2 As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim pLR As New Polybius
Dim pRR As New Polybius
On Error GoTo Catch
Call pLR.Make(LR, vtUpperAlpha, 5)
Call pRR.Make(RR, vtUpperAlpha, 5)
Char1 = Left(Digraph, 1)
Char2 = Mid(Digraph, 2, 1)
pRR.RC Char1, R, C
pLR.RC Char2, Y, X
DecodeTwo = pLR.chrVal(R, X) & pRR.chrVal(Y, C)
Exit Function
Catch:
DecodeTwo = Err.Description
End Function

' Twosquare Digraph Encryption (Deprecated)

Public Function EncodeTwo(Digraph As String, LR As Variant, RR As Variant) As String
Dim Char1 As String
Dim Char2 As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim pLR As New Polybius
Dim pRR As New Polybius
On Error GoTo Catch
Call pLR.Make(LR, vtUpperAlpha, 5)
Call pRR.Make(RR, vtUpperAlpha, 5)
Char1 = Left(Digraph, 1)
Char2 = Mid(Digraph, 2, 1)
pLR.RC Char1, R, C
pRR.RC Char2, Y, X
EncodeTwo = pRR.chrVal(R, X) & pLR.chrVal(Y, C)
Exit Function
Catch:
EncodeTwo = Err.Description
End Function

' Playfair encryption/decription methods

' internal functions for the playfair encode/decode methods.
' these wrap around the range if the integer gets too large or too small
Private Function Down(I As Integer) As Integer
I = I - 1
If I = 0 Then I = iPolyCols
Down = I
End Function

Private Function Up(I As Integer) As Integer
I = I + 1
If I > iPolyCols Then I = 1
Up = I
End Function

' This function decodes a digraph using a reference to a 5x5 grid
' This is the new Playfair decryption method using Polybius squares

Private Function pDecode(Digraph As String, Poly As Polybius) As String
Dim X As Integer, Y As Integer
Dim R As Integer, C As Integer
Dim Char1 As String
Dim Char2 As String
Dim Result As String
Char1 = Left(Digraph, 1)
Char2 = Mid(Digraph, 2, 1)
Poly.RC Char1, R, C
Poly.RC Char2, Y, X
If (R = Y) Then ' 1 line
    If (X = C) Then ' 1 cell (double letter) - Not technically allowed
        Result = Poly.chrVal(Down(R), Down(C)) & Poly.chrVal(Down(Y), Down(X))
    Else
        Result = Poly.chrVal(R, Down(C)) & Poly.chrVal(R, Down(X))
    End If
Else
    If (X = C) Then ' 1 col
        Result = Poly.chrVal(Down(R), C) & Poly.chrVal(Down(Y), C)
    Else ' rectangle
        Result = Poly.chrVal(R, X) & Poly.chrVal(Y, C)
    End If
End If

pDecode = Result
End Function

' This function encodes a digraph using a reference to a 5x5 grid
' This is the new Playfair encryption method using Polybius squares

Private Function pEncode(Digraph As String, Poly As Polybius) As String
Dim X As Integer, Y As Integer
Dim R As Integer, C As Integer
Dim Char1 As String
Dim Char2 As String
Dim Result As String
Char1 = UCase(Left(Digraph, 1))
If Len(Digraph) > 1 Then
    Char2 = Mid(Digraph, 2, 1)
Else
    Char2 = "X"
End If
Poly.RC Char1, R, C
Poly.RC Char2, Y, X
If (R = Y) Then ' 1 line
    If (X = C) Then ' 1 cell - NOT technically allowed
        Result = Poly.chrVal(Up(R), Up(C)) & Poly.chrVal(Up(Y), Up(X))
    Else
        Result = Poly.chrVal(R, Up(C)) & Poly.chrVal(R, Up(X))
    End If
Else
    If (X = C) Then ' 1 col
        Result = Poly.chrVal(Up(R), C) & Poly.chrVal(Up(Y), C)
    Else ' rectangle
        Result = Poly.chrVal(R, X) & Poly.chrVal(Y, C)
    End If
End If

pEncode = Result
End Function

' This function encodes a digraph using a reference to a 5x5 grid
' This is the old Playfair encryption method
' the result of ENCODE(A1, B1:F5) depends on the values in A1 and B1:F5...
' Deprecated; retained for compatibility

Public Function Encode(Digraph As String, Rng As Variant) As String
Dim pSquare As New Polybius
On Error GoTo Catch
Call pSquare.Make(Rng, vtUpperAlpha, 5)
Encode = pEncode(Digraph, pSquare)
Exit Function
Catch:
Encode = Err.Description
End Function

' This function decodes a digraph using a reference to a 5x5 grid
' This is the Playfair decryption method
' the result of DECODE(A1, B1:F5) depends on the values in A1 and B1:F5...
' Deprecated; retained for compatibility
Public Function Decode(Digraph As String, Rng As Variant) As String
Dim pSquare As New Polybius
On Error GoTo Catch
Call pSquare.Make(Rng, vtUpperAlpha, 5)
Decode = pDecode(Digraph, pSquare)
Exit Function
Catch:
Decode = Err.Description
End Function

' This function is the general Playfair encryption method
Public Function EncPlayfair(text As String, Rng As Variant, Optional Pad As String = "X") As String
Dim I As Integer
Dim Ch As String
Dim Digraph As String
Dim LeftCh As String
Dim Punctuation As String
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch
Call pSquare.Make(Rng, ValType.vtUpperAlpha, 5)
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Ch < "A" Or (Ch > "Z" And Ch < "a") Or Ch > "z" Then
        If LeftCh = "" Then
            Work = Work & Ch
        Else
            Punctuation = Punctuation & Ch
        End If
    Else
        If LeftCh = "" Then
            LeftCh = Ch
        Else
            If UCase(LeftCh) = UCase(Ch) And Pad <> "" Then
                Digraph = LeftCh & Pad
                LeftCh = Ch
            Else
                Digraph = LeftCh & Ch
                LeftCh = ""
            End If
            Digraph = pEncode(Digraph, pSquare)
            Work = Work & Left(Digraph, 1) & Punctuation & Right(Digraph, 1)
            Punctuation = ""
        End If
    End If
Next I
If LeftCh <> "" Then
    Digraph = LeftCh & Pad
    Digraph = pEncode(Digraph, pSquare)
    Work = Work & Left(Digraph, 1) & Punctuation & Right(Digraph, 1)
End If
'Work = MCase(Work, text)
EncPlayfair = Work
Exit Function
Catch:
EncPlayfair = Err.Description
End Function

' This function is the general Playfair decryption method

Public Function DecPlayfair(text As String, Rng As Variant) As String
Dim I As Integer
Dim Ch As String
Dim Digraph As String
Dim LeftCh As String
Dim Punctuation As String
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch
Call pSquare.Make(Rng, ValType.vtUpperAlpha, 5)
For I = 1 To Len(text)
    Ch = Mid(text, I, 1)
    If Ch < "A" Or (Ch > "Z" And Ch < "a") Or Ch > "z" Then
        If LeftCh = "" Then
            Work = Work & Ch
        Else
            Punctuation = Punctuation & Ch
        End If
    Else
        If LeftCh = "" Then
            LeftCh = Ch
        Else
            Digraph = LeftCh & Ch
            Digraph = pDecode(Digraph, pSquare)
            Work = Work & Left(Digraph, 1) & Punctuation & Right(Digraph, 1)
            LeftCh = ""
            Punctuation = ""
        End If
    End If
Next I
If LeftCh <> "" Then ' this shouldn't happen?
    Digraph = pDecode(LeftCh, pSquare)
    Work = Work & Left(Digraph, 1) & Punctuation & Right(Digraph, 1)
End If
'Work = MCase(Work, text)
DecPlayfair = Work
Exit Function
Catch:
DecPlayfair = Err.Description
End Function

' This function is the general twosquare encryption method

Public Function EncTwosquare(text As String, LR As Variant, RR As Variant) As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim I As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pLR As New Polybius
Dim pRR As New Polybius

On Error GoTo Catch

Call pLR.Make(LR, vtUpperAlpha, 5)
Call pRR.Make(RR, vtUpperAlpha, 5)

For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    pLR.RC Char1, R, C
    
    Char2 = Mid(text, I + 1, 1)
    If Char2 = "" Then Char2 = "X"
    pRR.RC Char2, Y, X
    
    Work = Work & pRR.chrVal(R, X) & pLR.chrVal(Y, C)
Next I

EncTwosquare = Work
Exit Function
Catch:
EncTwosquare = Err.Description
End Function

' This function is the general twosquare encryption method

Public Function DecTwosquare(text As String, LR As Variant, RR As Variant) As String
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim I As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pLR As New Polybius
Dim pRR As New Polybius

On Error GoTo Catch

Call pLR.Make(LR, vtUpperAlpha, 5)
Call pRR.Make(RR, vtUpperAlpha, 5)

For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    pRR.RC Char1, R, C
    
    Char2 = Mid(text, I + 1, 1)
    pLR.RC Char2, Y, X
    
    Work = Work & pLR.chrVal(R, X) & pRR.chrVal(Y, C)
Next I

DecTwosquare = Work
Exit Function
Catch:
DecTwosquare = Err.Description
End Function

' This function is the general foursquare encryption method

Public Function EncFoursquare(text As String, TL As Variant, TR As Variant, BL As Variant, Optional BR As Variant) As String
Dim I As Integer
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pTL As New Polybius
Dim pTR As New Polybius
Dim pBL As New Polybius
Dim pBR As New Polybius
On Error GoTo Catch
Call pTL.Make(TL, vtUpperAlpha, 5)
Call pTR.Make(TR, vtUpperAlpha, 5)
Call pBL.Make(BL, vtUpperAlpha, 5)
If IsMissing(BR) Or IsEmpty(BR) Then ' Use Top Left
    Call pBR.Make(TL, vtUpperAlpha, 5)
Else
    Call pBR.Make(BR, vtUpperAlpha, 5)
End If
For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    Char2 = Mid(text, I + 1, 1)
    If Char2 = "" Then Char2 = "X"
    pTL.RC Char1, R, C
    pBR.RC Char2, Y, X
    Work = Work & pTR.chrVal(R, X) & pBL.chrVal(Y, C)
Next I
EncFoursquare = Work
Exit Function
Catch:
EncFoursquare = Err.Description
End Function

' This function is the general Foursquare decryption method

Public Function DecFoursquare(text As String, TL As Variant, TR As Variant, BL As Variant, Optional BR As Variant) As String
Dim I As Integer
Dim R As Integer, C As Integer
Dim X As Integer, Y As Integer
Dim Char1 As String
Dim Char2 As String
Dim Work As String
Dim pTL As New Polybius
Dim pTR As New Polybius
Dim pBL As New Polybius
Dim pBR As New Polybius
On Error GoTo Catch
Call pTL.Make(TL, vtUpperAlpha, 5)
Call pTR.Make(TR, vtUpperAlpha, 5)
Call pBL.Make(BL, vtUpperAlpha, 5)
If IsMissing(BR) Or IsEmpty(BR) Then
    Call pBR.Make(TL, vtUpperAlpha, 5)
Else
    Call pBR.Make(BR, vtUpperAlpha, 5)
End If
For I = 1 To Len(text) Step 2
    Char1 = Mid(text, I, 1)
    Char2 = Mid(text, I + 1, 1)
    If Char2 = "" Then Char2 = "X"
    pTR.RC Char1, R, C
    pBL.RC Char2, Y, X
    Work = Work & pTL.chrVal(R, X) & pBR.chrVal(Y, C)
Next I
DecFoursquare = Work
Exit Function
Catch:
DecFoursquare = Err.Description
End Function

' This function is the 5x5 Checkerboard encryption method
Public Function encChecker(text As String, Rng As Variant, Optional Repl As String = "J", Optional strColNames As String, Optional strRowNames As String) As String
Dim pSquare As New Polybius
Dim Alpha As String
Dim Work As String
On Error GoTo Catch

If Len(strColNames) > 5 Or Len(strRowNames) > 5 Then
    InitRand text
End If

' Remove non-alphabetic characters and replace substitution charater
text = Compact(text, "", Repl)
Alpha = Compact(range2str(Rng), "", Repl)

' Get Column Names
If strColNames = "" Then strColNames = "01234"
If strRowNames = "" Then strRowNames = strColNames

' Initialize square
Call pSquare.Make(Alpha, ValType.vtUpperAlpha, 5, , strColNames, strRowNames)

' Encode to row/column names
Work = pSquare.encString(text)

encChecker = Work
Exit Function
Catch:
encChecker = Err.Description
End Function

' This function is the Checkerboard decryption method
Public Function decChecker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
Dim LT As Integer
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch

' Get Column Names
If strColNames = "" Then strColNames = "01234"
If strRowNames = "" Then strRowNames = strColNames

' Initialize square
Call pSquare.Make(Rng, ValType.vtUpperAlpha, 5, , strColNames, strRowNames)

' Limit to only the characters in the column/row names
text = pSquare.Filter(text)

' Now decode with Polybius Square
Work = pSquare.decString(text)

decChecker = Work
Exit Function

Catch:
decChecker = Err.Description
End Function

' This function is the 6x6 Checkerboard encryption method
Public Function enc6Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
Dim pSquare As New Polybius
'Dim Alpha As String
Dim Work As String
On Error GoTo Catch

If Len(strColNames) > 6 Or Len(strRowNames) > 6 Then
    InitRand text
End If

' Get Column Names
If strColNames = "" Then strColNames = "012345"
If strRowNames = "" Then strRowNames = strColNames

' Initialize square
Call pSquare.Make(Rng, ValType.vtAnum, 6, , strColNames, strRowNames)

' Encode to row/column names
Work = pSquare.encString(text)

enc6Checker = Work
Exit Function
Catch:
enc6Checker = Err.Description
End Function

' This function is the Checkerboard decryption method
Public Function dec6Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
Dim LT As Integer
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch

' Get Column Names
If strColNames = "" Then strColNames = "012345"
If strRowNames = "" Then strRowNames = strColNames

' Initialize square
Call pSquare.Make(Rng, ValType.vtAnum, 6, , strColNames, strRowNames)

' Limit to only the characters in the column/row names
text = pSquare.Filter(text)

' Now decode with Polybius Square
Work = pSquare.decString(text)

dec6Checker = Work
Exit Function

Catch:
dec6Checker = Err.Description
End Function

' This function is the 7x7 Checkerboard encryption method
Public Function enc7Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
Dim pSquare As New Polybius
'Dim Alpha As String
Dim Work As String
On Error GoTo Catch

If Len(strColNames) > 7 Or Len(strRowNames) > 7 Then
    InitRand text
End If

' Get Column Names
If strColNames = "" Then strColNames = "0123456"
If strRowNames = "" Then strRowNames = strColNames

' Initialize square
Call pSquare.Make(Rng, ValType.vtASCII, 7, , strColNames, strRowNames)

' Encode to row/column names
Work = pSquare.encString(UCase(text))

enc7Checker = Work
Exit Function
Catch:
enc7Checker = Err.Description
End Function

' This function is the Checkerboard decryption method
Public Function dec7Checker(text As String, Rng As Variant, Optional strColNames As String, Optional strRowNames As String) As String
Dim LT As Integer
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch

' Get Column Names
If strColNames = "" Then strColNames = "0123456"
If strRowNames = "" Then strRowNames = strColNames

' Initialize square
Call pSquare.Make(Rng, ValType.vtASCII, 7, , strColNames, strRowNames)

' Limit to only the characters in the column/row names
text = pSquare.Filter(text)

' Now decode with Polybius Square
Work = pSquare.decString(text)

dec7Checker = Work
Exit Function

Catch:
dec7Checker = Err.Description
End Function

' This function is the Trisquare encryption method
Public Function encTrisquare(text As String, S1 As Variant, S2 As Variant, S3 As Variant, Optional Repl As String = "J") As String
Dim I As Integer
Dim R1 As Integer, C1 As Integer
Dim R2 As Integer, C2 As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim pSquare3 As New Polybius
Dim Work As String
On Error GoTo Catch

InitRand text ' Repeatable random

' Remove non-alphabetic characters and replace substitution charater, pad with "X" if needed
text = Compact(text, "+", Repl)

' Initialize squares
Call pSquare1.Make(S1, ValType.vtUpperAlpha, 5)
Call pSquare2.Make(S2, ValType.vtUpperAlpha, 5)
Call pSquare3.Make(S3, ValType.vtUpperAlpha, 5)

' Encode digraphs
For I = 1 To Len(text) Step 2
    pSquare1.RC Mid(text, I, 1), R1, C1 ' Row1 Col1 from square 1 for char 1
    pSquare2.RC Mid(text, I + 1, 1), R2, C2 ' Row2 Col2 from square 2 for char 2
    ' letter from random row/col1 from square 1, row1/col2 from square 3, row2/random col from square 2...
    Work = Work & pSquare1.chrVal(Int(Rnd() * 5) + 1, C1) & pSquare3.chrVal(R1, C2) & pSquare2.chrVal(R2, Int(Rnd() * 5) + 1)
Next I

encTrisquare = Work
Exit Function
Catch:
encTrisquare = Err.Description
End Function

' This function is the Trisquare decryption method
Public Function decTrisquare(text As String, S1 As Variant, S2 As Variant, S3 As Variant) As String
Dim I As Integer, LT As Integer
Dim R1 As Integer, C1 As Integer
Dim R2 As Integer, C2 As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim pSquare3 As New Polybius
Dim Work As String
On Error GoTo Catch

' Initialize squares
Call pSquare1.Make(S1, ValType.vtUpperAlpha, 5)
Call pSquare2.Make(S2, ValType.vtUpperAlpha, 5)
Call pSquare3.Make(S3, ValType.vtUpperAlpha, 5)

' Limit to only alphabetic (remove padding)
text = ToAlpha(text)
LT = Len(text)

If LT Mod 3 <> 0 Then
    decTrisquare = "ERROR: Text is not a multiple of length 3- [" & text & "]"
    Exit Function
End If

' Now decode with Polybius Squares
For I = 1 To LT Step 3
    C1 = pSquare1.Col(Mid(text, I, 1)) ' get col1 from square 1
    pSquare3.RC Mid(text, I + 1, 1), R1, C2 ' get row1/col2 from square 3
    R2 = pSquare2.Row(Mid(text, I + 2, 1)) ' get row2 from square 2
    Work = Work & pSquare1.chrVal(R1, C1) & pSquare2.chrVal(R2, C2)
Next I

decTrisquare = Work
Exit Function

Catch:
decTrisquare = Err.Description
End Function

' This function is the BIFID encryption method
Public Function encBIFID(text As String, Period As Integer, S1 As Variant, Optional S2 As Variant = Nothing, Optional Repl As String = "J") As String
Dim I As Integer, J As Integer
Dim Ln As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim Work As String
Dim Tmp As String
Dim Blk As String
Dim Ch As String
Const Colnames As String = "12345"
On Error GoTo Catch

If Period < 0 Then
    encBIFID = "ERROR: Period < 0 [" & Period & "]"
    Exit Function
End If

' Remove non-alphabetic characters and replace substitution charater
If Repl = "" Then Repl = "J"
If Period = 0 Then Tmp = "."
text = Compact(text, Tmp, Repl)

' Initialize squares
Call pSquare1.Make(S1, ValType.vtUpperAlpha, 5, , Colnames)
If S2 Is Nothing Then
    Set pSquare2 = pSquare1
Else
    If S2 = "" Then
        Set pSquare2 = pSquare1
    Else
        Call pSquare2.Make(S2, ValType.vtUpperAlpha, 5, , Colnames)
    End If
End If

I = 1
Ln = Len(text)
While I <= Ln
    J = 0
    Ch = ""
    Blk = ""
    While (I <= Ln) And ((Period = 0 And Ch <> " ") Or (J < Period))
        Ch = Mid(text, I, 1)
        I = I + 1
        If Ch >= "A" And Ch <= "Z" Then
            J = J + 1
            Blk = Blk & Ch
        End If
    Wend
    If J > 0 Then
        Tmp = ""
        Blk = pSquare1.encString(Blk)
        
        Blk = Blk & Mid(Blk, 2, J * 2)
        For J = 1 To Len(Blk) Step 2
            Tmp = Tmp & Mid(Blk, J, 1)
        Next J
    
        Work = Work & pSquare2.decString(Tmp)
    End If
Wend

If Period = 0 Then ' insert punctuation
    Tmp = Work
    Work = ""
    J = 1
    For I = 1 To Len(text)
        Ch = Mid(text, I, 1)
        If Ch >= "A" And Ch <= "Z" Then
            Work = Work & Mid(Tmp, J, 1)
            J = J + 1
        Else
            Work = Work & Ch
        End If
    Next I
End If

encBIFID = Work
Exit Function
Catch:
encBIFID = Err.Description
End Function


' This function is the BIFID decryption method
Public Function decBIFID(text As String, Period As Integer, S1 As Variant, Optional S2 As Variant = Nothing) As String
Dim I As Integer, J As Integer
Dim P2 As Integer
Dim Ln As Integer, BlkLn As Integer
Dim pSquare1 As New Polybius
Dim pSquare2 As New Polybius
Dim Work As String
Dim Tmp As String
Dim Blk As String
Dim Ch As String
Const Colnames As String = "12345"
On Error GoTo Catch

If Period < 0 Then
    decBIFID = "ERROR: Period < 0 [" & Period & "]"
    Exit Function
End If

' Remove non-alphabetic characters and replace substitution charater
If Period > 0 Then text = ToAlpha(text)

' Initialize squares
Call pSquare1.Make(S1, ValType.vtUpperAlpha, 5, , Colnames)
If S2 Is Nothing Then
    Set pSquare2 = pSquare1
Else
    If S2 = "" Then
        Set pSquare2 = pSquare1
    Else
        Call pSquare2.Make(S2, ValType.vtUpperAlpha, 5, , Colnames)
    End If
End If

I = 1
Ln = Len(text)
While I <= Ln
    J = 0
    Ch = ""
    Blk = ""
    While (I <= Ln) And ((Period = 0 And Ch <> " ") Or (J < Period))
        Ch = Mid(text, I, 1)
        I = I + 1
        If Ch >= "A" And Ch <= "Z" Then
            J = J + 1
            Blk = Blk & Ch
        End If
    Wend
    If J > 0 Then
        Blk = pSquare2.encString(Blk)
        
        Tmp = ""
        BlkLn = Len(Blk) \ 2
        For J = 1 To BlkLn
            Tmp = Tmp & Mid(Blk, J, 1) & Mid(Blk, J + BlkLn, 1)
        Next J
    
        Work = Work & pSquare1.decString(Tmp)
    End If
Wend

If Period = 0 Then ' insert punctuation
    Tmp = Work
    Work = ""
    J = 1
    For I = 1 To Len(text)
        Ch = Mid(text, I, 1)
        If Ch >= "A" And Ch <= "Z" Then
            Work = Work & Mid(Tmp, J, 1)
            J = J + 1
        Else
            Work = Work & Ch
        End If
    Next I
End If

decBIFID = Work
Exit Function
Catch:
decBIFID = Err.Description
End Function

