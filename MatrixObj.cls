VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "MatrixObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
'CLASS:
'       MatrixObj (base for matrix Manipulation)
'
'DEPENDENCIES:
'       AlphaList (VBA can't inherit, so just embedding)
'
'USAGE:
'       Matrix.Init [Filter [, Repl]] - Initialize alphabet
'       Matrix.Parse key              - Initialize array
'       text = Matrix.Filter(text)    - Filter text for values in alphabet
'       [Matrix.invert]               - to decrypt instead of encrypt
'       Matrix.Multiply text          - Multiply text vector by matrix
'
'       Beginning with an alphalist, parse a key into the array.  Filter some text for
'       values in the alphalist, then multiply the text vector by the array.  Optionally,
'       invert the matrix before multiplying to decrypt.
'
'       Will throw an error if the key cannot be parsed - either zero determinant OR inverse
'       determinant shares cofactors with the alphalist length.  For example, a standard
'       alphabet (length 26) shares a factor of 2 with any even determinant, and 13 with a
'       determinant of 13.  Create a different key matrix in these cases.
'
'       Caller must trap errors.
'
'       726, "MatrixObj:Parse", "Alphabet is not initialized"
'       720, "MatrixObj:Parse", "Key index value " & L & " too large; value must be < " & tList.Count
'       719, "MatrixObj:Parse", "Empty Key"
'       721, "MatrixObj:Parse", "Key letter " & Ch & " not found in filter list"
'       722, "MatrixObj:Parse", "Determinant of matrix is zero, cannot decrypt with this key"
'       723, "MatrixObj:Parse", "Determinant " & Det & " shares common factors with alphabet length " & tList.Count & ", cannot decrypt with this key"
'
'       726, "MatrixObj:Multiply", "Matrix is not initialized"
'       724, "MatrixObj:Multiply", "Text contains characters not found in alphabet : " & XLAT(text, "^" & tList.strAlpha, , xlBoth)
'       725, "MatrixObj:Multiply", "Text length (" & Ln & ") is not a multiple of matrix height (" & RC & ")"
'
'       726, "MatrixObj:Invert", "Matrix is not initialized"
'
'GROUPS:
'       MatrixObj
'            Char, Count, Size, Exists, Init, Invert, Multiply, Parse, Filter, Block
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 03/13/2021 Chip Bloch    -Original Version
'-------------------------------------------------------------------------------------------

' We are implementing an NxN matrix in a vector (single dimension array)
'
'  (0) (1) (2)       a b c       (0,0) (0,1) (0,2)
'  (3) (4) (5)  <->  d e f  <->  (1,0) (1,1) (1,2)
'  (6) (7) (8)       g h i       (2,0) (2,1) (2,2)

Private Enum Indices ' Just to simplify translating the arrays from R,C notation:
    a_
    b_
    c_
    d_
    e_
    f_
    g_
    h_
    i_
    [_Last]
End Enum

Private tList As New AlphaList ' VBA doesn't support inheritance, so we'll just embed this
Public Det As Long             ' the determinant
Public DI As Long              ' The inverse determinant
Public RC As Integer           ' number of rows/columns
Private Sz As Integer          ' number of elements
Private Mat() As Long          ' the matrix

' Initialize the AlphaList (filter)
Public Sub Init(Filt As Variant, Repl As Variant)
    tList.Init Filt, Repl
End Sub

' Return number of elements in matrix
Public Property Get Size() As Integer
Size = Sz
End Property

' Return count of characters in alphabet
Public Property Get Count() As Integer
Count = tList.Count
End Property

' Return character at index in alphabet
Public Property Get Char(I As Integer) As String
Char = tList.Char(I)
End Property

' True if the character exists in the alphabet
Public Property Get Exists(Ch As String) As Boolean
Exists = tList.Exists(Ch)
End Property

' Filter a string based on the alphabet
Public Function Filter(text As String) As String
Filter = tList.Filter(text)
End Function

' Parse key into matrix
'
' Key can be either a string or a series of numbers separated by spaces
' If numbers, these will correspond to characters in the filter (usually A=0, B=1...)
' RC will contain height of matrix
' mat() will contain key values
' Caller is expected to trap errors
Public Sub Parse(key As String)
Dim R As Integer, C As Integer
Dim P As Integer
Dim V As Integer
Dim L As Long

Dim Kv() As String

Dim Wrk As String
Dim Ch As String

If tList.Count < 3 Then
    Err.Raise 726, "MatrixObj:Parse", "Alphabet is not initialized"
End If

' Parse key
' Key is expected to be a string of characters in the default alphabet, or a space delimited list of integers
Wrk = key
If ToAlpha(key) = "" Then ' No text, check for space separated digits
    ' Split numbers into array
    Kv = Split(ToDigit(key, " "), " ")
    
    If UBound(Kv) > 1 Then
        Wrk = ""
        ' and parse into string using text filter
        For C = 0 To Application.Min(UBound(Kv), 9)
            Ch = Kv(C)
            If Ch <> "" Then
                L = Kv(C)
                If L >= tList.Count Then ' index is out of range of filter list
                    Err.Raise 720, "MatrixObj:Parse", "Key index value " & L & " too large; value must be < " & tList.Count
                End If
                V = L
                Wrk = Wrk & tList.Char(V)
            End If
        Next C
    End If
End If

If Wrk = "" Then
    Err.Raise 719, "MatrixObj:Parse", "Empty Key"
End If

' Determine key length, pad to square value (4 or 9)
If Len(Wrk) <= 4 Then
    RC = 2
Else
    RC = 3
End If

Sz = RC * RC

' Pad to min length
Do While Len(Wrk) < Sz
    Wrk = Wrk & tList.strAlpha
Loop
Wrk = Left(Wrk, Sz)

' Fill matrix from key, using alphabet
ReDim Mat(Sz - 1)
For P = 1 To Sz
    Ch = Mid(Wrk, P, 1)
    If Not tList.Exists(Ch) Then
        Err.Raise 721, "MatrixObj:Parse", "Key letter " & Ch & " not found in filter list"
    End If
    Mat(P - 1) = tList(Ch)
Next P

' Calculate determinant
If RC = 2 Then
    ' ad - bc
    Det = Mat(a_) * Mat(d_) - Mat(b_) * Mat(c_)
Else ' RC = 3
    ' aei+bfg+cdh-ceg-bdi-afh
    Det = Mat(a_) * Mat(e_) * Mat(i_) + Mat(b_) * Mat(f_) * Mat(g_) + Mat(c_) * Mat(d_) * Mat(h_) _
        - Mat(a_) * Mat(f_) * Mat(h_) - Mat(b_) * Mat(d_) * Mat(i_) - Mat(c_) * Mat(e_) * Mat(g_)
End If

Det = (tList.Count + (Det Mod tList.Count)) Mod tList.Count

If Det = 0 Then
    Err.Raise 722, "MatrixObj:Parse", "Determinant of matrix is zero, cannot decrypt with this key"
End If

' find multiplicative inverse
P = 1
Do
    If (Det * P) Mod tList.Count = 1 Then
        DI = P
        Exit Do
    End If
    P = P + 1
Loop Until P = tList.Count

If DI = 0 Then
    Err.Raise 723, "MatrixObj:Parse", "Determinant " & Det & " shares common factors with alphabet length " & tList.Count & ", cannot decrypt with this key"
End If

End Sub

' Returns a text string of matrix values suitable for display in a cell

Public Function Block() As String
Dim R As Integer, C As Integer
Dim Wrk As String
Dim Fixed As String
Fixed = String$(4, Chr(160))
' Going rows, columns here because that's how we want to display...
For R = 0 To Sz - 1 Step RC
    ' add a line feed after the previous row
    If Wrk <> "" Then Wrk = Wrk & Chr(10)
    For C = 0 To RC - 1
        ' fixed 4 character width per cell = values may be 2 digits wide
        Wrk = Wrk & Right(Fixed & Mat(R + C), 4)
    Next C
Next R
Block = Wrk
End Function

' perform matrix multiplication against a text vector

Public Function Multiply(text As String) As String
Dim P As Integer
Dim R As Integer
Dim L As Long
Dim V As Integer
Dim Vec() As Long
Dim Ch As String
Dim Wrk As String
Dim Ln As Integer

' some error checking
If Sz = 0 Then
    Err.Raise 726, "MatrixObj:Multiply", "Matrix is not initialized"
End If

If text <> Filter(text) Then
    Err.Raise 724, "MatrixObj:Multiply", "Text contains characters not found in alphabet : " & XLAT(text, "^" & tList.strAlpha, , xlBoth)
End If

Ln = Len(text)

If Ln Mod RC <> 0 Then
    Err.Raise 725, "MatrixObj:Multiply", "Text length (" & Ln & ") is not a multiple of matrix height (" & RC & ")"
End If

' Do the multiplication
ReDim Vec(RC - 1) ' text vector
For P = 1 To Ln Step RC
    ' load the next RC letters into the vector
    For R = 0 To RC - 1
        Ch = Mid(text, P + R, 1)
        Vec(R) = tList(Ch)
    Next R
    ' Now multiply one row at a time
    For R = 0 To Sz - 1 Step RC
        If RC = 2 Then
            L = Mat(R) * Vec(0) + Mat(R + 1) * Vec(1)
        Else
            L = Mat(R) * Vec(0) + Mat(R + 1) * Vec(1) + Mat(R + 2) * Vec(2)
        End If
        V = L Mod tList.Count
        Wrk = Wrk & tList.Char(V)
    Next R
Next P
Multiply = Wrk
End Function

' Invert the matrix

Public Function Invert()
Dim P As Integer
Dim V As Long
Dim Adj() As Long

If Sz = 0 Then
    Err.Raise 726, "MatrixObj:Invert", "Matrix is not initialized"
End If

' For decryption, here there be inverting the matrix

' Adjugate matrix
ReDim Adj(Sz - 1)
Select Case RC
    Case 2
        Adj(a_) = Mat(d_)
        Adj(b_) = -Mat(b_)
        Adj(c_) = -Mat(c_)
        Adj(d_) = Mat(a_)
    Case 3
        ' Every cell is the determinant of the transformed matrix without the current row/column
        Adj(a_) = Mat(e_) * Mat(i_) - Mat(f_) * Mat(h_) ' ei - fh
        Adj(b_) = Mat(c_) * Mat(h_) - Mat(b_) * Mat(i_) ' ch - bi - every other one is reversed to change sign
        Adj(c_) = Mat(b_) * Mat(f_) - Mat(c_) * Mat(e_) ' bf - ce
        Adj(d_) = Mat(f_) * Mat(g_) - Mat(d_) * Mat(i_) ' fg - di
        Adj(e_) = Mat(a_) * Mat(i_) - Mat(c_) * Mat(g_) ' ai - cg
        Adj(f_) = Mat(c_) * Mat(d_) - Mat(a_) * Mat(f_) ' cd - af
        Adj(g_) = Mat(d_) * Mat(h_) - Mat(e_) * Mat(g_) ' dh - eg
        Adj(h_) = Mat(b_) * Mat(g_) - Mat(a_) * Mat(h_) ' bg - ah
        Adj(i_) = Mat(a_) * Mat(e_) - Mat(b_) * Mat(d_) ' ae - bd
End Select

' Scalar Multiply by inverse determinant (DI)
For P = 0 To Sz - 1
    V = Adj(P)
    If V < 0 Then V = (V Mod tList.Count) + tList.Count
    Mat(P) = (DI * V) Mod tList.Count
Next P
End Function
