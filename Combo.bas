Attribute VB_Name = "Combo"
Option Explicit
' To save an add-in, use Ctrl+G then ThisWorkbook.Save

'-------------------------------------------------------------------------------------------
' MODULE:
'       Combo
'
' USAGE:
'       Combination cipher routines (two or more cipher forms, e.g. transposition and
'       encryption)
'
' GROUPS:
'       Aphid
'            encAphid, decAphid
'
'       Bazeries
'            encBazeries, decBazeries
'
'       Nicodemus
'            encNicodemus, decNicodemus
'
'       ADFGX
'            encADFGX, decADFGX
'
'       ADFGVX
'            encADFGVX, decADFGVX
'
' ---Date--- ----Name----  --Description----------------------------------------------------
' 06/30/2020 Chip Bloch    -Added ADFGX (in Playfair)
' 07/26/2020 Chip Bloch    -Migrated ADFGX to Combo
' 07/26/2020 Chip Bloch    -Original version; Migrated from Trans, Playfair
'                          -Calling TList, decCCol in decADFGX
' 07/28/2020 Chip Bloch    -Change Pad to Sep on encNicodemus, encADFGX
'                          -removed Pad from decNicodemus
' 07/31/2020 Chip Bloch    -Added Sep to encCCol, passed in from encADFGX
'                          -calling polybius.encString in encADFGX
' 08/05/2020 Chip Bloch    -Added encADFGVX, decADFGVX
' 08/06/2020 Chip Bloch    -Modified polybius make method (prep for moving column names)
' 08/08/2020 Chip Bloch    -Reordered parms for Polybius
' 08/08/2020 Chip Bloch    -Calling Filter in Polybius for decADFG*X functions
' 08/24/2020 Chip Bloch    -Allowed ASCII Key with Nicodemus
'                          -Allowed Checkerboard (multi-column) for ADFGX
'                          -Allowed Checkerboard (multi-column) for ADFGVX
' 09/11/2020 Chip Bloch    -Retiring enc5Col (called from Nicodemus)
' 09/12/2020 Chip Bloch    -Retired dec5Col.  Nicodemus now uses encCCol/decCCol
' 09/15/2020 Chip Bloch    -Added encBazeries, decBazeries
' 12/06/2020 Chip Bloch    -Made ADFGX Dynamic
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType (pSquare.Make)
' 12/11/2020 Chip Bloch    -Made Bazieries Dynamic
' 12/12/2020 Chip Bloch    -Trapped empty key in *Bazieres, Folded plain text to lower case
' 02/09/2021 Chip Bloch    -Added Reference to AddSep
'                          -Added rowcol order to ADFGX
' 04/18/2021 Chip Bloch    -Added encAphid, decAphid
'-------------------------------------------------------------------------------------------

' Encode Nicodemus (Vigenaire + Column Transposition)
Public Function encNicodemus(text As String, Key As String, Optional Method As CipherType = CipherType.ctVigenaire, Optional Sep As String)
'Key = ToAlpha(Key) ' must be alpha for Vigenaire
text = VigEncode(ToAlpha(text), Key, , Method)
'text = enc5Col(text, Key, Sep)
text = encCCol(text, Key, "", Sep, 5)

encNicodemus = text
End Function

' Decode Nicodemus
Public Function decNicodemus(text As String, Key As String, Optional Method As CipherType = CipherType.ctVigenaire)
'Key = ToAlpha(Key) ' must be alpha for Vigenaire
text = decCCol(ToAlpha(text), Key, 5)
'text = dec5Col(ToAlpha(text), Key)
text = VigDecode(text, Key, , Method)
decNicodemus = text
End Function

' This function is the ADFGX encryption method (Polybius encryption + Columnar transposition)
Public Function encADFGX(text As String, Rng As Variant, Key As String, Optional Sep As String = "", Optional Repl As String = "J", Optional strColNames As String, _
    Optional RCO As String) As String
Dim pSquare As New Polybius
Dim FT As fltType
Dim Work As String

On Error GoTo Catch

' Initialize Square (dynamic)
Call pSquare.Make(Rng, -1)

' Check RCO (RowCol Order)
Select Case UCase(RCO)
    Case "", "RC"
        FT = ftSquare
    Case "RCCR"
        FT = ftSqRC
    Case "CRRC"
        FT = ftSqCR
    Case "CR"
        FT = ftColRows
    Case Else
        Err.Raise 302, "encADFGX", "Error: Unknown RowCol Order: " & RCO & ", expecting RC, RCCR, CRRC or CR"
End Select

'Get Column Names
If strColNames = "" Then
    Select Case pSquare.Cols
        Case 7
            strColNames = "ADFGJVX" ' 7x7 - Non-Standard
        Case 6
            strColNames = "ADFGVX"
        Case Else
            strColNames = "ADFGX"
    End Select
End If

pSquare.Colnames = strColNames

If Len(strColNames) > pSquare.Cols Then ' Also non-standard
    InitRand text
End If

' Remove non-alphabetic characters from plain text and replace substitution character
text = Compact(text, "", Repl, pSquare.PolyVals)

' Encode to row/column names
Work = pSquare.encString(text, FT)

' And finally, incomplete columnar encryption
Work = encCCol(Work, Key, "", Sep)

encADFGX = Work
Exit Function
Catch:
encADFGX = Err.Description
End Function

' This function is the ADFGX decryption method
Public Function decADFGX(text As String, Rng As Variant, Key As String, Optional strColNames As String, Optional RCO As String) As String
Dim FT As fltType
Dim FTRC As fltType
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch

' Initialize Square (dynamic)
Call pSquare.Make(Rng, -1)

Select Case UCase(RCO)
    Case "", "RC"
        FT = ftSquare
        FTRC = ftRowCols
    Case "RCCR"
        FT = ftSqRC
        FTRC = ftSqRC
    Case "CRRC"
        FT = ftSqCR
        FTRC = ftSqCR
    Case "CR"
        FT = ftColRows
        FTRC = ftColRows
    Case Else
        Err.Raise 302, "decADFGX", "Error: Unknown RowCol Order: " & RCO & ", expecting RC, RCCR, CRRC or CR"
End Select

'Get Column Names
If strColNames = "" Then
    Select Case pSquare.Cols
        Case 7
            strColNames = "ADFGJVX"
        Case 6
            strColNames = "ADFGVX"
        Case Else
            strColNames = "ADFGX"
    End Select
End If

pSquare.Colnames = strColNames

' Limit to only the 5 characters in the column names
Work = pSquare.Filter(text, FTRC)

' Columnar Transposition
Work = decCCol(Work, Key)

' Now decode with Polybius Square
Work = pSquare.decString(Work, FT)

decADFGX = Work
Exit Function

Catch:
decADFGX = Err.Description
End Function

' This function is the ADFGX encryption method (Polybius encryption + Columnar Transposition) (deprecated)
Public Function encADFGVX(text As String, Rng As Variant, Key As String, Optional Sep As String = "", Optional strColNames As String) As String
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch

'Get Column Names
If strColNames = "" Then strColNames = "ADFGVX"

If Len(strColNames) > 6 Then
    InitRand text
End If

' Remove non-alphabetic/numeric characters
text = ToANum(text)

' Initialize Square
Call pSquare.Make(Rng, 6, textType.ttAnumUpper, , strColNames)

' Encode to row/column names
Work = pSquare.encString(text)

' And finally, incomplete columnar encryption
Work = encCCol(Work, Key, "", Sep)

encADFGVX = Work
Exit Function
Catch:
encADFGVX = Err.Description
End Function

' This function is the ADFGX decryption method
Public Function decADFGVX(text As String, Rng As Variant, Key As String, Optional strColNames As String) As String
Dim pSquare As New Polybius
Dim Work As String
On Error GoTo Catch

' Get Column Names
If strColNames = "" Then strColNames = "ADFGVX"

' Initialize Square
Call pSquare.Make(Rng, 6, textType.ttAnumUpper, , strColNames)

' Limit to only the 5 characters in the column names
text = pSquare.Filter(text)

' Columnar Transposition
text = decCCol(text, Key)

' Now decode with Polybius Square
Work = pSquare.decString(text)

decADFGVX = Work
Exit Function

Catch:
decADFGVX = Err.Description
End Function


' This function is the Bazeries encryption method (Polybius encryption + Columnar Transposition)
Public Function encBazeries(text As String, L As Long, Optional Rng As Variant, Optional Repl As String = "J", Optional Sep As String = "") As String
Dim Plain As New Polybius
Dim K As Integer
Dim Kv As Integer
Dim kLn As Integer
Dim P As Integer
Dim PT As String
Dim CT As String
Dim cipher As New Polybius
'Dim Dup As New AlphaList
Dim Key As String
Const strCols As String = "1234567"
Dim Work As String
On Error GoTo Catch

If L <= 0 Or L > 999999 Then ' out of range
    encBazeries = NumToText(L)
    Exit Function
End If

' Set up key
Key = L
kLn = Len(Key)

' Ciphertext square: Default straight alpha in key text order
If IsMissing(Rng) Or IsEmpty(Rng) Then ' Default
    CT = Dedup(NumToText(L), "*", ttAlpha25, Repl)
    Call cipher.Make(CT, 5, textType.ttAlphaUpper)
Else
    CT = range2str(Rng)
    If Len(CT) > 26 Then ' Dynamic
        Call cipher.Make(CT, -1)
    Else ' use passed in value
        CT = Dedup(CT, "*", ttAlpha25, Repl)
        Call cipher.Make(CT, 5, textType.ttAlphaUpper)
    End If
End If
cipher.Colnames = Left(strCols, cipher.Cols)

' Plaintext square: Default Alpha25 transpose
PT = Dedup("", "*", cipher.PolyVals, Repl)
PT = Transmute(PT, cipher.Cols)
Call Plain.Make(PT, cipher.Cols, cipher.Values)
Plain.Colnames = Left(strCols, Plain.Cols)

' Parse text
text = Compact(text, "", Repl, Plain.PolyVals)

P = 1
K = 1

' Paste reversed groups together
While P <= Len(text)
    Kv = Mid(Key, K, 1)
    Work = Work & Reverse(Mid(text, P, Kv))
    P = P + Kv
    K = K + 1
    If K > kLn Then
        K = 1
    End If
Wend

' Encode plaintext to row/column coordinates
Work = Plain.encString(Work)
' Decode ciphertext from row/column coordinates
Work = cipher.decString(Work)

' Optional Group - don't group if ASCII
Work = AddSep(Work, Sep)

encBazeries = Work
Exit Function
Catch:
encBazeries = Err.Description
End Function

' This function is the Bazeries encryption method (Polybius encryption + Columnar Transposition)
Public Function decBazeries(text As String, L As Long, Optional Rng As Variant, Optional Repl As String = "J") As String
Dim Plain As New Polybius
Dim K As Integer
Dim Kv As Integer
Dim kLn As Integer
Dim P As Integer
Dim PT As String
Dim cipher As New Polybius
Dim CT As String
Dim Key As String
Const strCols As String = "1234567"
Dim Work As String
On Error GoTo Catch

If L <= 0 Or L > 999999 Then ' out of range
    decBazeries = NumToText(L)
    Exit Function
End If

' Set up key
Key = L
kLn = Len(Key)

' Ciphertext square: Default straight alpha in key text order
If IsMissing(Rng) Or IsEmpty(Rng) Then ' Default
    CT = Dedup(NumToText(L), "*", ttAlpha25, Repl)
    Call cipher.Make(CT, 5, textType.ttAlphaUpper)
Else
    CT = range2str(Rng)
    If Len(CT) > 26 Then ' Dynamic
        Call cipher.Make(CT, -1)
    Else ' use passed in value
        CT = Dedup(CT, "*", ttAlpha25, Repl)
        Call cipher.Make(CT, 5, textType.ttAlphaUpper)
    End If
End If
cipher.Colnames = Left(strCols, cipher.Cols)

' Plaintext square: Default Alpha25 transpose
PT = Dedup("", "*", cipher.PolyVals, Repl)
PT = LCase(Transmute(PT, cipher.Cols))
Call Plain.Make(PT, cipher.Cols, cipher.Values)
Plain.Colnames = Left(strCols, Plain.Cols)

' Parse text
text = cipher.Filter(text, ftSquare)
'text = Compact(text, "", Repl, Plain.PolyVals)

P = 1
K = 1

' Paste reversed groups together
While P <= Len(text)
    Kv = Mid(Key, K, 1)
    Work = Work & Reverse(Mid(text, P, Kv))
    P = P + Kv
    K = K + 1
    If K > kLn Then
        K = 1
    End If
Wend

' Encode ciphertext from row/column coordinates
Work = cipher.encString(Work)
' Decode plaintext to row/column coordinates
Work = Plain.decString(Work)

decBazeries = Work
Exit Function
Catch:
decBazeries = Err.Description
End Function

Public Function encAphid(text As String, PassPhrase As String, Optional Serial As String, Optional tType As textType = ttAlpha25Upper, Optional Repl As Variant, _
    Optional Sep As String, Optional Pass2 As String, Optional Period As Integer, Optional Exp As String = "&") As String
Dim I As Integer
Dim C As Integer
Dim Kn As Integer
Dim Cn As Integer
Dim S1 As New Polybius
Dim Wrk As String
Dim Square As String
Dim Keys() As String
Dim Cd As String
On Error GoTo Catch

If Exp = "" Then Exp = "&"
' Make square 1 from passphrase
Square = Dedup(PassPhrase, Exp, tType, Repl)
S1.Make Square, -1
S1.Colnames = Left("1234567", S1.Cols)

' Compact text, remove control characters, replace values
text = Compact(text, "", Repl, tType)
If text = "" Then Exit Function

' Encode string
Wrk = S1.encString(text)

' Seriate input (Split rows/columns
If Period > 1 Or Period = -1 Then
    Wrk = Seriate_(Wrk, Period, st_Deseriate)
End If

' Match case of serial
If tType < 0 Then
    Serial = UCase(Serial)
ElseIf tType > 0 Then
    Serial = LCase(Serial)
End If

'Split passphrase into words
Keys = Split(PassPhrase, " ")

Keys(0) = Serial & Keys(0) ' Add serial to first word

' Generate keys for columnar encryption
Kn = UBound(Keys)
For I = 0 To Kn
    Cd = Cd & Keys(I)
    If Len(Cd) > 3 Or I = Kn Then
        ' key = alpha, sort identical letters in reverse
        Keys(C) = AltKey("=" & Cd, ktAlpha)
        C = C + 1 ' Add one to codes count
        Cd = ""
    End If
Next I

' incomplete columnar encryption for every code
For I = 0 To C - 1
    Wrk = encCCol(Wrk, Keys(I), "")
Next I

' rejoin "rows" and "columns"
If Period > 1 Or Period = -1 Then
    Wrk = Seriate_(Wrk, Period, st_Seriate)
End If

' Use optional passphrase 2 to decrypt

If Pass2 <> "" Or S1.PolyVals = ttAlphaPlus Then
    'Square = Dedup(Pass2, Exp, tType, Repl)
    If Pass2 <> "" Then
        Square = Dedup(Pass2, Exp, tType, Repl)
    End If
    If S1.PolyVals = ttAlphaPlus Then
        Square = Paginate(Square)
    End If
    Set S1 = New Polybius
    S1.Make Square, -1
    S1.Colnames = Left("1234567", S1.Cols)
End If

' "decrypt", prepend serial
Wrk = Serial & S1.decString(Wrk)

' Optional separator
Wrk = AddSep(Wrk, Sep)

encAphid = Wrk

Exit Function
Catch:
encAphid = Err.Description
End Function

Public Function decAphid(text As String, PassPhrase As String, Optional Serial As String, Optional tType As textType = ttAlpha25Upper, Optional Repl As Variant, _
    Optional Sep As String, Optional Pass2 As String, Optional Period As Integer, Optional Exp As String = "&") As String
Dim I As Integer
Dim C As Integer
Dim Kn As Integer
Dim Cn As Integer
Dim S1 As New Polybius
Dim Wrk As String
Dim Keys() As String
Dim Cd As String
On Error GoTo Catch

If Exp = "" Then Exp = "&"

If Pass2 = "" Then
    Wrk = PassPhrase
Else
    Wrk = Pass2
End If
Wrk = Dedup(Wrk, Exp, tType, Repl)
If Len(Wrk) = 52 Then
    S1.Make Paginate(Wrk), -1
Else
    S1.Make Wrk, -1
End If

' Make square 1 from passphrase
S1.Colnames = Left("1234567", S1.Cols)

' Remove separator
text = UngroupBy(text, Sep)

 ' remove serial if present
If Serial <> "" Then
    ' Match case of serial
    If tType < 0 Then
        Serial = UCase(Serial)
    ElseIf tType > 0 Then
        Serial = LCase(Serial)
    End If
    
    If Left(text, Len(Serial)) = Serial Then
        text = Mid(text, Len(Serial) + 1, Len(text))
    End If
End If

' Remove non-alphabetic characters ?
'text = S1.Filter(text, ftSquare)

' Encrypt
Wrk = S1.encString(text)

' split
If Period > 1 Or Period = -1 Then
    Wrk = Seriate_(Wrk, Period, st_Deseriate)
End If

'Split passphrase into words
Keys = Split(PassPhrase, " ")

Keys(0) = Serial & Keys(0) ' Add serial to first word

' Generate keys for columnar decryption
Kn = UBound(Keys)
For I = 0 To Kn
    Cd = Cd & Keys(I)
    If Len(Cd) > 3 Or I = Kn Then
        ' key = alpha, sort identical letters in reverse
        Keys(C) = AltKey("=" & Cd, ktAlpha)
        C = C + 1 ' Add one to codes count
        Cd = ""
    End If
Next I

' incomplete columnar decryption for every code (reverse order)
For I = C - 1 To 0 Step -1
    Wrk = decCCol(Wrk, Keys(I))
Next I

' rejoin rows, cols
If Period > 1 Or Period = -1 Then
    Wrk = Seriate_(Wrk, Period, st_Seriate)
End If

' rebuild square1 if needed
If Pass2 <> "" Or tType = ttAlphaPlus Then
    Set S1 = New Polybius
    S1.Make Dedup(PassPhrase, Exp, tType, Repl), -1
    S1.Colnames = Left("1234567", S1.Cols)
End If

Wrk = S1.decString(Wrk)

decAphid = Wrk

Exit Function
Catch:
decAphid = Err.Description
End Function

