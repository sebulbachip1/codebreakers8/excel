October 1st, 2020
Initial public baseline
--------------------------------------------------------------------------------------------------------------------
November 30th, 2020
November updates.
Base
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
Cipher
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
Pollux
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
RowCol
' 10/09/2020 Chip Bloch    -Added Dependencies to description block
General
' 10/11/2020 Chip Bloch    -Added toDwarf
Trans
' 10/13/2020 Chip Bloch    -Added Offset to encRailFence, decRailFence
Stats
' 11/06/2020 Chip Bloch    -Added Random()
Trans
' 11/21/2020 Chip Bloch    -Updated handling of Offset for Rail Fence
' 11/21/2020 Chip Bloch    -Added Shuffle Cipher encShuffle, decShuffle
Index
' 11/22/2020 Chip Bloch    -Added substType.stMultiFib for Fibbonaci randomization
Polybius
' 11/23/2020 Chip Bloch    -Added RUp/RDown/CUp/CDown
Digraphic
' 11/25/2020 Chip Bloch    -Added EncSeriated, DecSeriated
Cipher
' 11/28/2020 Chip Bloch    -Added Portax
General
' 11/28/2020 Chip Bloch    -Made cs_Alphabet public
Cipher
' 11/29/2020 Chip Bloch    -Added Quagmires
--------------------------------------------------------------------------------------------------------------------
December 20th, 2020
December updates
Polybius
' 12/04/2020 Chip Bloch    -Added Dynamic Square Size based on key (make)
' 12/05/2020 Chip Bloch    -Added Values property
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType
'                          -Added PolyVals
General
' 12/04/2020 Chip Bloch    -Added Repl to Dedup
' 12/05/2020 Chip Bloch    -Added textType to Compact
' 12/10/2020 Chip Bloch    -Migrated some variables to Globals
' 12/12/2020 Chip Bloch    -Updated dedup (got rid of goto, using lists)
'                          -Updated GroupBy: 0 count = len, <1 = sqr(len)
' 12/13/2020 Chip Bloch    -Corrected ASCII Filter in Dedup (Case sensitive)
RowCol
' 12/05/2020 Chip Bloch    -Made Checkerboard Dynamic (deprecated 6x6, 7x7 checkerboard)
' 12/07/2020 Chip Bloch    -Made BIFID Dynamic
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType (pSquare.Make)
' 12/12/2020 Chip Bloch    -Made DisplayBlock allow dynamic count (0) for rows
' 12/12/2020 Chip Bloch    -Added KeySquare to generate a Polybius key
' 12/13/2020 Chip Bloch    -Made Rows and strRows optional (displayBlock)
Digraphic
' 12/05/2020 Chip Bloch    -Dynamic Playfair squares (5x5, 6x6, 7x7)
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType (pSquare.Make)
' 12/17/2020 Chip Bloch    -Made Tri-Square, Seriated Playfair, Two-Square, Four-Square Dynamic
Combo
' 12/06/2020 Chip Bloch    -Made ADFGX Dynamic (deprecated ADFGVX)
' 12/11/2020 Chip Bloch    -Migrated ValType, textType to Globals, deprecated ValType (pSquare.Make)
' 12/11/2020 Chip Bloch    -Made Bazieries Dynamic
' 12/12/2020 Chip Bloch    -Trapped empty key in *Bazieres, Folded plain text to lower case
Globals
' 12/10/2020 Chip Bloch    -Migrated textType, cs_Alphabet from General
Trans
' 12/17/2020 Chip Bloch    -Added nested transmute (fill, extract) * undocumented so far
--------------------------------------------------------------------------------------------------------------------
January 25th, 2021
January updates
General
' 12/28/2020 Chip Bloch    -Converted Dedup and Compact to accept a range as well as a
'                           string for the replacement character (24 character ciphers)
' 01/01/2021 Chip Bloch    -Added bonus characters to Compact()
' 01/06/2021 Chip Bloch    -Compact now respects case translation (upper/lower)
RowCol
' 12/28/2020 Chip Bloch    -Converted KeySquare to accept a range as well as a
'                           string for the replacement character (24 character ciphers)
' 12/28/2020 Chip Bloch    -Converted encMondi to pass an array of replacement strings to Compact()
' 12/31/2020 Chip Bloch    -Changed KeySquare to pass wildcard for Alphabet
' 01/03/2021 Chip Bloch    -Added Nihilist Transposition
' 01/05/2021 Chip Bloch    -Added Phillips, PhillipsRC
Digraphic
' 12/31/2020 Chip Bloch    -Made Digraphid ignore separator if used in key
Globals
' 01/03/2021 Chip Bloch    -Added ttASCIIAny
Cipher
' 12/31/2020 Chip Bloch    -Made TRIFID Ignore separator in key
' 01/01/2021 Chip Bloch    -Added Ragbaby
Polybius
' 12/31/2020 Chip Bloch    -Added error check for slash escape having identical row/column
'                           name in encString/decString
' 01/03/2021 Chip Bloch    -Added ttASCIIAny (ASCII, allow dups in square)
OrderedKey
' 01/04/2021 Chip Bloch    -Added NxtIdx, PrvIdx
Trans
' 12/26/2020 Chip Bloch    -Added OrderedTraversal
' 01/06/2021 Chip Bloch    -Added Swagman, Swagman key
' 01/23/2021 Chip Bloch    -Added encGrille,decGrille,genGrilleKey
' 01/24/2021 Chip Bloch    -Added grilleQuarter (undocumented)
HoleObj
' 01/09/2021 Chip Bloch    -Original Version
GrilleObj
' 01/21/2021 Chip Bloch    -Original Version
--------------------------------------------------------------------------------------------------------------------
February 28th, 2021
February updates
Trans
' 01/29/2021 Chip Bloch    -Added textType to encShuffle (filter)
' 02/07/2021 Chip Bloch    -Added encCadenas, decCadenas
' 02/09/2021 Chip Bloch    -Referencing ToText, UnGroupBy, AddSep
' 02/23/2021 Chip Bloch    -Added Myszkowski (encCCol, decCCol)
Stats
' 01/30/2021 Chip Bloch    -Adjusted Lower Dim() to (1 To N) in some functions
Base
' 01/31/2021 Chip Bloch    -Changed XOREncode default for non-alphabetic characters to "Omit"
Index
' 01/31/2021 Chip Bloch    -Updated documentation block
'                          -Corrected bug with single random values in encLLookup
' 02/01/2021 Chip Bloch    -Only split wildcards on separator when it is present in the list
'                           (allows codes like "? " not just "?")
Polybius
' 02/06/2021 Chip Bloch    -Added Alternating Row/Columns for encString, decString
' 02/07/2021 Chip Bloch    -Added Alternating Row/Columns for Filter
RowCol
' 02/07/2021 Chip Bloch    -Added alternating Row/Col to Checkerboard
' 02/09/2021 Chip Bloch    -Added reference to AddSep
Digraphic
' 02/09/2021 Chip Bloch    -Added reference to AddSep
' 02/21/2021 Chip Bloch    -Added Digraphic Substitution (Digraphic "Tabula Recta")
'                          -Added Vertical option for TwoSquare
Combo
' 02/09/2021 Chip Bloch    -Added Reference to AddSep
'                          -Added rowcol order to ADFGX
General
' 02/10/2021 Chip Bloch    -Added ToText, Ungroup, AddSep
' 02/21/2021 Chip Bloch    -Added Pad to ToText, Compact Final Replacement Character
'                          -Added GoldBug
'                          -Not folding "f" to "F" in toDwarf()
--------------------------------------------------------------------------------------------------------------------
April 3rd, 2021
March updates
Cipher
' 03/08/2021 Chip Bloch    -Added Shift Cipher
' 03/10/2021 Chip Bloch    -Added encHill, decHill, genHillKey, MatrixSquare
' 03/13/2021 Chip Bloch    -Converted Gromark, CONDI, Ragbaby, Shift to AlphaList
' 03/30/2021 Chip Bloch    -Quagmire using AlphaList
'                          -Slidefair not using replacements
AlphaList
' 03/12/2021 Chip Bloch    -Original Version
' 03/21/2021 Chip Bloch    -Added oZero offset, Items
' 03/22/2021 Chip Bloch    -Added ChkRlist_ (Prep for dedup)
' 03/29/2021 Chip Bloch    -Migrated Dedup, Compact from General
' 04/03/2021 Chip Bloch    -Added Recompact
MatrixObj
' 03/13/2021 Chip Bloch    -Original Version
General
' 03/13/2021 Chip Bloch    -Added "^" (not) option to XLAT
' 03/29/2021 Chip Bloch    -Migrated core Dedup and Compact to AlphaList()
Stats
' 03/21/2021 Chip Bloch    -Using AlphaList in GetIC (Dynamic Alphabet)
'                          -Added GetDIC
Trans
' 03/23/2021 Chip Bloch    -Trapped zero length string in encCCol, decCCol
Polybius
' 03/25/2021 Chip Bloch    -Added OutCh to decString, make
RowCol
' 03/25/2021 Chip Bloch    -Added OutCh to decStraddle, encStraddle
' 03/30/2021 Chip Bloch    -Updated encPhillips to check key for pad before adding
' 04/03/2021 Chip Bloch    -Calling recompact in encNihilistSub
Index
' 03/30/2021 Chip Bloch    -Updated CreateHomophonicKey using Dedup("", "*", ttAlpha25Upper, Repl)
Digraphic
' 04/03/2021 Chip Bloch    -Trapped filtered values missing from keys in *DiSub
'                          -Using Undup in *DiSub
--------------------------------------------------------------------------------------------------------------------
May 2nd, 2021
April updates
Alphalist
' 04/10/2021 Chip Bloch    -Added "&" default text (reverse)
'                          -Added numFilter, textFilter
' 04/11/2021 Chip Bloch    -Eliminated possibility of double pad for final letter
'                          -Added default textType for empty (ttAlphaUpper)
' 04/17/2021 Chip Bloch    -Bug: Collapse on Init_ has no override
' 04/21/2021 Chip Bloch    -Renamed Items as Item, NumFilter as toOffset, textFilter as toChars
' 04/22/2021 Chip Bloch    -Added oASCII for toOffset, toChars
'                          -Fixed bug with ttASCII not allowing lowercase
' 04/27/2021 Chip Bloch    -Added ttAlphaPlus (50 letter alphabet, 2x25)
' 04/28/2021 Chip Bloch    -Added SwpCh
OrderedKey
' 04/10/2021 Chip Bloch    -Added Inverse order for equal characters ("=")
Cipher
' 04/11/2021 Chip Bloch    -Altered hill cipher handling of period for 2x2 matrices
'                          -Added default for period -1 (length of string \ matrix.RC)
' 04/17/2021 Chip Bloch    -Calling Seriate_ in encHill,decHill
'                          -Moved MaxAttempts_ to top of module
' 04/23/2021 Chip Bloch    -Calling Seriate_ in encTRIFID, decTRIFID
Digraphic
' 04/11/2021 Chip Bloch    -Added period -1 in DIGRAPHID
' 04/22/2021 Chip Bloch    -Calling Seriate_ in DIGRAPHID
General
' 04/11/2021 Chip Bloch    -Added AlphaVal, textVal
' 04/21/2021 Chip Bloch    -Renamed AlphaVal as toOffset, textVal as toChars
' 04/22/2021 Chip Bloch    -Added AsHex option to toOffset, toChars
' 04/25/2021 Chip Bloch    -Migrated Alphabet, Alpha25, Goldbug to Globals
' 04/28/2021 Chip Bloch    -Updated Reverse to make SwpCh "sticky" - stays with following character
Trans
' 04/13/2021 Chip Bloch    -Added Seriate,DeSeriate
' 04/17/2021 Chip Bloch    -Added Enum for Seriate_
' 04/19/2021 Chip Bloch    -Changed parms on Seriate_ to ByVal
' 04/28/2021 Chip Bloch    -Updated Transmute to make SwpCh "sticky" - stays with following character
Combo
' 04/18/2021 Chip Bloch    -Added encAphid, decAphid
Stats
' 04/21/2021 Chip Bloch    -Renamed Alphalist.Items to AlphaList.Item
Base
' 04/25/2021 Chip Bloch    -Migrated xAlpha32, xAlphaXOR, xAlpha32Hex, xAlphaPunct to Globals
Globals
' 04/25/2021 Chip Bloch    -Migrated NullCh from PolyBius
'                          -Migrated Alphabet, Alpha25 and GoldBug from General
'                          -Added NullChr()
'                          -Migrated xAlpha32, xAlphaXOR, xAlpha32Hex, xAlphaPunct from base
' 04/27/2021 Chip Bloch    -Added ttAlphaPlus (50 character alphabet, 2x25)
' 04/28/2021 Chip Bloch    -Added SwpCh
Polybius
' 04/27/2021 Chip Bloch    -Added AlphaPlus
' 04/28/2021 Chip Bloch    -Added SwpCh
RowCol
' 04/05/2021 Chip Bloch    -Added NI(twoBox) cipher
' 04/11/2021 Chip Bloch    -Added period -1 to NI, BIFID ciphers
' 04/15/2021 Chip Bloch    -Added Seriation to encChecker/decChecker (RRCC, CCRR)
' 04/19/2021 Chip Bloch    -Added call to Seriate_ in NI.
' 04/28/2021 Chip Bloch    -Updated DisplayBlock, KeySquare for pages in ttAlphaPlus
'                          -Added Paginate for ttAlphaPlus
' 05/01/2021 Chiop Bloch   -Made Repl a variant for encChecker (used with ttAlphaPlus)

